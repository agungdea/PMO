FROM kyoryo/dotnetbase:1.0.0-preview2-sdk

COPY /src/App.Web/bin/Release/netcoreapp1.0/publish/. /app/Application
COPY /src/App.EmailSender/bin/Release/netcoreapp1.0/publish/. /app/EmailSender
COPY /app.supervisord.conf /app
COPY /supervisord.conf /app

#RUN apt-get -y install apt-transport-https
#RUN echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-5.x.list
#RUN apt-get -y update && apt-get -y --force-yes install elasticsearch

WORKDIR /app/Application

RUN rm -rf /etc/supervisor/supervisord.conf
RUN cp /app/supervisord.conf /etc/supervisor/
RUN cp /app/app.supervisord.conf /etc/supervisor/conf.d/

EXPOSE 5001/tcp

# ENTRYPOINT ["dotnet","App.Web.dll","--server.urls","http://*:5001"]
#ENTRYPOINT ["service","supervisor","start"]