﻿using App.Domain.DTO.App;
using App.Domain.DTO.Core;
using App.Domain.DTO.Identity;
using App.Domain.DTO.Localization;
using App.Domain.DTO.Workflow;
using App.Domain.Models.App;
using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using App.Domain.Models.Localization;
using App.Domain.Models.Workflow;
using AutoMapper;

namespace App.Domain.Mapper
{
    public static class DtoMapping
    {
        public static void Map(IMapperConfigurationExpression cfg)
        {
            #region App

            cfg.CreateMap<Category, CategoryDto>()
                .ForMember(x => x.ParentId, opt => opt.MapFrom(x => x.ParentId == null ? 0 : x.ParentId))
                .ReverseMap();
            cfg.CreateMap<AreaMapping, AreaMappingDto>()
                .ReverseMap();
            cfg.CreateMap<WBSBoQGrantTotal, WBSBoQGrantTotalDto>()
                .ReverseMap();
            cfg.CreateMap<TransformerPeriod, TransformerPeriodDto>()
                .ReverseMap();
            cfg.CreateMap<TransformerSi, TransformerSiDto>()
                .ReverseMap();
            cfg.CreateMap<TransformerProgram, TransformerProgramDto>()
                .ReverseMap();

            cfg.CreateMap<Justification, JustificationDto>()
                .ReverseMap();

            cfg.CreateMap<Aki, AkiDto>()
                 .ForMember(x => x.CreatedAt, opt => opt.MapFrom(x => x.CreatedAt))
                .ForMember(x => x.LastEditAt, opt => opt.MapFrom(x => x.LastUpdateTime))
                .ReverseMap();

            cfg.CreateMap<Unit, UnitDto>()
                .ReverseMap();

            cfg.CreateMap<Designator, DesignatorDto>()
                .ReverseMap();

            cfg.CreateMap<Harga, HargaDto>()
                .ReverseMap();

            cfg.CreateMap<Currency, CurrencyDto>()
               .ReverseMap();

            cfg.CreateMap<WBS, WBSDto>()
               .ReverseMap();

            cfg.CreateMap<WBSTree, WBSTreeDto>()
              .ReverseMap();

            cfg.CreateMap<WBStreeCategory, WBStreeCategoryDto>()
              .ReverseMap();

            cfg.CreateMap<ActTemplate, ActTemplateDto>()
              .ReverseMap();

            cfg.CreateMap<WBSSchedule, WBSScheduleDto>()
              .ReverseMap();

            cfg.CreateMap<WBSScheduleTemp, WBSScheduleTempDto>()
             .ReverseMap();

            cfg.CreateMap<ActTmpChild, ActTemplateChildDto>()
              .ForMember(x => x.ISLEAF, opt => opt.MapFrom(x => x.ISLEAF == "1" ? true : false))
              .ReverseMap();

            cfg.CreateMap<GroupWf, GroupWfDto>()
            .ForMember(x => x.isApprover, opt => opt.MapFrom(x => x.OtherInfo))
            .ReverseMap();
            #endregion

            #region Identity

            cfg.CreateMap<ApplicationRole, ApplicationRoleDto>()
                .ReverseMap();

            cfg.CreateMap<ApplicationUser, ApplicationUserDto>()
                .ForMember(x => x.Username, opt => opt.MapFrom(x => x.UserName))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.UserProfile.Name))
                .ReverseMap();

            cfg.CreateMap<WebSetting, MasterClaim>()
                .ReverseMap();

            cfg.CreateMap<AppAction, AppActionDto>()
                .ReverseMap();

            cfg.CreateMap<AppActionClaim, AppActionClaimDto>()
                .ReverseMap();

            #endregion

            #region Core

            cfg.CreateMap<WebSetting, WebSettingDto>()
                .ReverseMap();

            cfg.CreateMap<EmailArchieve, EmailArchieveDto>()
                .ReverseMap();

            cfg.CreateMap<Log, LogDto>()
                .ReverseMap();

            #endregion

            #region Localization

            cfg.CreateMap<Language, LanguageDto>()
                .ReverseMap();

            cfg.CreateMap<LocaleResource, LocaleResourceDto>()
                .ForMember(x => x.Language, opt => opt.MapFrom(x => x.Language))
                .ReverseMap();

            #endregion

            #region Workflow

            cfg.CreateMap<WfActivityView, WfActivityViewDto>()
                .ReverseMap();

            #endregion
        }
    }
}
