﻿using System;

namespace App.Domain
{
    public interface IBaseCacheEntity
    {
    }

    public interface ICacheEntity<TKey> : IBaseCacheEntity
    {
        TKey Id { get; set; }
    }

    public interface ICacheEntity : ICacheEntity<Guid>
    {
    }
}
