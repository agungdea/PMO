﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain
{
    public interface ISoftDeleteEntity
    {
        Boolean IsDeleted { get; set; }
        DateTime? DeletedDate { get; set; }
        string  DeletedBy { get; set; }
    }
}
