﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Domain
{
    public class BaseEntity<T> : BaseModel, IBaseEntity<T>
    {
        public BaseEntity()
        {
        }

        public BaseEntity(DateTime createdAt)
            : this()
        {
            CreatedAt = createdAt;
        }

        public BaseEntity(DateTime createdAt, string creator)
            : this(createdAt)
        {
            CreatedBy = creator;
        }

        [Key]
        public virtual T Id { get; set; }
    }
}
