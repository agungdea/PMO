﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace App.Domain.Models.Workflow
{
    public class WfActivityView : BaseEntity<string>
    {
        public string ViewName { get; set; }

        public string Variables { get; set; }

        public Dictionary<string, string> VariableDict
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Variables))
                    return null;

                var variables = JsonConvert.DeserializeObject<Dictionary<string, string>>(Variables);
                
                return variables;
            }
        }

        public void UpdateVariables(Dictionary<string, string> variables)
        {
            Variables = JsonConvert.SerializeObject(variables);
        }
    }
}
