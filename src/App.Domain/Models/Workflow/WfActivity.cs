﻿using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.Workflow
{
    public class WfActivity : BaseEntity<string>
    {
        public const string ACTIVITY_NOTRUNNING = "open.not_running.not_started";
        public const string ACTIVITY_COMPLETED = "closed.completed";

        public WfActivity()
        {
        }

        public WfActivity(string id, string state)
        {
            Id = id;
            State = state;
        }

        public string ActivityDefId { get; set; }
        public string State { get; set; }
        public string Status
        {
            get
            {
                return State?.Split('.')[1];
            }
        }

        public long WfProcessId { get; set; }

        [ForeignKey("WfProcessId")]
        public virtual WfProcess WfProcess { get; set; }

        [ForeignKey("ActivityDefId")]
        public virtual WfActivityView WfActivityView { get; set; }
    }
}
