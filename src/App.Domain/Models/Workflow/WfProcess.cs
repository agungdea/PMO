﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.Workflow
{
    public class WfProcess : BaseEntity<long>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; set; }

        public string ProcessId { get; set; }

        public string ProcessDefinitionId { get; set; }

        public string State { get; set; }

        public string Requester { get; set; }

        public string ActivityId { get; set; }

        public string PrevActivityId { get; set; }

        public string totalApproval { get; set; }

        public string currentApproval { get; set; }

        [NotMapped]
        public List<string> States
        {
            get
            {
                return JsonConvert.DeserializeObject<List<string>>(State);
            }
        }

        [ForeignKey("ActivityId")]
        public virtual WfActivity Activity { get; set; }

        [ForeignKey("PrevActivityId")]
        public virtual WfActivity PrevActivity { get; set; }

        public virtual List<WfActivity> Activities { get; set; }
    }
}
