﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Domain.Models.Core
{
    public class EmailArchieve : Entity
    {
        #region Ctor

        public EmailArchieve()
        {
            Id = Guid.NewGuid();
        }

        public EmailArchieve(string from, string tos, string subject, string message)
            : this()
        {
            From = from;
            Tos = tos;
            Subject = subject;
            HtmlMessage = message;
        }

        public EmailArchieve(string from, 
            string tos, 
            string subject, 
            string message, 
            DateTime createdDate)
            : this(from, tos, subject, message)
        {
            CreatedAt = createdDate;
        }

        public EmailArchieve(string from, 
            string tos,
            string subject,
            string message,
            DateTime createdDate,
            string creator)
            : this(from, tos, subject, message, createdDate)
        {
            CreatedBy = creator;
        }

        #endregion

        #region Props

        [Required]
        public string From { get; set; }

        public string FromName { get; set; }

        [Required]
        public string Tos { get; set; }

        public string Cc { get; set; }

        public string Bcc { get; set; }

        public string ReplyTo { get; set; }

        public string ReplyToName { get; set; }

        [Required]
        public string Subject { get; set; }
        
        public string HtmlMessage { get; set; }

        public string PlainMessage { get; set; }

        public bool IsSent { get; set; }

        public DateTime? SentDate { get; set; }

        public string ExceptionSendingMessage { get; set; }

        public int TrySentCount { get; set; }

        public DateTime? LastTrySentDate { get; set; }

        #endregion
    }
}
