﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Domain.Models.Core
{
    public class EntityRouting : IEntity, ICacheEntity
    {
        public EntityRouting()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        public string Slug { get; set; }

        public string Name { get; set; }

        public Guid EntityId { get; set; }

        public int EntityRoutingTypeId { get; set; }

        public EntityRoutingType EntityRoutingType { get; set; }
    }
}
