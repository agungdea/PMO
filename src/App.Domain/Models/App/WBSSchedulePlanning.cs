﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class WBSSchedulePlanning : Entity
    {
        public WBSSchedulePlanning()
        {
            Id = Guid.NewGuid();
        }

        public Guid IdWBSTree { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public Guid IdActTmpChild { get; set; }

        [ForeignKey("IdWBSTree")]
        public virtual WBSTree WBSTree { get; set; }

        [ForeignKey("IdActTmpChild")]
        public virtual ActTmpChild ActTmpChild { get; set; }
    }
}
