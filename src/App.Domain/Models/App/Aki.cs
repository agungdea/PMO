﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Domain.Models.App
{
    public class Aki : BaseEntity<Guid>, ICacheEntity<Guid>
    {

        public const string STATUS_DRAFT = "Draft";
        public const string STATUS_WRITE = "WRITE";
        public const string STATUS_SUBMITTED = "submit";
        public const string STATUS_REJECTED = "reject";
        public const string STATUS_ApproveUnitBisnis = "Unit Bisnis approved";
        public const string STATUS_BA_APPROVED = " BA Approved";
       
        #region Ctor
        public Aki()
        {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Props
        public override Guid Id { get; set; }
        [Required]
        public string AkiName { get; set; }
        [Required]
        public string PeriodId { get; set; }
        [Required]
        public string PeriodName { get; set; }
        [Required]
        public string StrategiInitiativeId { get; set; }
        [Required]
        public string StrategiInitiativeName { get; set; }
        [Required]
        public string ProgramId { get; set; }
        [Required]
        public string ProgramName { get; set; }
        [Required]
        public string Document { get; set; }
        
        public string wf_processId { get; set; }
        #endregion
       
        public virtual IList<AkiValuation> AkiValuations { get; set; }
        //CustomeField1 Digunakan Untuk path NJKI
        //CustomeField2 Digunakan Untuk Statusdraf
        //CustomeField3 Digunakan Untuk Approver
        //OtherInfo Digunakan Untuk Unit
    }
}
