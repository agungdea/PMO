﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.App
{
    public class Harga : IEntity
    {
        public Harga()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        [Column("PART_NUMBER")]
        public string IdDesignator { get; set; }

        [Column("PK_MTU")]
        public string MataUang { get; set; }

        [Column("UNIT_PRICE_MATERIALS")]
        public decimal HargaMaterial { get; set; }

        [Column("UNIT_PRICE_SERVICES")]
        public decimal HargaJasa { get; set; }
        
        [Column("LOKASI")]
        public string Lokasi { get; set; }

        [NotMapped]
        public virtual Designator Designator { get; set; }
    }
}
