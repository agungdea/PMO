﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class ActTemplate : BaseModel
    {
        public ActTemplate()
        {
            Id = Guid.NewGuid();
        }

        #region Props
        [Key]
        [Column("KD_ACT_TEMP")]
        public Guid Id { get; set; }
        [Column("NM_ACT_TEMP")]
        public string Name { get; set; }
        [Column("GRUP")]
        public string Grup { get; set; }
        [Column("DURASI")]
        public int? Durasi { get; set; }
        #endregion

        public virtual IList<ActTmpChild> ActivityTempChild { get; set; }
    }
}
