﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.App
{
    public class Category : BaseEntity<int>,ISoftDeleteEntity
    {

        #region Ctor
        public Category()
        {
          
        }
        #endregion

        #region Props
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int? ParentId { get; set; }
        [DefaultValue(false)]
        public Boolean IsDeleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string DeletedBy { get; set; }
        #endregion
    }
}
