﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.App
{
    public class WBSCurrency : Entity
    {
        public Guid WBSId { get; set; }

        public Guid CurrencyId { get; set; }

        public decimal Rate { get; set; }

        [ForeignKey("WBSId")]
        public virtual WBS WBS { get; set; }

        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }
    }
}
