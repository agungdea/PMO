﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class WfActivityAki :BaseEntity<string>
    {
        public const string OPEN_ACTIVITY = "open.not_running.not_started";
        public override string Id { get; set; }
        public string serviceLevelMonitor { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string dateCreated { get; set; }
        public string AssigmentTo { get; set; }
        public string wf_processId { get; set; }
        public Guid AkiId { get; set; }
        #region Relationship
        [ForeignKey("AkiId")]
        public virtual Aki Akis { get; set; }
        #endregion
    }

}
