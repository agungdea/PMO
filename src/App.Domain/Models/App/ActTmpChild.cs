﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class ActTmpChild : BaseModel
    {
        public ActTmpChild()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }

        [Column("KD_ACT_TEMP")]
        public Guid IdActTmp { get; set; }

        [Column("ACT_ID")]
        public int ActId { get; set; }

        [Column("ACT_PARENT")]
        public int? ActParent { get; set; }

        [Column("NM_ACT")]
        public string NamaAct { get; set; }

        [Column("BOBOT")]
        public Decimal? Bobot { get; set; }

        [Column("DURASI")]
        public int? Durasi { get; set; }

        [Column("PREDECESSOR")]
        public int? Predecessor { get; set; }

        [Column("LAGGING")]
        public int? Lagging { get; set; }

        [Column("RELASI")]
        public string Relasi { get; set; }

        [Column("URUTAN")]
        public int? Urutan { get; set; }

        [Column("ISLEAF")]
        public string ISLEAF { get; set; }

        [ForeignKey("IdActTmp")]
        public virtual ActTemplate ActTemplates { get; set; }
    }
}
