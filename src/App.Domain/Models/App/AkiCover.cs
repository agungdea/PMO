﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class AkiCover : BaseEntity<Guid>, ICacheEntity<Guid>
    {
        #region Ctor
        public AkiCover()
        {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Props
        public override Guid Id { get; set; }
        public Guid AkiId { get; set; }
        public string ProposalId { get; set; }
        public int? JumlahLop { get; set; }
        public string LopId { get; set; }
        public string Location { get; set; }
        public string Kapasitas { get; set; }
        public string PenanggungJawab { get; set; }
        public string NomorAkun { get; set; }
        public decimal? NilaiProgram { get; set; }
        public string CapexLine { get; set; }
        public string WaktuPenyelesaian { get; set; }
        public string RencanaSelesai { get; set; }
        public string PemilihanTeknologi { get; set; }
        public string Usulan { get; set; }
        public string Alasan { get; set; }
        public float? DiscountRate { get; set; }
        public string CAGR { get; set; }
        public string Roi { get; set; }
        public float? InterestRate { get; set; }
        public decimal? NPV { get; set; }
        public decimal? IRR { get; set; }
        public decimal? BCR { get; set; }
        public int? PayBackPeriode { get; set; }
        public string ARPU { get; set; }
        public string Level { get; set; }
        public string KategoriInvestasi { get; set; }
        public float? Taxes { get; set; }
        #endregion

        #region Relationship
        [ForeignKey("AkiId")]
        public virtual Aki Akis { get; set; }
        #endregion
    }
}
