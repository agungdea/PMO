﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class WBSScheduleTemp : Entity
    {
        public WBSScheduleTemp()
        {
            Id = Guid.NewGuid();
        }

        public Guid IdWBSTree { get; set; }
        public int DurasiReal { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public DateTime FinishRencana { get; set; }
        public DateTime FinishPerkiraan { get; set; }
        public Guid IdActTmpChild { get; set; }

        [ForeignKey("IdWBSTree")]
        public virtual WBSTree WBSTree { get; set; }

        [ForeignKey("IdActTmpChild")]
        public virtual ActTmpChild ActTmpChild { get; set; }
    }
}
