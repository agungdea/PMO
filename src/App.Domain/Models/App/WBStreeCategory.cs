﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class WBStreeCategory : IEntity
    {
        #region Ctor
        public WBStreeCategory()
        {
            Id = Guid.NewGuid();
        }
        #endregion

        #region Prop
        [Key]
        public Guid Id {get;set;}
        public Guid WBStreeId { get; set; }
        public int CategoryId { get; set; }
        #endregion

        #region Relationship
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set;}
        [ForeignKey("WBStreeId")]
        public virtual WBSTree WBSTree { get; set; }
        #endregion
    }
}
