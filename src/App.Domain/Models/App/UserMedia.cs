﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class UserMedia : Entity
    {
        public UserMedia()
        {
            Id = Guid.NewGuid();
        }

        public string UnitId { get; set; }
        public string MediaDirectory { get; set; }
    }
}
