﻿using System;

namespace App.Domain.Models.App
{
    public class WBS : Entity
    {
        public const string WBS_TYPE_WBS = "WBS";
        public const string WBS_TYPE_WBS_HAS_LOCATION = "WBSLocation";
        public const string WBS_TYPE_WBS_HAS_WBS = "WBSHasWBS";
        public const string WBS_TYPE_LOCATION = "Location";
        public const string WBS_TYPE_WP = "WP";

        public WBS()
        {
            Id = Guid.NewGuid();
        }
        
        public string NamaKegiatan { get; set; }
        public string Unit { get; set; }
    }
}
