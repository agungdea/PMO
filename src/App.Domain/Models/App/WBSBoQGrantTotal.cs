﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class WBSBoQGrantTotal : IEntity
    {
        public WBSBoQGrantTotal ()
            {
            Id = Guid.NewGuid();
            }
        [Key]
        public Guid Id {get;set;}
        public Guid IdWBStree { get; set; }
        public Guid CurrencyId { get; set; }
        public decimal? BoQBeforeConverstion { get; set; }
        public decimal BoQTotalConversion { get; set; }

        [ForeignKey("CurrencyId")]
        public virtual Currency Currency { get; set; }

        [ForeignKey("IdWBStree")]
        public virtual WBSTree WBSTree { get; set; }

    }
}
