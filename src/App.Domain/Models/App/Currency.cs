﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Domain.Models.App
{
    public class Currency : IEntity
    {
        public const string CurrencyRp = "RP.";
        public Currency()
        {
            Id = Guid.NewGuid();
        }
        [Key]
        public Guid Id { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyValue { get; set; }
    }
}
