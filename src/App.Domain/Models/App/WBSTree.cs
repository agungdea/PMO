﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.App
{
    public class WBSTree : Entity
    {
        public WBSTree()
        {
            Id = Guid.NewGuid();
        }

        #region Prop
        public string Name { get; set; }
        public Guid IdWBS { get; set; }
        public Guid? ParentId { get; set; }
        public string Lokasi { get; set; }
        public string Type { get; set; }
        public string GrupHarga { get; set; }
        public int Order { get; set; }


        [ForeignKey("ParentId")]
        public virtual WBSTree Parent { get; set; }
        public virtual List<WBSTree> Children { get; set; }
        #endregion

        #region Relationship
        [ForeignKey("IdWBS")]
        public virtual WBS WBS { get; set; }

        [NotMapped]
        public virtual AreaMapping AreaMapping { get; set; }
        #endregion

    }
}
