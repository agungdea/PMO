﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.App
{
    public class Unit : IBaseEntity<string>, ISoftDeleteEntity
    {
        #region Constants
        public const string STLevelDirectorat = "2";
        public const string STLevelUnitBisnis = "3";
        public const string STLevelUnitKerja = "4";
        public const string STLevelBagian = "5";
        #endregion

        [Key]
        [Column("GRUP")]
        public string Id { get; set; }

        [Column("NM_UNIT")]
        public string NamaUnit { get; set; }

        [Column("NM_SINGKAT")]
        public string NamaSingkat { get; set; }

        [Column("GRUP_PARENT")]
        public string ParentId { get; set; }

        [Column("ST_LEVEL")]
        public string STLevel { get; set; }

        [ForeignKey("ParentId")]
        public virtual Unit Parent { get; set; }

        public virtual List<Unit> Children { get; set; }
        #region Soft delete
        [DefaultValue(false)]
        public Boolean IsDeleted { get; set; }
        public DateTime? DeletedDate { get; set;}
        public string DeletedBy { get; set; }
        #endregion
    }
}
