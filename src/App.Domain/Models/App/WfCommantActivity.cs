﻿using App.Domain.Models.Workflow;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class WfCommantActivity : BaseEntity<long>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override long Id { get; set; }
        public string Comment { get; set; }
        public Boolean IsAggree { get; set; }
        public string NIK { get; set; }
        public string Name { get; set; }
        public string ProcessId { get; set; }
        public string Status { get; set; }
    }
}
