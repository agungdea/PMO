﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Domain.Models.App
{
    public class AreaMapping : BaseEntity<Guid>, ICacheEntity<Guid>
    {

        public AreaMapping() {
            Id = new Guid();
        }
        [Key]
        public override Guid Id { get; set; }
        public String AreaRegional { get; set; }
        public String AreaWitel { get; set; }
        public String AreaDatel { get; set; }
        public String AreaStoName { get; set; }
        public String AreaSto { get; set; }
        public String AreaKode { get; set; }

    }
}
