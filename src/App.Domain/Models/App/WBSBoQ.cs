﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.App
{
    public class WBSBoQ : Entity
    {
        public WBSBoQ()
        {
            Id = Guid.NewGuid();
        }

        public int Qty { get; set; }

        public string IdDesignator { get; set; }

        public string GrupHarga { get; set; }

        public decimal HargaMaterial { get; set; }

        public decimal HargaJasa { get; set; }

        public Guid IdWBSTree { get; set; }

        [ForeignKey("IdWBSTree")]
        public virtual WBSTree WBSTree { get; set; }
    }
}
