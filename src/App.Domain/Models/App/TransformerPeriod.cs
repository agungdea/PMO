﻿using System.ComponentModel.DataAnnotations;

namespace App.Domain.Models.App
{
    public class TransformerPeriod
    {
        [Key]
        public string Period { get; set; }
        public string Display_name { get; set; }

    }
}
