﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class AkiValuation : BaseEntity<Guid>, ICacheEntity<Guid>
    {
        public AkiValuation()
        {
            Id = Guid.NewGuid();
        }

        #region Props
        public override Guid Id { get; set; }
        public Guid AkiId { get; set; }
        public decimal? Revenue { get; set; }
        public decimal? Expenses { get; set; }
        public decimal? Ebitda { get; set; }
        public decimal? Depreciation { get; set; }
        public decimal? Ebit { get; set; }
        public decimal? Taxes { get; set; }
        public decimal? NetIncome { get; set; }
        public decimal? AddBackDepresiation { get; set; }
        public decimal? NetCashFlow { get; set; }
        public decimal? CumulativeNetCashFlow { get; set; }
        public int? Year { get; set; }
        #endregion

        #region Relationship
        [ForeignKey("AkiId")]
        public virtual Aki Akis { get; set; }
        #endregion
    }
}
