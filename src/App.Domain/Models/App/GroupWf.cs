﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.Models.App
{
    public class GroupWf : BaseEntity<Guid>
    {
        public const string GROUP_WF_Capex = "Group Capex";
        public const string GROUP_WF_WBS = "Group BOC";
       
        #region Ctor
        public GroupWf()
        {
            Id = Guid.NewGuid();
        }
        #endregion
        public string Nik { get; set; }
        public string GrupName { get; set; }
        //isApprover menggunakan otherInfo
    }
   
}
