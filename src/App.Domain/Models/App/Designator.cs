﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.App
{
    public class Designator : IBaseEntity<long>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("ID")]
        public long Id { get; set; }

        [Column("PART_NUMBER")]
        public string IdDesignator { get; set; }

        [Column("DESKRIPSI")]
        public string Deskripsi { get; set; }

        [Column("SATUAN")]
        public string Satuan { get; set; }

        [NotMapped]
        public virtual List<Harga> Hargas { get; set; }
    }
}