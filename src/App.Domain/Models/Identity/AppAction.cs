﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Domain.Models.Identity
{
    public class AppAction : IBaseEntity<string>, ICacheEntity<string>
    {
        public AppAction()
        {
            Id = Guid.NewGuid().ToString();
        }

        [Key]
        public string Id { get; set; }

        public string Name { get; set; }

        public virtual List<AppActionClaim> ActionClaims { get; set; }
    }
}
