﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Models.Identity
{
    public class UserProfile : IBaseEntity<int>
    {
        #region Props

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayName("Nama")]
        [MaxLength(200)]
        public string Name { get; set; }

        [DisplayName("Tempat Lahir")]
        public string Birthplace { get; set; }

        [DisplayName("Tanggal Lahir")]
        public DateTime? Birthdate { get; set; }

        [DisplayName("Alamat")]
        public string Address { get; set; }

        [DisplayName("Foto")]
        public string Photo { get; set; }

        [Column("UserId")]
        public string ApplicationUserId { get; set; }

        #endregion

        #region Relationships

        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        #endregion
    }
}
