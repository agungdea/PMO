﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel;

namespace App.Domain.Models.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public const string UNIT_CLAIMS = "unit";
        public const string UNIT_DISPO_CLAIMS = "unitDisposisi";
        public const string ALL_Unit = "SeeUnit";
        public const string ALL_Regional = "SeeRegional";
        #region Relationships

        [DisplayName("Profile")]
        public virtual UserProfile UserProfile { get; set; }

        #endregion
    }
}
