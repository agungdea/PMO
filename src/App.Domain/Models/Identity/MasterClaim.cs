﻿using App.Domain.Models.Core;

namespace App.Domain.Models.Identity
{
    public class MasterClaim : WebSetting
    {
        public string Type
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;
            }
        }

        public string ClaimValues
        {
            get
            {
                return CustomField1;
            }
            set
            {
                CustomField1 = value;
            }
        }

        public string[] Values
        {
            get
            {
                return ClaimValues.Split(',');
            }
        }
    }

    public enum ClaimType
    {
        User,
        Role,
        Action
    }
}