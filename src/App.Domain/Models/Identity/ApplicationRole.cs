﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace App.Domain.Models.Identity
{
    public class ApplicationRole : IdentityRole
    {
        #region Constants

        public const string Administrator = "Administrator";
        public const string User = "User";
        public const string AdminReg = "Admin Reg";
        public const string PurchaseRequisition = "Purchase Requisition";
        public const string AdminAndSuperadmin = "Administrator,Admin Reg";
        public const string CategoryManagement = "Category Management";
        public const string StoManagement = "STO Management";
        #endregion

        #region Props

        public string Description { get; set; }
        public string OtherInfo { get; set; }

        #endregion
    }
}
