﻿using System;

namespace App.Domain
{
    public interface IEntity : IBaseEntity<Guid>
    {
    }
}
