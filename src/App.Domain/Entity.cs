﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Domain
{
    public class Entity : BaseModel, IEntity
    {
        public Entity()
        {
            Id = Guid.NewGuid();
        }

        public Entity(DateTime createdAt)
            : this()
        {
            CreatedAt = createdAt;
        }

        public Entity(DateTime createdAt, string creator)
            : this(createdAt)
        {
            CreatedBy = creator;
        }

        [Key]
        public Guid Id { get; set; }
    }
}
