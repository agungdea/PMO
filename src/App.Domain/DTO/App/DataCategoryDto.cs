﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    
    public class DataCategoryDto
    {
        public string id { get; set; }
        public string text { get; set; }
        public string type { get; set; }
        public string ParentId { get; set; }

        public List<DataCategoryDto> children { get; set; }
        public dynamic state { get; set; }
        public string icon { get; set; }
    }
}
