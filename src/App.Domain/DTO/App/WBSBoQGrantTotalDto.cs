﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class WBSBoQGrantTotalDto
    {
        public Guid Id { get; set; }
        public Guid IdWBStree { get; set; }
        public Guid IdWBS { get; set; }
        public Guid CurrencyId { get; set; }
        public decimal? BoQBeforeConverstion { get; set; }
        public decimal BoQTotalConversion { get; set; }
        public String Text { get; set; }
        public decimal? Total { get; set; }
        public String CurrencyValue { get; set; }
        public String CurrencyCode { get; set; }
        public String Header { get; set; }
        public Guid? Parent { get; set; }
        public String Category { get; set; }
    }
}
