﻿using System;

namespace App.Domain.DTO.App
{
    public class WBSBoQDto
    {
        public int? Qty { get; set; }

        public string IdDesignator { get; set; }

        public string Deskripsi { get; set; }

        public string Satuan { get; set; }

        public string GrupHarga { get; set; }

        public string MataUang { get; set; }

        public decimal HargaMaterial { get; set; }

        public decimal HargaJasa { get; set; }

        public Guid IdWBSTree { get; set; }
        public decimal TotalItemBoq { get; set; }
    }

    public class WBSBoQJustificationDto
    {
        public Guid WbstreeId { get; set; }
        public Guid BoqId { get; set; }
        public string LocationName { get; set; }
        public string DesignatorDeskripsi { get; set; }
        public long DesignatorId { get; set; }
        public string Currencies { get; set; }
        public decimal HargaJasa { get; set; }
        public decimal HargaMaterial { get; set; }
        public int Qty { get; set; }
        public string GrupHarga { get; set; }
        public decimal Jumlah { get; set; }
    }
}
