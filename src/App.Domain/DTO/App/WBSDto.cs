﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class WBSDto
    {
        public Guid Id { get; set; }
        public string NamaKegiatan { get; set; }
        public string Unit { get; set; }
    }
}
