﻿using System;

namespace App.Domain.DTO.App
{
    public class HargaDto
    {
        public long Id { get; set; }
        
        public string IdDesignator { get; set; }

        public string Deskripsi { get; set; }

        public string Satuan { get; set; }

        public string MataUang { get; set; }
        
        public decimal HargaMaterial { get; set; }
        
        public decimal HargaJasa { get; set; }
        
        public string Lokasi { get; set; }
    }
}
