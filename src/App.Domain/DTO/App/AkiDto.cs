﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class AkiDto
    {
        public Guid Id { get; set; }
        public string AkiName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? LastEditAt { get; set; }
        public string PeriodId { get; set; }
        public string PeriodName { get; set; }
        public string StrategiInitiativeId { get; set; }
        public string StrategiInitiativeName { get; set; }
        public string ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string Document { get; set; }
        public string wf_processId { get; set; }
    }
}
