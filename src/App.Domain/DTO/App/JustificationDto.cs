﻿using App.Domain.Models.App;
using System;

namespace App.Domain.DTO.App
{
    public class JustificationDto : BaseDto
    {
        public Guid Id { get; set; }
        public string Penyusun { get; set; }
        public string NomorDRK { get; set; }
        public decimal JumlahAnggaran { get; set; }
        public Guid? WBSId { get; set; }
        public string NomorDRP { get; set; }
        public string JenisKebutuhan { get; set; }
        public Guid? AKIId { get; set; }
        public SpendingType JenisPengeluaran { get; set; }
        public string PusatPertanggungjawaban { get; set; }
        public string KelasAsset { get; set; }
        public string LatarBelakang { get; set; }
        public string NoteLatarBelakang { get; set; }
        public string AspekStrategis { get; set; }
        public string NoteAspekStrategis { get; set; }
        public string AspekBisnis { get; set; }
        public string NoteAspekBisnis { get; set; }
        public string SpesifikasiTeknik { get; set; }
        public string NoteSpesifikasiTeknik { get; set; }
        public string JumlahKebutuhanBarangJasa { get; set; }
        public string BasicDesign { get; set; }
        public bool ShowBoQ { get; set; }
        public string TOR { get; set; }
        public string DasarKebutuhanBarangJasa { get; set; }
        public string LampiranDasarKebutuhanBarangJasa { get; set; }
        public string NoteJumlahKebutuhan { get; set; }
        public string RencanaPelaksanaan { get; set; }
        public string NoteRencanaPelaksanaan { get; set; }
        public string DistribusiPenggunaan { get; set; }
        public bool ShowWBS { get; set; }
        public string NoteDistribusiPenggunaan { get; set; }
        public string PosisiPersediaan { get; set; }
        public string NotePosisiPersediaan { get; set; }
        public string Anggaran { get; set; }
        public string NoteAnggaran { get; set; }
        public string Penutup { get; set; }
        public string NotePenutup { get; set; }
        public string JustificationStatus { get; set; }
    }
}
