﻿using System;

namespace App.Domain.DTO.App
{
    public class AreaMappingDto
    {
        public Guid Id { get; set; }
        public string AreaRegional { get; set; }
        public string AreaWitel { get; set; }
        public string AreaDatel { get; set; }
        public string AreaStoName { get; set; }
        public string AreaSto { get; set; }
        public string AreaKode { get; set; }
    }
}
