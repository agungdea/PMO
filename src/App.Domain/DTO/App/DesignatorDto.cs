﻿namespace App.Domain.DTO.App
{
    public class DesignatorDto
    {
        public long Id { get; set; }
        
        public string IdDesignator { get; set; }
        
        public string Deskripsi { get; set; }
        
        public string Satuan { get; set; }
    }
}
