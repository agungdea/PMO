﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class WBSTreeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid IdWBS { get; set; }
        public Guid? ParentId { get; set; }
        public string Lokasi { get; set; }
        public string Type { get; set; }
    }
    public class WBSTreeJustificationDto
    {
        public Guid IdWBS { get; set; }
        public string Text { get; set; }
        public Guid IdWBStree { get; set; }
        public Guid? Parent { get; set; }
        public string Header { get; set; }
    }
}
