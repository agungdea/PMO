﻿using System;

namespace App.Domain.DTO.App
{
    public class UserMediaDto
    {
        public Guid Id { get; set; }
        public string UnitId { get; set; }
        public string MediaDirectory { get; set; }
    }
}
