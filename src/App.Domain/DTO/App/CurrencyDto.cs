﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class CurrencyDto
    {
        public Guid Id { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyValue { get; set; }
    }
}
