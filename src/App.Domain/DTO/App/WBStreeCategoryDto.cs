﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class WBStreeCategoryDto
    {
        public Guid Id { get; set; }
        public Guid WBStreeId { get; set; }
        public int CategoryId { get; set; }
      
    }
}
