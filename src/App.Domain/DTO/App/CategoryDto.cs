﻿using System.Collections.Generic;

namespace App.Domain.DTO.App
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
    }

}
