﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class WBSScheduleTempDto
    {
        public Guid Id { get; set; }
        public Guid IdWBSTree { get; set; }
        public int? DurasiReal { get; set; }
        public string Start { get; set; }
        public string Finish { get; set; }
        public Guid IdActTmpChild { get; set; }
        public Guid IdActTmp { get; set; }
        public int ActId { get; set; }
        public int? ActParent { get; set; }
        public string NamaAct { get; set; }
        public Decimal? Bobot { get; set; }
        public int? Durasi { get; set; }
        public int? Predecessor { get; set; }
        public int? Lagging { get; set; }
        public string Relasi { get; set; }
        public int? Urutan { get; set; }
        public string ISLEAF { get; set; }
        public string OtherInfo { get; set; }

    }
}
