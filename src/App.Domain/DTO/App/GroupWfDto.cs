﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class GroupWfDto
    {

        public Guid Id { get; set; }
        public string Nik { get; set; }
        public string GrupName { get; set; }
        public string isApprover { get; set; }
    }
}
