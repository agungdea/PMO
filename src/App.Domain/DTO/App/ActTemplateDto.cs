﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class ActTemplateDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Grup { get; set; }
        public int? Durasi { get; set; }
    }
}
