﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class UnitDto
    {
        public string Id { get; set; }
        public string NamaUnit { get; set; }
        public string ParentId { get; set; }
        public string STLevel { get; set; }
    }
}
