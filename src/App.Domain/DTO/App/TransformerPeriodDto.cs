﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class TransformerPeriodDto
    {

        public string Period { get; set; }
        public string Display_name { get; set; }

    }
}
