﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class TransformerSiDto
    {
        public string Id { get; set; }
        public string Abbreviation { get; set; }
        public string Name { get; set; }
        public string Parent_id { get; set; }
        public string PeriodId { get; set; }
    }
}
