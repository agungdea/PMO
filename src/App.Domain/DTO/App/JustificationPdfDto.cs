﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class JustificationPdfDto
    {
        #region Const
        public const string LATAR_BELAKANG = "I. LATAR BELAKANG";
        public const string ASPEK_STRATEGIS = "II. ASPEK STRATEGIS";
        public const string ASPEK_BISNIS = "III. ASPEK BISNIS";
        public const string SPESIFIKASI_TEKNIK = "IV. SPESIFIKASI TEKNIK";
        public const string JUMLAH_KEBUTUHAN = "V. JUMLAH KEBUTUHAN";
        public const string RENCANA_PELAKSANAAN = "VI. RENCANA PELAKSANAAN";
        public const string DISTRIBUSI_PENGGUNAAN = "VII. DISTRIBUSI PENGGUNAAN";
        public const string POSISI_PERSEDIAAN = "VIII. POSISI PERSEDIAAN";
        public const string ANGGARAN = "IX. ANGGARAN";
        public const string PENUTUP = "X. PENUTUP";
        #endregion

        public string Header { get; set; }
        public string Content { get; set; }
    }


    public class JustificationPdfHeaderDto
    {
        public string NamaKegiatan { get; set; }
        public string Penyusun { get; set; }
        public string NomorDRK { get; set; }
        public decimal JumlahAnggaran { get; set; }
        public string NomorDRP { get; set; }
        public string JenisKebutuhan { get; set; }
        public string PusatPertanggungjawaban { get; set; }
        public string KelasAsset { get; set; }
        public string NamaAKI { get; set; }

        public List<JustificationPdfDto> Content;
    }
    public class JustificationBOQTable
    {
        public string GroupHarga { get; set; }
        public string Item { get; set; }
        public string MataUang { get; set; }
        public string Kuantitas { get; set; }
        public string HargaJasa { get; set; }
        public string HargaMaterial { get; set; }
        public string Total { get; set; }
    }
}
