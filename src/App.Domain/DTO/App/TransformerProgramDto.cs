﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class TransformerProgramDto
    {
        public string Id { get; set; }
        public string Btp { get; set; }
        public string BusinessRisk { get; set; }
        public string Description { get; set; }
        public string QuickWin { get; set; }
        public string RiskMitigation { get; set; }
        public string Title { get; set; }
        public string BodChampion_id { get; set; }
        public string Initiative_id { get; set; }
        public string Status { get; set; }
        public string ProgramStatus { get; set; }
        public string WorkflowStatus { get; set; }
        public DateTime Date_submitted { get; set; }
        public string Generator { get; set; }
        public string Quantifier { get; set; }
        public DateTime Date_approved { get; set; }
        public string Draft { get; set; }
        public string Planamount { get; set; }
        public string BusinessRisk2 { get; set; }
        public string RiskMitigation2 { get; set; }
        public string Comments { get; set; }
        public string Planuser { get; set; }
        public string Weight { get; set; }
        public string Revision { get; set; }
        public string ProgramType { get; set; }
        public string Organization_id { get; set; }
        public string Order { get; set; }
        public string Backactual { get; set; }
        public string Backchgreq { get; set; }
        public string Issuemgt { get; set; }
        public string Progressworkflowmode_id { get; set; }
        public string enabler { get; set; }
        public string Warflag { get; set; }
        public string Usedependency { get; set; }
        public DateTime? Closeddate { get; set; }
        public string SiId { get; set; }
    }
}
