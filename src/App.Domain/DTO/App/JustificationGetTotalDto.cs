﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Domain.DTO.App
{
    public class JustificationGetTotalDto
    {
        public Guid IdWBStree { get; set; }
        public Guid? Parent { get; set; }
      
    }
}
