﻿namespace App.Domain.DTO.Identity
{
    public class ApplicationUserDto
    {
        #region Props

        public string Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string Phone { get; set; }

        #endregion
    }
}
