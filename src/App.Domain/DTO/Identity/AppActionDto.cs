﻿namespace App.Domain.DTO.Identity
{
    public class AppActionDto
    {
        #region Props

        public string Id { get; set; }

        public string Name { get; set; }

        #endregion
    }
}
