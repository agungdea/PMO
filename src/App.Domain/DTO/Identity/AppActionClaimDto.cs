﻿namespace App.Domain.DTO.Identity
{
    public class AppActionClaimDto
    {
        #region Props

        public int Id { get; set; }

        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }
        
        public string ActionId { get; set; }

        public AppActionDto Action { get; set; }

        #endregion
    }
}
