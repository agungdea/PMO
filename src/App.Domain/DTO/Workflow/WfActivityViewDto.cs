﻿namespace App.Domain.DTO.Workflow
{
    public class WfActivityViewDto : BaseDto
    {
        public string Id { get; set; }

        public string ViewName { get; set; }

        public string Variables { get; set; }
    }
}
