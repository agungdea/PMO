﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace App.Data.Repository.LemonRepository
{
    interface ILemonRepository<T>
        where T : class
    {
        IQueryable<T> GetAll(params Expression<Func<T, object>>[] includes);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        T GetSingle(params object[] ids);
        void Add(T entity);
        void Add(IEnumerable<T> entities);
        void Delete(T entity);
        void Delete(IEnumerable<T> entities);
        void Update(T entity);
        void Update(IEnumerable<T> entities);
        void Save();
        void ExecuteQuery(string query);
        IQueryable<T> Table { get; }
        IQueryable<T> TableNoTracking { get; }
    
    }
}
