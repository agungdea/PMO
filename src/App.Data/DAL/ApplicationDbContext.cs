﻿using App.Data.Utils;
using App.Domain;
using App.Domain.Models.App;
using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using App.Domain.Models.Localization;
using App.Domain.Models.Workflow;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Modular.Core;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace App.Data.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        #region Identity
        public virtual new DbSet<ApplicationRole> Roles { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }
        public virtual DbSet<AppAction> AppActions { get; set; }
        public virtual DbSet<AppActionClaim> AppActionClaims { get; set; }
        #endregion

        #region Core
        public virtual DbSet<WebSetting> WebSettings { get; set; }
        public virtual DbSet<EmailArchieve> EmailArchieves { get; set; }
        public virtual DbSet<Log> Logs { get; set; }
        public virtual DbSet<EntityRoutingType> EntityRoutingTypes { get; set; }
        public virtual DbSet<EntityRouting> EntityRoutings { get; set; }
        #endregion

        #region Localization
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<LocaleResource> LocaleResources { get; set; }
        #endregion

        #region App
        public virtual DbSet<WBSSchedule> WBSSchedule { get; set; }
        public virtual DbSet<ActTemplate> ActTemplate { get; set; }
        public virtual DbSet<ActTmpChild> ActTmpChild { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<AreaMapping> AreaMapping { get; set; }
        public virtual DbSet<Currency> Currencies { get; set; }
        public virtual DbSet<Justification> Justifications { get; set; }
        public virtual DbSet<Aki> Akis { get; set; }
        public virtual DbSet<AkiCover> AkiCovers { get; set; }
        public virtual DbSet<AkiValuation> AkiValuations { get; set; }
        public virtual DbSet<TransformerPeriod> TransformerPeriod { get; set; }
        public virtual DbSet<TransformerSi> TransformerSi { get; set; }
        public virtual DbSet<TransformerProgram> TransformerProgram { get; set; }
        public virtual DbSet<Designator> Designators { get; set; }
        public virtual DbSet<Harga> Hargas { get; set; }
        public virtual DbSet<WBS> WBSS { get; set; }
        public virtual DbSet<WBSCurrency> WBSCurrencies { get; set; }
        public virtual DbSet<WBSBoQ> WBSBoQs { get; set; }
        public virtual DbSet<WBSTree> WBSTrees { get; set; }
        public virtual DbSet<WBStreeCategory> WBStreeCategory { get; set; }
        public virtual DbSet<WBSBoQGrantTotal> WBSBoQGrantTotal { get; set; }
        public virtual DbSet<WBSScheduleTemp> WBSScheduleTemp { get; set; }
        public virtual DbSet<UserMedia> UserMedia { get; set; }
        public virtual DbSet<WBSSchedulePlanning> WBSSchedulePlanning { get; set; }
        
        #endregion

        #region Workflow
        public virtual DbSet<WfProcess> WfProcesses { get; set; }
        public virtual DbSet<WfActivity> WfActivities { get; set; }
        public virtual DbSet<WfActivityView> WfActivityViews { get; set; }
        public virtual DbSet<WfActivityAki> WfActivityAki { get; set; }

        public virtual DbSet<GroupWf> GroupWf { get; set; }
        public virtual DbSet<WfCommantActivity> WfCommantActivity { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.RemovePluralizingTableNameConvention();

            builder.HasPostgresExtension("uuid-ossp");

            List<Type> typeToRegisters = new List<Type>();

            foreach (var module in GlobalConfiguration.Modules)
            {
                typeToRegisters.AddRange(module.Assembly.DefinedTypes.Select(t => t.AsType()));
            }

            RegisterEntities(builder, typeToRegisters);

            RegisterConvention(builder);

            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>()
                .HasOne(x => x.UserProfile)
                .WithOne(x => x.ApplicationUser)
                .HasForeignKey<UserProfile>(x => x.ApplicationUserId);

            builder.Entity<UserUnit>()
              .HasKey(x => new { x.UserId, x.UnitId });

            builder.Entity<WfActivity>()
                .HasOne(x => x.WfProcess)
                .WithMany(x => x.Activities);

            RegisterCustomMappings(builder, typeToRegisters);
        }

        private static void RegisterConvention(ModelBuilder modelBuilder)
        {
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                if (entity.ClrType.Namespace.StartsWith("App.Domain"))
                    continue;

                if (entity.ClrType.Namespace != null)
                {
                    var nameParts = entity.ClrType.Namespace.Split('.');
                    var tableName = string.Concat(nameParts[1], "_", entity.ClrType.Name);
                    modelBuilder.Entity(entity.Name).ToTable(tableName);
                }
            }
        }

        private static void RegisterEntities(ModelBuilder modelBuilder, IEnumerable<Type> typeToRegisters)
        {
            var entityTypes = typeToRegisters.Where(x => x.GetTypeInfo().IsSubclassOf(typeof(Entity)) && !x.GetTypeInfo().IsAbstract);
            foreach (var type in entityTypes)
            {
                modelBuilder.Entity(type);
            }
        }

        private static void RegisterCustomMappings(ModelBuilder modelBuilder, IEnumerable<Type> typeToRegisters)
        {

            var customModelBuilderTypes = typeToRegisters.Where(x => typeof(ICustomModelBuilder).IsAssignableFrom(x));
            foreach (var builderType in customModelBuilderTypes)
            {
                if (builderType != null && builderType != typeof(ICustomModelBuilder))
                {
                    var builder = (ICustomModelBuilder)Activator.CreateInstance(builderType);
                    builder.Build(modelBuilder);
                }
            }

        }
    }
}
