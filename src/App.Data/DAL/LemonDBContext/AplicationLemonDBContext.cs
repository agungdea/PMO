﻿using App.Domain.Models.lemonTelkom;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Data.DAL.LemonDBContext
{
    public class AplicationLemonDBContext : DbContext
    {
        public AplicationLemonDBContext(DbContextOptions<AplicationLemonDBContext> options) : base(options)
        {
            
    }
        public virtual DbSet<test> test { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<test>().ToTable("test");
        }
    }
}
