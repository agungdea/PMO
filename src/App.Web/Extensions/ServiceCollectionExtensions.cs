﻿using App.Data.DAL;
using App.Data.DAL.LemonDBContext;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Localization;
using App.Web.App_Start;
using App.Web.Utils.SsoConfig;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.AspNetCore.NameConvention;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Modular.Core;
using Modular.Core.Caching;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using static App.Web.Startup;

namespace App.Web.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection LoadInstalledModules(this IServiceCollection services,
            IList<ModuleInfo> modules, IHostingEnvironment hostingEnvironment)
        {
            var moduleRootFolder = new DirectoryInfo(Path.Combine(hostingEnvironment.ContentRootPath, "Modules"));
            var moduleFolders = moduleRootFolder.GetDirectories();

            foreach (var moduleFolder in moduleFolders)
            {
                var binFolder = new DirectoryInfo(Path.Combine(moduleFolder.FullName, "bin"));
                if (!binFolder.Exists)
                {
                    continue;
                }

                foreach (var file in binFolder.GetFileSystemInfos("*.dll", SearchOption.AllDirectories))
                {
                    Assembly assembly = null;
                    try
                    {
                        assembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(file.FullName);
                    }
                    catch (FileLoadException ex)
                    {
                        if (ex.Message == "Assembly with same name is already loaded")
                        {
                            // Get loaded assembly
                            assembly = Assembly.Load(new AssemblyName(Path.GetFileNameWithoutExtension(file.Name)));
                        }
                        else
                        {
                            throw;
                        }
                    }

                    if (assembly.FullName.Contains(moduleFolder.Name))
                    {
                        modules.Add(new ModuleInfo
                        {
                            Name = moduleFolder.Name,
                            Assembly = assembly,
                            Path = moduleFolder.FullName
                        });
                    }
                }
            }

            GlobalConfiguration.Modules = modules;
            return services;
        }

        public static IServiceCollection AddCustomizedDatatables(this IServiceCollection services)
        {
            var a = typeof(Npgsql.NpgsqlFactory).AssemblyQualifiedName;
            var dtTablesOptions = new DataTables.AspNet.AspNetCore.Options(10, true, true, true, new CamelCaseRequestNameConvention(),
               new CamelCaseResponseNameConvention());

            services.RegisterDataTables(dtTablesOptions);

            return services;
        }

        public static IServiceCollection AddCustomizedMvc(this IServiceCollection services, IList<ModuleInfo> modules)
        {
            var mapperConfig = MapperConfig.Map();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin",
                    opt => opt.AllowAnyOrigin());
            });

            var mvcBuilder = services.AddMvc()
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .AddRazorOptions(o =>
                {
                    foreach (var module in modules)
                    {
                        o.AdditionalCompilationReferences.Add(MetadataReference.CreateFromFile(module.Assembly.Location));
                    }
                });

            foreach (var module in modules)
            {
                // Register controller from modules
                mvcBuilder.AddApplicationPart(module.Assembly);

                // Register dependency in modules
                var moduleInitializerType =
                    module.Assembly.GetTypes().FirstOrDefault(x => typeof(IModuleInitializer).IsAssignableFrom(x));
                if ((moduleInitializerType != null) && (moduleInitializerType != typeof(IModuleInitializer)))
                {
                    var moduleInitializer = (IModuleInitializer)Activator.CreateInstance(moduleInitializerType);
                    moduleInitializer.Init(services, mapperConfig);
                }
            }

            services.AddAutoMapper(opt =>
            {
                opt.AddProfile(mapperConfig);
            });

            return services;
        }

        public static IServiceCollection AddCustomizedIdentity(this IServiceCollection services,
            string emailConfirmationTokenProviderName,
            string passwordResetTokenProviderName)
        {
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new ModuleViewLocationExpander());
            });

            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = false;
                options.AuthenticationDescriptions.Add(new Microsoft.AspNetCore.Http.Authentication.AuthenticationDescription()
                {
                    AuthenticationScheme = "Identity.Application"
                });
                options.ForwardWindowsAuthentication = false;
            });

            services.Configure<IdentityOptions>(options =>
            {
                options.Tokens.EmailConfirmationTokenProvider = emailConfirmationTokenProviderName;
                options.Tokens.PasswordResetTokenProvider = passwordResetTokenProviderName;
                options.SignIn.RequireConfirmedEmail = true;
                options.User.RequireUniqueEmail = true;
                options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromDays(365);
                options.Cookies.ApplicationCookie.CookieName = "BaseProject.Identity";
                options.Cookies.ApplicationCookie.SlidingExpiration = true;
                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 3;
            });

            services.Configure<ConfirmEmailDataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromHours(5);
            });

            services.Configure<PasswordResetProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromHours(1);
            });

            services.AddIdentity<ApplicationUser, ApplicationRole>(config =>
            {
                config.User.RequireUniqueEmail = true;
                config.Password = new PasswordOptions
                {
                    RequireDigit = false,
                    RequireNonAlphanumeric = false,
                    RequireLowercase = false,
                    RequireUppercase = false,
                    RequiredLength = 8,
                };
                config.Cookies.ApplicationCookie.LoginPath = "/Account/Login";
                config.Cookies.ApplicationCookie.AutomaticChallenge = true;
            })
                .AddEntityFrameworkStores<ApplicationDbContext, string>()
                .AddDefaultTokenProviders()
                .AddTokenProvider<ConfirmEmailDataProtectorTokenProvider<ApplicationUser>>(emailConfirmationTokenProviderName)
                .AddTokenProvider<PasswordResetDataProtectorTokenProvider<ApplicationUser>>(passwordResetTokenProviderName);

            services.AddIdentityServer()
                  .AddTemporarySigningCredential()
                  .AddInMemoryClients(IdSrvConfig.GetClients())
                  .AddInMemoryScopes(IdSrvConfig.GetScopes())
                  .AddAspNetIdentity<ApplicationUser>()
                  .AddProfileService<IdentityWithAdditionalClaimsProfileService>()
                  .AddExtensionGrantValidator<ExternalLoginGrantValidator>();

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder("Identity.Application")
                    .RequireAuthenticatedUser().Build();
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ClaimBasedAuthz",
                                  policy => policy.Requirements.Add(new ClaimDrivenRequirement()));
            });

            services.AddSingleton<IAuthorizationHandler, ClaimDrivenHandler>();

            return services;
        }

        public static IServiceCollection AddCustomizedDataStore(this IServiceCollection services,
            IConfigurationRoot configuration)
        {
            // Use a PostgresSQL Database
            var sqlConnectionString = configuration["DataAccessPostgreSqlProvider:ConnectionString"];

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(
                    sqlConnectionString,
                    b => b.MigrationsAssembly("App.Web")
                )
            );
            var MysqlConnectionString = configuration["DataAccessMySqlProvider:ConnectionString"];
            services.AddDbContext<AplicationLemonDBContext>(
                options =>
                options.UseMySql(
                    MysqlConnectionString,
                    b => b.MigrationsAssembly("App.Web")
                )
        );

            return services;
        }

        public static IServiceProvider Build(this IServiceCollection services,
            IConfigurationRoot configuration, IHostingEnvironment hostingEnvironment)
        {
            var builder = new ContainerBuilder();

            foreach (var module in GlobalConfiguration.Modules)
            {
                builder.RegisterAssemblyTypes(module.Assembly).AsImplementedInterfaces();
            }

            builder.RegisterInstance(configuration);
            builder.RegisterInstance(hostingEnvironment);
            builder.Populate(services);
            var container = builder.Build();
            return container.Resolve<IServiceProvider>();
        }

        public static void Seeding(this IServiceCollection services, IList<ModuleInfo> modules)
        {
            var container = services.BuildServiceProvider();
            var seedingHelper = container.GetService<SeedingHelper>();
            var configHelper = container.GetService<ConfigHelper>();

            var autoSeed = configHelper.GetConfigAsBool("app.seed.web.setting.on.restart");

            if (autoSeed)
            {
                seedingHelper.SeedWebSetting(modules);
            }

            autoSeed = configHelper.GetConfigAsBool("app.seed.localization.on.restart");

            if (autoSeed)
            {
                seedingHelper.SeedResouces(modules);
            }
        }

        public static void GenerateLocalizationJson(this IServiceCollection services)
        {
            var container = services.BuildServiceProvider();
            var languageService = container.GetService<ILanguageService>();
            var resourceService = container.GetService<ILocaleResourceService>();

            foreach (var language in languageService.GetAll())
            {
                var cultureCode = language.LanguageCulture;
                resourceService.GenerateResourceJson(cultureCode);
            }
        }

        public static void SetupCaching(this IServiceCollection services)
        {
            services.AddMemoryCache();

            services.AddSingleton<ICacheManager, MemoryCacheManager>();

            /// Redis Cache
            /// See https://docs.microsoft.com/en-us/aspnet/core/performance/caching/distributed 
            /// for more information

            //services.AddDistributedRedisCache(options =>
            //{
            //    options.Configuration = "localhost";
            //    options.InstanceName = "BaseProject";
            //});

            //services.AddSingleton<ICacheManager, DistributedCacheManager>();
        }
    }
}
