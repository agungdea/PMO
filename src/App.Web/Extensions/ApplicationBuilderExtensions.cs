﻿using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Modular.Core;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;

namespace App.Web.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseCustomizedIdentity(this IApplicationBuilder app,
            IConfigurationRoot configuration)
        {
            app.UseIdentity();

            app.UseIdentityServer();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var SSOAuthority = configuration["host:protocol"] + "://" + configuration["host:name"];

            var identityServerValidationOptions = new IdentityServerAuthenticationOptions
            {
                Authority = SSOAuthority,
                ScopeName = "client",
                ScopeSecret = "secret",
                AutomaticAuthenticate = true,
                RequireHttpsMetadata = false,
                SupportedTokens = SupportedTokens.Both,
                AutomaticChallenge = true,
                AdditionalScopes = new List<string>()
                {
                    "mobile-apps"
                }
            };

            app.UseIdentityServerAuthentication(identityServerValidationOptions);

            //app.UseFacebookAuthentication(new FacebookOptions
            //{
            //    AppId = "1660326547328529",
            //    AppSecret = "a711d7bcb6de66933ad3e2f623c3e44c"
            //});

            return app;
        }

        public static IApplicationBuilder UseCustomizedMvc(this IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.Routes.Add(new UrlSlugRoute(routes.DefaultHandler));

                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            return app;
        }

        public static IApplicationBuilder UseCustomizedStaticFiles(this IApplicationBuilder app, 
            IList<ModuleInfo> modules)
        {
            app.UseStaticFiles();

            foreach (var module in modules)
            {
                var wwwrootDir = new DirectoryInfo(Path.Combine(module.Path, "wwwroot"));
                if (!wwwrootDir.Exists)
                {
                    continue;
                }

                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(wwwrootDir.FullName),
                    RequestPath = new PathString("/" + module.ShortName)
                });
            }

            return app;
        }
    }
}
