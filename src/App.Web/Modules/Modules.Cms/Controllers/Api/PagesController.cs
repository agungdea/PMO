﻿using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Modules.Cms.Models;
using Modules.Cms.Services;
using Modules.Cms.ViewModels;
using System;

namespace Modules.Cms.Controllers.Api
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/pages")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class PagesController : BaseCacheApiController<Page, IPageService, PageViewModel, Guid>
    {
        public PagesController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IPageService service,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
    }
}
