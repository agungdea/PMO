﻿using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Modules.Cms.Models;
using Modules.Cms.Services;
using Modules.Cms.ViewModels;
using System;

namespace Modules.Cms.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class PageController : BaseController<Page, IPageService, PageViewModel, PageForm, Guid>
    {
        public PageController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            IPageService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        protected override void CreateData(Page item)
        {
            item.CreatedAt = DateTime.Now;
            item.CreatedBy = CurrentUser.Id;
        }

        protected override void UpdateData(Page item, PageForm model)
        {
            item.Name = model.Name;
            item.SeoTitle = model.SeoTitle;
            item.Body = model.Body;
            item.MetaTitle = model.MetaTitle;
            item.MetaKeywords = model.MetaKeywords;
            item.MetaDescription = model.MetaDescription;
            item.LastUpdateTime = DateTime.Now;
            item.LastEditedBy = CurrentUser.Id;
        }
    }
}
