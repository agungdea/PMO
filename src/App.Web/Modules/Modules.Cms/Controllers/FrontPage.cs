﻿using Microsoft.AspNetCore.Mvc;
using Modules.Cms.Services;
using System;

namespace Modules.Cms.Controllers
{
    public class FrontPageController : Controller
    {
        private readonly IPageService _pageService;

        public FrontPageController(IPageService pageService)
        {
            _pageService = pageService;
        }

        public IActionResult PageDetail(Guid id)
        {
            var page = _pageService.GetById(id);

            return View(page);
        }
    }
}
