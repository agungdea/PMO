﻿using System;
using App.Domain;
using App.Domain.Models.Core;

namespace Modules.Cms.Models
{
    public class Page : Content, ICacheEntity
    {
        public string Body { get; set; }
    }
}
