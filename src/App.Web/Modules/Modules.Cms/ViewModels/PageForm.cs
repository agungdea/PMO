﻿using System.ComponentModel.DataAnnotations;

namespace Modules.Cms.ViewModels
{
    public class PageForm
    {
        public PageForm()
        {
            IsPublished = true;
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public string SeoTitle { get; set; }

        public string MetaTitle { get; set; }

        public string MetaKeywords { get; set; }

        public string MetaDescription { get; set; }

        [Required]
        public string Body { get; set; }

        public bool IsPublished { get; set; }
    }
}
