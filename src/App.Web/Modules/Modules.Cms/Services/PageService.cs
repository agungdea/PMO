﻿using App.Data.Repository;
using App.Helper;
using App.Services;
using App.Services.Core;
using Modular.Core;
using Modular.Core.Caching;
using Modules.Cms.Models;
using System;

namespace Modules.Cms.Services
{
    public class PageService : CacheService<Page, IRepository<Page>>, IPageService
    {
        public const int PageEntityTypeId = 1;

        private readonly ConfigHelper _config;
        private readonly IEntityRoutingService _entityRoutingService;
        private const string PAGE_ALL = "app.cms.page.all";

        public PageService(IRepository<Page> repository,
            IEntityRoutingService entityRoutingService,
            ICacheManager cacheManager,
            ConfigHelper config) 
            : base(repository, cacheManager)
        {
            _entityRoutingService = entityRoutingService;
            _config = config;
            _keyItem = x => x.Id.ToString();

            _cacheKey = PAGE_ALL;

            _cacheInterval = _config.GetConfigAsInt("cache.cms.page.interval");
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;

            if (!GlobalConfiguration.CachingKeys.Contains(PAGE_ALL))
                GlobalConfiguration.CachingKeys.Add(PAGE_ALL);
        }

        public override Page Add(Page model)
        {
            model.SeoTitle = _entityRoutingService
                .ToSafeSlug(model.SeoTitle, model.Id, PageEntityTypeId);

            var result = base.Add(model);

            _entityRoutingService.Add(result.Name, result.SeoTitle, result.Id, PageEntityTypeId);

            return result;
        }

        public override Page Update(Page model)
        {
            model.SeoTitle = _entityRoutingService
                .ToSafeSlug(model.SeoTitle, model.Id, PageEntityTypeId);

            _entityRoutingService.Update(model.Name, model.SeoTitle, model.Id, PageEntityTypeId);

            return base.Update(model);
        }

        public override Page Delete(Page model)
        {
            _entityRoutingService.Remove(model.Id, PageEntityTypeId);
            return base.Delete(model);
        }

        public Page GetById(Guid id)
        {
            Page page = null;

            if(GetCache().ContainsKey(id.ToString()))
                page = GetCache()[id.ToString()];

            return page;
        }
    }
}
