﻿using App.Services;
using Modules.Cms.Models;
using System;

namespace Modules.Cms.Services
{
    public interface IPageService : ICacheService<Page>
    {
        Page GetById(Guid id);
    }
}
