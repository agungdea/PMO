﻿using AutoMapper.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Modular.Core;
using Modules.Cms.Models;
using Modules.Cms.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Modules.Cms
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void Init(IServiceCollection service, MapperConfigurationExpression mapper)
        {
            Mapper(mapper);

            var menu = new ModuleMenu()
            {
                Icon = "icon-note",
                Id = "menu-content-management",
                Text = "cms.menu.content.management",
                Area = "",
                Controller = "",
                Action = "",
                Order = 0,
                Children = new List<ModuleMenu>
                {
                    new ModuleMenu()
                    {
                        Id = "menu-page",
                        Text = "cms.menu.page.management",
                        Area = "Admin",
                        Controller = "Page",
                        Action = "Index",
                        Order = 0,
                        UseDictionary = true
                    }
                },
                UseDictionary = true
            };

            if (!GlobalConfiguration.ModuleMenus.Any(x => x.Id == menu.Id && x.Text == menu.Text))
            {
                GlobalConfiguration.ModuleMenus.Add(menu);
            }
        }

        private void Mapper(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<Page, PageForm>()
                .ReverseMap();

            mapper.CreateMap<Page, PageViewModel>()
                .ReverseMap();
        }
    }
}
