﻿using AutoMapper.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Modular.Core;

namespace Modules.Searching
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void Init(IServiceCollection service, MapperConfigurationExpression mapper)
        {
            Mapper(mapper);
        }

        private void Mapper(MapperConfigurationExpression mapper)
        {
        }
    }
}
