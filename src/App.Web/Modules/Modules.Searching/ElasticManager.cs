﻿using App.Helper;
using Nest;
using System;

namespace Modules.Searching
{
    public class ElasticManager
    {
        private static Uri node;
        private static ConnectionSettings settings;
        private static ElasticClient client;

        private readonly ConfigHelper _config;

        public ElasticManager(ConfigHelper config)
        {
            _config = config;

            node = new Uri(_config.GetConfig("elastic.url"));
            settings = new ConnectionSettings(node)
                .DefaultIndex(_config.GetConfig("elastic.index"))
                .DisableDirectStreaming();
            client = new ElasticClient(settings);
        }
    }
}
