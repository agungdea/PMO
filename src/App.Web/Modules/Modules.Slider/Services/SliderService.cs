﻿using App.Data.Repository;
using App.Domain.Models.Core;
using AutoMapper;
using Modules.Slider.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Modules.Slider.Services
{
    public class SliderService : ISliderService
    {
        private readonly IRepository<WebSetting> _repo;
        private readonly IMapper _mapper;

        public SliderService(IRepository<WebSetting> repository,
            IMapper mapper)
        {
            _repo = repository;
            _mapper = mapper;
        }

        private int MaxName()
        {
            var alls = GetAll();

            if (!alls.Any())
                return 0;

            return alls.Max(x =>
            {
                var number = x.Name.Substring(x.Name.Length - 3);
                var i = 0;

                int.TryParse(number, out i);

                return i;
            });
        }

        private int MaxOrder()
        {
            var alls = GetAll();

            if (!alls.Any())
                return 0;

            return alls.Max(x =>
            {
                if (string.IsNullOrWhiteSpace(x.Order))
                    return 0;

                return int.Parse(x.Order);
            });
        }

        private WebSetting GetByName(string name)
        {
            return _repo.Table.FirstOrDefault(x => x.Name == name);
        }

        public IList<SliderItem> GetAll()
        {
            var query = _repo
                .Table
                .Where(x => x.Name.StartsWith("slider.", StringComparison.OrdinalIgnoreCase));

            return _mapper.Map<List<SliderItem>>(query.ToList());
        }

        public SliderItem GetSlider(string name)
        {
            var item = GetByName(name);

            return _mapper.Map<SliderItem>(item);
        }

        public SliderItem Create(SliderItem model)
        {
            var sliderName = "slider." + (MaxName() + 1).ToString("000");
            var setting = new WebSetting()
            {
                Name = sliderName,
                Value = model.Value,
                CustomField1 = model.Link,
                CustomField2 = (MaxOrder() + 1).ToString("000"),
                CustomField3 = model.Caption,
                CreatedAt = DateTime.Now
            };

            _repo.Add(setting);

            return _mapper.Map<SliderItem>(setting);
        }

        public SliderItem Update(SliderItem model)
        {
            var setting = GetByName(model.Name);

            if (string.IsNullOrWhiteSpace(model.Order) && string.IsNullOrWhiteSpace(setting.CustomField2))
            {
                model.Order = (MaxOrder() + 1).ToString("000");
            }

            if (string.IsNullOrWhiteSpace(model.Order))
            {
                model.Order = setting.CustomField2;
            }

            setting.Value = model.Value;
            setting.CustomField1 = model.Link;
            setting.CustomField2 = model.Order;
            setting.CustomField3 = model.Caption;
            setting.LastUpdateTime = DateTime.Now;

            _repo.Update(setting);

            return _mapper.Map<SliderItem>(setting);
        }

        public SliderItem Delete(string name)
        {
            var item = GetByName(name);

            _repo.Delete(item);

            return _mapper.Map<SliderItem>(item);
        }

        public bool Reorder(List<string> names)
        {
            var result = true;
            try
            {
                var counter = 1;

                foreach (var name in names)
                {
                    var item = GetByName(name);

                    if (item == null)
                        continue;

                    item.CustomField2 = counter.ToString("000");

                    _repo.Update(item);

                    counter++;
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }
    }
}
