﻿using Modules.Slider.Models;
using System.Collections.Generic;

namespace Modules.Slider.Services
{
    public interface ISliderService
    {
        IList<SliderItem> GetAll();
        SliderItem GetSlider(string name);
        SliderItem Create(SliderItem model);
        SliderItem Update(SliderItem model);
        SliderItem Delete(string name);
        bool Reorder(List<string> names);
    }
}
