﻿using App.Domain.Models.Core;
using Newtonsoft.Json;
using System;

namespace Modules.Slider.Models
{
    public class SliderItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Caption { get; set; }
        public string Link { get; set; }
        public string Order { get; set; }
        public Attachment Attachment
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<Attachment>(Value);
                }
                catch (Exception)
                {
                    return new Attachment();
                }
            }
        }
    }
}
