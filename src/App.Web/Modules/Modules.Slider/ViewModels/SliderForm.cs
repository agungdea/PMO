﻿using System.ComponentModel.DataAnnotations;

namespace Modules.Slider.ViewModels
{
    public class SliderForm
    {
        public SliderForm()
        {
            Name = "slider.temp";
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Value { get; set; }

        public string Caption { get; set; }

        public string Link { get; set; }

        public string Order { get; set; }
    }
}
