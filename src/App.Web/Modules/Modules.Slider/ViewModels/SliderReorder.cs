﻿using System.Collections.Generic;

namespace Modules.Slider.ViewModels
{
    public class SliderReorder
    {
        public List<string> Names { get; set; }
    }
}
