﻿using App.Domain.Models.Core;
using AutoMapper.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Modular.Core;
using Modules.Slider.Models;
using Modules.Slider.ViewModels;
using System.Linq;

namespace Modules.Slider
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void Init(IServiceCollection services, MapperConfigurationExpression mapper)
        {
            Mapper(mapper);

            var menu = new ModuleMenu()
            {
                Icon = "icon-picture",
                Id = "menu-slider",
                Text = "slider.menu",
                Area = "Admin",
                Controller = "Slider",
                Action = "Index",
                Order = 1,
                UseDictionary = true
            };

            if(!GlobalConfiguration.ModuleMenus.Any(x => x.Id == menu.Id && x.Text == menu.Text))
            {
                GlobalConfiguration.ModuleMenus.Add(menu);
            }
        }

        private void Mapper(MapperConfigurationExpression mapper)
        {
            mapper.CreateMap<WebSetting, SliderItem>()
                .ForMember(x => x.Link, opt => opt.MapFrom(x => x.CustomField1))
                .ForMember(x => x.Order, opt => opt.MapFrom(x => x.CustomField2))
                .ForMember(x => x.Caption, opt => opt.MapFrom(x => x.CustomField3))
                .ReverseMap();

            mapper.CreateMap<SliderItem, SliderForm>()
                .ReverseMap();
        }
    }
}
