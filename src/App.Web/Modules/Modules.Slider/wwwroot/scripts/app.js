﻿var Slider = function () {
    var initialize = function () {
        $common.setMenu("#menu-slider");
        $("#table-slider").DataTable({
            order: [[3, 'asc']]
        });

        $(".btn-delete").on("click", function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            e.preventDefault();// used to stop its default behaviour
            bootbox.confirm($L.get("slider.text.delete.caption"), function (result) {
                if (result === true) {
                    $.ajax({
                        url: "/Admin/Slider/Delete?name=" + id,
                        type: "DELETE",
                        timeout: 60000,
                        dataType: "json",
                        contentType: "application/json",
                        success: function (response) {
                            $alert.success($L.get("slider.alert.delete.success"));
                            location.reload();
                        },
                        error: function (response) {
                            console.log(response);
                            $alert.error($L.get("slider.alert.delete.error"));
                        },
                        beforeSend: function () {
                            $("#body-overlay").removeClass("hide");
                        },
                        complete: function () {
                            $("#body-overlay").addClass("hide");
                        }
                    });
                };
            });
        });

        $(".btn-reorder").on("click", function (e) {
            e.preventDefault();
            var uri = $(this).attr("href");

            var modal = bootbox.dialog({
                title: $L.get("slider.text.reorder.slider"),
                message: '<p><i class="fa fa-spin fa-spinner"></i> ' + $L.get("common.text.loading") + '</p>'
            })
            .on('shown.bs.modal', function () {
                $(".dd").nestable({
                    maxDepth: 1
                });

                $("#SliderReorder").on("submit", function (e) {
                    e.preventDefault();

                    var uri = $(this).attr("action");

                    var data = {
                        Names: []
                    };

                    $.each($(this).find(".dd-item"), function (i, item) {
                        data.Names.push($(item).data("id"));
                    });

                    $.ajax({
                        url: uri,
                        method: "POST",
                        data: data,
                        success: function (resp) {
                            if (resp) {
                                $alert.success($L.get("slider.alert.reorder.success"));
                                location.reload();
                            } else {
                                $alert.error($L.get("slider.alert.reorder.failed"));
                            }
                        },
                        error: function (resp) {
                            $alert.error($L.get("slider.alert.reorder.error"));
                        }
                    });
                });
            });

            modal.init(function () {
                $.ajax({
                    url: uri,
                    success: function (resp) {
                        modal.find('.bootbox-body').html(resp);
                    }
                })
            });
        });
    }

    return {
        init: function () {
            initialize();
        }
    }
}();