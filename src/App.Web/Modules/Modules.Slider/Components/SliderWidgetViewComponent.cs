﻿using Microsoft.AspNetCore.Mvc;
using Modules.Slider.Services;

namespace Modules.Slider.Components
{
    public class SliderWidgetViewComponent : ViewComponent
    {
        private readonly ISliderService _sliderService;

        public SliderWidgetViewComponent(ISliderService sliderService)
        {
            _sliderService = sliderService;
        }

        public IViewComponentResult Invoke()
        {
            var model = _sliderService.GetAll();

            return View("/Modules/Modules.Slider/Views/Components/SliderWidget.cshtml", model);
        }
    }
}
