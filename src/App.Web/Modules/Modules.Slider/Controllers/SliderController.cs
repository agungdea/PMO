﻿using App.Domain.Models.Identity;
using App.Helper;
using App.Services.Core;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Modules.Slider.Infrastructure;
using Modules.Slider.Models;
using Modules.Slider.Services;
using Modules.Slider.ViewModels;
using System;
using System.IO;

namespace Modules.Slider.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class SliderController : Controller
    {
        private readonly ISliderService _service;
        private readonly FileHelper _fileHelper;
        private readonly IMapper _mapper;

        public SliderController(ISliderService service,
            FileHelper fileHelper,
            IMapper mapper)
        {
            _service = service;
            _fileHelper = fileHelper;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var sliders = _service.GetAll();

            return View(sliders);
        }

        public IActionResult Create()
        {
            var model = new SliderForm();

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(SliderForm model)
        {
            if(!ModelState.IsValid)
                return View(model);

            var temp = _service.Create(_mapper.Map<SliderItem>(model));

            var attachment = model.Value.ConvertToAttachment();
            if (attachment != null)
            {
                var newPath = $"{SliderConstant.SLIDER_DIRECTORY}/{temp.Id.ToString("n")}";
                attachment.CropedPath = _fileHelper.CreateCropped(attachment);
                attachment.Path = _fileHelper.FileMove(attachment.Path,
                    newPath,
                    temp.Id.ToString("n"));
                attachment.CropedPath = _fileHelper.FileMove(attachment.CropedPath,
                    newPath,
                    temp.Id.ToString("n") + "_crop");
                temp.Value = attachment.ConvertToString();
            }

            _service.Update(temp);

            return RedirectToAction("Index");
        }
        
        public IActionResult Edit(string name)
        {
            var sliderItem = _service.GetSlider(name);

            if (sliderItem == null)
                return NotFound();

            return View(_mapper.Map<SliderForm>(sliderItem));
        }

        [HttpPost]
        public IActionResult Edit(string name, SliderForm model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var sliderItem = _service.GetSlider(name);

            if (sliderItem == null)
                return NotFound();

            if(sliderItem.Name != model.Name)
            {
                ModelState.AddModelError("", "Invalid slider name");
                return View(model);
            }

            var attachment = model.Value.ConvertToAttachment();
            if (attachment != null && model.Value != sliderItem.Value)
            {
                var newPath = $"{SliderConstant.SLIDER_DIRECTORY}/{sliderItem.Id.ToString("n")}";
                attachment.CropedPath = _fileHelper.CreateCropped(attachment);
                attachment.Path = _fileHelper.FileMove(attachment.Path,
                    newPath,
                    sliderItem.Id.ToString("n"));
                attachment.CropedPath = _fileHelper.FileMove(attachment.CropedPath,
                    newPath,
                    sliderItem.Id.ToString("n") + "_crop");
                model.Value = attachment.ConvertToString();
            }

            _service.Update(_mapper.Map<SliderItem>(model));

            return RedirectToAction("Index");
        }

        [HttpDelete]
        [Produces("application/json")]
        public IActionResult Delete(string name)
        {
            if (_service.GetSlider(name) == null)
                return NotFound();

            var item = _service.Delete(name);

            _fileHelper.DeleteAttachment(item.Attachment);
            _fileHelper.DeleteDirectory(item.Attachment.Path);

            return Ok(item);
        }

        public IActionResult Reorder()
        {
            var sliders = _service.GetAll();

            return View(sliders);
        }

        [HttpPost]
        [Produces("application/json")]
        public IActionResult Reorder(SliderReorder model)
        {
            return Ok(_service.Reorder(model.Names));
        }
    }
}
