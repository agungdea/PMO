﻿using App.Domain.Mapper;
using App.Domain.Models.App;
using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using App.Domain.Models.Localization;
using App.Domain.Models.Workflow;
using App.Web.Models.ViewModels.App;
using App.Web.Models.ViewModels.Core;
using App.Web.Models.ViewModels.Identity;
using App.Web.Models.ViewModels.Localization;
using App.Web.Models.ViewModels.Workflow;
using AutoMapper.Configuration;

namespace App.Web.App_Start
{
    public static class MapperConfig
    {
        public static MapperConfigurationExpression Map()
        {
            var cfg = new MapperConfigurationExpression();

            #region App

            cfg.CreateMap<Category, CategoryViewModel>()
                .ForMember(x => x.ParentId, opt => opt.MapFrom(x => x.ParentId == null ? 0 : x.ParentId))
                .ReverseMap();

            cfg.CreateMap<Category, CategoryForm>()
                .ReverseMap();

            cfg.CreateMap<AreaMapping, AreaMappingViewModel>()
                .ReverseMap();

            cfg.CreateMap<AreaMapping, AreaMappingForm>()
                .ReverseMap();

            cfg.CreateMap<Justification, JustificationViewModel>()
                .ReverseMap();

            cfg.CreateMap<Justification, JustificationHeaderForm>()
                .ReverseMap();

            cfg.CreateMap<Justification, JustificationUraianForm>()
                .ReverseMap();

            cfg.CreateMap<Justification, JustificationJumlahKebutuhanForm>()
                .ReverseMap();

            cfg.CreateMap<Justification, JustificationRencanaPelaksanaanForm>()
                .ReverseMap();

            cfg.CreateMap<Justification, JustificationLainLainForm>()
                .ReverseMap();

            cfg.CreateMap<Aki, AkiForm>()
                .ForMember(x => x.DocumentNJKI, opt => opt.MapFrom(x => x.CustomField1))
                .ReverseMap();
            cfg.CreateMap<Aki, AkiFormNJKI>()
             .ForMember(x => x.DocumentNJKI, opt => opt.MapFrom(x => x.CustomField1))
             .ReverseMap();

            cfg.CreateMap<AkiCover, AkiCoverForm>()
                .ForMember(x => x.InvestmentCategory, opt => opt.MapFrom(x => x.KategoriInvestasi))
                .ForMember(x => x.NomorAccount, opt => opt.MapFrom(x => x.NomorAkun))
                .ForMember(x => x.Lokasi, opt => opt.MapFrom(x => x.Location))
                .ForMember(x => x.IdProposal, opt => opt.MapFrom(x => x.ProposalId))
                .ForMember(x => x.IdLop, opt => opt.MapFrom(x => x.LopId))
                .ForMember(x => x.PenanggungJawabvalue, opt => opt.MapFrom(x => x.CustomField1))
                .ForMember(x => x.LevelValue, opt => opt.MapFrom(x => x.CustomField2))
                .ReverseMap();

            cfg.CreateMap<Unit, UnitForm>()
                .ReverseMap();

            cfg.CreateMap<Unit, UnitViewModel>()
                .ReverseMap();

            cfg.CreateMap<Designator, DesignatorViewModel>()
                .ReverseMap();

            cfg.CreateMap<Designator, DesignatorForm>()
                .ReverseMap();

            cfg.CreateMap<Harga, HargaViewModel>()
                .ReverseMap();

            cfg.CreateMap<Harga, HargaForm>()
                .ReverseMap();

            cfg.CreateMap<Currency, CurrencyViewModel>()
                .ReverseMap();

            cfg.CreateMap<Currency, CurrencyForm>()
                .ReverseMap();

            cfg.CreateMap<WBS, WBSForm>()
                .ForMember(x => x.KodeUnit, opt => opt.MapFrom(x => x.Unit));

            cfg.CreateMap<WBSForm, WBS>()
                .ForMember(x => x.Unit, opt => opt.MapFrom(x => x.KodeUnit));

            cfg.CreateMap<WBSTree, WBSTreeViewModel>()
                .ReverseMap();

            cfg.CreateMap<WBSTree, WBSTreeForm>()
                .ReverseMap();

            cfg.CreateMap<WBSTreeForm, WBSTree>()
                .ReverseMap();

            cfg.CreateMap<WBSCurrencyForm, WBSCurrency>()
               .ReverseMap();

            cfg.CreateMap<ActivityTemplateViewModel, ActTemplate>()
             .ReverseMap();

            cfg.CreateMap<ActivityTemplateForm, ActTemplate>()
               .ReverseMap();

            cfg.CreateMap<EventTemplateForm, ActTmpChild>()
               .ForMember(x => x.ISLEAF, opt => opt.MapFrom(x => x.ISLEAF == true ? 1 : 0))
               .ReverseMap();

            cfg.CreateMap<ActTmpChild, EventTemplateForm>()
               .ForMember(x => x.ISLEAF, opt => opt.MapFrom(x => x.ISLEAF == "1" ? true : false))
               .ReverseMap();

            cfg.CreateMap<EventTemplateViewModel, ActTmpChild>()
               .ForMember(x => x.ISLEAF, opt => opt.MapFrom(x => x.ISLEAF == true ? 1 : 0))
               .ReverseMap();

            cfg.CreateMap<ActTmpChild, EventTemplateViewModel>()
               .ForMember(x => x.ISLEAF, opt => opt.MapFrom(x => x.ISLEAF == "1" ? true : false))
               .ReverseMap();

            cfg.CreateMap<UserMedia, UserMediaForm>()
               .ReverseMap();

            cfg.CreateMap<UserMedia, UserMediaViewModel>()
               .ReverseMap();

            cfg.CreateMap<GroupWf, GroupWfViewModel>()
              .ReverseMap();

            cfg.CreateMap<GroupWf, GroupWfForm>()
              .ReverseMap();
            #endregion

            #region Identity

            cfg.CreateMap<ApplicationUser, ApplicationUserViewModel>()
                .ForMember(x => x.Username, opt => opt.MapFrom(x => x.UserName))
                .ForMember(x => x.Phone, opt => opt.MapFrom(x => x.PhoneNumber))
                .ForMember(x => x.UserProfile, opt => opt.MapFrom(x => x.UserProfile))
                .ReverseMap();

            cfg.CreateMap<ApplicationUser, ApplicationUserForm>()
                .ForMember(x => x.Phone, opt => opt.MapFrom(x => x.PhoneNumber))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.UserProfile.Name))
                .ForMember(x => x.Birthplace, opt =>
                    opt.MapFrom(x => x.UserProfile.Birthplace))
                .ForMember(x => x.Birthdate, opt => opt.MapFrom(x => x.UserProfile.Birthdate))
                .ForMember(x => x.Address, opt => opt.MapFrom(x => x.UserProfile.Address))
                .ForMember(x => x.Photo, opt => opt.MapFrom(x => x.UserProfile.Photo))
                .ReverseMap();

            cfg.CreateMap<ApplicationUser, UpdateProfileForm>()
                .ForMember(x => x.Phone, opt => opt.MapFrom(x => x.PhoneNumber))
                .ForMember(x => x.Name, opt => opt.MapFrom(x => x.UserProfile.Name))
                .ForMember(x => x.Birthplace, opt =>
                    opt.MapFrom(x => x.UserProfile.Birthplace))
                .ForMember(x => x.Birthdate, opt => opt.MapFrom(x => x.UserProfile.Birthdate))
                .ForMember(x => x.Address, opt => opt.MapFrom(x => x.UserProfile.Address))
                .ForMember(x => x.Photo, opt => opt.MapFrom(x => x.UserProfile.Photo))
                .ReverseMap();

            cfg.CreateMap<ApplicationRole, RoleForm>()
                .ReverseMap();

            cfg.CreateMap<ApplicationRole, RoleViewModel>()
                .ReverseMap();

            cfg.CreateMap<UserProfile, UserProfileViewModel>()
                .ReverseMap();

            cfg.CreateMap<UserProfile, UserProfileFormViewModel>()
                .ReverseMap();

            cfg.CreateMap<MasterClaim, MasterClaimViewModel>()
                .ReverseMap();

            cfg.CreateMap<MasterClaim, MasterClaimForm>()
                .ReverseMap();

            cfg.CreateMap<AppAction, AppActionViewModel>()
                .ReverseMap();

            cfg.CreateMap<AppAction, AppActionForm>()
                .ReverseMap();

            #endregion

            #region Core

            cfg.CreateMap<WebSetting, WebSettingViewModel>()
                .ReverseMap();

            cfg.CreateMap<WebSetting, WebSettingForm>()
                .ReverseMap();

            cfg.CreateMap<Log, LogViewModel>()
                .ReverseMap();

            #endregion

            #region Localization

            cfg.CreateMap<Language, LanguageViewModel>()
                .ReverseMap();

            cfg.CreateMap<Language, LanguageForm>()
                .ReverseMap();

            cfg.CreateMap<LocaleResource, LocaleResourceViewModel>()
                .ReverseMap();

            cfg.CreateMap<LocaleResource, LocaleResourceForm>()
                .ReverseMap();

            #endregion

            #region Workflow

            cfg.CreateMap<WfActivityView, WfActivityViewViewModel>()
                .ReverseMap();

            cfg.CreateMap<WfActivityView, WfActivityViewForm>()
                .ForMember(x => x.ActivityName, opt => opt.MapFrom(x => x.Id));

            cfg.CreateMap<WfActivityViewForm, WfActivityView>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.ActivityName));

            #endregion

            DtoMapping.Map(cfg);

            return cfg;
        }
    }
}
