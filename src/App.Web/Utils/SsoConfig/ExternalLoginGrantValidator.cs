﻿using App.Domain.Models.Identity;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace App.Web.Utils.SsoConfig
{
    public class ExternalLoginGrantValidator : IExtensionGrantValidator
    {
        private UserManager<ApplicationUser> _userManager;
        public string GrantType => "external_login";

        public ExternalLoginGrantValidator(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task ValidateAsync(ExtensionGrantValidationContext context)
        {
            var providerName = context.Request.Raw.Get("provider_name");
            var providerKey = context.Request.Raw.Get("provider_key");

            if (string.IsNullOrEmpty(providerName) || string.IsNullOrWhiteSpace(providerKey))
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant);
                return;
            }

            var user = await _userManager.FindByLoginAsync(providerName, providerKey);

            if (user == null)
            {
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant);
                return;
            }

            context.Result = new GrantValidationResult(user.Id, GrantType);

            return;
        }
    }
}
