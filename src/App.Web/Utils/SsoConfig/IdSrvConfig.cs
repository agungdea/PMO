﻿using IdentityServer4.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace App.Web.Utils.SsoConfig
{
    public class IdSrvConfig
    {

        public static IEnumerable<Scope> GetScopes()
        {
            return new List<Scope>
            {
                StandardScopes.OpenId,
                StandardScopes.Profile,
                StandardScopes.Roles,
                StandardScopes.OfflineAccess,
                new Scope
                {
                    Name = "portal",
                    Description = "Portal Application"
                },
                new Scope
                {
                    Name = "client",
                    Description = "client Application",
                    ScopeSecrets = new List<Secret>()
                    {
                        new Secret("secret".Sha256())
                    }
                },
                new Scope
                {
                    Name = "mobile-apps",
                    DisplayName = "mobile-apps",
                    Description = "Mobile Application",
                    Type = ScopeType.Resource,

                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim("role"),
                        new ScopeClaim(ClaimTypes.Name),
                        new ScopeClaim(ClaimTypes.Email)
                    },

                    IncludeAllClaimsForUser = true
                }
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "client",
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,
                    AllowAccessTokensViaBrowser = true,
                    AccessTokenType = AccessTokenType.Reference,
                    //Supaya tidak mencari file Allow Consent
                    RequireConsent = false,
                   //redirect Login setelah suckses login
                    RedirectUris =
                    {
                       "http://localhost:5002/signin-oidc"
                    },
                    //redirect sesudah logout
                    PostLogoutRedirectUris =
                    {
                        "http://localhost:5002"
                       //  Startup.GetSSO_Client()
                    },
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {
                        StandardScopes.OpenId.Name,
                        StandardScopes.Profile.Name,
                        StandardScopes.OfflineAccess.Name,
                        "portal",
                        "client"
                    }
                },
                new Client
                {
                    ClientId = "mobile-apps",
                    AllowedGrantTypes = GrantTypes.List(
                        GrantType.ResourceOwnerPassword,
                        "external_login"
                    ),
                    AllowAccessTokensViaBrowser = true,
                    AccessTokenType = AccessTokenType.Jwt,
                    RequireConsent = false,
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = {
                        "mobile-apps"
                    },
                    AccessTokenLifetime = 60 * 60 * 365
                }
            };
        }
    }
}
