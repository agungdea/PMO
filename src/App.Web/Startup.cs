﻿using App.Data.Repository;
using App.Helper;
using App.Services;
using App.Web.Extensions;
using App.Web.filter;
using App.Web.Helper;
using App.Web.Models.ViewModels.App;
using App.Web.Models.ViewModels.Hosting;
using App.Web.Models.ViewModels.Lemon;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Modular.Core;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace App.Web
{
    public class Startup
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IList<ModuleInfo> modules = new List<ModuleInfo>();
        public class ConfirmEmailDataProtectorTokenProvider<TUser> : DataProtectorTokenProvider<TUser> where TUser : class
        {
            public ConfirmEmailDataProtectorTokenProvider(IDataProtectionProvider dataProtectionProvider, IOptions<ConfirmEmailDataProtectionTokenProviderOptions> options) : base(dataProtectionProvider, options)
            {
            }
        }

        public class ConfirmEmailDataProtectionTokenProviderOptions : DataProtectionTokenProviderOptions { }


        public class PasswordResetDataProtectorTokenProvider<TUser> : DataProtectorTokenProvider<TUser> where TUser : class
        {
            public PasswordResetDataProtectorTokenProvider(IDataProtectionProvider dataProtectionProvider, IOptions<PasswordResetProtectionTokenProviderOptions> options) : base(dataProtectionProvider, options)
            {
            }
        }

        public class PasswordResetProtectionTokenProviderOptions : DataProtectionTokenProviderOptions { }

        public Startup(IHostingEnvironment env)
        {
            _hostingEnvironment = env;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddUserSecrets();
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

			env.ConfigureNLog("nlog.config");
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        private const string EmailConfirmationTokenProviderName = "ConfirmEmail";
        private const string PasswordResetTokenProviderName = "PasswordReset";

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            GlobalConfiguration.WebRootPath = _hostingEnvironment.WebRootPath;
            GlobalConfiguration.ContentRootPath = _hostingEnvironment.ContentRootPath;
          //  services.LoadInstalledModules(modules, _hostingEnvironment);

            services.AddSingleton<IConfiguration>(Configuration);

            services.SetupCaching();

            services.AddMemoryCache();

            services.AddCustomizedDataStore(Configuration);
            services.Add(new ServiceDescriptor(typeof(ContextLemon), new ContextLemon(Configuration["DataAccessMySqlProvider:ConnectionString"])));
            services.Configure<HostConfiguration>(HostConfiguration =>
            {
                HostConfiguration.Name = Configuration["host:name"];
                HostConfiguration.Protocol = Configuration["host:protocol"];
            });

            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddCustomizedIdentity(EmailConfirmationTokenProviderName, 
                PasswordResetTokenProviderName);

            services.AddSession();

            services.AddScoped<LogFilter>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddCustomizedDatatables();

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            services.AddScoped<LogFilter>();

            ServicesRegistrar.Register(services);

            HelperRegistrar.Register(services);

            services.Seeding(modules);

            services.GenerateLocalizationJson();

            #region Modular

            services.AddCustomizedMvc(modules);

            return services.Build(Configuration, _hostingEnvironment);

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            var supportedCultures = new[]
           {
                new CultureInfo("en-US")
            };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddNLog();

            app.AddNLogWeb();
            
            LogManager.ReconfigExistingLoggers();

            //LogManager.Configuration.Variables["connectionString"] = Configuration["DataAccessPostgreSqlProvider:ConnectionString"];

            app.UseApplicationInsightsRequestTelemetry();
            app.UseApplicationInsightsExceptionTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseCustomizedStaticFiles(modules);

            app.UseCustomizedIdentity(Configuration);

            app.UseSession();

            app.UseCustomizedMvc();
        }
    }
}
