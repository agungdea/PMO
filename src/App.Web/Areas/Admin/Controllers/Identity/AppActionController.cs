﻿using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Identity;
using App.Web.Models.ViewModels.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;

namespace App.Web.Areas.Admin.Controllers.Identity
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.AdminAndSuperadmin)]
    public class AppActionController : BaseController<AppAction, IAppActionService, AppActionViewModel, AppActionForm, string>
    {
        private readonly IMasterClaimService _masterClaimService;

        public AppActionController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            IAppActionService service,
            IMasterClaimService masterClaimService,
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _masterClaimService = masterClaimService;
        }

        public override IActionResult Details(string id)
        {
            var claims = _masterClaimService.GetAll();

            ViewBag.MasterClaims = claims;

            ViewBag.CurrentClaims = Service
                .GetClaims(id)
                .Select(x => new Claim(x.ClaimType, x.ClaimValue))
                .ToList();

            ViewBag.ClaimId = id;
            ViewBag.ClaimType = ClaimType.Action;

            return base.Details(id);
        }

        protected override void UpdateData(AppAction item, AppActionForm model)
        {
            item.Name = model.Name;
        }
    }
}
