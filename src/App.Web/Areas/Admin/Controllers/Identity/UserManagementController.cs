﻿using App.Domain.Models.App;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using App.Web.Models.ViewModels.Hosting;
using App.Web.Models.ViewModels.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Identity
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.AdminAndSuperadmin)]
    public class UserManagementController : BaseController<ApplicationUser, IUserService, ApplicationUserViewModel, ApplicationUserForm, string>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMasterClaimService _masterClaimService;
        private readonly FileHelper _fileHelper;
        private readonly ConfigHelper _config;
        private readonly MailingHelper _mailingHelper;
        private readonly HostConfiguration _hostConfiguration;
        private readonly string _cropSuffix;
        private readonly string _userDir;
        private readonly IUnitService _unitService;

        public UserManagementController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IUserService service,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IUnitService unitService,
            IMasterClaimService masterClaimService,
            FileHelper fileHelper,
            ConfigHelper config,
            IUserHelper userHelper,
            IOptions<HostConfiguration> hostConfiguration,
            MailingHelper mailingHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _masterClaimService = masterClaimService;
            _fileHelper = fileHelper;
            _config = config;
            _mailingHelper = mailingHelper;
            _hostConfiguration = hostConfiguration.Value;
            _cropSuffix = _config.GetConfig("crop.suffix");
            _userDir = _config.GetConfig("user.upload.directory");
            _unitService = unitService;
        }

        public override IActionResult Details(string id)
        {
            var user = UserService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            var model = Mapper.Map<ApplicationUserViewModel>(user);

            model.Roles = _userManager
                .GetRolesAsync(user)
                .Result
                .ToList();

            var claims = _masterClaimService.GetAll();

            ViewBag.MasterClaims = claims;

            var currentClaims = _userManager
                .GetClaimsAsync(user)
                .Result;

            ViewBag.CurrentClaims = currentClaims;

            ViewBag.ClaimId = id;
            ViewBag.ClaimType = ClaimType.User;

            var unitId = currentClaims.FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;

            if (unitId != null)
            {
                var userUnitBagian = _unitService.GetById(unitId);

                if (userUnitBagian != null && userUnitBagian.STLevel == Unit.STLevelBagian)
                    unitId = userUnitBagian.ParentId;
                else
                    userUnitBagian = null;
                var userUnitKerja = _unitService.GetById(unitId);
                if (userUnitKerja != null && userUnitKerja.STLevel == Unit.STLevelUnitKerja)
                    unitId = userUnitKerja.ParentId;

                else
                {
                    userUnitKerja = null;
                }


                var userUnitBisnis = _unitService.GetById(unitId);
                var userUnitDirectorat = _unitService.GetById(userUnitBisnis.ParentId);

                if (userUnitDirectorat != null)
                {
                    model.Directorat = userUnitDirectorat.NamaUnit;
                }

                if (userUnitBisnis != null)
                {
                    model.UnitBisnis = userUnitBisnis.NamaUnit;
                }

                if (userUnitKerja != null)
                {
                    model.UnitKerja = userUnitKerja.NamaUnit;
                }

                if (userUnitBagian != null)
                {
                    model.Bagian = userUnitBagian.NamaUnit;
                }
            }

            return View(model);
        }

        public override IActionResult Create()
        {
            ViewBag.Roles = _roleManager.Roles.ToList();

            var model = new ApplicationUserForm()
            {
                Roles = new List<string>()
            };

            return View(model);
        }

        [HttpPost]
        public override IActionResult Create(ApplicationUserForm model)
        {
            if (ModelState.IsValid)
            {
                var defaultPassword = _config.GetConfig("user.default.password");
                var user = Mapper.Map<ApplicationUser>(model);

                user.UserProfile = new UserProfile();
                user.UserProfile.Name = model.Name;
                user.UserName = model.Nik;
                user.EmailConfirmed = true;
                var result = _userManager.CreateAsync(user, defaultPassword).Result;

                if (result.Succeeded)
                {
                    result = _userManager.AddToRolesAsync(user, model.Roles).Result;

                    if (result.Succeeded)
                    {
                        var unit = model.UnitBisnis;
                        if (model.UnitKerja != null)
                        {
                            unit = model.UnitKerja;
                        }
                        if (model.Bagian != null)
                        {
                            unit = model.Bagian;
                        }

                        result = _userManager.AddClaimAsync(user, new Claim(ApplicationUser.UNIT_CLAIMS, unit)).Result;
                        if (model.AllUnit == true)
                        {
                            result = _userManager.AddClaimAsync(user, new Claim(ApplicationUser.ALL_Unit, "true")).Result;
                        }
                        if (model.AllRegional == true)
                        {
                            result = _userManager.AddClaimAsync(user, new Claim(ApplicationUser.ALL_Regional, "true")).Result;
                        }

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        AddError(result.Errors);
                    }
                }
                else
                {
                    AddError(result.Errors);
                }
            }

            ViewBag.Roles = _roleManager.Roles.ToList();

            return View(model);
        }

        public override IActionResult Edit(string id)
        {
            ViewBag.Roles = _roleManager.Roles.ToList();
            ViewBag.Id = id;

            var user = UserService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            var model = Mapper.Map<ApplicationUserForm>(user);
            model.Nik = user.UserName;
            model.Roles = _userManager
                .GetRolesAsync(user)
                .Result
                .ToList();

            var claims = _userManager.GetClaimsAsync(user).Result;
            var unitId = claims.FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;

            var allaccess = claims.FirstOrDefault(x => x.Type == ApplicationUser.ALL_Unit)?.Value;
            var allregion= claims.FirstOrDefault(x => x.Type == ApplicationUser.ALL_Regional)?.Value;
            if (unitId != null)
            {
                var userUnitBagian = _unitService.GetById(unitId);

                if (userUnitBagian != null && userUnitBagian.STLevel == Unit.STLevelBagian)
                    unitId = userUnitBagian.ParentId;
                else
                {
                    userUnitBagian = null;
                }
                var userUnitKerja = _unitService.GetById(unitId);
                if (userUnitKerja != null && userUnitKerja.STLevel == Unit.STLevelUnitKerja)
                    unitId = userUnitKerja.ParentId;

                else
                {
                    userUnitKerja = null;
                }


                var userUnitBisnis = _unitService.GetById(unitId);
                var userUnitDirectorat = _unitService.GetById(userUnitBisnis.ParentId);

                if (userUnitDirectorat != null)
                {
                    model.Directorat = userUnitDirectorat.Id;
                    ViewBag.DirectoratName = userUnitDirectorat.NamaUnit;
                }

                if (userUnitBisnis != null)
                {
                    model.UnitBisnis = userUnitBisnis.Id;
                    ViewBag.UnitBisnisName = userUnitBisnis.NamaUnit;
                }

                if (userUnitKerja != null)
                {
                    model.UnitKerja = userUnitKerja.Id;
                    ViewBag.UnitKerjaName = userUnitKerja.NamaUnit;
                }

                if (userUnitBagian != null)
                {
                    model.Bagian = userUnitBagian.Id;
                    ViewBag.BagianName = userUnitBagian.NamaUnit;
                }
            }
            if (allregion == "true")
            {
                model.AllRegional = true;
            }
            if (allaccess == "true")
            {
                model.AllUnit = true;
            }

            return View(model);
        }

        [HttpPost]
        public override IActionResult Edit(string id, ApplicationUserForm model)
        {
            if (ModelState.IsValid)
            {
                var user = UserService.GetById(id);

                if (user == null)
                {
                    return NotFound();
                }

                user.Update(model);
                user.UserName = model.Nik;
                var result = _userManager.UpdateAsync(user).Result;

                if (result.Succeeded)
                {
                    var currentRoles = _userManager
                        .GetRolesAsync(user)
                        .Result;

                    result = _userManager
                        .RemoveFromRolesAsync(user, currentRoles)
                        .Result;

                    if (result.Succeeded)
                    {
                        result = _userManager
                            .AddToRolesAsync(user, model.Roles)
                            .Result;

                        var unitId = model.UnitBisnis;

                        if (model.UnitKerja != null)
                        {
                            unitId = model.UnitKerja;
                        }

                        if (model.Bagian != null)
                        {
                            unitId = model.Bagian;
                        }

                        var claims = _userManager.GetClaimsAsync(user).Result;
                        var currentUnit = claims.FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS);
                        var currentDisposisiUnit = claims.FirstOrDefault(x => x.Type == ApplicationUser.UNIT_DISPO_CLAIMS);
                        var unitClaim = new Claim(ApplicationUser.UNIT_CLAIMS, unitId);
                       
                        #region CLAIM ALL REGION
                        if (model.AllRegional == true)
                        {
                            var Region = claims.FirstOrDefault(x => x.Type == ApplicationUser.ALL_Regional)?.Value;
                            if (Region == null)
                            {
                                result = _userManager.AddClaimAsync(user, new Claim(ApplicationUser.ALL_Regional, "true")).Result;
                            }
                        }
                        else
                        {
                            var Region = claims.FirstOrDefault(x => x.Type == ApplicationUser.ALL_Regional)?.Value;
                            if (Region != null)
                            {
                                result = _userManager.RemoveClaimAsync(user, new Claim(ApplicationUser.ALL_Regional, "true")).Result;
                            }
                        }
                        #endregion

                        #region CLAIM ALL UNIT
                        //
                        if (model.AllUnit == true)
                        {
                            var unit = claims.FirstOrDefault(x => x.Type == ApplicationUser.ALL_Unit)?.Value;
                            if (unit == null)
                            {
                                result = _userManager.AddClaimAsync(user, new Claim(ApplicationUser.ALL_Unit, "true")).Result;
                            }
                        }
                        else
                        {
                            var unit = claims.FirstOrDefault(x => x.Type == ApplicationUser.ALL_Unit)?.Value;
                            if (unit != null)
                            {
                                result = _userManager.RemoveClaimAsync(user, new Claim(ApplicationUser.ALL_Unit, "true")).Result;
                            }
                        }
                        #endregion

                        #region CLAIM UNIT AND DISPOSISI
                        if (currentUnit != null)
                        {
                            result = _userManager.ReplaceClaimAsync(user, currentUnit,
                                unitClaim).Result;

                            if (model.Disposisi == true)
                            {
                                if (currentUnit.Value != null)
                                {
                                    var unitDisposisiClaim = new Claim(ApplicationUser.UNIT_DISPO_CLAIMS, currentUnit.Value);
                                    if (currentDisposisiUnit != null)
                                    {
                                        if (currentDisposisiUnit.Value != unitDisposisiClaim.Value)
                                            result = _userManager.ReplaceClaimAsync(user, currentDisposisiUnit,
                                                unitDisposisiClaim).Result;
                                    }
                                    else
                                    {
                                        result = _userManager.AddClaimAsync(user, unitDisposisiClaim).Result;
                                    }
                                }
                            }
                        }
                        else
                        {
                            result = _userManager.AddClaimAsync(user, unitClaim).Result;
                        }
                        #endregion

                        if (result.Succeeded)
                        {
                            if (!string.IsNullOrWhiteSpace(model.Photo) && (model.Photo != user.UserProfile.Photo))
                            {
                                var attachment = model.Photo.ConvertToAttachment();
                                if (attachment != null)
                                {
                                    var newPath = $"{_userDir}/{user.Id}";
                                    attachment.CropedPath = _fileHelper.CreateCropped(attachment);
                                    attachment.Path = _fileHelper.FileMove(attachment.Path,
                                        newPath,
                                        user.Id);
                                    attachment.CropedPath = _fileHelper.FileMove(attachment.CropedPath,
                                        newPath,
                                        user.Id + _cropSuffix);
                                    user.UserProfile.Photo = attachment.ConvertToString();
                                    result = _userManager.UpdateAsync(user).Result;

                                    if (result.Succeeded)
                                    {
                                        UserHelper.SetCache(user);
                                        return RedirectToAction("Details", new { id });
                                    }
                                    else
                                    {
                                        AddError(result.Errors);
                                    }
                                }
                            }

                            UserHelper.SetCache(user);
                            return RedirectToAction("Details", new { id });
                        }
                        else
                        {
                            AddError(result.Errors);
                        }
                    }
                    else
                    {
                        AddError(result.Errors);
                    }
                }
                else
                {
                    AddError(result.Errors);
                }
            }

            ViewBag.Id = id;
            ViewBag.Roles = _roleManager.Roles.ToList();

            return View(model);
        }
    }
}
