﻿using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Identity;
using App.Web.Models.ViewModels.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace App.Web.Areas.Admin.Controllers.Identity
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.AdminAndSuperadmin)]
    public class MasterClaimController
        : BaseController<MasterClaim, IMasterClaimService, MasterClaimViewModel, MasterClaimForm, Guid>
    {
        public MasterClaimController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            IMasterClaimService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        public override IActionResult Index()
        {
            var claims = Service.GetAll();

            return View(Mapper.Map<List<MasterClaimViewModel>>(claims));
        }

        public override IActionResult Edit(Guid id, MasterClaimForm model)
        {
            if (ModelState.IsValid)
            {
                var item = Service.GetById(id);

                if (item == null)
                {
                    return NotFound();
                }

                var before = item;

                UpdateData(item, model);

                var result = Service.Update(item);

                AfterUpdateData(before, item);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        protected override void CreateData(MasterClaim item)
        {
            item.CreatedAt = DateTime.Now;
            item.CreatedBy = CurrentUser?.Id;
        }

        protected override void UpdateData(MasterClaim item, MasterClaimForm model)
        {
            item.Type = model.Type;
            item.ClaimValues = model.ClaimValues;
            item.LastUpdateTime = DateTime.Now;
            item.LastEditedBy = CurrentUser?.Id;
        }
    }
}
