﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using App.Domain.Models.Identity;
using Microsoft.AspNetCore.Identity;
using App.Services.Identity;
using App.Helper;
using AutoMapper;
using App.Web.Models.ViewModels.Identity;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserProfileService _userProfileService;
        private readonly IUserHelper _userHelper;
        private readonly IMapper _mapper;
        private readonly FileHelper _fileHelper;
        private readonly ConfigHelper _config;
        private readonly string _cropSuffix;
        private readonly string _userDir;

        public ProfileController(UserManager<ApplicationUser> userManager,
            IUserProfileService userProfileService,
            IUserHelper userHelper,
            IMapper mapper,
            FileHelper fileHelper,
            ConfigHelper config)
        {
            _userManager = userManager;
            _userProfileService = userProfileService;
            _userHelper = userHelper;
            _mapper = mapper;
            _fileHelper = fileHelper;
            _config = config;
            _cropSuffix = _config.GetConfig("crop.suffix");
            _userDir = _config.GetConfig("user.upload.directory");
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var user = _userHelper.GetUser(User);

            ViewBag.ModelForm = _mapper.Map<UpdateProfileForm>(user);

            return View(_mapper.Map<ApplicationUserViewModel>(user));
        }

        public IActionResult UpdateProfile()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> UpdateProfile(UpdateProfileForm model)
        {
            var user = _userHelper.GetUser(User);

            if (!ModelState.IsValid)
            {
                ViewBag.ModelForm = model;
                ModelState.AddModelError("updateError", "");
                return View("Index", _mapper.Map<ApplicationUserViewModel>(user));
            }

            user.Update(model);

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                if (!string.IsNullOrWhiteSpace(model.Photo) && (model.Photo != user.UserProfile.Photo))
                {
                    var attachment = model.Photo.ConvertToAttachment();
                    if (attachment != null)
                    {
                        var newPath = $"{_userDir}/{user.Id}";
                        attachment.CropedPath = _fileHelper.CreateCropped(attachment);
                        attachment.Path = _fileHelper.FileMove(attachment.Path,
                            newPath,
                            user.Id);
                        attachment.CropedPath = _fileHelper.FileMove(attachment.CropedPath,
                            newPath,
                            user.Id + _cropSuffix);
                        user.UserProfile.Photo = attachment.ConvertToString();
                        result = await _userManager.UpdateAsync(user);

                        if (result.Succeeded)
                        {
                            _userHelper.SetCache(user);
                            TempData["Success"] = true;
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            AddError(result.Errors, "updateError");
                        }
                    }
                }

                _userHelper.SetCache(user);
                TempData["Success"] = true;
                return RedirectToAction("Index");
            }
            else
            {
                AddError(result.Errors, "updateError");
            }

            _userHelper.SetCache(user);
            TempData["Success"] = true;
            return RedirectToAction("Index");
        }

        public IActionResult ChangePassword()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordForm model)
        {
            var user = _userHelper.GetUser(User);

            if (!ModelState.IsValid)
            {
                TempData["PasswordSuccess"] = false;
                ModelState.AddModelError("passwordError", "");
                ViewBag.ModelForm = _mapper.Map<UpdateProfileForm>(user);
                return View("Index", _mapper.Map<ApplicationUserViewModel>(user));
            }

            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

            if (result.Succeeded)
            {
                TempData["PasswordSuccess"] = true;
                return RedirectToAction("Index");
            }

            AddError(result.Errors, "passwordError");

            TempData["PasswordSuccess"] = false;
            ViewBag.ModelForm = _mapper.Map<UpdateProfileForm>(user);
            return View("Index", _mapper.Map<ApplicationUserViewModel>(user));
        }

        private void AddError(IEnumerable<IdentityError> errors, string key = "")
        {
            var errorString = "";
            var first = true;
            var last = errors.Last();
            foreach (var error in errors)
            {
                if (!first)
                {
                    errorString += "<li>";
                }

                errorString += $"{error.Description}";

                if (error.Code != last.Code)
                {
                    errorString += "</li>";
                }

                first = false;
            }
            ModelState.AddModelError(key, errorString);
        }
    }
}
