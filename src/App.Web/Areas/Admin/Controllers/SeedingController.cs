﻿using App.Domain.Models.Identity;
using App.Helper;
using App.Web.Models.ViewModels.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Modular.Core;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class SeedingController : Controller
    {
        private readonly SeedingHelper _seedingHelper;

        public SeedingController(
            SeedingHelper seedingHelper)
        {
            _seedingHelper = seedingHelper;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Seed(SeedingViewModel model)
        {
            if (model.WebSetting)
                _seedingHelper.SeedWebSetting(GlobalConfiguration.Modules);

            if (model.Locale)
                _seedingHelper.SeedResouces(GlobalConfiguration.Modules);

            return RedirectToAction("Index");
        }
    }
}
