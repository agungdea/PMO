﻿using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Core;
using App.Services.Identity;
using App.Web.Models.ViewModels.Core;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Core
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class WebSettingController : BaseController<WebSetting, IWebSettingService, WebSettingViewModel, WebSettingForm, Guid>
    {
        public WebSettingController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService,
            IMapper mapper,
            IWebSettingService service,
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        protected override void CreateData(WebSetting item)
        {
            item.CreatedAt = DateTime.Now;
            item.CreatedBy = CurrentUser?.Id;
        }

        protected override void UpdateData(WebSetting item, WebSettingForm model)
        {
            item.Update(model);
            item.LastUpdateTime = DateTime.Now;
            item.LastEditedBy = CurrentUser?.Id;
        }
    }
}
