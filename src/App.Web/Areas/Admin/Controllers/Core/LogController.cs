﻿using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Core;
using App.Services.Identity;
using App.Web.Models.ViewModels.Core;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Core
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class LogController : BaseController<Log, ILogService, LogViewModel, LogViewModel, int>
    {
        public LogController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            ILogService service,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
    }
}
