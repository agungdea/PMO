﻿using App.Domain.Models.Identity;
using App.Domain.Models.Workflow;
using App.Helper;
using App.Infrastructure;
using App.Services.Identity;
using App.Services.Workflow;
using App.Web.Models.ViewModels.Workflow;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace App.Web.Areas.Admin.Controllers.Workflow
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class WfActivityViewController :
        BaseController<WfActivityView, 
            IWfActivityViewService, 
            WfActivityViewViewModel, 
            WfActivityViewForm, 
            string>
    {
        public WfActivityViewController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            IWfActivityViewService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        protected override void CreateData(WfActivityView item)
        {
            item.CreatedAt = DateTime.Now;
            item.CreatedBy = CurrentUser.Id;
        }

        protected override void UpdateData(WfActivityView item, WfActivityViewForm model)
        {
            item.Update(model);
            item.LastUpdateTime = DateTime.Now;
            item.LastEditedBy = CurrentUser.Id;
        }
    }
}
