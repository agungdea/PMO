﻿using System;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class CurrencyController : BaseController<Currency, ICurrencyService, CurrencyViewModel, CurrencyForm, Guid>
    {
        public CurrencyController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            ICurrencyService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        protected override void UpdateData(Currency item, CurrencyForm model)
        {
            item.CurrencyCode = model.CurrencyCode;
            item.CurrencyValue = model.CurrencyValue;
        }
    }
}
