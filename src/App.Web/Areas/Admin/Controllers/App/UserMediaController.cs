﻿using App.Domain.Models.App;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class UserMediaController : BaseController<UserMedia, IUserMediaService, UserMediaViewModel, UserMediaForm, Guid>
    {
        public UserMediaController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IUserMediaService service,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }


        public override IActionResult Index()
        {
            var userMedia = Service.GetAll(x => x.Creator).Where(x => x.CreatedBy == CurrentUser.Id).ToList();

            List<UserMediaViewModel> model = new List<UserMediaViewModel>();

            if (userMedia != null)
            {


                foreach (var item in userMedia)
                {
                    var temp = new UserMediaViewModel();

                    temp.Id = item.Id;
                    temp.UnitId = item.UnitId;
                    temp.MediaDirectory = item.MediaDirectory;

                    model.Add(temp);
                }
            }

            return View(model);
        }
    }
}
