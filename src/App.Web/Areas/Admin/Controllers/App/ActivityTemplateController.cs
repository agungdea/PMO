﻿using System;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
   
    [Area("Admin")]
    [Authorize(Policy = "ClaimBasedAuthz")]
    public class ActivityTemplateController : BaseController<ActTemplate, IActivityTemplateService, ActivityTemplateViewModel, ActivityTemplateForm, Guid>
    {
        private readonly IUnitService _unitService;
        public ActivityTemplateController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IActivityTemplateService service,
            IUnitService unitService,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _unitService = unitService;
        }


        public override IActionResult Details(Guid Id)
        {
            var template = Service.GetById(Id);

            if (null == template)
            {
                return NotFound();
            }

            var model = Mapper.Map<ActivityTemplateViewModel>(template);

            var unitId = template.Grup;

            if (unitId != null)
            {
                var userUnitBagian = _unitService.GetById(unitId);

                if (userUnitBagian != null && userUnitBagian.STLevel == Unit.STLevelBagian)
                    unitId = userUnitBagian.ParentId;
                else
                    userUnitBagian = null;

                var userUnitKerja = _unitService.GetById(unitId);
                var userUnitBisnis = _unitService.GetById(userUnitKerja.ParentId);
                var userUnitDirectorat = _unitService.GetById(userUnitBisnis.ParentId);

                if (userUnitDirectorat != null)
                {
                    model.Directorat = userUnitDirectorat.NamaUnit;
                }

                if (userUnitBisnis != null)
                {
                    model.UnitBisnis = userUnitBisnis.NamaUnit;
                }

                if (userUnitKerja != null)
                {
                    model.UnitKerja = userUnitKerja.NamaUnit;
                }

                if (userUnitBagian != null)
                {
                    model.Bagian = userUnitBagian.NamaUnit;
                }
            }

            return View(model);
        } 

        
        [HttpPost]
        public override IActionResult Create(ActivityTemplateForm model)
        {
            if (ModelState.IsValid)
            {
                var unit = model.UnitKerja;

                if (model.Bagian != null)
                {
                    unit = model.Bagian;
                }

                var result = Mapper.Map<ActTemplate>(model);

                result.Grup = unit;
                result.CreatedAt = DateTime.Now;
                result.CreatedBy = CurrentUser.Id;

                Service.Add(result);

                return RedirectToAction("Index");
            }

            return View(model);
        }


        public override IActionResult Edit(Guid Id)
        {

            var template = Service.GetById(Id);

            if (null == template)
            {
                return NotFound();
            }

            var model = Mapper.Map<ActivityTemplateForm>(template);

            var unitId = template.Grup;

            if (unitId != null)
            {
                var userUnitBagian = _unitService.GetById(unitId);

                if (userUnitBagian != null && userUnitBagian.STLevel == Unit.STLevelBagian)
                    unitId = userUnitBagian.ParentId;
                else
                    userUnitBagian = null;

                var userUnitKerja = _unitService.GetById(unitId);
                var userUnitBisnis = _unitService.GetById(userUnitKerja.ParentId);
                var userUnitDirectorat = _unitService.GetById(userUnitBisnis.ParentId);

                if (userUnitDirectorat != null)
                {
                    model.Directorat = userUnitDirectorat.Id;
                    ViewBag.DirectoratName = userUnitDirectorat.NamaUnit;
                }

                if (userUnitBisnis != null)
                {
                    model.UnitBisnis = userUnitBisnis.Id;
                    ViewBag.UnitBisnisName = userUnitBisnis.NamaUnit;
                }

                if (userUnitKerja != null)
                {
                    model.UnitKerja = userUnitKerja.Id;
                    ViewBag.UnitKerjaName = userUnitKerja.NamaUnit;
                }

                if (userUnitBagian != null)
                {
                    model.Bagian = userUnitBagian.Id;
                    ViewBag.BagianName = userUnitBagian.NamaUnit;
                }
               
            }

            return View(model);
        }
        

        [HttpPost]
        public override IActionResult Edit(Guid Id, ActivityTemplateForm model)
        {

            var template = Service.GetById(Id);

            if (null == template)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                template.Update(model);

                template.LastEditedBy = CurrentUser.Id;

                Service.Update(template);

                return RedirectToAction("Details", new { Id = Id });
            }

            return View(model);
        }
    }
}
