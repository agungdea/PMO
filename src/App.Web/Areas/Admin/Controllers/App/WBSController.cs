﻿using App.Domain.Models.App;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    [Authorize(Policy = "ClaimBasedAuthz")]
    public class WBSController : BaseController<WBS, IWBSService, WBSViewModel, WBSForm, Guid>
    {
        private readonly IUnitService _unitService;
        private readonly IWBSTreeService _wbsTreeService;
        private readonly IWBSBoQService _wbsBoQService;
        private readonly IHargaService _hargaService;
        private readonly ICurrencyService _currencyServices;
        private readonly IWBSCurrencyService _WBSCurrencyServices;
        private readonly ExcelHelper _excelhelper;
        private readonly IWBSScheduleService _wbsPlanningService;

        private readonly IActivityTemplateService _actTemplateService;
        private readonly IWBStreeCategoryService _wbstreeCategoryService;

        public WBSController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IWBSService service,
            IUserHelper userHelper,
            IUnitService unitService,
            IWBSTreeService wbsTreeService,
            ICurrencyService currencyServices,
            IActivityTemplateService actTemplateService,
            IWBSBoQService wbsBoQService,
            IHargaService hargaService,
            IWBSCurrencyService WBSCurrencyServices,
            IWBStreeCategoryService wbstreeCategoryService,
            ICurrencyService currencyService,
            IWBSScheduleService wbsPlanningService,
            ExcelHelper excelhelper,
            UserManager<ApplicationUser> userManager)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _unitService = unitService;
            _wbsTreeService = wbsTreeService;
            _wbsBoQService = wbsBoQService;
            _hargaService = hargaService;
            _currencyServices = currencyServices;
            _WBSCurrencyServices = WBSCurrencyServices;
            _excelhelper = excelhelper;
            _wbsPlanningService = wbsPlanningService;
            _actTemplateService = actTemplateService;
            _wbstreeCategoryService = wbstreeCategoryService;
        }


        public override IActionResult Details(Guid id)
        {
            ViewBag.IdWBS = id;
            ViewBag.Items = _currencyServices.GetAll().Select(x => new SelectListItem { Text = x.CurrencyCode, Value = x.Id.ToString() });

            var model = new WBSForm();

            model = Mapper.Map<WBSForm>(Service.GetById(id));

            model.WBSCurrencies = Mapper.Map<List<WBSCurrencyForm>>(_WBSCurrencyServices.GetAll(x => x.Currency).Where(x => x.WBSId == id).ToList());

            return View(model);
        }


        public IActionResult DetailsBoQ(Guid id)
        {
            var lokasi = _wbsTreeService.GetById(id);

            if (lokasi == null)
                return NotFound();

            var wbs = Service.GetById(lokasi.IdWBS);

            var model = new WBSBoQViewModel()
            {
                Lokasi = lokasi,
                WBS = wbs
            };

            var currencies = _currencyServices.GetAll();

            ViewBag.Currencies = currencies;

            var category = _wbstreeCategoryService.GetAll(x => x.Category).Where(x => x.WBStreeId == id).FirstOrDefault();

            if (category != null)
            {
                ViewBag.Category = category.Category.Name;
            }

            return View(model);
        }

        public IActionResult DetailsSchedule(Guid id)
        {
            var lokasi = _wbsTreeService.GetById(id);

            if (lokasi == null)
                return NotFound();

            var wbs = Service.GetById(lokasi.IdWBS);

            var model = new WBSBoQViewModel()
            {
                Lokasi = lokasi,
                WBS = wbs
            };

            var category = _wbstreeCategoryService.GetAll(x => x.Category).Where(x => x.WBStreeId == id).FirstOrDefault();

            if (category != null)
            {
                ViewBag.Category = category.Category.Name;
            }

            return View(model);
        }

        public override IActionResult Create()
        {
            var unitId = User
                .Claims
                .FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;

            if (unitId == null)
            {
                TempData["Alert"] = "Anda tidak memiliki hak untuk melakukan pembuatan WBS";
                return RedirectToAction("Index");
            }
            ViewBag.Items = _currencyServices.GetAll().Where(x => x.CurrencyValue != Currency.CurrencyRp).Select(x => new SelectListItem { Text = x.CurrencyCode, Value = x.Id.ToString() });
            return View();
        }

        [HttpPost]
        public override IActionResult Create(WBSForm model)
        {
            if (ModelState.IsValid)
            {
                var item = Mapper.Map<WBS>(model);

                CreateData(item);

                Service.Add(item);

                AfterCreateData(item);
                var tempModelWBSCurrency = Mapper.Map<List<WBSCurrency>>(model.WBSCurrencies);
                if (tempModelWBSCurrency != null)
                {
                    foreach (var itemWBSCurrency in tempModelWBSCurrency)
                    {
                        itemWBSCurrency.WBSId = item.Id;
                        itemWBSCurrency.CreatedBy = CurrentUser.Id;
                        itemWBSCurrency.CreatedAt = DateTime.Now;
                        _WBSCurrencyServices.Add(itemWBSCurrency);
                    }
                }
                return RedirectToAction("Edit", new { item.Id });
            }
            ViewBag.Items = _currencyServices.GetAll().Where(x => x.CurrencyValue != Currency.CurrencyRp).Select(x => new SelectListItem { Text = x.CurrencyCode, Value = x.Id.ToString() });
            return View(model);
        }

        public override IActionResult Edit(Guid id)
        {
            ViewBag.IdWBS = id;
            ViewBag.Items = _currencyServices.GetAll().Where(x => x.CurrencyValue != Currency.CurrencyRp).Select(x => new SelectListItem { Text = x.CurrencyCode, Value = x.Id.ToString() });

            var model = new WBSForm();

            model = Mapper.Map<WBSForm>(Service.GetById(id));

            model.WBSCurrencies = Mapper.Map<List<WBSCurrencyForm>>(_WBSCurrencyServices.GetAll(x => x.Currency).Where(x => x.WBSId == id).ToList());

            return View(model);
        }

        [HttpPost]
        public override IActionResult Edit(Guid id, WBSForm model)
        {
            ViewBag.IdWBS = id;

            if (ModelState.IsValid)
            {
                var item = Service.GetById(id);

                if (item == null)
                {
                    return NotFound();
                }
                if (model.WBSCurrencies == null)
                {
                    ViewBag.IdWBS = id;
                    ViewBag.Items = _currencyServices.GetAll().Where(x => x.CurrencyValue != Currency.CurrencyRp).Select(x => new SelectListItem { Text = x.CurrencyCode, Value = x.Id.ToString() });
                    model = Mapper.Map<WBSForm>(Service.GetById(id));
                    model.WBSCurrencies = Mapper.Map<List<WBSCurrencyForm>>(_WBSCurrencyServices.GetAll(x => x.Currency).Where(x => x.WBSId == id).ToList());
                    TempData["Alert"] = "Harap isi WBS Currency ";
                    return View(model);
                }
                var before = item;

                UpdateData(item, model);

                var result = Service.Update(item);

                AfterUpdateData(before, item);

                DeleteWBSCurrency(id);

                var tempModelWBSCurrency = Mapper.Map<List<WBSCurrency>>(model.WBSCurrencies);

                if (tempModelWBSCurrency != null)
                {
                    foreach (var itemWBSCurrency in tempModelWBSCurrency)
                    {
                        itemWBSCurrency.WBSId = id;
                        itemWBSCurrency.CreatedBy = CurrentUser.Id;
                        itemWBSCurrency.CreatedAt = DateTime.Now;

                        _WBSCurrencyServices.Add(itemWBSCurrency);
                    }
                }

                return RedirectToAction("Edit", new { id = id });

            }
            ViewBag.IdWBS = id;
            ViewBag.Items = _currencyServices.GetAll().Where(x => x.CurrencyValue != Currency.CurrencyRp).Select(x => new SelectListItem { Text = x.CurrencyCode, Value = x.Id.ToString() });
            model = Mapper.Map<WBSForm>(Service.GetById(id));
            model.WBSCurrencies = Mapper.Map<List<WBSCurrencyForm>>(_WBSCurrencyServices.GetAll(x => x.Currency).Where(x => x.WBSId == id).ToList());
            TempData["Alert"] = "Data Tidak berhasil disimpan ";
            return View(model);
        }
        [HttpPost]
        public IActionResult UploadBoq(Guid id, WBSBoQViewModel model)
        {
            if (ModelState.IsValid)
            {

                var attachment = model.FileBoQ.ConvertToAttachment();
                var data = _excelhelper.ValidasiImportBOQ(attachment.Path, CurrentUser.Id);
                if (data == false)
                {
                    TempData["Alert"] = "Tamplete tidak sesuai Download Ulang Tamplate";
                    return RedirectToAction("BoQ", new { id = id });
                }
                var databoq = _excelhelper.ValidasiUploadMataUangBOQ(attachment.Path);
                if (databoq.Count > 0)
                {
                    string messqurenncy="Mata-uang ";
                    foreach (var item in databoq)
                    {
                        messqurenncy += item+",";
                    }
                    TempData["Alert"] = "Anda nenggunakan "+ messqurenncy + "  Harap set terlebih dahulu nilai tukar";
                    return RedirectToAction("BoQ", new { id = id });
                }

                _excelhelper.ImportBOQ(attachment.Path, CurrentUser.Id);
                return RedirectToAction("DetailsBoQ", new { id = id });
            }

            TempData["Alert"] = "File Upload Belum Dilampirkan";

            return RedirectToAction("BoQ", new { id = id });

        }


        public IActionResult BoQ(Guid id)
        {
            var lokasi = _wbsTreeService.GetById(id);

            if (lokasi == null)
                return NotFound();

            var wbs = Service.GetById(lokasi.IdWBS);

            var model = new WBSBoQViewModel()
            {
                Lokasi = lokasi,
                WBS = wbs
            };

            var currencies = _currencyServices.GetAll();

            ViewBag.Currencies = currencies;

            var BoQ = _wbsBoQService.CheckStatusBoq(id);

            if (BoQ)
            {
                ViewBag.HasBoQ = BoQ;
            }
            else
            {
                ViewBag.HasBoQ = false;
            }

            return View(model);
        }

        public IActionResult Schedule(Guid id)
        {
            var lokasi = _wbsTreeService.GetById(id);

            if (lokasi == null)
                return NotFound();

            var wbs = Service.GetById(lokasi.IdWBS);

            var model = new WBSBoQViewModel()
            {
                Lokasi = lokasi,
                WBS = wbs
            };

            ViewBag.Id = id;

            var HasSchedule = _wbsPlanningService.GetAll(x => x.ActTmpChild).Where(x => x.IdWBSTree == id).FirstOrDefault();

            if (HasSchedule != null)
            {
                ViewBag.HasSchedule = true;
                ViewBag.TemplateId = HasSchedule.ActTmpChild.IdActTmp;
                ViewBag.NamaAct = _actTemplateService.GetAll().Where(x => x.Id == HasSchedule.ActTmpChild.IdActTmp).Select(x => x.Name).FirstOrDefault();

            }
            else
            {
                ViewBag.HasSchedule = false;
            }

            return View(model);
        }

        protected override void CreateData(WBS item)
        {
            item.CreatedAt = DateTime.Now;
            item.CreatedBy = CurrentUser.Id;
        }

        protected override void UpdateData(WBS item, WBSForm model)
        {
            item.LastUpdateTime = DateTime.Now;
            item.LastEditedBy = CurrentUser.Id;
        }

        public void DeleteWBSCurrency(Guid id)
        {
            List<WBSCurrency> temp = new List<WBSCurrency>();

            temp = _WBSCurrencyServices.GetAll().Where(x => x.WBSId == id).ToList();

            if (temp != null)
            {
                foreach (var item in temp)
                {
                    _WBSCurrencyServices.Delete(item);
                }
            }
        }

    }
}
