﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using App.Domain.Models.Identity;
using App.Infrastructure;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Domain.Models.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class TransformerPeriodController : BaseController<TransformerPeriod, ITransformerPeriodService, TransformerPeriod, TransformerPeriod, string>
    {
        public TransformerPeriodController(IHttpContextAccessor httpContextAccessor,
            IUserService userService, 
            IMapper mapper,
            ITransformerPeriodService
            service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {

        }
    }
}
