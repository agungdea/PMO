﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    [Authorize(Policy = "ClaimBasedAuthz")]
    public class GroupWfController : BaseController<GroupWf, IGroupWfService, GroupWfViewModel, GroupWfForm, Guid>
    {
        public GroupWfController(IHttpContextAccessor httpContextAccessor, IUserService userService, IMapper mapper, IGroupWfService service, IUserHelper userHelper) : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        // GET: /<controller>/
        public override IActionResult Index()
        {
            return View();
        }
        public override IActionResult Create()
        {
            var gruplist = new List<SelectListItem>();
            gruplist.Add(new SelectListItem { Text = GroupWf.GROUP_WF_WBS, Value = GroupWf.GROUP_WF_WBS, });
            gruplist.Add(new SelectListItem { Text = GroupWf.GROUP_WF_Capex, Value = GroupWf.GROUP_WF_Capex, });
            ViewBag.Gruplist = gruplist;
            return View();
        }
        public override IActionResult Create(GroupWfForm model)
        {
            if (ModelState.IsValid)
            {
                var item = Mapper.Map<GroupWf>(model);
                if (model.isApprover == true || model.GrupName== GroupWf.GROUP_WF_WBS)
                {
                    item.OtherInfo = "true";
                }
                else {
                    item.OtherInfo = "false";
                }
                Service.Add(item);
                return RedirectToAction("Index");
            }
            var gruplist = new List<SelectListItem>();
            gruplist.Add(new SelectListItem { Text = GroupWf.GROUP_WF_WBS, Value = GroupWf.GROUP_WF_WBS, });
            gruplist.Add(new SelectListItem { Text = GroupWf.GROUP_WF_Capex, Value = GroupWf.GROUP_WF_Capex, });
            ViewBag.Gruplist = gruplist;

            return View(model);
        }
    }
}
