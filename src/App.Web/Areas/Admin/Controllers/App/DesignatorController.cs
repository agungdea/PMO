﻿using App.Domain.Models.App;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class DesignatorController :
        BaseController<Designator, IDesignatorService, DesignatorViewModel, DesignatorForm, long>
    {
        private readonly IHargaService _hargaService;

        public DesignatorController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            IDesignatorService service, 
            IUserHelper userHelper,
            IHargaService hargaService) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _hargaService = hargaService;
        }

        [HttpPost]
        public override IActionResult Create(DesignatorForm model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var designator = Mapper.Map<Designator>(model);

            designator = Service.Add(designator);

            if(model.Hargas != null)
            {
                var hargas = model.Hargas;

                hargas.ForEach(x =>
                {
                    x.Id = Guid.NewGuid();
                    x.IdDesignator = designator.IdDesignator;

                    var harga = Mapper.Map<Harga>(x);
                    _hargaService.Add(harga);
                });
            }
            
            return RedirectToAction("Index");
        }

        public override IActionResult Edit(long id)
        {
            var designator = Service.GetById(id);

            if (designator == null)
                return NotFound();

            var vm = Mapper.Map<DesignatorForm>(designator);

            return View(vm);
        }

        [HttpPost]
        public override IActionResult Edit(long id, DesignatorForm model)
        {
            if (!ModelState.IsValid)
                return View(model);
            
            var designator = Service.GetById(id);

            if (designator == null)
                return NotFound();

            designator.Update(model);

            Service.Update(designator);

            var currentHargas = designator.Hargas;
            var toDeleteHargas = currentHargas;

            if(model.Hargas != null)
            {
                foreach(var item in model.Hargas)
                {
                    if (item.Id.HasValue)
                    {
                        if(currentHargas != null)
                        {
                            var current = currentHargas.FirstOrDefault(x => x.Id == item.Id);
                            if(current != null)
                            {
                                current.Update(item);
                                _hargaService.Update(current);
                                toDeleteHargas.RemoveAll(x => x.Id == current.Id);
                            }
                        }
                    }
                    else
                    {
                        var newHarga = Mapper.Map<Harga>(item);
                        newHarga.Id = Guid.NewGuid();
                        newHarga.IdDesignator = model.IdDesignator;
                        _hargaService.Add(newHarga);
                    }
                }
            }

            if(toDeleteHargas != null)
            {
                toDeleteHargas.ForEach(x =>
                {
                    _hargaService.Delete(x);
                });
            }

            return RedirectToAction("Index");
        }
    }
}
