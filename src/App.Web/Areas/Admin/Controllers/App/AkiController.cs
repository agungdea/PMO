﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using App.Domain.Models.Identity;
using App.Web.Models.ViewModels.App;
using App.Domain.Models.App;
using App.Services.App;
using App.Infrastructure;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Rendering;
using App.Web.Models.ViewModels;
using App.Web.Helper;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    [Authorize(Policy = "ClaimBasedAuthz")]
    public class AkiController : BaseController<Aki, IAkiService, AkiViewModel, AkiForm, Guid>
    {
        private readonly IAkiCoverService _akiCoverService;
        private readonly IUnitService _unitservice;
        private readonly IAkiValuationService _akiValuationService;
        private const string LEVEL_CONFIG_NAME = "level.category";
        private const string AKI_DIRECTORY = "aki.upload.directory";
        private const string ROITYPE_CONFIG_NAME = "proyeksi.roi.level";
        private readonly ConfigHelper _config;
        private readonly FileHelper _fileHelper;
        private string _akiDirectori;
        private readonly ExcelHelper _excelHelper;
        private readonly IWfCommantActivityService _wfCommantActivityService;
        public AkiController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            ConfigHelper config,
            FileHelper fileHelper,
            ExcelHelper excelHelper,
            IUnitService unitservice,
            IAkiCoverService akiCoverService,
            IAkiValuationService akiValuationService,
             IWfCommantActivityService wfCommantActivityService,
            IMapper mapper, IAkiService service, IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _akiCoverService = akiCoverService;
            _akiValuationService = akiValuationService;
            _config = config;
            _unitservice = unitservice;
            _fileHelper = fileHelper;
            _wfCommantActivityService = wfCommantActivityService;
            _akiDirectori = _config.GetConfig(AKI_DIRECTORY);
            _excelHelper = excelHelper;
        }
        public IActionResult MyTask()
        {
            return View();
        }

        public override IActionResult Details(Guid id)
        {
            var model = _akiCoverService.GetAllAboutAkiByIdAki(id);
            var listapprovel = new List<string>();
            if (model== null)
            {
                return NotFound();
            }
            if (!string.IsNullOrWhiteSpace(model.Akis.CustomField3))
            {
                var dataApprover = model.Akis.CustomField3.Split(',').ToList();

                foreach (var x in dataApprover)
                {
                    if (!string.IsNullOrWhiteSpace(x))
                    {
                       // var Nik = UserService.GetById(x).UserName;
                        ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
                        var data = context.GetbyNikUser(x).First();
                        if (data != null)
                        {
                            listapprovel.Add("(" + data.nik + " - " + "" + data.nama + " - " + data.Jabatan + ")");
                        }
                    }
                }
                ViewBag.ListAproval = listapprovel;
                ViewBag.komentar = new List<WfCommantActivity>();
                if (!string.IsNullOrWhiteSpace(model.Akis.wf_processId))
                {
                    ViewBag.komentar = _wfCommantActivityService.GetAll().Where(x => x.ProcessId.Equals(model.Akis.wf_processId)).OrderBy(x=>x.CreatedAt);
                }
                
            }
          
        
            return View(model);
        }
        public  IActionResult DetailsDocumentApprover(Guid id)
        {
            var model = _akiCoverService.GetAllAboutAkiByIdAki(id);
            var listapprovel = new List<string>();
            if (model == null)
            {
                return NotFound();
            }
            if (!string.IsNullOrWhiteSpace(model.Akis.CustomField3))
            {
                var dataApprover = model.Akis.CustomField3.Split(',').ToList();

                foreach (var x in dataApprover)
                {
                    if (!string.IsNullOrWhiteSpace(x))
                    {
                        ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
                        var data = context.GetbyNikUser(x).First();
                        if (data != null)
                        {
                            listapprovel.Add("(" + data.nik + " - " + "" + data.nama + " - " + data.Jabatan + ")");
                        }
                    }
                }
                ViewBag.ListAproval = listapprovel;
                ViewBag.komentar = new List<WfCommantActivity>();
                if (!string.IsNullOrWhiteSpace(model.Akis.wf_processId))
                {
                    ViewBag.komentar = _wfCommantActivityService.GetAll().Where(x => x.ProcessId.Equals(model.Akis.wf_processId)).OrderBy(x => x.CreatedAt);
                }

            }


            return View(model);
        }
        public IActionResult CreateAKIHeader()
        {
            var model = new AkiForm();
            model.AkiCover = new AkiCoverForm();
            var selector = new List<LevelCategorySelectorViewModel>();
            var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
            var values = levelChoice.Split(',').ToList();

            foreach (var item in values)
            {
                selector.Add(new LevelCategorySelectorViewModel()
                {
                    Id = item,
                    Name = item
                });
            }
            var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
            model.AkiCover.SourceLevelList = selectSource.ToList();
            return View(model);
        }

        public IActionResult EditAKIHeader(Guid Id)
        {
            var aki = Service.GetById(Id);

            if (aki == null)
            {
                return NotFound();
            }

            var akiCover = _akiCoverService.GetAkiCoverByIdAki(Id);

            var model = Mapper.Map<AkiFormNJKI>(aki);
            model.DocumentNJKI = aki.CustomField1;
            model.AkiCover = Mapper.Map<AkiCoverForm>(akiCover);

            if (model.AkiCover.Roi != null)
            {
                model.AkiCover.Rois = JsonConvert.DeserializeObject<List<RoiViewModel>>(model.AkiCover.Roi);
            }

            var selector = new List<LevelCategorySelectorViewModel>();
            var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
            var values = levelChoice.Split(',').ToList();

            foreach (var item in values)
            {
                selector.Add(new LevelCategorySelectorViewModel()
                {
                    Id = item,
                    Name = item
                });
            }
            var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

            model.AkiCover.SourceLevelList = selectSource.ToList();

            var selectorRoi = new List<ProyeksiRoiViewModel>();
            var roiChoice = _config.GetConfig(ROITYPE_CONFIG_NAME);
            var valuesRoi = roiChoice.Split(',').ToList();

            foreach (var itemRoi in valuesRoi)
            {
                selectorRoi.Add(new ProyeksiRoiViewModel()
                {
                    Id = itemRoi,
                    Name = itemRoi
                });
            }
            var RoisSource = selectorRoi.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

            ViewBag.Items = RoisSource;
            return View(model);
        }

        [HttpPost]
        public IActionResult CreateAKIHeader(AkiForm model)
        {
            var check = Service.GetById(model.Id);
            Aki item = Mapper.Map<Aki>(model);
            if (ModelState.IsValid)
            {
                if (check == null)
                {
                    List<AkiValuation> resourceValuation = new List<AkiValuation>();
                    item.CreatedAt = DateTime.Now;
                    item.CreatedBy = CurrentUser.Id;
                    var unitid = UserHelper.GetUserUnit(CurrentUser.Id);
                    item.OtherInfo = unitid;
                    item.Document = "";
                    item.CustomField2 = Aki.STATUS_WRITE;
                    Service.Add(item);
                    AkiCover akiCover = Mapper.Map<AkiCover>(model.AkiCover);

                    akiCover.AkiId = item.Id;
                    akiCover.CreatedAt = DateTime.Now;
                    akiCover.CreatedBy = CurrentUser.Id;
                    akiCover.KategoriInvestasi = model.AkiCover.InvestmentCategory;
                    //akiCover.CustomField2 = model.AkiCover.LevelValue;
                    akiCover.Level = model.AkiCover.Level;

                    _akiCoverService.Add(akiCover);
                }
                else
                {
                    item = Service.GetById(model.Id);
                    var akiCover = _akiCoverService.GetAkiCoverByIdAki(model.Id);
                    if (item == null || akiCover == null)
                    {
                        return RedirectToAction("EditAKIHeader", new { id = model.Id });
                    }
                    else
                    {
                        item.StrategiInitiativeId = model.StrategiInitiativeId;
                        item.StrategiInitiativeName = model.StrategiInitiativeName;
                        item.ProgramId = model.ProgramId;
                        item.ProgramName = model.ProgramName;
                        item.ProgramId = model.PeriodId;
                        item.ProgramName = model.PeriodName;
                        item.AkiName = model.AkiName;
                        item.LastEditedBy = CurrentUser.Id;
                        item.LastUpdateTime = DateTime.Now;
                        Service.Update(item);
                        akiCover.KategoriInvestasi = model.AkiCover.InvestmentCategory;
                        akiCover.Level = model.AkiCover.Level;
                        akiCover.LastEditedBy = CurrentUser.Id;
                        akiCover.LastUpdateTime = DateTime.Now;
                        _akiCoverService.Update(akiCover);
                    }

                }
                return RedirectToAction("CreateNJKI", new { id = item.Id });

            }
            else
            {
                model.AkiCover = new AkiCoverForm();
                var selector = new List<LevelCategorySelectorViewModel>();
                var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
                var values = levelChoice.Split(',').ToList();
                foreach (var items in values)
                {
                    selector.Add(new LevelCategorySelectorViewModel()
                    {
                        Id = items,
                        Name = items
                    });
                }
                var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
                model.AkiCover.SourceLevelList = selectSource.ToList();
                return View(model);
            }

        }

        public IActionResult CreateNJKI(Guid Id)
        {
            var aki = Service.GetById(Id);

            if (aki == null)
            {
                return NotFound();
            }

            var akiCover = _akiCoverService.GetAkiCoverByIdAki(Id);

            var model = Mapper.Map<AkiFormNJKI>(aki);
            model.DocumentNJKI = aki.CustomField1;
            model.AkiCover = Mapper.Map<AkiCoverForm>(akiCover);

            if (model.AkiCover.Roi != null)
            {
                model.AkiCover.Rois = JsonConvert.DeserializeObject<List<RoiViewModel>>(model.AkiCover.Roi);
            }

            var selectorRoi = new List<ProyeksiRoiViewModel>();
            var roiChoice = _config.GetConfig(ROITYPE_CONFIG_NAME);
            var valuesRoi = roiChoice.Split(',').ToList();

            foreach (var itemRoi in valuesRoi)
            {
                selectorRoi.Add(new ProyeksiRoiViewModel()
                {
                    Id = itemRoi,
                    Name = itemRoi
                });
            }
            var RoisSource = selectorRoi.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

            ViewBag.Items = RoisSource;
            ViewBag.AkiTittle = model.AkiName;
            var unitId = model.AkiCover.PenanggungJawab;
            if (unitId != null)
            {
                var userUnitBagian = _unitservice.GetById(unitId);

                if (userUnitBagian != null && userUnitBagian.STLevel == Unit.STLevelBagian)
                    unitId = userUnitBagian.ParentId;
                else
                {
                    userUnitBagian = null;
                }
                var userUnitKerja = _unitservice.GetById(unitId);
                if (userUnitKerja != null && userUnitKerja.STLevel == Unit.STLevelUnitKerja)
                    unitId = userUnitKerja.ParentId;

                else
                {
                    userUnitKerja = null;
                }


                var userUnitBisnis = _unitservice.GetById(unitId);
                var userUnitDirectorat = _unitservice.GetById(userUnitBisnis.ParentId);

                if (userUnitDirectorat != null)
                {
                    model.Directorat = userUnitDirectorat.Id;
                    ViewBag.DirectoratName = userUnitDirectorat.NamaUnit;
                }

                if (userUnitBisnis != null)
                {
                    model.UnitBisnis = userUnitBisnis.Id;
                    ViewBag.UnitBisnisName = userUnitBisnis.NamaUnit;
                }

                if (userUnitKerja != null)
                {
                    model.UnitKerja = userUnitKerja.Id;
                    ViewBag.UnitKerjaName = userUnitKerja.NamaUnit;
                }
                
            }
            return View(model);
        }


        public IActionResult CreateValuation(Guid Id)
        {
            var aki = Service.GetById(Id);

            if (aki == null)
            {
                return NotFound();
            }

            var akiCover = _akiCoverService.GetAkiCoverByIdAki(Id);

            var model = Mapper.Map<AkiFormNJKI>(aki);
            model.AkiCover = Mapper.Map<AkiCoverForm>(akiCover);
            ViewBag.AkiCover = _akiCoverService.GetAllAboutAkiByIdAki(Id);
            ViewBag.listuser = "[]";
            if (!string.IsNullOrWhiteSpace(aki.CustomField3))
            {
                //model.listUser
                var datauser = aki.CustomField3.Split(',').ToList();
                string datajson = "[";
                foreach (var usr in datauser)
                {
                    if (!string.IsNullOrWhiteSpace(usr))
                    {
                        ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
                        var data = context.GetbyNikUser(usr).First();

                        if (data != null)
                        {
                            datajson += "{";
                            datajson += $"\"id\":\"{usr}\",\n";
                            datajson += $"\"text\":\"({data.nik}) - ({data.nama}) - ({data.Jabatan})\"";
                            datajson += "},";
                        }
                    }
                }
                datajson += "]";
                ViewBag.listuser = datajson;
                ViewBag.statusAki = aki.CustomField2;
                
            }
            ViewBag.AkiTittle = model.AkiName;
            ViewBag.AkiNilaiProgram = model.AkiCover.NilaiProgram;
            var userUnit = _unitservice.GetById(model.AkiCover.PenanggungJawab);

            ViewBag.penanggungJawab = "";
            if (userUnit != null)
            {
                ViewBag.penanggungJawab = userUnit.NamaUnit;
            }

            return View(model);
        }


        [HttpPost]
        public IActionResult CreateValuation(AkiForm model)
        {
            if (ModelState.IsValid)
            {
                var dataki = Service.GetById(model.Id);
                var data = _akiCoverService.GetAllAboutAkiByIdAki(model.Id);
                data.ARPU = model.AkiCover.ARPU;
                data.CAGR = model.AkiCover.CAGR;
                data.DiscountRate = model.AkiCover.DiscountRate;
                data.IRR = model.AkiCover.IRR;
                data.NPV = model.AkiCover.NPV;
                data.PayBackPeriode = model.AkiCover.PaybackPeriode;

                string userApproval = "";
                var nilai = 0;
                foreach (var datalistuser in model.listUser)
                {
                    nilai++;
                    userApproval += datalistuser;
                    if (nilai < model.listUser.Count())
                    {
                        userApproval += ",";
                    }
                }
                if (!string.IsNullOrWhiteSpace(userApproval))
                {
                    dataki.CustomField3 = userApproval;
                }
                dataki.CustomField2 = Aki.STATUS_DRAFT;
                Service.Update(dataki);
                _akiCoverService.Update(data);
                return RedirectToAction("Details", new { id = model.Id });
            }
            ViewBag.AkiTittle = model.AkiName;
            ViewBag.AkiNilaiProgram = model.AkiCover.NilaiProgram;
            ViewBag.penanggungJawab = model.AkiCover.PenanggungJawab;
            return View(model);
        }

        [HttpPost]
        public IActionResult CreateNJKI(Guid Id, AkiFormNJKI model)
        {
            
            if (ModelState.IsValid)
            {
                var attachment = model.Document.ConvertToAttachment();
                var path = attachment.Path;
                var item = Service.GetById(Id);
                var currentattachment = item.Document.ConvertToAttachment();
                string currentpath = null;
                if (currentattachment != null)
                {
                    currentpath = currentattachment.Path;
                }

                if (currentpath != path)
                {
                    if (!string.IsNullOrWhiteSpace(model.Document))
                    {
                        var Validateexcel = _excelHelper.Validateexcel(path);

                        if (Validateexcel != "Ok")
                        {
                            var selector = new List<LevelCategorySelectorViewModel>();
                            var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
                            var values = levelChoice.Split(',').ToList();

                            foreach (var items in values)
                            {
                                selector.Add(new LevelCategorySelectorViewModel()
                                {
                                    Id = items,
                                    Name = items
                                });
                            }
                            var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                            model.AkiCover.SourceLevelList = selectSource.ToList();

                            var selectorRoi = new List<ProyeksiRoiViewModel>();
                            var roiChoice = _config.GetConfig(ROITYPE_CONFIG_NAME);
                            var valuesRoi = roiChoice.Split(',').ToList();

                            foreach (var itemRoi in valuesRoi)
                            {
                                selectorRoi.Add(new ProyeksiRoiViewModel()
                                {
                                    Id = itemRoi,
                                    Name = itemRoi
                                });
                            }
                            var RoisSource = selectorRoi.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                            ViewBag.Items = RoisSource;
                            ViewBag.massageexcel = Validateexcel;
                            return View(model);
                        }

                        var newPath = $"{_akiDirectori}/{CurrentUser.Id}/{item.Id}";

                        attachment.Path = _fileHelper.FileMove(attachment.Path,
                               newPath,
                               model.AkiName);

                        item.Document = attachment.ConvertToString();

                        _excelHelper.UpdateExcelAkivaluation(attachment.Path, Id);

                    }
                }

                if (!string.IsNullOrWhiteSpace(model.DocumentNJKI) && model.DocumentNJKI != item.CustomField1)
                {
                    var attachmentNJKI = model.DocumentNJKI.ConvertToAttachment();

                    if (attachmentNJKI != null)
                    {
                        var newPath = $"{_akiDirectori}/{CurrentUser.Id}/{item.Id}";

                        attachmentNJKI.Path = _fileHelper.FileMove(attachmentNJKI.Path,
                               newPath,
                               model.AkiName + "NJKI");
                        item.CustomField1 = attachmentNJKI.ConvertToString();

                    }
                }

                item.LastUpdateTime = DateTime.Now;

                item.LastEditedBy = CurrentUser.Id;

                item.UpdateNJKI(model);

                Service.Update(item);
                model.AkiCover.PenanggungJawab = model.UnitBisnis;

                if (model.UnitKerja != null)
                {
                    model.AkiCover.PenanggungJawab= model.UnitKerja;
                }


                UpdateCover(Id, model.AkiCover);

                return RedirectToAction("CreateValuation", new { id = item.Id });
            }
            else
            {
                model.AkiCover = new AkiCoverForm();
                var selector = new List<LevelCategorySelectorViewModel>();
                var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
                var values = levelChoice.Split(',').ToList();

                foreach (var items in values)
                {
                    selector.Add(new LevelCategorySelectorViewModel()
                    {
                        Id = items,
                        Name = items
                    });
                }
                var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
                model.AkiCover.SourceLevelList = selectSource.ToList();
                var selectorRoi = new List<ProyeksiRoiViewModel>();
                var roiChoice = _config.GetConfig(ROITYPE_CONFIG_NAME);
                var valuesRoi = roiChoice.Split(',').ToList();

                foreach (var itemRoi in valuesRoi)
                {
                    selectorRoi.Add(new ProyeksiRoiViewModel()
                    {
                        Id = itemRoi,
                        Name = itemRoi
                    });
                }
                var RoisSource = selectorRoi.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                ViewBag.Items = RoisSource;
                ViewBag.AkiTittle = model.AkiName;
                ViewBag.AkiNilaiProgram = model.AkiCover.NilaiProgram;
                ViewBag.penanggungJawab = model.AkiCover.PenanggungJawab;
                return View(model);
            }
        }


        public override IActionResult Create()
        {
            return RedirectToAction("CreateAKIHeader");
        }



        [HttpPost]
        public override IActionResult Create(AkiForm model)
        {
            Aki item = Mapper.Map<Aki>(model);
            string pathNJKI = null;
            if (ModelState.IsValid)
            {

                List<AkiValuation> resourceValuation = new List<AkiValuation>();
                if (!string.IsNullOrWhiteSpace(model.Document))
                {

                    var attachment = model.Document.ConvertToAttachment();
                    var Validateexcel = _excelHelper.Validateexcel(attachment.Path);

                    if (Validateexcel != "Ok")
                    {
                        var selector = new List<LevelCategorySelectorViewModel>();
                        var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
                        var values = levelChoice.Split(',').ToList();

                        foreach (var items in values)
                        {
                            selector.Add(new LevelCategorySelectorViewModel()
                            {
                                Id = items,
                                Name = items
                            });
                        }
                        var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
                        model.AkiCover.SourceLevelList = selectSource.ToList();
                        var selectorRoi = new List<ProyeksiRoiViewModel>();
                        var roiChoice = _config.GetConfig(ROITYPE_CONFIG_NAME);
                        var valuesRoi = roiChoice.Split(',').ToList();

                        foreach (var itemRoi in valuesRoi)
                        {
                            selectorRoi.Add(new ProyeksiRoiViewModel()
                            {
                                Id = itemRoi,
                                Name = itemRoi
                            });
                        }
                        var RoisSource = selectorRoi.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                        ViewBag.Items = RoisSource;
                        ViewBag.massageexcel = Validateexcel;
                        return View(model);
                    }

                    var newPath = $"{_akiDirectori}/{CurrentUser.Id}/{item.Id}";

                    attachment.Path = _fileHelper.FileMove(attachment.Path,
                           newPath,
                           model.AkiName);

                    item.Document = attachment.ConvertToString();

                    resourceValuation = _excelHelper.GetExcelAkivaluation(attachment.Path);

                }
                if (!string.IsNullOrWhiteSpace(model.DocumentNJKI))
                {
                    var attachment = model.DocumentNJKI.ConvertToAttachment();
                    var newPath = $"{_akiDirectori}/{CurrentUser.Id}/{item.Id}";

                    attachment.Path = _fileHelper.FileMove(attachment.Path,
                           newPath,
                           model.AkiName + "NJKI");

                    item.CustomField1 = attachment.ConvertToString();
                    pathNJKI = attachment.ConvertToString();
                }

                // item.CustomField1 = pathNJKI;
                item.CreatedAt = DateTime.Now;
                item.CreatedBy = CurrentUser.Id;
                var unitid = UserHelper.GetUserUnit(CurrentUser.Id);
                item.OtherInfo = unitid;
                Service.Add(item);

                if (model.AkiCover.Rois != null)
                {
                    model.AkiCover.Roi = JsonConvert.SerializeObject(model.AkiCover.Rois);
                }

                AkiCover akiCover = Mapper.Map<AkiCover>(model.AkiCover);

                akiCover.AkiId = item.Id;
                akiCover.CreatedAt = DateTime.Now;
                akiCover.CreatedBy = CurrentUser.Id;
                akiCover.KategoriInvestasi = model.AkiCover.InvestmentCategory;
                akiCover.Location = model.AkiCover.Lokasi;
                akiCover.ProposalId = model.AkiCover.IdProposal;
                akiCover.LopId = model.AkiCover.IdLop;
                akiCover.CustomField1 = model.AkiCover.PenanggungJawabvalue;
                akiCover.CustomField2 = model.AkiCover.LevelValue;
                akiCover.NomorAkun = model.AkiCover.NomorAccount;

                _akiCoverService.Add(akiCover);

                if (resourceValuation.Any())
                {
                    foreach (var valuation in resourceValuation)
                    {
                        valuation.AkiId = item.Id;
                        _akiValuationService.Add(valuation);
                    }
                }

                return RedirectToAction("Index");
            }
            else
            {
                model.AkiCover = new AkiCoverForm();
                var selector = new List<LevelCategorySelectorViewModel>();
                var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
                var values = levelChoice.Split(',').ToList();

                foreach (var items in values)
                {
                    selector.Add(new LevelCategorySelectorViewModel()
                    {
                        Id = items,
                        Name = items
                    });
                }
                var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                model.AkiCover.SourceLevelList = selectSource.ToList();

                var selectorRoi = new List<ProyeksiRoiViewModel>();
                var roiChoice = _config.GetConfig(ROITYPE_CONFIG_NAME);
                var valuesRoi = roiChoice.Split(',').ToList();

                foreach (var itemRoi in valuesRoi)
                {
                    selectorRoi.Add(new ProyeksiRoiViewModel()
                    {
                        Id = itemRoi,
                        Name = itemRoi
                    });
                }
                var RoisSource = selectorRoi.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                ViewBag.Items = RoisSource;
                //model.AkiCover.SourcePenanggungjawab = _unitservice.GetSelectListItem();
                return View(model);
            }

        }

        public override IActionResult Edit(Guid Id)
        {
            var data = Service.GetById(Id);
            if (data.CustomField2 == Aki.STATUS_SUBMITTED || data.CustomField2 == Aki.STATUS_REJECTED)
            {
                return RedirectToAction("Details", new { id = Id });
            }
            else if (data.CustomField2 == Aki.STATUS_DRAFT)
            {
                return RedirectToAction("CreateValuation", new { id = Id });
            }
            else
            {
                return RedirectToAction("EditAKIHeader", new { id = Id });
            }


        }


        [HttpPost]
        public override IActionResult Edit(Guid Id, AkiForm model)
        {
            if (ModelState.IsValid)
            {
                var attachment = model.Document.ConvertToAttachment();
                var path = attachment.Path;
                var item = Service.GetById(Id);
                var currentattachment = item.Document.ConvertToAttachment();
                var currentpath = currentattachment.Path;
                if (currentpath != path)
                {
                    if (!string.IsNullOrWhiteSpace(model.Document))
                    {
                        var Validateexcel = _excelHelper.Validateexcel(path);

                        if (Validateexcel != "Ok")
                        {
                            var selector = new List<LevelCategorySelectorViewModel>();
                            var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
                            var values = levelChoice.Split(',').ToList();

                            foreach (var items in values)
                            {
                                selector.Add(new LevelCategorySelectorViewModel()
                                {
                                    Id = items,
                                    Name = items
                                });
                            }
                            var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                            model.AkiCover.SourceLevelList = selectSource.ToList();

                            var selectorRoi = new List<ProyeksiRoiViewModel>();
                            var roiChoice = _config.GetConfig(ROITYPE_CONFIG_NAME);
                            var valuesRoi = roiChoice.Split(',').ToList();

                            foreach (var itemRoi in valuesRoi)
                            {
                                selectorRoi.Add(new ProyeksiRoiViewModel()
                                {
                                    Id = itemRoi,
                                    Name = itemRoi
                                });
                            }
                            var RoisSource = selectorRoi.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                            ViewBag.Items = RoisSource;
                            ViewBag.massageexcel = Validateexcel;
                            return View(model);
                        }

                        var newPath = $"{_akiDirectori}/{CurrentUser.Id}/{item.Id}";

                        attachment.Path = _fileHelper.FileMove(attachment.Path,
                               newPath,
                               model.AkiName);

                        model.Document = attachment.ConvertToString();

                        _excelHelper.UpdateExcelAkivaluation(attachment.Path, Id);

                    }
                }

                if (!string.IsNullOrWhiteSpace(model.DocumentNJKI) && model.DocumentNJKI != item.CustomField1)
                {
                    var attachmentNJKI = model.DocumentNJKI.ConvertToAttachment();

                    if (attachmentNJKI != null)
                    {
                        var newPath = $"{_akiDirectori}/{CurrentUser.Id}/{item.Id}";

                        attachmentNJKI.Path = _fileHelper.FileMove(attachmentNJKI.Path,
                               newPath,
                               model.AkiName + "NJKI");
                        item.CustomField1 = attachmentNJKI.ConvertToString();

                    }
                }

                item.LastUpdateTime = DateTime.Now;

                item.LastEditedBy = CurrentUser.Id;

                item.Update(model);

                Service.Update(item);

                UpdateCover(Id, model.AkiCover);

                return RedirectToAction("Index");
            }
            else
            {
                model.AkiCover = new AkiCoverForm();
                var selector = new List<LevelCategorySelectorViewModel>();
                var levelChoice = _config.GetConfig(LEVEL_CONFIG_NAME);
                var values = levelChoice.Split(',').ToList();

                foreach (var items in values)
                {
                    selector.Add(new LevelCategorySelectorViewModel()
                    {
                        Id = items,
                        Name = items
                    });
                }
                var selectSource = selector.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
                model.AkiCover.SourceLevelList = selectSource.ToList();
                var selectorRoi = new List<ProyeksiRoiViewModel>();
                var roiChoice = _config.GetConfig(ROITYPE_CONFIG_NAME);
                var valuesRoi = roiChoice.Split(',').ToList();

                foreach (var itemRoi in valuesRoi)
                {
                    selectorRoi.Add(new ProyeksiRoiViewModel()
                    {
                        Id = itemRoi,
                        Name = itemRoi
                    });
                }
                var RoisSource = selectorRoi.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() });

                ViewBag.Items = RoisSource;
                return View(model);
            }
        }
        public IActionResult ViewExcel(Guid Id)
        {
            var aki = Service.GetById(Id);

            if (aki == null)
            {
                return NotFound();
            }

            var model = Mapper.Map<AkiForm>(aki);

            var attachment = model.Document.ConvertToAttachment();

            ViewBag.Attachment = attachment.Path;

            return View();
        }
        public IActionResult ViewPdf(Guid Id)
        {
            var aki = Service.GetById(Id);
            ViewBag.Attachment = null;
            if (aki == null)
            {
                return NotFound();
            }
            string attachment = null;
            ViewBag.Attachment = null;
            var model = Mapper.Map<AkiForm>(aki);
            model.DocumentNJKI = aki.CustomField1;
            if (!string.IsNullOrWhiteSpace(model.DocumentNJKI))
            {
                attachment = model.DocumentNJKI;
                ViewBag.Attachment = attachment.ConvertToAttachment().Path;

            }

            return View();
        }

        public void UpdateCover(Guid Id, AkiCoverForm akiForm)
        {
            var akiCover = _akiCoverService.GetAkiCoverByIdAki(Id);

            if (akiCover.CustomField1 != akiForm.PenanggungJawabvalue)
            {
                akiCover.CustomField1 = akiForm.PenanggungJawabvalue;
            }
            if (akiCover.CustomField2 != akiForm.LevelValue)
            {
                akiCover.CustomField2 = akiForm.LevelValue;
            }
            akiCover.LastEditedBy = CurrentUser.Id;
            akiCover.LastUpdateTime = DateTime.Now;

            if (akiForm.Rois != null && akiCover.Roi == null)
            {
                akiCover.Roi = JsonConvert.SerializeObject(akiForm.Rois);
            }

            if (akiCover.Roi != null)
            {
                var oldRoi = JsonConvert.DeserializeObject<List<RoiViewModel>>(akiCover.Roi);

                var newRoi = new List<RoiViewModel>();


                if (akiForm.Rois != null)
                {
                    newRoi = akiForm.Rois.ToList();
                }

                if (akiForm.Rois != null && oldRoi != akiForm.Rois)
                {
                    var toDelete = new List<RoiViewModel>();
                    var toAdd = new List<RoiViewModel>();

                    foreach (var attch in newRoi)
                    {
                        if (!oldRoi.Contains(attch))
                        {
                            toAdd.Add(attch);
                        }
                    }

                    foreach (var attch in oldRoi)
                    {
                        if (!newRoi.Contains(attch))
                        {
                            toDelete.Add(attch);
                        }
                    }

                    akiCover.Roi = JsonConvert.SerializeObject(newRoi);
                }

            }

            else
            {
                akiCover.Roi = null;
            }

            akiCover.Update(akiForm);

            _akiCoverService.Update(akiCover);

        }
    }
}
