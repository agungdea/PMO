﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using App.Domain.Models.Identity;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
   [Authorize(Roles = ApplicationRole.Administrator)]
    public class UnitController : BaseController<Unit, IUnitService, UnitViewModel, UnitForm, string>
    {
        public UnitController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IUnitService service,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        protected override void UpdateData(Unit item, UnitForm model)
        {
            item.NamaUnit = model.NamaUnit;
            item.NamaSingkat = model.NamaSingkat;
            item.ParentId = model.ParentId;
            item.STLevel = model.STLevel;
        }
    }
}
