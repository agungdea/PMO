﻿using System;
using Microsoft.AspNetCore.Mvc;
using App.Domain.Models.App;
using App.Infrastructure;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    public class WBSScheduleController : BaseController<WBSSchedule, IWBSScheduleService, WBSViewModel, WBSForm, Guid>
    {
        public WBSScheduleController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, IMapper mapper,
            IWBSScheduleService service,
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
    }
}
