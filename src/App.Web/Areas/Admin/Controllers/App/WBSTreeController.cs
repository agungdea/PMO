﻿using App.Domain.Models.App;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    public class WBSTreeController : BaseController<WBSTree, IWBSTreeService, WBSTreeViewModel, WBSTreeForm, Guid>
    {
        public WBSTreeController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IWBSTreeService service,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        public IActionResult CreateWBS(Guid idWbs, Guid? parent)
        {
            var model = new WBSTreeForm
            {
                IdWBS = idWbs,
                ParentId = parent,
                Type = WBS.WBS_TYPE_WBS
            };

            return View("Create", model);
        }

        public IActionResult CreateLokasi(Guid idWbs, Guid? parent)
        {
            var model = new WBSTreeForm
            {
                IdWBS = idWbs,
                ParentId = parent,
                Type = WBS.WBS_TYPE_LOCATION
            };

            return View("CreateLokasi", model);
        }

        [HttpPost]
        public override IActionResult Create(WBSTreeForm model)
        {
            if (ModelState.IsValid)
            {
                var item = Mapper.Map<WBSTree>(model);

                if (item.ParentId.HasValue)
                {
                    if (item.Type == WBS.WBS_TYPE_LOCATION)
                    {
                        UpdateParent(item.ParentId.Value, WBS.WBS_TYPE_WP);
                    }
                    else if (item.Type == WBS.WBS_TYPE_WBS)
                    {
                        UpdateParent(item.ParentId.Value, WBS.WBS_TYPE_WBS_HAS_WBS);
                    }
                }

                item.CreatedAt = DateTime.Now;
                item.CreatedBy = CurrentUser.Id;

                Service.Add(item);

                return Ok(model);
            }

            return BadRequest(model);
        }

        public IActionResult EditWBS(Guid id)
        {
            var wbs = Service.GetById(id);

            if (wbs == null)
                return NotFound();

            ViewBag.Id = id;

            var model = Mapper.Map<WBSTreeForm>(wbs);

            return View("Edit", model);
        }

        public IActionResult EditLokasi(Guid id)
        {
            var lokasi = Service.GetById(id);

            if (lokasi == null)
                return NotFound();

            ViewBag.Id = id;

            var model = Mapper.Map<WBSTreeForm>(lokasi);

            model.Regional = lokasi.AreaMapping?.AreaRegional;
            model.Witel = lokasi.AreaMapping?.AreaWitel;
            model.Datel = lokasi.AreaMapping?.AreaDatel;
            model.NamaLokasi = $"{lokasi.AreaMapping?.AreaStoName} ({lokasi.AreaMapping?.AreaSto})";

            if (lokasi.GrupHarga != null)
            {
                model.HasKHS = true;
            }

            return View("EditLokasi", model);
        }

        [HttpPost]
        public override IActionResult Edit(Guid id, WBSTreeForm model)
        {
            if (ModelState.IsValid)
            {
                var item = Service.GetById(id);

                if (item == null)
                {
                    return NotFound();
                }

                item.Name = model.Name;
                item.Lokasi = model.Lokasi;
                item.GrupHarga = model.GrupHarga;
                item.LastUpdateTime = DateTime.Now;
                item.LastEditedBy = CurrentUser.Id;

                var result = Service.Update(item);

                return Ok(model);
            }

            return BadRequest(model);
        }

        private void UpdateParent(Guid parentId, string type)
        {
            var item = Service.GetById(parentId);
            item.Type = type;
            Service.Update(item);
            if (item.ParentId.HasValue)
            {
                if (item.Type == WBS.WBS_TYPE_WP)
                {
                    type = WBS.WBS_TYPE_WBS_HAS_LOCATION;
                }

                UpdateParent(item.ParentId.Value, type);
            }
        }
    }
}
