﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class EventTemplateController : BaseController<ActTmpChild, IActivityTemplateChildService, EventTemplateViewModel, EventTemplateForm, Guid>
    {
        public EventTemplateController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IActivityTemplateChildService service,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        public IActionResult ListEvent(Guid Id)
        {
            ViewBag.IdActivity = Id;

            return View();
        }

        public override IActionResult Details(Guid Id)
        {

            var activity = Service.GetById(Id);

            if (activity == null)
            {
                return NotFound();
            }

            var model = new EventTemplateViewModel();

            if (activity.ISLEAF == "1")
            {
                model.ISLEAF = true;
            }
            else
            {
                model.ISLEAF = false;
            }

            model = Mapper.Map<EventTemplateViewModel>(activity);

            ViewBag.ActivityParentName = Service.GetAll().Where(x => x.ActId == activity.ActParent).Select(x => x.NamaAct).FirstOrDefault();

            return View(model);
        }

        public override IActionResult Edit(Guid Id)
        {

            var activity = Service.GetById(Id);

            if (activity == null)
            {
                return NotFound();
            }

            var model = new EventTemplateForm();

            if (activity.ISLEAF == "1")
            {
                model.ISLEAF = true;
            }
            else
            {
                model.ISLEAF = false;
            }

            model = Mapper.Map<EventTemplateForm>(activity);

            ViewBag.ActivityParentName = Service.GetAll().Where(x => x.ActId == activity.ActParent).Select(x => x.NamaAct).FirstOrDefault();

            return View(model);
        }

        [HttpPost]
        public override IActionResult Edit(Guid Id, EventTemplateForm model)
        {
            var activity = Service.GetById(Id);

            if (activity == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                activity.Update(model);
                activity.LastEditedBy = CurrentUser.Id;

                Service.Update(activity);

                return RedirectToAction("Details", new { Id = Id });
            }

            return View(model);
        }


        [HttpPost]
        public override IActionResult Create(EventTemplateForm model)
        {
            if (ModelState.IsValid)
            {
                var activityId = Service.GetActivityId(model.NamaAct);

                if (activityId == 0)
                {
                    model.ActId = Service.GenerateActivityId();
                }
                else
                {
                    model.ActId = activityId;
                }

                var result = Mapper.Map<ActTmpChild>(model);

                result.CreatedAt = DateTime.Now;
                result.CreatedBy = CurrentUser.Id;

                Service.Add(result);

                return RedirectToAction("ListEvent", new { id = model.IdActTmp });
            }

            return View(model);
        }
    }
}
