﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Services.Workflow;
using App.Web.Helper;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace App.Web.Areas.Admin.Controllers.App
{

    [Area("Admin")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class JustificationController
        : BaseController<Justification, IJustificationService, JustificationViewModel, JustificationHeaderForm, Guid>
    {
        private readonly IWfProcessService _wfProcessService;
        private readonly ConfigHelper _config;
        private readonly LocalizationHelper _localization;
        private readonly IAkiService _akiService;
        private readonly IWBSService _wbsService;
        private readonly FileHelper _fileHelper;
        private readonly string _justificationDir;
        private readonly PdfGenerator _pdfHelper;
        private readonly IWBSBoQService _wbsBoQService;
        private readonly IWBSBoQGrantTotalService _wbsBoQGrantTotalService;
        private readonly string _webBaseUrl;
        private readonly IHostingEnvironment _env;

        public JustificationController(IHttpContextAccessor httpContextAccessor,
            IWBSBoQGrantTotalService wbsBoQGrantTotalService,
            IWBSBoQService wbsBoQService,
            IUserService userService,
            IHostingEnvironment env,
            IMapper mapper,
            IJustificationService service,
            IWfProcessService wfProcessService,
            IUserHelper userHelper,
            IAkiService akiService,
            IWBSService wbsService,
            FileHelper fileHelper,
            ConfigHelper config,
            PdfGenerator pdfHelper,
            LocalizationHelper localization)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _wbsBoQGrantTotalService = wbsBoQGrantTotalService;
            _wbsBoQService = wbsBoQService;
            _wfProcessService = wfProcessService;
            _fileHelper = fileHelper;
            _config = config;
            _localization = localization;
            _akiService = akiService;
            _wbsService = wbsService;
            _justificationDir = _config.GetConfig("justification.upload.directory");
            _pdfHelper = pdfHelper;
            _webBaseUrl = _config.GetConfig("web.base.url");
            _env = env;
        }

        public override IActionResult Details(Guid id)
        {
            return RedirectToAction("JDetails", new { id });
        }

        public IActionResult JDetails(Guid id)
        {
            var justification = Service.GetById(id);

            if (justification == null)
                return NotFound();

            var model = Mapper.Map<JustificationViewModel>(justification);


            if (model.AKIId != null)
            {
                var aki = _akiService.GetById(model.AKIId);
                model.AkiName = aki.AkiName;
            }

            if (model.WBSId != null)
            {
                var kegiatan = _wbsService.GetById(model.WBSId);
                model.NamaKegiatan = kegiatan.NamaKegiatan;
                model.ListBoq = _wbsBoQService.GetBoqPerWbs((Guid)model.WBSId);
                model.ListLocation = _wbsBoQGrantTotalService.GetWBsPerlocation((Guid)model.WBSId);
            }

            return View("Details", model);
        }

        public IActionResult Download(Guid Id)
        {
            var justification = Service.GetById(Id);

            if (justification == null)
            {
                return null;
            }

            var documentPath = _env.WebRootPath + "/" + justification.CustomField3;

            var fileName = Path.GetFileName(justification.CustomField3);
            var filepath = documentPath;
            byte[] fileBytes = System.IO.File.ReadAllBytes(filepath);

            return File(fileBytes, "application/x-msdownload", fileName);
        }


        [HttpGet]
        public IActionResult GenerateReport(Guid Id)
        {
            var tempjustification = Service.GetById(Id);

            if (tempjustification == null)
            {
                return BadRequest();
            }

            if (string.IsNullOrWhiteSpace(tempjustification.CustomField3))
            {
                var newPath = $"{_justificationDir}/{Id}/";

                var urls = _webBaseUrl + "/Admin/Justification/GetHtml/" + tempjustification.Id;

                var data = _pdfHelper.HtmlToPdf(Id.ToString(), urls);

                if (!string.IsNullOrWhiteSpace(data))
                {
                    urls = _fileHelper.FileMove(data,
                            newPath + "PDF Report",
                            Id.ToString());

                    tempjustification.CustomField3 = urls;

                    Service.Update(tempjustification);
                }
            }

            return Download(Id);
        }

        public override IActionResult Edit(Guid id)
        {
            return RedirectToAction("CreateHeader", new { id = id });
        }

        [HttpPost]
        public override IActionResult Edit(Guid id, JustificationHeaderForm model)
        {
            return NotFound();
        }

        public override IActionResult Create()
        {
            var model = new Justification();

            return View(Mapper.Map<JustificationHeaderForm>(model));
        }

        public IActionResult CreateHeader(Guid id)
        {
            var model = Service.GetById(id);

            if (model == null)
                return NotFound();

            if (!model.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { id });

            if (model.AKIId != null)
            {
                var aki = _akiService.GetById(model.AKIId);

                ViewBag.AKIName = aki.AkiName;
            }

            if (model.WBSId != null)
            {
                var kegiatan = _wbsService.GetById(model.WBSId);
                ViewBag.KegiatanName = null;
                if (kegiatan != null)
                {
                    ViewBag.KegiatanName = kegiatan.NamaKegiatan;
                }


            }

            return View("Create", Mapper.Map<JustificationHeaderForm>(model));
        }

        [HttpPost]
        public override IActionResult Create(JustificationHeaderForm model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var justification = Service.GetById(model.Id);

            if (justification == null)
            {
                justification = new Justification
                {
                    CreatedAt = DateTime.Now,
                    CreatedBy = CurrentUser.Id,
                    OtherInfo = UserHelper.GetUserUnit(CurrentUser.Id),
                    CustomField1 = Justification.STATUS_SAP_BEFORE_SUBMIT
                };

                justification.Update(model);

                Service.Add(justification);
            }
            else
            {
                if (!justification.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                    return RedirectToAction("Details", new { justification.Id });

                justification.Update(model);

                Service.Update(justification);
            }

            if (string.IsNullOrWhiteSpace(model.RedirectUrl))
                return RedirectToAction("CreateUraian", new { id = justification.Id });

            return LocalRedirect(model.RedirectUrl);
        }

        public IActionResult CreateUraian(Guid id)
        {
            var model = Service.GetById(id);

            if (model == null)
                return NotFound();

            if (!model.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { id });

            ViewBag.Id = id;

            return View(Mapper.Map<JustificationUraianForm>(model));
        }

        [HttpPost]
        public IActionResult CreateUraian(Guid id, JustificationUraianForm model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Id = id;

                return View(model);
            }

            var justification = Service.GetById(id);

            if (!justification.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { justification.Id });

            justification.LastEditedBy = CurrentUser.Id;
            justification.LastUpdateTime = DateTime.Now;

            justification.Update(model);

            Service.Update(justification);

            if (string.IsNullOrWhiteSpace(model.RedirectUrl))
                return RedirectToAction("CreateJumlahKebutuhan", new { id = justification.Id });

            return LocalRedirect(model.RedirectUrl);
        }

        public IActionResult CreateJumlahKebutuhan(Guid id)
        {
            var model = Service.GetById(id);

            if (model == null)
                return NotFound();

            if (!model.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { id });

            ViewBag.Id = id;

            return View(Mapper.Map<JustificationJumlahKebutuhanForm>(model));
        }

        [HttpPost]
        public IActionResult CreateJumlahKebutuhan(Guid id, JustificationJumlahKebutuhanForm model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Id = id;

                return View(model);
            }

            var newPath = $"{_justificationDir}/{id}/";

            var justification = Service.GetById(id);

            if (!justification.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { justification.Id });

            if (!string.IsNullOrWhiteSpace(model.BasicDesign) && (model.BasicDesign != justification.BasicDesign))
            {
                var attachment = model.BasicDesign.ConvertToAttachment();

                attachment.Path = _fileHelper.FileMove(attachment.Path,
                newPath + "Basic Design",
                id.ToString());
                model.BasicDesign = attachment.ConvertToString();
            }

            if (!string.IsNullOrWhiteSpace(model.TOR) && (model.TOR != justification.TOR))
            {
                var attachment = model.TOR.ConvertToAttachment();

                attachment.Path = _fileHelper.FileMove(attachment.Path,
                newPath + "TOR",
                id.ToString());
                model.TOR = attachment.ConvertToString();
            }

            if (!string.IsNullOrWhiteSpace(model.LampiranDasarKebutuhanBarangJasa) && (model.LampiranDasarKebutuhanBarangJasa != justification.LampiranDasarKebutuhanBarangJasa))
            {
                var attachment = model.LampiranDasarKebutuhanBarangJasa.ConvertToAttachment();

                attachment.Path = _fileHelper.FileMove(attachment.Path,
                newPath + "Lampiran",
                id.ToString());
                model.LampiranDasarKebutuhanBarangJasa = attachment.ConvertToString();
            }

            justification.LastEditedBy = CurrentUser.Id;
            justification.LastUpdateTime = DateTime.Now;

            justification.Update(model);

            Service.Update(justification);

            if (string.IsNullOrWhiteSpace(model.RedirectUrl))
                return RedirectToAction("CreateRencanaPelaksanaan", new { id = justification.Id });

            return LocalRedirect(model.RedirectUrl);
        }

        public IActionResult CreateRencanaPelaksanaan(Guid id)
        {
            var model = Service.GetById(id);

            if (model == null)
                return NotFound();

            if (!model.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { id });

            ViewBag.Id = id;

            return View(Mapper.Map<JustificationRencanaPelaksanaanForm>(model));
        }

        [HttpPost]
        public IActionResult CreateRencanaPelaksanaan(Guid id, JustificationRencanaPelaksanaanForm model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Id = id;

                return View(model);
            }

            var justification = Service.GetById(id);

            if (!justification.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { justification.Id });

            justification.LastEditedBy = CurrentUser.Id;
            justification.LastUpdateTime = DateTime.Now;

            justification.Update(model);

            Service.Update(justification);

            if (string.IsNullOrWhiteSpace(model.RedirectUrl))
                return RedirectToAction("CreateLainLain", new { id = justification.Id });

            return LocalRedirect(model.RedirectUrl);
        }

        public IActionResult CreateLainLain(Guid id)
        {
            var model = Service.GetById(id);

            if (model == null)
                return NotFound();

            if (!model.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { id });

            ViewBag.Id = id;

            return View(Mapper.Map<JustificationLainLainForm>(model));
        }

        [HttpPost]
        public IActionResult CreateLainLain(Guid id, JustificationLainLainForm model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Id = id;

                return View(model);
            }

            var justification = Service.GetById(id);

            if (!justification.IsJustificationEditableAndSubmitable(CurrentUser.Id))
                return RedirectToAction("Details", new { justification.Id });

            justification.LastEditedBy = CurrentUser.Id;
            justification.LastUpdateTime = DateTime.Now;

            justification.Update(model);

            Service.Update(justification);

            if (string.IsNullOrWhiteSpace(model.RedirectUrl))
                return RedirectToAction("Details", new { id = justification.Id });

            return LocalRedirect(model.RedirectUrl);
        }

        [HttpPost]
        public async Task<IActionResult> Submit(Guid id)
        {
            if (Service.GetById(id) == null)
                return NotFound();

            var callbackUrl = Url.Action("Details",
                       "Justification",
                       new { id = id, area = "Admin" },
                       _config.GetConfig("host.protocol"),
                       _config.GetConfig("host.name"));

            var justification = await Service.Submit(id, callbackUrl);

            if (justification.JustificationStatus != Justification.STATUS_SUBMITTED)
            {
                TempData["alert"] = _localization.GetLocalized("justification.submit.text.failed");
            }

            return RedirectToAction("Details", new { id });
        }

        [HttpPost]
        public async Task<IActionResult> Approval(Guid id, JustificationApprovalForm model)
        {
            if (Service.GetById(id) == null)
                return NotFound();

            var param = new Dictionary<string, string>();

            param.Add("var_status", model.Status);
            param.Add("var_note", model.Note);

            var justification = await Service.Approve(id, param);

            if (justification.JustificationStatus == Justification.STATUS_APPROVED)
            {
                // do final approval thing
            }

            return RedirectToAction("Details", new { id });
        }

        [HttpPost]
        public IActionResult NeedReview(Guid id, JustificationApprovalForm model)
        {
            var justification = Service.GetById(id);

            if (justification == null)
                return NotFound();

            justification.JustificationStatus = Justification.STATUS_NEED_REVIEW;

            Service.Update(justification);

            return RedirectToAction("Index");
        }


        public List<JustificationPdfDto> GetContent(Guid id)
        {
            var justification = Service.GetById(id);

            List<JustificationPdfDto> listModel = new List<JustificationPdfDto>();

            var temp = new JustificationPdfDto();

            if (justification.LatarBelakang != null)
            {
                temp.Header = JustificationPdfDto.LATAR_BELAKANG;
                temp.Content = Extension.StripHTML(justification.LatarBelakang);

                listModel.Add(temp);
            }

            if (justification.AspekStrategis != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.ASPEK_STRATEGIS;
                temp.Content = Extension.StripHTML(justification.AspekStrategis);

                listModel.Add(temp);
            }

            if (justification.AspekBisnis != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.ASPEK_BISNIS;
                temp.Content = Extension.StripHTML(justification.AspekBisnis);

                listModel.Add(temp);
            }

            if (justification.SpesifikasiTeknik != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.SPESIFIKASI_TEKNIK;
                temp.Content = Extension.StripHTML(justification.SpesifikasiTeknik);

                listModel.Add(temp);
            }

            if (justification.JumlahKebutuhanBarangJasa != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.JUMLAH_KEBUTUHAN;
                temp.Content = Extension.StripHTML(justification.JumlahKebutuhanBarangJasa);

                listModel.Add(temp);
            }

            if (justification.RencanaPelaksanaan != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.RENCANA_PELAKSANAAN;
                temp.Content = Extension.StripHTML(justification.RencanaPelaksanaan);

                listModel.Add(temp);
            }

            if (justification.DistribusiPenggunaan != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.DISTRIBUSI_PENGGUNAAN;
                temp.Content = Extension.StripHTML(justification.DistribusiPenggunaan);

                listModel.Add(temp);
            }

            if (justification.PosisiPersediaan != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.POSISI_PERSEDIAAN;
                temp.Content = Extension.StripHTML(justification.PosisiPersediaan);

                listModel.Add(temp);
            }

            if (justification.Anggaran != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.ANGGARAN;
                temp.Content = Extension.StripHTML(justification.Anggaran);

                listModel.Add(temp);
            }

            if (justification.Penutup != null)
            {
                temp = new JustificationPdfDto();

                temp.Header = JustificationPdfDto.PENUTUP;
                temp.Content = Extension.StripHTML(justification.Penutup);

                listModel.Add(temp);
            }

            return listModel;
        }

        public IActionResult GetHtml(Guid id)
        {
            var justification = Service.GetById(id);

            if (justification == null)
                return NotFound();

            var model = Mapper.Map<JustificationViewModel>(justification);


            if (model.AKIId != null)
            {
                var aki = _akiService.GetById(model.AKIId);
                model.AkiName = aki.AkiName;
            }

            if (model.WBSId != null)
            {
                var kegiatan = _wbsService.GetById(model.WBSId);
                model.NamaKegiatan = kegiatan.NamaKegiatan;
                model.ListBoq = _wbsBoQService.GetBoqPerWbs((Guid)model.WBSId);
                model.ListLocation = _wbsBoQGrantTotalService.GetWBsPerlocation((Guid)model.WBSId);
            }

            return View("GetHtml", model);
        }


        public IActionResult GetAttachment(string path)
        {

            var documentPath = _env.WebRootPath + "/" + path;

            var fileName = Path.GetFileName(path);
            var filepath = documentPath;
            byte[] fileBytes = System.IO.File.ReadAllBytes(filepath);

            return File(fileBytes, "application/x-msdownload", fileName);
        }
    }
}
