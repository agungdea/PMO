﻿using App.Domain.Models.App;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    [Authorize(Policy = "ClaimBasedAuthz")]
    public class CategoryController : BaseController<Category, ICategoryService, CategoryViewModel, CategoryForm, int>
    {
        public CategoryController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService,
            IMapper mapper, 
            ICategoryService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
    }
}
