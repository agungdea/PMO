﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using App.Domain.Models.Identity;
using App.Domain.Models.App;
using App.Infrastructure;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using App.Web.Models.ViewModels.Core;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.App
{
    [Area("Admin")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class StoController : BaseController<AreaMapping, IAreaMappingService, AreaMappingViewModel, AreaMappingUpdateForm, Guid>
    {
        private readonly ConfigHelper _config;
        private readonly string _stoDir;
        private readonly string _stoFileName;
        private readonly string _importStoPath;
        private readonly FileHelper _fileHelper;
        private readonly SeedingHelper _seedingHelper;

        public StoController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IAreaMappingService service,
            SeedingHelper seedingHelper,
            ConfigHelper config,
            FileHelper fileHelper,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _config = config;
            _stoDir = _config.GetConfig("sto.upload.directory");
            _stoFileName = _config.GetConfig("sto.file.name");
            _importStoPath = _config.GetConfig("import.data.sto.path");
            _fileHelper = fileHelper;
            _seedingHelper = seedingHelper;
        }


        public override IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public override IActionResult Create(AreaMappingUpdateForm model)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrWhiteSpace(model.Source))
                {
                    var attachment = model.Source.ConvertToAttachment();

                    var newPath = $"{_stoDir}/{_stoFileName}";

                    if (attachment != null)
                    {
                        attachment.Path = _fileHelper.FileMove(attachment.Path,
                        _stoDir,
                        _stoFileName);
                    }

                    if (!string.IsNullOrWhiteSpace(_importStoPath))
                    {
                        _seedingHelper.SeedArea(_importStoPath);

                        return RedirectToAction("Index");
                    }
                }
            }

            return View(model);
        }

    }
}
