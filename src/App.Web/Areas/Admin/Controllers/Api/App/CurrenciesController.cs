﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/currencies")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class CurrenciesController : BaseApiController<Currency, ICurrencyService, CurrencyDto, Guid>
    {
        public CurrenciesController(IHttpContextAccessor httpContextAccessor,
            IUserService userService, 
            IMapper mapper, 
            ICurrencyService service,
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        [Route("code")]
        public IActionResult GetCurrencies()
        {
            return Ok(Service.GetAll().Select(x => new { id = x.CurrencyValue, text = x.CurrencyValue }));
        }        
    }
}
