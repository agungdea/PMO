using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App.Domain.Models.App;
using App.Infrastructure;
using App.Services.App;
using App.Domain.DTO.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using System.Net.Http;
using Newtonsoft.Json;
using App.Web.Models.ViewModels.App;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/transformers")]
    public class TransformersSiController : BaseApiController<TransformerSi, ITranformerSiService, TransformerPeriodDto, string>

    {
        private readonly ConfigHelper _configHelper;

        private readonly string _apiUrlEndPoint;
        public TransformersSiController(IHttpContextAccessor httpContextAccessor, IUserService userService, IMapper mapper, ConfigHelper configHelper, ITranformerSiService service, IUserHelper userHelper) : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _configHelper = configHelper;
            _apiUrlEndPoint = _configHelper.GetConfig("transformer.rest.api.url");
        }

        [HttpPost]
        [Route("getsi")]
        public IActionResult GetSi(string Period, string term)
        {

            string apiEndpoint = _apiUrlEndPoint + "&period=" + Period + "&method=getsi";
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiEndpoint).Result;

            string result = response.Content.ReadAsStringAsync().Result.ToString();

            var Sidynamic = JsonConvert.DeserializeObject<dynamic>(result);

            var temp = Sidynamic.item;
            List<TransformerSiViewModel> item = JsonConvert.DeserializeObject<List<TransformerSiViewModel>>(temp.ToString());
            var dataperiode = from Si in item
                              select new
                              {
                                  id = Si.id,
                                  text = Si.name.ToString() + " - " + Si.abbreviation.ToString()
                              };
            if (term == null)
            {
                return Ok(dataperiode.ToArray());
            }
            else
            {
                return Ok(dataperiode.Where(x => x.text.ToLower().Contains(term.ToLower())).ToArray());
            }
            
        }
    }
}