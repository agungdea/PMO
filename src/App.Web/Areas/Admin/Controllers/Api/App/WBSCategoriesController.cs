﻿using System;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Services.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Linq;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/wbscategories")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class WBSCategoriesController : BaseApiController<WBStreeCategory, IWBStreeCategoryService, WBStreeCategoryDto, Guid>
    {
        public WBSCategoriesController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IWBStreeCategoryService service,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }

        [HttpGet]
        [Route("gettree/{WBSTreeId}")]
        public IActionResult GetTree(Guid WBSTreeId, int? parentid)
        {
            var data = Service.GetChildData(WBSTreeId, parentid);
            return Ok(data);
        }

        [HttpPost]
        [Route("postwbscategory")]
        public IActionResult PostData(Guid WBSId, int categoryId)
        {
            var model = new WBStreeCategory();
            //var result = Service.GetByWBSIdAndCategoryId(WBSId, categoryId);
            var result = Service.GetAll().Where(x => x.WBStreeId.Equals(WBSId));
            if (result != null)
            {
                foreach (var data in result)
                {
                    Service.Delete(data);
                }
                model.CategoryId = categoryId;
                model.WBStreeId = WBSId;
                Service.Add(model);
            }
            if (result == null)
            {

                model.CategoryId = categoryId;
                model.WBStreeId = WBSId;

                Service.Add(model);
            }

            return Ok(model);
        }

        [HttpGet]
        [Route("getwbscategory")]
        public IActionResult GetData(Guid WBSId)
        {
            var result = Service.GetCategoryIdByWBSId(WBSId);


            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }

        [HttpDelete]
        [Route("deletewbscategory")]
        public IActionResult DeleteData(Guid WBSId, int categoryId)
        {
            var result = Service.GetByWBSIdAndCategoryId(WBSId, categoryId);

            if (result == null)
            {
                return BadRequest();
            }

            Service.Delete(result);

            return Ok(result);
        }

        [HttpGet]
        [Route("checkwbscategory")]
        public IActionResult CHeckData(int categoryId)
        {
            var result = Service.CheckExistWBSTreeCategory(categoryId);

            return Ok(result);
        }
        [HttpGet]
        [Route("getcategoriesId")]
        public IActionResult getCategoryId(Guid WBSId)
        {
            var result = Service.getCategoryById(WBSId);

            if (result == null)
            {
                return BadRequest();
            }

            return Ok(result);
        }
    }
}
