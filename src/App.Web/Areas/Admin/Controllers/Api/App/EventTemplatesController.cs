﻿using System;
using Microsoft.AspNetCore.Mvc;
using App.Domain.Models.App;
using App.Services.App;
using App.Domain.DTO.App;
using App.Infrastructure;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using DataTables.AspNet.Core;
using DataTables.AspNet.AspNetCore;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using App.Domain.Models.Identity;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/eventtemplates")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class EventTemplatesController : BaseApiController<ActTmpChild, IActivityTemplateChildService, ActTemplateChildDto, Guid>
    {
        private readonly IWBSScheduleService _wbsscheduleService;
        private readonly IActivityTemplateService _templateService;
        private readonly IWBSScheduleTempService _iWBSScheduleTempService;
        private readonly IWBSScheduleService _iWBSScheduleService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitService _unitService;
        public EventTemplatesController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IActivityTemplateChildService service,
            IWBSScheduleService wbsscheduleService,
            IWBSScheduleTempService iWBSScheduleTempService,
            IActivityTemplateService templateService,
            IWBSScheduleService iWBSScheduleService,
            IUnitService unitService,
            UserManager<ApplicationUser> userManager,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _wbsscheduleService = wbsscheduleService;
            _templateService = templateService;
            _iWBSScheduleService = iWBSScheduleService;
            _iWBSScheduleTempService = iWBSScheduleTempService;
            _unitService = unitService;
            _userManager = userManager;
        }

        [HttpPost]
        [Route("PostDatatables/{id}")]
        public IActionResult PostDataTables(Guid Id, IDataTablesRequest request)
        {
            var getByTemplateId = $"IdActTmp.ToString() ==\"{Id}\"";

            var response = Service.GetDataTablesResponse<ActTemplateChildDto>(request,
               Mapper,
               getByTemplateId);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        [Route("PostDatatablesvalue/{id}")]
        public IActionResult DataTables(Guid Id, Guid Idwbs, IDataTablesRequest request)
        {
            var response = _iWBSScheduleService.GetWBSScheduleTemp(Idwbs, Id, request);

            return new DataTablesJsonResult(response, true);
        }

        [Route("activity")]
        public IActionResult GetActivity(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return BadRequest();
            }
            var grup = Service.QueryActivity(query);

            return Ok(grup);
        }

        [Route("parent")]
        public IActionResult GetParentActivity()
        {
            return Ok(Service.GetAll().Select(x => new { id = x.ActId, text = x.NamaAct }).Distinct());
        }

        [Route("getdatacurvas/{id}")]
        public IActionResult getdata(Guid id)
        {
            var data = _wbsscheduleService.getCurvaS(id);

            return Ok(data.Sum(x => x.Value));
        }



        [Route("getvaluecurvas/{id}")]
        public IActionResult getvaluecurvas(Guid id)
        {

            var data = _wbsscheduleService.getCurvaS(id);
            decimal? temp = 0;
            var val = new Dictionary<DateTime, Decimal?>();
            string[] ChartDate = new string[data.Count()];
            Decimal?[] ChartCurrentCompletion = new Decimal?[data.Count()];
            Dictionary<string, object> Data = new Dictionary<string, object>();
            var i = 0;
            foreach (var datacurves in data)
            {

                temp += datacurves.Value;
                ChartDate[i] = String.Format("{0:MM/dd/yyyy}", datacurves.Key);
                ChartCurrentCompletion[i] = temp;
                i++;
            }
            //var u = 0;
            //decimal? temp2 = 0;
            //var Perkiraan = _wbsscheduleService.getCurvaSPerkiraan(id);
            //Decimal?[] ChartTargetCompletion = new Decimal?[Perkiraan.Count()];
            //foreach (var datacurves in Perkiraan)
            //{
            //    temp2 += datacurves.Value;
            //    ChartTargetCompletion[u] = temp2;
            //    u++;
            //}


            Data.Add("ChartDate", ChartDate);
            Data.Add("ChartCurrentCompletion", ChartCurrentCompletion);
            // Data.Add("ChartTargetCompletion", ChartTargetCompletion);
            return Ok(Data);
        }

        [Route("getBobot")]
        public IActionResult GetBobot(Guid id)
        {
            var banyakhari = Service.GetAll().Where(x => x.IdActTmp.Equals(id) && x.Durasi.HasValue).Count();
            var data = Service.GetAll().Where(x => x.IdActTmp.Equals(id)).Sum(x => x.Durasi);
            return Ok(data- banyakhari);
        }

        [Route("gettemplates")]
        public IActionResult gettemplates(bool showAll, string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return BadRequest();
            }

            var templates = new List<ActTemplate>();

            if (showAll)
            {
                templates = _templateService.Query(null, query);
            }
            else
            {
                var user = _userManager.FindByIdAsync(CurrentUser.Id).Result;

                var claims = _userManager.GetClaimsAsync(user).Result;

                var unitId = claims
                    .FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;

                if (unitId != null)
                {
                    templates = _templateService.Query(unitId, query);
                }
                templates = _templateService.Query(unitId, query);
            }

            return Ok(templates);
        }


        [Route("getctemplatebyId")]
        public IActionResult getctemplatebyId(Guid Id)
        {
            var template = Service.GetAll().Where(x => x.IdActTmp == Id && x.ActParent != null && x.Bobot != null).OrderBy(x => x.Urutan).ToList();

            if (!template.Any())
            {
                return BadRequest();
            }

            return Ok(template);
        }


    }
}
