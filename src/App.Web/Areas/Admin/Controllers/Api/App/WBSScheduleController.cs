using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Domain.DTO.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using App.Web.Models.ViewModels.App;
using DataTables.AspNet.Core;
using DataTables.AspNet.AspNetCore;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/WBSSchedule")]
    public class WBSScheduleController : BaseApiController<WBSSchedule, IWBSScheduleService, WBSScheduleDto, Guid>
    {
        private readonly IActivityTemplateChildService _childTemplateService;

        public WBSScheduleController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IWBSScheduleService service,
            IActivityTemplateChildService childTemplateService,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _childTemplateService = childTemplateService;
        }

        [HttpPost]
        [Route("getschedulebyid/{id}")]
        public IActionResult DataTables(Guid Id, IDataTablesRequest request)
        {
            var response = Service.GetWBSScheduleById(Id, request);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        [Route("postschedulefromtamplate")]
        public IActionResult PostscheduleFromTenplate(List<WBSScheduleViewModel> listmodel)
        {

            if (!ModelState.IsValid)
                return BadRequest();
            var fordelete = Service.GetAll().Where(x => x.IdWBSTree == listmodel[0].IdWBSTree);
            foreach (var fordeleteshecule in fordelete)
            {
                Service.Delete(fordeleteshecule);
            }

            DateTime lastFinish = new DateTime();

            for (int i = 0; i < listmodel.Count(); i++)
            {

                var modelSchedule = new WBSSchedule();

                if (i == 0)
                {
                    modelSchedule.Start = listmodel[i].Start;
                    modelSchedule.Finish = listmodel[i].Start.AddDays(double.Parse((listmodel[i].DurasiReal-1).ToString()));
                    modelSchedule.IdActTmpChild = listmodel[i].IdActTmpChild;
                    modelSchedule.IdWBSTree = listmodel[i].IdWBSTree;
                    modelSchedule.DurasiReal = listmodel[i].DurasiReal;
                    lastFinish = modelSchedule.Finish;
                }
                else
                {
                    modelSchedule.Start = lastFinish;
                    modelSchedule.Finish = modelSchedule.Start.AddDays(double.Parse((listmodel[i].DurasiReal-1).ToString()));
                    modelSchedule.IdActTmpChild = listmodel[i].IdActTmpChild;
                    modelSchedule.IdWBSTree = listmodel[i].IdWBSTree;
                    modelSchedule.DurasiReal = listmodel[i].DurasiReal;
                    lastFinish = modelSchedule.Finish;
                }

                Service.Add(modelSchedule);
            }


            return Ok(listmodel);
        }


        [HttpPost]
        [Route("postschedule")]
        public IActionResult Postschedule(List<WBSScheduleViewModel> listmodel)
        {
            if (!ModelState.IsValid)
                return BadRequest();


            foreach (var model in listmodel)
            {
                var data = Service.GetAll().Where(x => x.IdWBSTree == model.IdWBSTree && x.IdActTmpChild == model.IdActTmpChild).FirstOrDefault();

                var templateDurasi = _childTemplateService.GetById(model.IdActTmpChild);

                if (data != null)
                {
                    var modeldata = data;

                    modeldata.Start = model.Start;
                    modeldata.Finish = model.Finish;
                    modeldata.DurasiReal = int.Parse((modeldata.Finish - modeldata.Start).TotalDays.ToString()) + 1;
                    Service.Update(modeldata);
                }
                else
                {

                    var modeldata = new WBSSchedule();
                    modeldata.Start = model.Start;

                    modeldata.Finish = model.Finish;
                    modeldata.DurasiReal = int.Parse((modeldata.Finish - modeldata.Start).TotalDays.ToString()) + 1;
                    modeldata.IdWBSTree = model.IdWBSTree;
                    modeldata.IdActTmpChild = model.IdActTmpChild;
                    Service.Add(modeldata);
                }

            }
            return Ok(listmodel);
        }

    }
}