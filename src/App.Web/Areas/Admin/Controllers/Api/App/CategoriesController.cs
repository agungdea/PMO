using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/categories")]
    //  [Authorize(Policy = "ClaimBasedAuthz")]
    public class CategoriesController : BaseApiController<Category, ICategoryService, CategoryDto, int>
    {
        private readonly IWBStreeCategoryService _wbstreeCategoryService;
        public CategoriesController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            ICategoryService service,
            IWBStreeCategoryService wbstreeCategoryService,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _wbstreeCategoryService = wbstreeCategoryService;
        }

        [HttpGet]
        [Route("gettree")]
        public IActionResult GetTree(int? parentid)
        {
            var data = Service.GetChildData(parentid);
            return Ok(data);
        }
        [HttpGet]
        [Route("gettall/{id}")]
        public IActionResult Alldata(Guid Id)
        {
            var data = Service.GetAlldata(Id);
            return Ok(data);
        }

        [HttpPut]
        [Route("edit/{id}")]
        public IActionResult EditDataCategory(int id, CategoryForm model)
        {
            var item = Service.GetById(id);

            if (item == null)
            {
                return NotFound();
            }

            item.Name = model.Name;
            item.LastEditedBy = CurrentUser.Id;
            item.LastUpdateTime = DateTime.Now;

            Service.Update(item);

            return Ok(item);
        }


        [HttpPost]
        public IActionResult PostDataCategory(CategoryForm model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            Category item = Mapper.Map<Category>(model);

            if (model.ParentId != null)
            {
                item.ParentId = model.ParentId;
            }

            item.CreatedAt = DateTime.Now;
            item.CreatedBy = CurrentUser.Id;

            Service.Add(item);

            return Ok(item);
        }

        [Authorize(Policy = "ClaimBasedAuthz")]
        [HttpDelete]
        [Route("delete/{id}")]
        public override IActionResult Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var getbyid = Service.GetById(id);
            var getall = Service.GetAll().Where(x => x.ParentId == id);
            if (getall.Count() > 0)
            {
                foreach (var get in getall)
                {
                    Service.DeleteChildData(get.Id);
                    Service.Delete(get);
                }
            }

            Service.Delete(getbyid);
            return Ok(id);
        }

        [Route("getCategory/{id}")]
        public IActionResult getCategory(Guid id)
        {
           var categoryid= _wbstreeCategoryService.GetAll().First(x => x.WBStreeId.Equals(id)).CategoryId;

            return Ok(categoryid);
        }
        [HttpGet]
        [Route("getcountcategory/{id}")]
        public IActionResult getCountCategory(Guid id)
        {
            var categoryid = _wbstreeCategoryService.GetAll().Where(x => x.WBStreeId.Equals(id)).Count();

            return Ok(categoryid);
        }

        [HttpPost]
        [Route("reorder")]
        public IActionResult Reorder(int id, int parentId)
        {
            var from = Service.GetById(id);
            var to = Service.GetById(parentId);

            if (from == null && to == null)
            {
                return NotFound();
            }

            from.ParentId = to.Id;
            from.LastEditedBy = CurrentUser.Id;
            from.LastUpdateTime = DateTime.Now;

            Service.Update(from);

            return Ok(from);
        }

    }
}