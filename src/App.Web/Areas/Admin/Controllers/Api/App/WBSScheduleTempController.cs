using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Domain.DTO.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("api/WBSScheduleTemp")]
    public class WBSScheduleTempController : BaseApiController<WBSScheduleTemp, IWBSScheduleTempService, WBSScheduleTempDto, Guid>
    {
        public WBSScheduleTempController
            (IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper,
            IWBSScheduleTempService service,
            IUserHelper userHelper) : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
        
    }
}