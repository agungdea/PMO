﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Domain.DTO.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using App.Web.Models.ViewModels.App;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/usermedias")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class UserMediasController : BaseApiController<UserMedia, IUserMediaService, UserMediaDto, Guid>
    {

        private readonly FileHelper _fileHelper;
        private readonly ConfigHelper _config;
        private readonly string _cropSuffix;
        private readonly string _userMediaDir;
        public UserMediasController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            FileHelper fileHelper,
            ConfigHelper config,
            IUserMediaService service,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _fileHelper = fileHelper;
            _config = config;
            _cropSuffix = _config.GetConfig("crop.suffix");
            _userMediaDir = _config.GetConfig("user.media.upload.directory");
        }


        [HttpPost]
        [Route("CreateMedia")]
        public IActionResult CreateMedia(UserMediaForm model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var attachment = model.MediaDirectory.ConvertToAttachment();

            var filename = string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);

            var newPath = $"{_userMediaDir}";
            attachment.CropedPath = _fileHelper.CreateCropped(attachment);
            attachment.Path = _fileHelper.FileMove(attachment.Path,
                newPath,
                filename);
            attachment.CropedPath = _fileHelper.FileMove(attachment.CropedPath,
                newPath,
                filename + _cropSuffix);
            model.MediaDirectory = attachment.ConvertToString();

            var result = Mapper.Map<UserMedia>(model);

            result.CreatedAt = DateTime.Now;
            result.CreatedBy = CurrentUser.Id;

            Service.Add(result);

            return RedirectToRoute(new
            {
                controller = "UserMedia",
                action = "Index",
                area = "Admin"
            });
        }

        [HttpDelete("{id}")]
        public override IActionResult Delete(Guid id)
        {
            var item = Service.GetById(id);
            if (null == item)
            {
                return Json(BadRequest());
            }

            Service.Delete(item);

            _fileHelper.DeleteAttachment(item.MediaDirectory.ConvertToAttachment());

            return Ok(item);
        }

    }
}
