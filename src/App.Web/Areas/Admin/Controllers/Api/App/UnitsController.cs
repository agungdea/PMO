﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Domain.DTO.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using App.Web.Models.ViewModels.App;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/units")]
    // [Authorize(Policy = "ClaimBasedAuthz")]
    public class UnitsController : BaseApiController<Unit, IUnitService, UnitDto, string>
    {
        private readonly IUnitService _unitservice;
        public UnitsController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IUnitService service,
            IUnitService unitservice,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _unitservice = unitservice;
        }

        [HttpPost]
        [Route("getpenangungJawab")]
        public IActionResult getPenanggungJawab(string term)
        {
            var data = _unitservice.GetSelectListItem();
            if (!string.IsNullOrWhiteSpace(term))
            {
                return Ok(data.Select(x => new { text = x.Text, id = x.Value }).Where(x => x.text.ToLower().Contains(term.ToLower())).Take(10).OrderBy(x => x.id));
            }
            return Ok(data.Select(x => new { text = x.Text, id = x.Value }).Take(10).OrderBy(x=>x.text));
        }
        public override IActionResult Delete(string id)
        {
            var getall = Service.GetAll().Where(x => x.ParentId == id);
            if (getall.Count() > 0)
            {
                foreach (var get in getall)
                {
                    if(id!=get.Id)
                    Service.DeleteChildData(get.Id);
                    Service.Delete(get);
                }
            }
            return base.Delete(id);
        }
        [HttpGet]
        [Route("getselectItem")]
        public IActionResult GetSelectItem(string level, string parents, string term)
        {
            var unit = Service.GetUnitByParameters(level, parents).Select(x => new Unit
            {
                Id = x.Id,
                NamaSingkat = x.NamaUnit
            }).OrderBy(x=>x.NamaUnit);

            if (parents == null)
            {
                return BadRequest();
            }

            var unitChoice = new List<SelectUnitItem>();

            if (unitChoice == null)
            {
                return BadRequest();
            }

            foreach (var item in unit)
            {
                var select = new SelectUnitItem()
                {
                    Id = item.Id,
                    Text = item.NamaSingkat
                };

                unitChoice.Add(select);
            }

            if (term != null)
            {
                return Ok(unitChoice.Where(x => x.Text.ToLower().Contains(term.ToLower())).OrderBy(x => x.Text));
            }

            return Ok(unitChoice.OrderBy(x => x.Text));
        }

        [HttpPost]
        [Route("ValidateUnit")]
        public IActionResult ValidateUnit(string id)
        {
            var unit = Service.ValidateUnit(id);

            if (unit)
            {
                return Ok(false);
            }

            return Ok(true);
        }
    }
}
