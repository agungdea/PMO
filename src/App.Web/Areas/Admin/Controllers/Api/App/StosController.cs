﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Domain.DTO.App;
using App.Services.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/stos")]
    public class StosController : BaseCacheApiController<AreaMapping, IAreaMappingService, AreaMappingDto, Guid>
    {
        private readonly SeedingHelper _seedingHelper;

        public StosController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IAreaMappingService service,
            SeedingHelper seedingHelper,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _seedingHelper = seedingHelper;
        }

        [HttpGet]
        [Route("autocomplete")]
        public List<AreaMapping> _GetArea(string query)
        {
            
            List<AreaMapping> Area = new List<AreaMapping>(); 
            var result = (from sto in Service.GetAll().Where(x => x.AreaKode.Contains(query) || x.AreaStoName.ToLower().Contains(query.ToLower()) || x.AreaWitel.ToLower().Contains(query.ToLower()) || x.AreaDatel.ToLower().Contains(query.ToLower()) || x.AreaRegional.ToLower().Contains(query.ToLower()))
            /*Service.GetAll()  //sto.AreaKode.Contains(query)*/
            orderby sto.AreaKode
            select sto).Take(10).ToList();


            return result;
        }

        [HttpGet]
        [Route("getstos")]
        public IActionResult getStos(string q)
        {
            var count = Service.GetAll().Count();
           
            var item = Service.GetAll().Take(10);
            if (q != null)
            {
                item = Service.GetAll().Where(x=>x.AreaStoName.ToLower().Contains(q.ToLower()) || x.AreaWitel.ToLower().Contains(q.ToLower()) || x.AreaDatel.ToLower().Contains(q.ToLower()) || x.AreaRegional.ToLower().Contains(q.ToLower())).Take(10);
            }
            var variable = new
            {
                total_count = count,
                incomplete_results=false,
                items = item
            };
            return Ok(variable);

        }

        [HttpGet]
        [Route("getregional")]
        public IActionResult GetRegional(string search)
        {
            var items = Service.GetRegional(search);

            return Ok(items);
        }

        [HttpGet]
        [Route("getwitel")]
        public IActionResult GetWitel(string regional, string search)
        {
            var items = Service.GetWitel(regional, search);

            return Ok(items);
        }

        [HttpGet]
        [Route("getdatel")]
        public IActionResult GetDatel(string witel, string search)
        {
            var items = Service.GetDatel(witel, search);

            return Ok(items);
        }

        [HttpGet]
        [Route("getsto")]
        public IActionResult GetSTO(string datel, string search)
        {
            var items = Service.GetSTO(datel, search);

            return Ok(items);
        }
    }
}
