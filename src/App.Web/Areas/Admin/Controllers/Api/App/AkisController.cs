﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Domain.DTO.App;
using App.Domain.Models.Identity;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using DataTables.AspNet.Core;
using DataTables.AspNet.AspNetCore;
using App.Web.Models.ViewModels.App;
using App.Web.Helper;
using Microsoft.AspNetCore.Identity;
using App.Services.Utils;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using App.Services.Workflow;
using App.Services.Core;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/akis")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class AkisController : BaseCacheApiController<Aki, IAkiService, AkiDto, Guid>
    {
        private readonly ExcelHelper _excelHelper;
        private readonly IAkiCoverService _akicoverservice;
        private readonly IWfActivityAkiService _wfActivityAkiService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitService _unitService;
        private readonly IUserService _userservice;
        private readonly ConfigHelper _configHelper;
        public string _DefaultPnd;
        private readonly IWebSettingService _webSettingService;
        private readonly IWfProcessService _wfProcessService;
        private readonly IWfCommantActivityService _wfCommantActivityService;
        private readonly IGroupWfService _groupWfService;
        
        private readonly string _jogetProcessDefId;
        public AkisController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IAkiService service,
            ExcelHelper excelHelper,
            UserManager<ApplicationUser> userManager,
            IUnitService unitService,
            ConfigHelper configHelper,
            IWfActivityAkiService wfActivityAkiService,
            IWebSettingService webSettingService,
            IUserService userservice,
            IWfCommantActivityService wfCommantActivityService,
            IWfProcessService wfProcessService,
            IAkiCoverService akicoverservice,
            IGroupWfService groupWfService,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _userservice = userservice;
            _wfActivityAkiService = wfActivityAkiService;
            _unitService = unitService;
            _excelHelper = excelHelper;
            _groupWfService = groupWfService;
            _wfCommantActivityService = wfCommantActivityService;
            _wfProcessService = wfProcessService;
            _userManager = userManager;
            _akicoverservice = akicoverservice;
            _configHelper = configHelper;
            _DefaultPnd = _configHelper.GetConfig("user.defaut.unitpnd");
            _webSettingService = webSettingService;
            _jogetProcessDefId = _webSettingService.GetByName("joget.wf.justification.process.def.id")?.Value;

        }

        [HttpGet]
        [Route("getdatafromexcel")]
        public IActionResult GetDataFromExcel(string source)
        {
            var ExcelUpload = _excelHelper.UploadExcel(source);
            return Ok(ExcelUpload);
        }
        [HttpGet]
        [Route("DataTablesMyTask")]
        public DataTablesJsonResult DataTablesMyTask(Guid id, IDataTablesRequest request)
        {
            Boolean iscapex = false;
            if (_groupWfService.GetAll().Where(x => x.Nik.Equals(CurrentUser.UserName)).Count() > 0)
            {
                 iscapex =  true;
            }

            var response = _wfActivityAkiService.DataTablesMyTask(CurrentUser.UserName, iscapex, request);

            return new DataTablesJsonResult(response, true);
        }
        [HttpGet]
        [Route("datatablesjoin")]
        public IActionResult Datatablesjoin(IDataTablesRequest request)
        {
            var user = _userManager.FindByIdAsync(CurrentUser.Id).Result;
            var claims = _userManager.GetClaimsAsync(user).Result;
            var unitId = claims
                .FirstOrDefault(x => x.Type == ApplicationUser.ALL_Unit)?.Value;
            var innncludes = new Expression<Func<ApplicationUser, object>>[1];
            innncludes[0] = (x => x.UserProfile);
            var data = from aki in Service.GetAll()
                       join _user in _userservice.GetAll(innncludes) on aki.CreatedBy equals _user.Id
                       join akicover in _akicoverservice.GetAll() on aki.Id equals akicover.AkiId
                     into akidefault
                       from akicover in akidefault.DefaultIfEmpty()
                           // join _unit in _unitService.GetAll() on akicover.PenanggungJawab equals _unit.Id
                       orderby aki.CreatedAt descending
                       select new
                       {
                           Id = aki.Id,
                           AkiName = aki.AkiName,
                           NilaiProgram = akicover.NilaiProgram == null ? 0 : akicover.NilaiProgram,
                           PenanggungJawab = string.IsNullOrWhiteSpace(akicover.PenanggungJawab) ? "-" : _unitService.GetById(akicover.PenanggungJawab).NamaUnit,
                           Units = aki.OtherInfo,
                           isAction = aki.CreatedBy == CurrentUser.Id ? true : false,
                           status = aki.CustomField2,
                           CreatedBy = _user.UserName + " (" + _user.UserProfile.Name + ")",
                           aki.CreatedAt,
                       };
            var admin = User.IsAdministrator();
            var IsUser = User.IsUser();
            var getunit = UserHelper.GetUserUnit(CurrentUser.Id);
            if (getunit == null && admin == false)
            {
                return NotFound();
            }

            if (admin == false && string.IsNullOrWhiteSpace(unitId))
            {
                var Listunit = _unitService.GetUnitLevel(getunit);
                var listUnitdispsisi = _unitService.GetUnitLevel(getunit);

                var RegionId = claims
                  .FirstOrDefault(x => x.Type == ApplicationUser.ALL_Regional)?.Value;
                if (RegionId != null)
                {
                    Listunit = _unitService.GetUnitLevel(_DefaultPnd);
                    data = data.Where(x => Listunit.Contains(x.Units));
                }
                else
                {
                    data = data.Where(x => Listunit.Contains(x.Units));
                }


            }
            var filteredData = data.AsQueryable();

            var searchQuery = request.Search.Value;
            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            return new DataTablesJsonResult(DataTablesResponse.Create(request, filteredData.Count(), filteredData.Count(), dataPage), true);
        }
        [HttpPost]
        [Route("SubmitAki/{Id}")]
        public IActionResult SubmitDataAki(Guid Id)
        {
            var dataaki = Service.GetById(Id);
            if (dataaki == null)
            {
                return Ok("DataAkiIsnull");
            }
            if (dataaki.CustomField3 == null || string.IsNullOrWhiteSpace(dataaki.CustomField3))
            {
                return Ok("DataApprovalIsnull");
            }
            dataaki.CustomField2 = Aki.STATUS_SUBMITTED;

            Service.Update(dataaki);
            return Ok(dataaki);
        }

        [HttpPost]
        [Route("GetDataAKI")]
        public IActionResult GetDataAKI(string query)
        {
            var aki = Service.GetAll().Select(x => new Aki
            {
                Id = x.Id,
                AkiName = x.AkiName
            });

            var akiChoice = new List<SelectAkiItem>();

            if (akiChoice == null)
            {
                return BadRequest();
            }

            foreach (var item in aki)
            {
                var select = new SelectAkiItem()
                {
                    Id = item.Id.ToString(),
                    Text = item.AkiName
                };

                akiChoice.Add(select);
            }

            if (query != null)
            {
                return Ok(akiChoice.Where(x => x.Text.ToLower().Contains(query.ToLower())));
            }

            return Ok(akiChoice);
        }


        [HttpPost]
        [Route("CompliteProces/{IdAki}")]
        public async Task<IActionResult> CompliteProces(Guid IdAki, string comment, Boolean islike)
        {
            var userjoget = CurrentUser.UserName;
            var dataaki = Service.GetById(IdAki);

            if (dataaki == null)
            {
                return Ok("DataAkiKosong");
            }
            if (dataaki.wf_processId == null)
            {
                return Ok("CheckWfProcess");
            }
            var Act = _wfActivityAkiService.GetAll().Where(x => x.wf_processId == dataaki.wf_processId && x.state == WfActivityAki.OPEN_ACTIVITY).First();
            if (Act == null)
            {
                return Ok("ApprovalTidakDitemukan");
            }
            var proces = _wfProcessService.GetAll().Where(x => x.ProcessId == dataaki.wf_processId).First();
            if (string.IsNullOrEmpty(proces.currentApproval) || string.IsNullOrEmpty(proces.totalApproval))
            {
                return Ok("ApprovalTidakDitemukan");
            }
            var curenntApproval = (int.Parse(proces.currentApproval)) + 1;
            var variable = new Dictionary<string, string>();
            
            variable.Add("var_status", Aki.STATUS_ApproveUnitBisnis);
            variable.Add("var_currentApproval", curenntApproval.ToString());
            variable.Add("var_DocumentId", IdAki.ToString());
            variable.Add("loginAs", userjoget);
            var test = _wfProcessService.CompleteActivitySubmit(Act.Id, variable);
            if (test.Result.error != null)
            {
                return Ok("errorcomplite");

            }
            var dataApprover = dataaki.CustomField3.Split(',').ToList();
            string approver = "";
            int gecurenapproval = 0;
            //mendefinisikan  approver;
            foreach (var appr in dataApprover)
            {
                if (gecurenapproval == curenntApproval)
                {
                    var datausr = await _userManager.FindByNameAsync(appr);
                    if (datausr != null)
                    {
                        approver = appr;
                    }
                    break;
                }
                gecurenapproval++;
            }
            if (curenntApproval != int.Parse(proces.totalApproval))
            {
                if (string.IsNullOrWhiteSpace(approver))
                {
                    return Ok("ApproverTidakada");
                }
                var JogetcekUserApproval = _wfProcessService.CekUser(approver);
                if (JogetcekUserApproval.Result.error != null)
                {
                    return Ok("ApproverTidakada");

                }
                if (JogetcekUserApproval.Result.total == 0)
                {
                    return Ok("ApproverTidakada");
                }
            }

            var aktifivity = await _wfProcessService.listActivity(dataaki.wf_processId);
            List<WfActivityAki> dataAktifivity = JsonConvert.DeserializeObject<List<WfActivityAki>>(aktifivity["data"].ToString());

            foreach (var act in dataAktifivity)
            {
                var cek = _wfActivityAkiService.GetById(act.Id);
                if (cek != null)
                {

                    cek.state = act.state;
                    cek.dateCreated = act.dateCreated;
                    _wfActivityAkiService.Update(cek);

                }
                else
                {
                    if (!act.name.ToLower().Contains("tools"))
                    {
                        if (!act.name.ToLower().Contains("route"))
                        {
                            if (curenntApproval != int.Parse(proces.totalApproval))
                            {
                                var variableAssigment = new Dictionary<string, string>();
                                variableAssigment.Add("processId", dataaki.wf_processId);
                                variableAssigment.Add("activityId", act.Id);
                                variableAssigment.Add("username", approver);
                                variableAssigment.Add("replaceUser", userjoget);
                                var assigment = _wfProcessService.Assigment(_jogetProcessDefId, variableAssigment);
                                act.AssigmentTo = approver;
                            }
                            else {
                                act.AssigmentTo = GroupWf.GROUP_WF_Capex;
                            }
                            act.AkiId = IdAki;
                            act.wf_processId = dataaki.wf_processId;
                            _wfActivityAkiService.Add(act);

                        }
                    }

                }
            }
            proces.currentApproval = curenntApproval.ToString();
            _wfProcessService.Update(proces);


            var modelcomment = new WfCommantActivity();
            modelcomment.ProcessId = proces.ProcessId;
            modelcomment.Status = Aki.STATUS_ApproveUnitBisnis;
            modelcomment.Comment = comment;
            modelcomment.IsAggree = islike;
            modelcomment.NIK = userjoget;
            modelcomment.Name = CurrentUser.UserProfile.Name;
            modelcomment.CreatedAt = DateTime.Now;
            _wfCommantActivityService.Add(modelcomment);

            #region update status local
            dataaki.CustomField2 = Aki.STATUS_ApproveUnitBisnis;
            Service.Update(dataaki);
            #endregion
            return Ok(aktifivity);
        }



        [HttpPost]
        [Route("startWf/{IdAki}")]
        public async Task<IActionResult> StartWf(Guid IdAki, string comment, Boolean islike)
        {
            #region Start process

            //var userjoget = CurrentUser.UserName;
            var userjoget = CurrentUser.UserName;
            var dataaki = Service.GetById(IdAki);
            if (dataaki == null)
            {
                return Ok("CheckApprovaldanadokumen");
            }
            if (dataaki.CustomField3 == null)
            {
                return Ok("CheckApprovaldanadokumen");
            }
            var dataApprover = dataaki.CustomField3.Split(',').ToList();
            string approver = "";

            //mendefinisikan  approver pertama;
            var defaultApprover = 0;
            foreach (var appr in dataApprover)
            {
                var datausr = await _userManager.FindByNameAsync(appr);
                if (datausr != null)
                {
                    approver = appr;
                }
                break;

            }
            var JogetcekUser = _wfProcessService.CekUser(userjoget);
            if (JogetcekUser.Result.error != null)
            {
                return Ok("CheckJogetUserId");

            }
            if (JogetcekUser.Result.total == 0)
            {
                return Ok("CheckJogetUserId");
            }


            if (string.IsNullOrWhiteSpace(approver))
            {
                return Ok("ApproverPertamaTidakada");
            }
            var JogetcekUserApproval = _wfProcessService.CekUser(approver);
            if (JogetcekUserApproval.Result.error != null)
            {
                return Ok("CheckJogetUserIdApproval1");

            }
            if (JogetcekUserApproval.Result.total == 0)
            {
                return Ok("CheckJogetUserIdApproval1");
            }

            var data = _wfProcessService.Start(_jogetProcessDefId, userjoget);

            if (data.Result == null)
            {
                return Ok("errorstart");
            }
            if (data.Result.error != null)
            {
                return Ok("errorstart");
            }
            dataaki.wf_processId = data.Result.processId;

            var listAktifivity =  _wfProcessService.listActivity(dataaki.wf_processId);
            var dynamic = JsonConvert.DeserializeObject<dynamic>(listAktifivity.Result.ToString());
            var activity = new WfActivityAki();
            activity.Id = dynamic.data.id;
            activity.name = dynamic.data.name;
            activity.dateCreated = dynamic.data.dateCreated;
            activity.serviceLevelMonitor = dynamic.data.serviceLevelMonitor;
            activity.state = dynamic.data.state;
            activity.wf_processId = dataaki.wf_processId;
            activity.AkiId = IdAki;
            activity.AssigmentTo = userjoget;
            if (!activity.name.ToLower().Contains("tools"))
            {
                if (!activity.name.ToLower().Contains("route"))
                {
                    var res = _wfActivityAkiService.Add(activity);
                }
            }
            #endregion

            #region complite Activity Submit
            var variable = new Dictionary<string, string>();
            variable.Add("var_status", Aki.STATUS_SUBMITTED);
            variable.Add("var_totalApproval", dataApprover.Count.ToString());
            variable.Add("var_currentApproval", defaultApprover.ToString());
            variable.Add("var_DocumentId", IdAki.ToString());
            variable.Add("loginAs", userjoget);
            var test =  _wfProcessService.CompleteActivitySubmit(activity.Id, variable);
            if (test.Result.error != null)
            {
                return Ok("errorcomplite");
            }
            var aktifivity = await _wfProcessService.listActivity(dataaki.wf_processId);
            List<WfActivityAki> dataAktifivity = JsonConvert.DeserializeObject<List<WfActivityAki>>(aktifivity["data"].ToString());

            foreach (var act in dataAktifivity)
            {
                var cek = _wfActivityAkiService.GetById(act.Id);
                if (cek != null)
                {

                    cek.state = act.state;
                    cek.dateCreated = act.dateCreated;
                    _wfActivityAkiService.Update(cek);

                }
                else
                {
                    if (!act.name.ToLower().Contains("tools"))
                    {
                        if (!act.name.ToLower().Contains("route"))
                        {
                            var variableAssigment = new Dictionary<string, string>();
                            variableAssigment.Add("processId", dataaki.wf_processId);
                            variableAssigment.Add("activityId", act.Id);
                            variableAssigment.Add("username", approver);
                            variableAssigment.Add("replaceUser", userjoget);
                            var assigment = _wfProcessService.Assigment(_jogetProcessDefId, variableAssigment);


                            act.AkiId = IdAki;
                            act.AssigmentTo = approver;
                            act.wf_processId = dataaki.wf_processId;
                            _wfActivityAkiService.Add(act);

                        }
                    }

                }
            }
            var datawfproccess = await _wfProcessService.ViewProcess(dataaki.wf_processId);
            datawfproccess.totalApproval = dataApprover.Count.ToString();
            datawfproccess.currentApproval = defaultApprover.ToString();
            _wfProcessService.Add(datawfproccess);
            #endregion

            #region create Comment
            var modelcomment = new WfCommantActivity();
            modelcomment.ProcessId = data.Result.processId;
            modelcomment.Status = Aki.STATUS_SUBMITTED;
            modelcomment.Comment = comment;
            modelcomment.IsAggree = islike;
            modelcomment.NIK = userjoget;
            modelcomment.Name = CurrentUser.UserProfile.Name;
            modelcomment.CreatedAt = DateTime.Now;
            _wfCommantActivityService.Add(modelcomment);

            #endregion

            #region update status local
            dataaki.CustomField2 = Aki.STATUS_SUBMITTED;
            Service.Update(dataaki);
            #endregion
            return Ok(aktifivity);
        }

        [HttpPost]
        [Route("RejectWf/{IdAki}")]
        public async Task<IActionResult> RejectWf(Guid IdAki, string comment, Boolean islike)
        {
            var userjoget = CurrentUser.UserName;
            var dataaki = Service.GetById(IdAki);
            if (dataaki == null)
            {
                return BadRequest();
            }
            var Act = _wfActivityAkiService.GetAll().Where(x => x.wf_processId == dataaki.wf_processId && x.state == WfActivityAki.OPEN_ACTIVITY).First();
            if (Act == null)
            {
                return Ok("ApprovalTidakDitemukan");
            }
            var proces = _wfProcessService.GetAll().Where(x => x.ProcessId == dataaki.wf_processId).First();
            if (string.IsNullOrEmpty(proces.currentApproval) || string.IsNullOrEmpty(proces.totalApproval))
            {
                return Ok("ApprovalTidakDitemukan");
            }
            var variable = new Dictionary<string, string>();
            variable.Add("var_status", Aki.STATUS_REJECTED);
            variable.Add("var_DocumentId", IdAki.ToString());
            variable.Add("loginAs", userjoget);
            var test =  _wfProcessService.CompleteActivitySubmit(Act.Id, variable);
            if (test.Result.error != null)
            {
                return Ok("errorcomplite");
            }
            var aktifivity = await _wfProcessService.listActivity(dataaki.wf_processId);
            List<WfActivityAki> dataAktifivity = JsonConvert.DeserializeObject<List<WfActivityAki>>(aktifivity["data"].ToString());

            foreach (var act in dataAktifivity)
            {
                var cek = _wfActivityAkiService.GetById(act.Id);
                if (cek != null)
                {
                    cek.state = act.state;
                    cek.dateCreated = act.dateCreated;
                    _wfActivityAkiService.Update(cek);
                }
                else
                {
                    if (!act.name.ToLower().Contains("tools"))
                    {
                        if (!act.name.ToLower().Contains("route"))
                        {
                            act.AkiId = IdAki;
                            act.AssigmentTo = userjoget;
                            act.wf_processId = dataaki.wf_processId;
                            _wfActivityAkiService.Add(act);
                        }
                    }

                }
            }
            var datawfproccess = await _wfProcessService.ViewProcess(dataaki.wf_processId);

            if (datawfproccess.ProcessId == proces.ProcessId)
            {
                proces.State = datawfproccess.State;
                _wfProcessService.Update(proces);
            }
            else
            {
                _wfProcessService.Update(datawfproccess);
            }


            #region create Comment
            var modelcomment = new WfCommantActivity();
            modelcomment.ProcessId = proces.ProcessId;
            modelcomment.Status = Aki.STATUS_REJECTED;
            modelcomment.Comment = comment;
            modelcomment.IsAggree = islike;
            modelcomment.NIK = userjoget;
            modelcomment.Name = CurrentUser.UserProfile.Name;
            modelcomment.CreatedAt = DateTime.Now;
            _wfCommantActivityService.Add(modelcomment);

            #endregion
            dataaki.CustomField2 = Aki.STATUS_REJECTED;
            Service.Update(dataaki);
            return Ok(aktifivity);
        }


        [HttpPost]
        [Route("startrejectWf/{IdAki}")]
        public async Task<IActionResult> StartRejcetWf(Guid IdAki, string comment, Boolean islike)
        {
            var userjoget = CurrentUser.UserName;
            var dataaki = Service.GetById(IdAki);
            if (dataaki == null)
            {
                return BadRequest();
            }
            var data = _wfProcessService.Start(_jogetProcessDefId, userjoget);
            if (data.Result != null)
            {

                dataaki.wf_processId = data.Result.processId;
                if (data.Result == null)
                {
                    return Ok("CheckJogetUserId");
                }
                if (data.Result.error != null)
                {
                    return Ok("CheckJogetUserId");
                }

                var listAktifivity =  _wfProcessService.listActivity(dataaki.wf_processId);
                var dynamic = JsonConvert.DeserializeObject<dynamic>(listAktifivity.Result.ToString());

                var activity = new WfActivityAki();
                activity.Id = dynamic.data.id;
                activity.name = dynamic.data.name;
                activity.dateCreated = dynamic.data.dateCreated;
                activity.serviceLevelMonitor = dynamic.data.serviceLevelMonitor;
                activity.state = dynamic.data.state;
                activity.wf_processId = dataaki.wf_processId;
                activity.AkiId = IdAki;
                activity.AssigmentTo = userjoget;


                if (!activity.name.ToLower().Contains("tools"))
                {
                    if (!activity.name.ToLower().Contains("route"))
                    {
                        var res = _wfActivityAkiService.Add(activity);
                    }
                }

                var variable = new Dictionary<string, string>();
                variable.Add("var_status", Aki.STATUS_REJECTED);
                variable.Add("var_DocumentId", IdAki.ToString());
                variable.Add("loginAs", activity.AssigmentTo);
                var test =  _wfProcessService.CompleteActivitySubmit(activity.Id, variable);
                if (test.Result.error != null)
                {
                    return Ok("errorcomplite");
                }
                var aktifivity = await _wfProcessService.listActivity(dataaki.wf_processId);
                List<WfActivityAki> dataAktifivity = JsonConvert.DeserializeObject<List<WfActivityAki>>(aktifivity["data"].ToString());

                foreach (var act in dataAktifivity)
                {
                    var cek = _wfActivityAkiService.GetById(act.Id);
                    if (cek != null)
                    {
                        cek.state = act.state;
                        cek.dateCreated = act.dateCreated;
                        _wfActivityAkiService.Update(cek);
                    }
                    else
                    {
                        if (!act.name.ToLower().Contains("tools"))
                        {
                            if (!act.name.ToLower().Contains("route"))
                            {
                                act.AkiId = IdAki;
                                act.AssigmentTo = userjoget;
                                act.wf_processId = dataaki.wf_processId;
                                _wfActivityAkiService.Add(act);
                            }
                        }

                    }
                }
                var datawfproccess = await _wfProcessService.ViewProcess(dataaki.wf_processId);
                _wfProcessService.Add(datawfproccess);

                #region create Comment
                var modelcomment = new WfCommantActivity();
                modelcomment.ProcessId = data.Result.processId;
                modelcomment.Status = Aki.STATUS_REJECTED;
                modelcomment.Comment = comment;
                modelcomment.IsAggree = islike;
                modelcomment.NIK = userjoget;
                modelcomment.Name = CurrentUser.UserProfile.Name;
                modelcomment.CreatedAt = DateTime.Now;
                _wfCommantActivityService.Add(modelcomment);

                #endregion
                dataaki.CustomField2 = Aki.STATUS_REJECTED;
                Service.Update(dataaki);
                return Ok(aktifivity);

            }
            return Ok("errorstart");
        }

        [HttpPost]
        [Route("testApi")]
        public async Task<IActionResult> testApi()
        {
            var userjoget = "910135";
            var variableAssigment = new Dictionary<string, string>();
            //  variableAssigment.Add("processDefId", _jogetProcessDefId);
            variableAssigment.Add("processId", "217_FlowAKI_AkiWorkflow1");
            variableAssigment.Add("activityId", "355_217_FlowAKI_AkiWorkflow1_AprovalUnitBisnis");
            variableAssigment.Add("username", userjoget);
            variableAssigment.Add("replaceUser", "admin");

            var assigment = _wfProcessService.Assigment(_jogetProcessDefId, variableAssigment);

            return Ok(assigment);
        }

    }
}
