﻿using System;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Domain.DTO.App;
using App.Services.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/activitytemplates")]
    //[Authorize(Policy = "ClaimBasedAuthz")]
    public class ActivityTemplatesController : BaseApiController<ActTemplate, IActivityTemplateService, ActTemplateDto, Guid>
    {
        
        public ActivityTemplatesController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService,
            IMapper mapper, 
            IActivityTemplateService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
        [Authorize(Policy = "ClaimBasedAuthz")]
        public override IActionResult Delete(Guid id)
        {
            return base.Delete(id);
        }
    }
}
