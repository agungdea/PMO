﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Domain.DTO.App;
using App.Services.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using App.Web.Models.ViewModels.App;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/wbsscheduleplanning")]
    public class WBSSchedulePlanningsController : BaseApiController<WBSSchedulePlanning, IWBSSchedulePlanningService, WBSScheduleDto, Guid>
    {

        private readonly IActivityTemplateChildService _childTemplateService;
        private readonly IActivityTemplateService _templateService;
        private readonly IWBSScheduleService _scheduleService;
        public WBSSchedulePlanningsController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IWBSSchedulePlanningService service,
            IActivityTemplateChildService childTemplateService,
            IActivityTemplateService templateService,
            IWBSScheduleService scheduleService,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _childTemplateService = childTemplateService;
            _scheduleService = scheduleService;
            _templateService = templateService;
        }

        [HttpPost]
        [Route("postschedule")]
        public IActionResult Postschedule(List<WBSSchedulePlanningViewModel> listmodel)
        {

            if (!ModelState.IsValid)
                return BadRequest();

            DateTime lastFinish = new DateTime();

            for (int i = 0; i < listmodel.Count(); i++)
            {

                var modelSchedule = new WBSSchedulePlanning();

                if (i == 0)
                {
                    modelSchedule.Start = listmodel[i].Start;
                    modelSchedule.Finish = listmodel[i].Start.AddDays(double.Parse(listmodel[i].Durasi.ToString()));
                    modelSchedule.IdActTmpChild = listmodel[i].IdActTmpChild;
                    modelSchedule.IdWBSTree = listmodel[i].IdWBSTree;
                    lastFinish = modelSchedule.Finish;
                }
                else
                {
                    modelSchedule.Start = lastFinish;
                    modelSchedule.Finish = modelSchedule.Start.AddDays(double.Parse(listmodel[i].Durasi.ToString()));
                    modelSchedule.IdActTmpChild = listmodel[i].IdActTmpChild;
                    modelSchedule.IdWBSTree = listmodel[i].IdWBSTree;
                    lastFinish = modelSchedule.Finish;
                }

                Service.Add(modelSchedule);
            }


            return Ok(listmodel);
        }

        [Route("getvaluecurvasplanning/{id}")]
        public IActionResult getvaluecurvasplanning(Guid id)
        {



            var Data = new List<decimal?[]>();

            var datareal = _scheduleService.GetAll().Where(c => c.IdWBSTree.Equals(id));
            if (datareal != null)
            {
                decimal? tempaktual = 0;

                var datarealcurvas = _scheduleService.getCurvaS(id);
                var z = 0;
                Decimal?[] ChartAktual = new Decimal?[datarealcurvas.Count()];
                string[] ChartDate = new string[datarealcurvas.Count()];
                foreach (var datacurves in datarealcurvas.OrderBy(x => x.Key))
                {

                    tempaktual += datacurves.Value;
                    ChartAktual[z] = tempaktual;
                    var dateTime = new DateTime(datacurves.Key.Year, datacurves.Key.Month, datacurves.Key.Day, 0, 0, 0, DateTimeKind.Utc);
                    var dateTimeOffset = new DateTimeOffset(dateTime);
                    var unixDateTime = dateTimeOffset.ToUnixTimeSeconds();
                    var dataxx = datacurves.Key.Ticks;
                    decimal?[] a = new decimal?[2];
                    a[1] = tempaktual;
                    //  a[0] = unixDateTime;
                    const double LongAdj = 1000.0;
                    DateTime mydate = Convert.ToDateTime(datacurves.Key);
                    DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    a[0] = (Decimal)((mydate - UnixEpoch).TotalSeconds * LongAdj);
                    Data.Add(a);

                    //Data.Add(String.Format("{0:MM/dd/yyyy}", datacurves.Key), tempaktual);
                    z++;

                }

            }




            return Ok(Data.ToArray());
        }
    }
}
