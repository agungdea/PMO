﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/designators")]
    [Authorize]
    public class DesignatorsController : 
        BaseApiController<Designator, IDesignatorService, DesignatorDto, long>
    {
        private readonly IHargaService _hargaService;
        private readonly IWBSBoQService _boqService;
        public DesignatorsController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            IDesignatorService service,
            IWBSBoQService boqService,
            IUserHelper userHelper,
            IHargaService hargaService) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _hargaService = hargaService;
            _boqService = boqService;
        }

        [HttpPost]
        [Route("DatatablesByPaket")]
        public IActionResult PostDatatablesByPaket(IDataTablesRequest request, string paket)
        {
            var response = _hargaService.GetDataTablesResponsePaket(request, Mapper, paket);

            return new DataTablesJsonResult(response, true);
        }

        [Route("grup")]
        public IActionResult GetGrup(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
            {
                return BadRequest();
            }
            var grup = _hargaService.QueryGrup(query);

            return Ok(grup);
        }

        [Route("grupdesignator")]
        public IActionResult GetGrupByDesignator(string idDesignator, string query)
        {
            var grup = _hargaService.QueryGrupByDesignator(idDesignator, query);

            return Ok(grup);
        }
        public override IActionResult Delete(long id)
        {
            var designator = Service.GetById(id);
            if (designator != null)
            {

                var data = _boqService.GetAll().Where(x => x.IdDesignator.Equals(designator.IdDesignator)).Count();
                if (data != 0)
                {
                    return Ok("Designator");
                }
            }
          
            return base.Delete(id);
        }
        [Route("matauang")]
        public IActionResult GetMataUang(string query)
        {
            var mataUang = _hargaService.QueryMataUang(query);

            return Ok(mataUang);
        }


        [Route("designator")]
        public IActionResult GetDesignator(string query)
        {
            var designators = Service.QueryDesignator(query);

            return Ok(designators);
        }
        [HttpPost]
        [Route("validatedesignator")]
        public IActionResult ValidateDesignator(string idDesignator)
        {
            var Designator = Service.GetAll().Where(x=>x.IdDesignator.Equals(idDesignator));

            if (Designator!=null)
            {
                return Ok(false);
            }

            return Ok(true);
        }
    }
}
