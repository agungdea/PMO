﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Helper;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/wbses")]
    public class WBSesController : BaseApiController<WBS, IWBSService, WBSDto, Guid>
    {
        private readonly IWBSTreeService _wbstreeservice;
        private readonly IWBSBoQService _wbsBoQService;
        private readonly IWBSBoQGrantTotalService _wbsBoQGrantTotalService;
        private readonly IJustificationService _justificationService;
        private readonly IUnitService _unitService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWBSCurrencyService _wbsCurrencyService;
        private readonly ICurrencyService _currencyService;
        public WBSesController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IWBSTreeService wbstreeservice,
            IWBSService service,
            IUserHelper userHelper,
            IJustificationService justificationService,
            IWBSBoQGrantTotalService wbsBoQGrantTotalService,
            UserManager<ApplicationUser> userManager,
            IWBSCurrencyService wbsCurrencyService,
            ICurrencyService currencyService,
             IUnitService unitService,
            IWBSBoQService wbsBoQService)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _wbsBoQService = wbsBoQService;
            _currencyService = currencyService;
            _wbsCurrencyService = wbsCurrencyService;
            _wbsBoQGrantTotalService = wbsBoQGrantTotalService;
            _userManager = userManager;
            _unitService = unitService;
            _justificationService = justificationService;
        }

        [HttpPost]
        [Route("postboq")]
        public IActionResult PostBoQ(WBSBoQForm model)
        {
            if (!ModelState.IsValid)
                return BadRequest(model);

            var item = new WBSBoQ
            {
                IdDesignator = model.Designator,
                GrupHarga = model.GrupHarga,
                HargaJasa = model.HargaJasa,
                HargaMaterial = model.HargaMaterial,
                IdWBSTree = model.IdWBSTree,
                Qty = model.Qty,
                CreatedAt = DateTime.Now,
                CreatedBy = CurrentUser.Id,
                OtherInfo = model.MataUang
            };
            //if (model.MataUang != Currency.CurrencyRp)
            //{
            //    var datacurrency = _currencyService.GetAll().First(x => x.CurrencyValue.Equals(model.MataUang));
            //    if (datacurrency != null)
            //    {
            //        var datawbscurrency = _wbsCurrencyService.GetAll().First(x => x.CurrencyId.Equals(model.MataUang));
            //    }
            //}
            var checkboqcurrency=_wbsCurrencyService.CekBoq(model.IdWBSTree, model.MataUang);
            if (checkboqcurrency)
            {
                item = _wbsBoQService.AddOrUpdate(item);
               
                return Ok(item);
            }

            else {
                
                return Ok("BoqCurrencies");

            }
           
        }

        [HttpGet]
        [Route("Datatables")]
        public override IActionResult GetDataTables(IDataTablesRequest request)
        {
            var user = _userManager.FindByIdAsync(CurrentUser.Id).Result;

            var claims = _userManager.GetClaimsAsync(user).Result;

            var unitId = claims
                .FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;
            if (unitId == null)
            {
                return BadRequest();
            }
            var conten = _unitService.GetUnitLevel(unitId);
            var unitDisposisiId = claims
             .FirstOrDefault(x => x.Type == ApplicationUser.UNIT_DISPO_CLAIMS)?.Value;
            if (unitDisposisiId != null)
            {
                var contendisposisi = _unitService.GetUnitLevel(unitDisposisiId);
                conten.AddRange(contendisposisi);
            }
            var admin = User.IsAdministrator();
            var response = Service.GetDataTablesWBS(conten, request, admin);

            return new DataTablesJsonResult(response, true);
        }
        [Authorize(Policy = "ClaimBasedAuthz")]
        public override IActionResult Delete(Guid id)
        {
            var cekjustifikasi = _justificationService.GetAll().Where(x => x.WBSId.Equals(id)).Count();

            if (cekjustifikasi > 0)
            {
                return Ok("Justification");
            }
            return base.Delete(id);

        }
        [Authorize(Policy = "ClaimBasedAuthz")]
        [HttpDelete]
        [Route("deleteboq")]
        public IActionResult DeleteBoQ(WBSBoQForm model)
        {
            var item = new WBSBoQ
            {
                IdDesignator = model.Designator,
                GrupHarga = model.GrupHarga,
                IdWBSTree = model.IdWBSTree,
                OtherInfo = model.MataUang
            };

            item = _wbsBoQService.Delete(item);

            return Ok(item);
        }
        [HttpPost]
        [Route("getJustifikasiCurrency/{id}")]
        public DataTablesJsonResult GetWBSJustifikasiCurrency(Guid id, IDataTablesRequest request)
        {
            var response = _wbsBoQGrantTotalService.GetTotalPerCurrency(id, request);
            return new DataTablesJsonResult(response, true);
        }
        [HttpPost]
        [Route("getJustifikasiPerLocationCurrency/{id}")]
        public DataTablesJsonResult GetWBSJustifikasiPerLocationCurrency(Guid id, IDataTablesRequest request)
        {
            var response = _wbsBoQGrantTotalService.GetWBStotalPerlocation(id, request);
            return new DataTablesJsonResult(response, true);
        }
        [HttpPost]
        [Route("getJustifikasiPerLocation/{id}")]
        public DataTablesJsonResult getJustifikasiPerLocation(Guid id, IDataTablesRequest request)
        {
            var response = _wbsBoQGrantTotalService.GetWBsPerlocation(id, request);
            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        [Route("getkegiatan")]
        public IActionResult GetKegiatan(string query)
        {

            var user = _userManager.FindByIdAsync(CurrentUser.Id).Result;

            var claims = _userManager.GetClaimsAsync(user).Result;

            var unitId = claims
                .FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;
            if (unitId == null)
            {
                return BadRequest();
            }
            var conten =_unitService.GetUnitLevel(unitId);
            var kegiatan = from data in Service.GetAll().Where(x => conten.Contains(x.Unit))
                           select new SelectWBSItem
                           {
                               Id = data.Id.ToString(),
                               Text = data.NamaKegiatan
                           };
            if (kegiatan == null)
            {
                return BadRequest();
            }

            if (query != null)
            {
                return Ok(kegiatan.Where(x => x.Text.ToLower().Contains(query.ToLower())));
            }

            return Ok(kegiatan);
        }
    }
}
