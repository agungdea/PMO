using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Domain.DTO.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using App.Web.Models.ViewModels.App;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/WBSGrantTotals")]
    public class WBSGrantTotalsController : BaseApiController<WBSBoQGrantTotal, IWBSBoQGrantTotalService, WBSBoQGrantTotalDto, Guid>
    {
        private readonly IWBSTreeService _wbsTreeService;
        private readonly ICurrencyService _currencyService;
        private readonly IWBSCurrencyService _wbsCurrencyService;
        public WBSGrantTotalsController(IHttpContextAccessor httpContextAccessor,
            IUserService userService, IMapper mapper,
            IWBSBoQGrantTotalService service,
            IWBSTreeService wbsTreeService,
            IWBSCurrencyService wbsCurrencyService,
            ICurrencyService currencyService,
        IUserHelper userHelper) : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _wbsTreeService = wbsTreeService;
            _currencyService = currencyService;
            _wbsCurrencyService = wbsCurrencyService;
        }

        [HttpGet]
        [Route("getTotalWbstree")]
        public IActionResult GetTotal(Guid? idWbstree)
        {
            var total = Service.GetAll(x => x.Currency, x => x.WBSTree).Where(x => x.IdWBStree == idWbstree).ToList();

            var model = new List<WBSGrandTotalViewModel>();

            foreach (var item in total)
            {
                var tempModel = new WBSGrandTotalViewModel();

                tempModel.CurrencyName = item.Currency.CurrencyValue;
                tempModel.BeforeConvertion = item.BoQBeforeConverstion;
                tempModel.AfterConvertion = item.BoQTotalConversion;

                tempModel.Rate = _wbsCurrencyService.GetAll(x => x.Currency).Where(x => x.WBSId == item.WBSTree.IdWBS && x.CurrencyId == item.CurrencyId).Select(x => x.Rate).FirstOrDefault();

                model.Add(tempModel);
            }

            return Ok(model);
        }

        [HttpGet]
        [Route("getTotalWbs")]
        public IActionResult WbsGetTotal(Guid IdWb)
        {
            var listguid = new List<Guid>();
            var ListGuid = _wbsTreeService.getListguid(IdWb, listguid);
            if (ListGuid.Count() == 0)
            {
                var IdWBStree = _wbsTreeService.GetById(IdWb);

                if (IdWBStree != null && IdWBStree.Type == WBS.WBS_TYPE_LOCATION)
                {
                    listguid.Add(IdWBStree.Id);
                }
            }

            var data = from GrantTotal in Service.GetAll().Where(x => listguid.Contains(x.IdWBStree))
                       join curr in _currencyService.GetAll() on GrantTotal.CurrencyId equals curr.Id
                       join wbstree in _wbsTreeService.GetAll() on GrantTotal.IdWBStree equals wbstree.Id
                       select new
                       {
                           Total = GrantTotal.BoQBeforeConverstion == null ? GrantTotal.BoQTotalConversion : GrantTotal.BoQBeforeConverstion,
                           currency = curr.CurrencyValue
                       };
            var datasum = from datatotal in data
                          group
                           datatotal by new
                           {
                               datatotal.currency

                           } into g
                          select new
                          {
                              Total = g.Sum(x => x.Total),
                              currency = g.Key.currency
                          };
            return Ok(datasum.ToList());
        }

        [HttpGet]
        [Route("getTotalWbsforSAP")]
        public IActionResult WbsGetTotalforSAP(Guid IdWb)
        {
            var data = from GrantTotal in Service.GetAll()
                       join curr in _currencyService.GetAll() on GrantTotal.CurrencyId equals curr.Id
                       join wbstree in _wbsTreeService.GetAll() on GrantTotal.IdWBStree equals wbstree.Id
                       where wbstree.IdWBS.Equals(IdWb)
                       select new
                       {
                           Total = GrantTotal.BoQBeforeConverstion == null ? GrantTotal.BoQTotalConversion : GrantTotal.BoQBeforeConverstion,
                           currency = curr.CurrencyValue
                       };
            var datasum = from datatotal in data
                          group
                           datatotal by new
                           {
                               datatotal.currency

                           } into g
                          select new
                          {
                              Total = g.Sum(x => x.Total),
                              currency = g.Key.currency
                          };
            return Ok(datasum.ToList());
        }
    }
}