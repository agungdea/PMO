﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using DataTables.AspNet.Core;
using App.Web.Helper;
using Microsoft.AspNetCore.Identity;
using App.Domain.Models.Identity;
using System.Collections.Generic;
using DataTables.AspNet.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace App.Web.Areas.Admin.Controllers.Api.App
{

    [Produces("application/json")]
    [Route("admin/api/justifications")]
    public class JustificationsController : BaseApiController<Justification, IJustificationService, JustificationDto, Guid>
    {
        private readonly string sapUsername;
        private readonly string sapPassword;
        private readonly FileHelper _fileHelper;
        private readonly PdfGenerator _filePDF;
        private readonly ConfigHelper _config;
        private readonly string _justificationDir;
        private readonly IUnitService _unitService;
        private readonly IWBSService _wbsService;
        private readonly IWBSBoQGrantTotalService _wbsBoQGrantTotalService;
        private readonly string _apiUrlEndPoint;
        public JustificationsController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IHostingEnvironment env,
            IMapper mapper,
            IJustificationService service,
            PdfGenerator filePDF,
            FileHelper fileHelper,
            ConfigHelper config,
            IWBSService wbsService,
            IUserHelper userHelper,

            IWBSBoQGrantTotalService wbsBoQGrantTotalService,
            IUnitService unitService)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _unitService = unitService;
            _fileHelper = fileHelper;
            _config = config;
            _wbsService = wbsService;
            _wbsBoQGrantTotalService = wbsBoQGrantTotalService;
            _justificationDir = _config.GetConfig("justification.upload.directory");
            _filePDF = filePDF;
            sapUsername = _config.GetConfig("justification.sap.auth.username");
            sapPassword = _config.GetConfig("justification.sap.auth.password");
            _apiUrlEndPoint = _config.GetConfig("sap.rest.api.url");
        }

        [HttpPost]
        [Route("justificationRejected/{Id}")]
        public IActionResult justificationRejected(Guid Id)
        {
            var data = Service.GetById(Id);
            if (data == null)
            {
                return NotFound();
            }
            data.JustificationStatus = Justification.STATUS_REJECTED;
            Service.Update(data);
            return Ok(data);
        }

        [HttpPost]
        [Route("justificationPR/{Id}")]
        public async Task<IActionResult> justificationPR(Guid Id, JustificationSapForm model)
        {
            //default plant="T741"
            //default PURCH_ORG="T741"
            //default G_L_ACCT = "0051342001"
            //default PUR_GROUP = "260"
            // default FUNDS_CTR /COST_CTR  = "T741Z11"
            //USERNAME = "790126"
            //PASSWORD = "telkom2017"
            //url=http://epcapi-pilot2.aon.telkom.co.id/index.php?r=services/brc/
            var datajustifikasi = Service.GetById(Id);
            if (datajustifikasi == null)
            {
                return NotFound();
            }
            var wbsid = datajustifikasi.WBSId;
            var getwbs = _wbsService.GetById(datajustifikasi.WBSId);

            var gettotalboq = _wbsBoQGrantTotalService.GetWBsPerlocationlist(wbsid);
            if (getwbs == null)
            {
                return NotFound();
            }
            var datasend = new SapViewModel();


            #region Authen
            var data = new USER_AUTH();
            data.USERNAME = sapUsername;
            data.PASSWORD = sapPassword;
            datasend.USER_AUTH = data;
            #endregion

            #region Each Data SAP PR
            var REQUISITIONITEMSList = new List<REQUISITION_ITEMS>();
            var REQUISITION_ACCOUNT_ASSIGNMENTList = new List<REQUISITION_ACCOUNT_ASSIGNMENT>();
            var counteachx = 0;
            foreach (var totalboq in gettotalboq)
            {
                counteachx = counteachx + 10;
                var REQUISITIONITEMS = new REQUISITION_ITEMS();
                REQUISITIONITEMS.PREQ_NO = "";
                REQUISITIONITEMS.PREQ_ITEM = counteachx.ToString().PadLeft(4, '0');
                REQUISITIONITEMS.DOC_TYPE = "NB";
                REQUISITIONITEMS.CREATED_BY = "";
                REQUISITIONITEMS.PUR_GROUP = model.Purgroup;
                REQUISITIONITEMS.PREQ_NAME = "Test2";
                REQUISITIONITEMS.PREQ_DATE = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString("d2") + "-" + DateTime.Now.Day.ToString("d2");
                REQUISITIONITEMS.SHORT_TEXT = getwbs.NamaKegiatan;
                REQUISITIONITEMS.MATERIAL = "";
                REQUISITIONITEMS.PUR_MAT = "";
                REQUISITIONITEMS.PLANT = model.Plant;
                REQUISITIONITEMS.STORE_LOC = "";

                REQUISITIONITEMS.MAT_GRP = "";
                REQUISITIONITEMS.SUPPL_PLNT = "";
                REQUISITIONITEMS.QUANTITY = "1.000";
                REQUISITIONITEMS.UNIT = "";
                REQUISITIONITEMS.DEL_DATCAT = "";
                REQUISITIONITEMS.DELIV_DATE = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString("d2") + "-" + DateTime.Now.Day.ToString("d2");
                REQUISITIONITEMS.REL_DATE = "0000-00-00";
                REQUISITIONITEMS.GR_PR_TIME = "0";
                REQUISITIONITEMS.C_AMT_BAPI = totalboq.Total;
                REQUISITIONITEMS.PRICE_UNIT = "0";
                REQUISITIONITEMS.ITEM_CAT = "0";
                if (datajustifikasi.JenisPengeluaran == SpendingType.OPEX)
                {
                    REQUISITIONITEMS.ACCTASSCAT = "K";
                    REQUISITIONITEMS.TRACKINGNO = "OPEX PROC";
                }
                if (datajustifikasi.JenisPengeluaran == SpendingType.CAPEX)
                {
                    REQUISITIONITEMS.ACCTASSCAT = "R";
                    REQUISITIONITEMS.TRACKINGNO = "CAPEX PROC";
                }
                REQUISITIONITEMS.DISTRIB = "";
                REQUISITIONITEMS.PART_INV = "";
                REQUISITIONITEMS.GR_IND = "";
                REQUISITIONITEMS.GR_NON_VAL = "";
                REQUISITIONITEMS.IR_IND = "";
                REQUISITIONITEMS.DES_VENDOR = "";
                REQUISITIONITEMS.FIXED_VEND = "";
                REQUISITIONITEMS.PURCH_ORG = model.Purchorg;
                REQUISITIONITEMS.AGREEMENT = "";
                REQUISITIONITEMS.AGMT_ITEM = "00000";
                REQUISITIONITEMS.INFO_REC = "";
                REQUISITIONITEMS.QUOTA_ARR = "";
                REQUISITIONITEMS.QUOTARRITM = "000";
                REQUISITIONITEMS.MRP_CONTR = "";
                REQUISITIONITEMS.BOMEXPL_NO = "";
                REQUISITIONITEMS.LAST_RESUB = "0000-00-00";
                REQUISITIONITEMS.RESUBMIS = "0";
                REQUISITIONITEMS.NO_RESUB = "0";
                REQUISITIONITEMS.VAL_TYPE = "";
                REQUISITIONITEMS.SPEC_STOCK = "";
                REQUISITIONITEMS.PO_UNIT = "";
                REQUISITIONITEMS.REV_LEV = "";
                REQUISITIONITEMS.PCKG_NO = "0000000000";
                REQUISITIONITEMS.KANBAN_IND = "";
                REQUISITIONITEMS.PO_PRICE = "";
                REQUISITIONITEMS.INT_OBJ_NO = "000000000000000000";
                REQUISITIONITEMS.PROMOTION = "";
                REQUISITIONITEMS.BATCH = "";
                REQUISITIONITEMS.VEND_MAT = "";
                REQUISITIONITEMS.ORDERED = "0.000";
                REQUISITIONITEMS.CURRENCY = totalboq.CurrencyCode;
                REQUISITIONITEMS.MANUF_PROF = "";
                REQUISITIONITEMS.MANU_MAT = "";
                REQUISITIONITEMS.MFR_NO = "";
                REQUISITIONITEMS.MFR_NO_EXT = "";
                REQUISITIONITEMS.DEL_DATCAT_EXT = "";
                REQUISITIONITEMS.ITEM_CAT_EXT = "";
                REQUISITIONITEMS.PREQ_UNIT_ISO = "UNT";
                REQUISITIONITEMS.PO_UNIT_ISO = "";
                REQUISITIONITEMS.GENERAL_RELEASE = "";
                REQUISITIONITEMS.MATERIAL_EXTERNAL = "";
                REQUISITIONITEMS.MATERIAL_GUID = "";
                REQUISITIONITEMS.MATERIAL_VERSION = "";
                REQUISITIONITEMS.PUR_MAT_EXTERNAL = "";
                REQUISITIONITEMS.PUR_MAT_GUID = "";
                REQUISITIONITEMS.PUR_MAT_VERSION = "";
                REQUISITIONITEMS.REASON_BLOCKING = "";
                REQUISITIONITEMS.PROCURING_PLANT = "";
                REQUISITIONITEMS.CMMT_ITEM = "001";
                REQUISITIONITEMS.FUNDS_CTR = model.CostCenter;//done
                REQUISITIONITEMS.FUND = "";
                REQUISITIONITEMS.RES_DOC = "";
                REQUISITIONITEMS.RES_ITEM = "000";
                REQUISITIONITEMS.FUNC_AREA = "";
                REQUISITIONITEMS.GRANT_NBR = "";
                REQUISITIONITEMS.FUND_LONG = "";
                REQUISITIONITEMS.BUDGET_PERIOD = "";
                REQUISITIONITEMSList.Add(REQUISITIONITEMS);


                var REQUISITIONACCOUNTASSIGNMENT = new REQUISITION_ACCOUNT_ASSIGNMENT();
                REQUISITIONACCOUNTASSIGNMENT.PREQ_NO = null;
                REQUISITIONACCOUNTASSIGNMENT.PREQ_ITEM = "00010";
                REQUISITIONACCOUNTASSIGNMENT.SERIAL_NO = "00";
                REQUISITIONACCOUNTASSIGNMENT.DELETE_IND = "";
                REQUISITIONACCOUNTASSIGNMENT.CREATED_ON = "0000-00-00";
                REQUISITIONACCOUNTASSIGNMENT.CREATED_BY = "";
                REQUISITIONACCOUNTASSIGNMENT.PREQ_QTY = "0.000";
                REQUISITIONACCOUNTASSIGNMENT.DISTR_PERC = "0.0";
                REQUISITIONACCOUNTASSIGNMENT.G_L_ACCT = model.GLAccount;//done
                REQUISITIONACCOUNTASSIGNMENT.BUS_AREA = "";
                REQUISITIONACCOUNTASSIGNMENT.COST_CTR = model.CostCenter;
                REQUISITIONACCOUNTASSIGNMENT.SD_DOC = "";
                REQUISITIONACCOUNTASSIGNMENT.BUS_AREA = "";
                REQUISITIONACCOUNTASSIGNMENT.SDOC_ITEM = "000000";
                REQUISITIONACCOUNTASSIGNMENT.SCHED_LINE = "0000";


                REQUISITIONACCOUNTASSIGNMENT.ASSET_NO = "";
                REQUISITIONACCOUNTASSIGNMENT.SUB_NUMBER = "";
                REQUISITIONACCOUNTASSIGNMENT.ORDER_NO = "";
                REQUISITIONACCOUNTASSIGNMENT.GR_RCPT = "";
                REQUISITIONACCOUNTASSIGNMENT.UNLOAD_PT = "";
                REQUISITIONACCOUNTASSIGNMENT.CO_AREA = "";
                REQUISITIONACCOUNTASSIGNMENT.TO_COSTCTR = "";
                REQUISITIONACCOUNTASSIGNMENT.CO_AREA = "";
                REQUISITIONACCOUNTASSIGNMENT.TO_PROJECT = "";
                REQUISITIONACCOUNTASSIGNMENT.COST_OBJ = "";

                REQUISITIONACCOUNTASSIGNMENT.PROF_SEGM = "0000000000";
                REQUISITIONACCOUNTASSIGNMENT.PROFIT_CTR = "";
                REQUISITIONACCOUNTASSIGNMENT.WBS_ELEM = "00000000";
                REQUISITIONACCOUNTASSIGNMENT.NETWORK = "";
                REQUISITIONACCOUNTASSIGNMENT.ROUTING_NO = "0000000000";
                REQUISITIONACCOUNTASSIGNMENT.RL_EST_KEY = "";
                REQUISITIONACCOUNTASSIGNMENT.COUNTER = "00000000";
                REQUISITIONACCOUNTASSIGNMENT.PART_ACCT = "";
                REQUISITIONACCOUNTASSIGNMENT.CMMT_ITEM = "001";
                REQUISITIONACCOUNTASSIGNMENT.REC_IND = "";
                REQUISITIONACCOUNTASSIGNMENT.FUNDS_CTR = model.CostCenter;//done
                REQUISITIONACCOUNTASSIGNMENT.FUND = "";
                REQUISITIONACCOUNTASSIGNMENT.FUNC_AREA = "";
                REQUISITIONACCOUNTASSIGNMENT.REF_DATE = "0000-00-00";

                REQUISITIONACCOUNTASSIGNMENT.CHANGE_ID = "";
                REQUISITIONACCOUNTASSIGNMENT.CURRENCY = "";
                REQUISITIONACCOUNTASSIGNMENT.PREQ_UNIT = "";
                REQUISITIONACCOUNTASSIGNMENT.WBS_ELEM_E = "";
                REQUISITIONACCOUNTASSIGNMENT.PROJ_EXT = "";
                REQUISITIONACCOUNTASSIGNMENT.ACTIVITY = "";
                REQUISITIONACCOUNTASSIGNMENT.FUNC_AREA_LONG = "";
                REQUISITIONACCOUNTASSIGNMENT.GRANT_NBR = "";
                REQUISITIONACCOUNTASSIGNMENT.CMMT_ITEM_LONG = "";
                REQUISITIONACCOUNTASSIGNMENT.RES_DOC = "";
                REQUISITIONACCOUNTASSIGNMENT.RES_ITEM = "000";
                REQUISITIONACCOUNTASSIGNMENT.FUND_LONG = "";
                REQUISITIONACCOUNTASSIGNMENT.BUDGET_PERIOD = "";
                REQUISITION_ACCOUNT_ASSIGNMENTList.Add(REQUISITIONACCOUNTASSIGNMENT);
            }
            datasend.REQUISITION_ITEMS = REQUISITIONITEMSList;
            datasend.REQUISITION_ACCOUNT_ASSIGNMENT = REQUISITION_ACCOUNT_ASSIGNMENTList;
            #endregion



            #region unknown Data SAP
            var extension = new EXTENSIONIN();
            datasend.EXTENSIONIN = extension;
            datasend.REQUISITION_ADDRDELIVERY = new REQUISITION_ADDRDELIVERY();
            datasend.REQUISITION_CONTRACT_LIMITS = new REQUISITION_CONTRACT_LIMITS();
            datasend.REQUISITION_ITEM_TEXT = new REQUISITION_ITEM_TEXT();
            datasend.REQUISITION_LIMITS = new REQUISITION_LIMITS();
            datasend.REQUISITION_SERVICES = new REQUISITION_SERVICES();
            datasend.REQUISITION_SERVICES_TEXT = new REQUISITION_SERVICES_TEXT();
            datasend.REQUISITION_SRV_ACCASS_VALUES = new REQUISITION_SRV_ACCASS_VALUES();
            #endregion

            string apiEndpoint = _apiUrlEndPoint;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            var convertjson = JsonConvert.SerializeObject(datasend);
            StringContent content = new StringContent(convertjson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(apiEndpoint, content);
            var hasil = response.Content.ReadAsStringAsync();
            var dynamicReturn = JsonConvert.DeserializeObject<dynamic>(hasil.Result);
            var datadynamicReturn = JsonConvert.DeserializeObject<Justificationreturnsap>(hasil.Result);
            if (!string.IsNullOrWhiteSpace(datadynamicReturn.item.MESSAGE_V1))
            {
                datajustifikasi.CustomField1 = Justification.STATUS_SAP_SUBMIT;
                datajustifikasi.CustomField2 = datadynamicReturn.item.MESSAGE_V1;
                Service.Update(datajustifikasi);
                return Ok(datadynamicReturn.item);
            }

            return Ok(datadynamicReturn.item);


        }
        [HttpGet]
        [Route("testWF")]
        public IActionResult jstest()
        {
            var jaskep =Service.GetById(Guid.Parse("bad037de-d90f-422c-905f-2bb3b6476ca8"));
           var data= Service.CreateWf(jaskep);

            return Ok(data);
        }

        [HttpPost]
        [Route("justificationApproved/{Id}")]
        public IActionResult justificationApproved(Guid Id)
        {
            var data = Service.GetById(Id);
            if (data == null)
            {
                return NotFound();
            }
            data.JustificationStatus = Justification.STATUS_APPROVED;
            Service.Update(data);
            return Ok(data);
        }

        [HttpPost]
        [Route("justificationNeedReview/{Id}")]
        public IActionResult justificationNeedReview(Guid Id)
        {
            var data = Service.GetById(Id);
            if (data == null)
            {
                return NotFound();
            }
            data.JustificationStatus = Justification.STATUS_NEED_REVIEW;
            Service.Update(data);
            return Ok(data);
        }

        [HttpPost]
        [Route("justificationSubmitted/{Id}")]
        public IActionResult justificationSubmitted(Guid Id)
        {
            var data = Service.GetById(Id);
            if (data == null)
            {
                return NotFound();
            }
            data.JustificationStatus = Justification.STATUS_SUBMITTED;
            Service.Update(data);
            return Ok(data);
        }

        

        public override IActionResult GetDataTables(IDataTablesRequest request)
        {
            var admin = User.IsAdministrator();
            var PurchaseRequisition = User.IsPurchaseRequisition();
            var IsUser = User.IsUser();
            //if (admin == false && PurchaseRequisition == false && IsUser == false)
            //{
            //    return NotFound();
            //}
            var getunit = UserHelper.GetUserUnit(CurrentUser.Id);
            //if (getunit == null && admin == false)
            //{
            //    return NotFound();
            //}
            var getUnitdispsisi = UserHelper.GetUserUnitBefore(CurrentUser.Id);
            var Listunit = _unitService.GetUnitLevel(getunit);
            var listUnitdispsisi = _unitService.GetUnitLevel(getunit);
            foreach (var dataunit in listUnitdispsisi)
            {
                Listunit.Add(dataunit);
            }


            //  DataTablesResponse GetDataJustification(List<string> UnitId, IDataTablesRequest request, Boolean IsAdmin, Boolean IsPurchaseRequisition, Boolean IsUser);
            var response = Service.GetDataJustification(Listunit, request, admin, PurchaseRequisition, IsUser);
            return new DataTablesJsonResult(response, true);

        }

        [HttpPost]
        [Route("AddNotes")]
        public IActionResult AddNotes(JustificationNoteForm model)
        {
            var result = Service.GetById(model.pk);

            if (result == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }


            switch (model.name)
            {
                case "NoteLatarBelakang":
                    result.NoteLatarBelakang = model.value;
                    break;
                case "NoteAspekStrategis":
                    result.NoteAspekStrategis = model.value;
                    break;
                case "NoteAspekBisnis":
                    result.NoteAspekBisnis = model.value;
                    break;
                case "NoteSpesifikasiTeknik":
                    result.NoteSpesifikasiTeknik = model.value;
                    break;
                case "NoteJumlahKebutuhan":
                    result.NoteJumlahKebutuhan = model.value;
                    break;
                case "NoteRencanaPelaksanaan":
                    result.NoteRencanaPelaksanaan = model.value;
                    break;
                case "NoteDistribusiPenggunaan":
                    result.NoteDistribusiPenggunaan = model.value;
                    break;
                case "NotePosisiPersediaan":
                    result.NotePosisiPersediaan = model.value;
                    break;
                case "NoteAnggaran":
                    result.NoteAnggaran = model.value;
                    break;
                case "NotePenutup":
                    result.NotePenutup = model.value;
                    break;
                default:
                    Console.WriteLine("InvalidValue");
                    break;
            }

            Service.Update(result);


            return Ok(result);
        }

        [Authorize(Policy = "ClaimBasedAuthz")]
        [HttpDelete("{id}")]
        public override IActionResult Delete(Guid id)
        {
            var item = Service.GetById(id);
            if (null == item)
            {
                return Json(BadRequest());
            }

            Service.Delete(item);

            if (item.BasicDesign != null || item.TOR != null || item.LampiranDasarKebutuhanBarangJasa != null)
            {
                var directory = $"{_justificationDir}/{id}";

                _fileHelper.FolderDelete(directory);
            }

            return Ok(item);
        }
    }
}
