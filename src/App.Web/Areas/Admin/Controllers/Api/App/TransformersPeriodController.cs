﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Services.App;
using App.Domain.Models.App;
using App.Domain.DTO.App;
using App.Infrastructure;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using App.Web.Models.ViewModels.App;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/transformers")]
    public class TransformersPeriodController : BaseApiController<TransformerPeriod, ITransformerPeriodService, TransformerPeriodDto, string>
    {
        private readonly ConfigHelper _configHelper;

        private readonly string _apiUrlEndPoint;

        public TransformersPeriodController(IHttpContextAccessor httpContextAccessor,
            IUserService userService, IMapper mapper,
            ConfigHelper configHelper,
            ITransformerPeriodService service, IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _configHelper = configHelper;
            _apiUrlEndPoint = _configHelper.GetConfig("transformer.rest.api.url");
        }

        [HttpGet]
        [Route("getperiod")]
        public IActionResult GetPeriod()
        {
            string apiEndpoint = _apiUrlEndPoint + "&method=getperiod";

            List<TransformerPeriod> period = new List<TransformerPeriod>();

            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiEndpoint).Result;

            string result = response.Content.ReadAsStringAsync().Result.ToString();

            var perioddynamic = JsonConvert.DeserializeObject<dynamic>(result);

            var data = perioddynamic.item;
            var datasave = new TransformerPeriod();

            foreach (var item in data)
            {
                datasave.Period = item.period;
                datasave.Display_name = item.display_name;
                var listexisting = Service.GetExistingTransformerPeriod(datasave.Period);
                if (listexisting != null && listexisting.Any(x => x.Period.Equals(item.period.ToString())))
                    continue;
                try
                {
                    Service.Add(datasave);
                }
                catch (Exception)
                {
                    continue;
                }

            }

            return Ok(true);
        }

        [HttpPost]
        [Route("getperiode")]
        public IActionResult GetPeriode(string term)
        {

            string apiEndpoint = _apiUrlEndPoint + "&method=getperiod";

            List<TransformerPeriod> period = new List<TransformerPeriod>();

            HttpClient client = new HttpClient();

            HttpResponseMessage response = client.GetAsync(apiEndpoint).Result;

            string result = response.Content.ReadAsStringAsync().Result.ToString();

            var perioddynamic = JsonConvert.DeserializeObject<dynamic>(result);

            var temp = perioddynamic.item;

            List<TransformerPeriodViewModel> item = JsonConvert.DeserializeObject<List<TransformerPeriodViewModel>>(temp.ToString());

            var dataperiode = from periode in item.Take(10)
                              select new
                              {
                                  id = periode.Period,
                                  text = periode.Display_name
                              };

            if (term != null)
            {
                dataperiode = from periode in item.Where(x => x.Display_name.ToLower().Contains(term.ToLower())).Take(10)
                              select new
                              {
                                  id = periode.Period,
                                  text = periode.Display_name
                              };

                return Ok(dataperiode.ToArray());
            }

            return Ok(dataperiode.ToArray());
        }
    }
}
