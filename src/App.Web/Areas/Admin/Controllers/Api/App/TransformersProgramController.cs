using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App.Infrastructure;
using App.Domain.Models.App;
using App.Services.App;
using App.Domain.DTO.App;
using App.Helper;
using App.Services.Identity;
using AutoMapper;
using System.Net.Http;
using Newtonsoft.Json;
using App.Web.Models.ViewModels.App;
using System.Text.RegularExpressions;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/transformers")]
    public class TransformersProgramController : BaseApiController<TransformerProgram, ITranformerProgramService, TransformerProgramDto, string>
    {
        private readonly ConfigHelper _configHelper;
        private readonly string _apiUrlEndPoint;

        public TransformersProgramController(IHttpContextAccessor httpContextAccessor, ConfigHelper configHelper, IUserService userService, IMapper mapper, ITranformerProgramService service, IUserHelper userHelper) : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _configHelper = configHelper;
            _apiUrlEndPoint = _configHelper.GetConfig("transformer.rest.api.url");
        }

        [HttpPost]
        [Route("getprogram")]
        public IActionResult GetPeriode(string si, string term)
        {

            string apiEndpoint = _apiUrlEndPoint + "&si=" + si + "&method=getprogram";
           HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync(apiEndpoint).Result;

            string result = response.Content.ReadAsStringAsync().Result.ToString();

            var Programdynamic = JsonConvert.DeserializeObject<dynamic>(result);

            var temp = Programdynamic.item;
            List<TransformerProgramViewModel> item = JsonConvert.DeserializeObject<List<TransformerProgramViewModel>>(temp.ToString());
            var dataProgram = from Si in item
                              select new
                              {
                                  Id = Si.id,
                                  Text = Si.title.ToString()
                              };
            if (term == null)
            {
                return Ok(dataProgram.ToArray());
            }
            else {
                return Ok(dataProgram.Where(x => x.Text.ToLower().Contains(term.ToLower())).ToArray());
            }

        }
    }

}