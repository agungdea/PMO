﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/wbsboqs")]
    public class WBSBoQsController : BaseApiController<WBSBoQ, IWBSBoQService, WBSBoQDto, Guid>
    {
        private readonly IDesignatorService _designatorService;
        private readonly IHargaService _hargaService;
        private readonly ExcelHelper _excelhelper;
        private readonly IWBSCurrencyService _wbsCurrencyService;

        public WBSBoQsController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IWBSBoQService service,
            IUserHelper userHelper,
            ExcelHelper excelhelper,
            IDesignatorService designatorService,
             IWBSCurrencyService wbsCurrencyService,
            IHargaService hargaService)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _excelhelper = excelhelper;
            _designatorService = designatorService;
            _wbsCurrencyService = wbsCurrencyService;
            _hargaService = hargaService;
        }

        [HttpPost]
        [Route("GetWBSBoQ")]
        public DataTablesJsonResult GetWBSBoQ(Guid id, IDataTablesRequest request)
        {
            var response = Service.GetBoQs(id, request, Mapper);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        [Route("GetWBSBoQOther")]
        public DataTablesJsonResult GetWBSBoQOther(Guid id, IDataTablesRequest request)
        {
            var response = Service.GetBoQs(id, request, Mapper, true);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        [Route("GetWBSBoQPreview")]
        public DataTablesJsonResult GetWBSBoQPreview(Guid id, IDataTablesRequest request)
        {
            var response = Service.GetBoQs(id, request, Mapper, false, true);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        [Route("GetTemplateExcel/{WbsTreeId}")]
        public IActionResult GetTemplate(Guid WbsTreeId)
        {
            string tamplate = _excelhelper.ExportBOQ(WbsTreeId);
            if (tamplate == "not ok")
            {
                return BadRequest();
            }
            return Ok(tamplate);
        }

        [HttpPost]
        [Route("PostTemplateExcel")]
        public IActionResult GetTemplate(string source)
        {
            var data = _excelhelper.ValidasiImportBOQ(source, CurrentUser.Id);
            if (data == false)
            {
                return Ok("DownloadTemplate");
            }
            var databoq = _excelhelper.ValidasiUploadMataUangBOQ(source);
            if (databoq.Count > 0)
            {
                string messqurenncy = "Mata-uang ";
                foreach (var item in databoq)
                {
                    messqurenncy += item + ",";
                }
                return Ok(messqurenncy);

            }
            string tamplate = _excelhelper.ImportBOQ(source, CurrentUser.Id);
            if (tamplate != "ok")
            {
                return Ok("DownloadTemplate");
            }
            return Ok(source);
        }

        [HttpPost]
        [Route("PostBoQLine")]
        public IActionResult PostBoQLine(WBSBoQDto form)
        {
            if (!ModelState.IsValid)
                return BadRequest(form);

           
            var checkboqcurrency = _wbsCurrencyService.CekBoq(form.IdWBSTree, form.MataUang);
            if (checkboqcurrency)
            {
                var designator = new Designator()
                {
                    IdDesignator = form.IdDesignator,
                    Deskripsi = form.Deskripsi,
                    Satuan = form.Satuan
                };

                if (!_designatorService.CheckDuplicate(designator.IdDesignator))
                {
                    _designatorService.Add(designator);
                }

                var harga = new Harga()
                {
                    IdDesignator = form.IdDesignator,
                    HargaJasa = form.HargaJasa,
                    HargaMaterial = form.HargaMaterial,
                    Lokasi = form.GrupHarga,
                    MataUang = form.MataUang
                };

                if (!_hargaService.Exist(harga.IdDesignator, harga.Lokasi))
                {
                    _hargaService.Add(harga);
                }

                var wbsBoq = new WBSBoQ()
                {
                    IdDesignator = form.IdDesignator,
                    CreatedAt = DateTime.Now,
                    CreatedBy = CurrentUser.Id,
                    IdWBSTree = form.IdWBSTree,
                    GrupHarga = form.GrupHarga,
                    HargaJasa = form.HargaJasa,
                    HargaMaterial = form.HargaMaterial,
                    Qty = form.Qty.Value,
                    OtherInfo = form.MataUang
                };

                Service.AddOrUpdate(wbsBoq);

                return Ok(wbsBoq);
            }

            else
            {

                return Ok("BoqCurrencies");

            }
        }


        [HttpDelete]
        [Route("DeleteBoQByIdWBS")]
        public IActionResult DeleteBoQByIdWBS(Guid id)
        {
            var WBSBoQ = Service.GetAll().Where(x => x.IdWBSTree == id).ToList();

            if (WBSBoQ.Any())
            {
                foreach (var item in WBSBoQ)
                {
                    Service.Delete(item);
                }
            }

            return Ok(WBSBoQ);
        }

        [HttpGet]
        [Route("getdatatablesboqperwbs/{id}")]
        public IActionResult GetDatatablesBoqperwbs(Guid id, IDataTablesRequest request)
        {
            var data = Service.GetBoqPerWbs(id, request);
            return new DataTablesJsonResult(data, true);

        }
    }
}
