﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.App;
using App.Services.Identity;
using App.Web.Models.ViewModels.App;
using App.Web.Models.ViewModels.Lemon;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Pomelo.Data.MySql;
using System;
using System.Collections.Generic;
using System.Linq;
using App.Web.Helper;
namespace App.Web.Areas.Admin.Controllers.Api.App
{
    [Produces("application/json")]
    [Route("admin/api/wbstrees")]
    public class WBSTreesController : BaseApiController<WBSTree, IWBSTreeService, WBSTreeDto, Guid>
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitService _unitService;
        private readonly IWBSService _wbsService;
        private readonly IWBSBoQService _wbsBoqService;
        private readonly IDesignatorService _designatorService;
        private readonly ConfigHelper _configHelper;
        private readonly string _DefaultEmailEndPoint;
        public WBSTreesController(
            IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper, IWBSTreeService service,
            IWBSService wbsService,
            IUnitService unitService,
            IDesignatorService designatorService,
            IWBSBoQService wbsBoqService,
             ConfigHelper configHelper,
        UserManager<ApplicationUser> userManager,
            IUserHelper userHelper) : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _wbsService = wbsService;
            _unitService = unitService;
            _userManager = userManager;
            _designatorService = designatorService;
            _wbsBoqService = wbsBoqService;
            _configHelper = configHelper;
            _DefaultEmailEndPoint = _configHelper.GetConfig("transformer.rest.api.url");

        }

        [HttpPost]
        [Route("createwbs")]
        public IActionResult PostDatawbstree(WBSTreeForm model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            WBSTree item = Mapper.Map<WBSTree>(model);

            if (model.ParentId != null)
            {
                item.ParentId = model.ParentId;
            }
            item.Type = WBS.WBS_TYPE_WBS;

            Service.Add(item);

            return Ok(item);
        }
        [HttpGet]
        [Route("getkelomokharga/{id}")]
        public IActionResult getKelomokHarga(Guid Id)
        {
            var datatree = Service.GetById(Id);
            if (datatree == null)
            {
                return NotFound();
            }
            var grup = datatree.GrupHarga;
            return Ok(grup);
        }

        [HttpGet]
        [Route("getwbstrees/{id}")]
        public IActionResult getWbstree(Guid Id)
        {
            var datatree = Service.GetPerentDataWbs(Id);
            return Ok(datatree);
        }

        [HttpGet]
        [Route("getchildwbstrees/{id}")]
        public IActionResult getWbsChildtree(Guid Id)
        {
            var datatree = Service.GetchildDataWbs(Id);
            return Ok(datatree);
        }

        [HttpGet]
        [Route("getwbschild/{id}")]
        public IActionResult getWbsalltree(Guid Id, Guid? Idperent)
        {

            if (Id == null)
            {
                return BadRequest();
            }
            var model = new WBSUnit();
            if (CurrentUser.Id == null)
            {
                return BadRequest();
            }
            var claims = _userManager.GetClaimsAsync(CurrentUser).Result;

            var unitId = claims
                          .FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;

            if (unitId == null)
            {
                return BadRequest();
            }
            var unitBagian = _unitService.GetById(unitId);

            if (unitBagian != null && unitBagian.STLevel == Unit.STLevelBagian)
                unitId = unitBagian.ParentId;
            else
                unitBagian = null;

            var unitKerja = _unitService.GetById(unitId);
            var unitBisnis = _unitService.GetById(unitKerja.ParentId);
            var wbs = _wbsService.GetById(Id);
            if (unitBisnis == null)
            {
                return BadRequest();
            }
            model.UnitBisnis = unitBisnis.NamaUnit;
            if (unitKerja == null)
            {
                return BadRequest();
            }
            model.UnitKerja = unitKerja.NamaUnit;
            model.KodeUnit = unitKerja.Id;
            if (wbs == null)
            {
                return BadRequest();
            }

            if (Idperent == null && Idperent == null)
            {

                string data = "[{\"icon\":\"fa fa-folder icon-state-warning icon-lg\",\n";
                data += $"\"id\":\"{wbs.Id}\",\n";
                data += $"\"text\":\"{wbs.NamaKegiatan}\",\n";
                data += "\"type\":\"root\",\n";
                data += "\"ParentId\":null,\n";
                data += "\"children\":true}]";

                var dynamic = JsonConvert.DeserializeObject<dynamic>(data);
                return Ok(dynamic);
            }
            var idunitbissnis = new Guid("00000000-0000-0000-0000-000000000001");
            if (Id == Idperent)
            {
                string data = "[{\"icon\":\"fa fa-folder icon-state-warning icon-lg\",\n";
                data += $"\"id\":\"{idunitbissnis}\",\n";
                data += $"\"text\":\"{unitBisnis.NamaUnit}\",\n";
                data += $"\"ParentId\":\"{Idperent}\",\n";
                data += "\"children\":true}]";
                var dynamic = JsonConvert.DeserializeObject<dynamic>(data);
                return Ok(dynamic);
            }
            var idunitkerja = new Guid("00000000-0000-0000-0000-000000000003");
            if (idunitbissnis == Idperent)
            {
                var boolechild = Service.GetAll().Where(c => c.ParentId.Equals(null) && c.IdWBS.Equals(Id)).Count() == 0 ? "\"children\":false" : "\"children\":true"; ;
                string data = "[{\"icon\":\"fa fa-folder icon-state-warning icon-lg\",\n";
                data += $"\"id\":\"{idunitkerja}\",\n";
                data += $"\"text\":\"{unitKerja.NamaUnit}\",\n";
                data += $"\"ParentId\":\"{idunitbissnis}\",\n";
                data += boolechild;
                data += "}]";
                var dynamic = JsonConvert.DeserializeObject<dynamic>(data);
                return Ok(dynamic);
            }
            if (idunitkerja == Idperent)
            {
                var x = from servicewbstree in Service.GetAll()
                        where servicewbstree.IdWBS.Equals(Id) && servicewbstree.ParentId.Equals(null)
                        select new
                        {
                            children = Service.GetAll().Where(c => c.ParentId.Equals(servicewbstree.Id)).Count() == 0 ? false : true,
                            icon = "fa fa-folder icon-state-warning icon-lg",
                            id = servicewbstree.Id.ToString(),
                            text = servicewbstree.Name,
                            ParentId = Idperent,
                        };
                return Ok(x.ToArray().OrderBy(c => c.id));
            }
            else
            {
                var x = from servicewbstree in Service.GetAll()
                        where servicewbstree.ParentId.Equals(Idperent) && servicewbstree.IdWBS.Equals(Id)
                        select new
                        {
                            children = Service.GetAll().Where(c => c.ParentId.Equals(servicewbstree.Id)).Count() == 0 ? false : true,
                            icon = Service.GetAll().Where(c => c.ParentId.Equals(servicewbstree.Id)).Count() == 0 ? "fa fa-file icon-state-success" : "fa fa-folder icon-state-warning icon-lg",
                            id = servicewbstree.Id.ToString(),
                            text = servicewbstree.Name,
                            ParentId = Idperent,
                        };
                return Ok(x.ToArray().OrderBy(c => c.id));
            }


        }
        [HttpGet]
        [Route("GetWbstree")]
        public IActionResult GetTree(Guid IdWbs)
        {
            var data = Service.GetLeveledName(IdWbs);
            var select2 = from dataselect in data
                          select new
                          {
                              text = dataselect.Value,
                              id = dataselect.Key
                          };

            return Ok(select2.ToList());
        }

        [HttpGet]
        [Route("GetLocationtree")]
        public IActionResult GetLocation(Guid ParentId)
        {
            var data = Service.GetLocation(ParentId);
            return Ok(data);
        }

        [HttpPost]
        [Route("datatablesjoin")]
        public IActionResult Datatablesjoin(Guid id, IDataTablesRequest request)
        {
            var listguid = new List<Guid>();

            var ListGuid = Service.getListguid(id, listguid);

            if (ListGuid.Count() == 0)
            {
                var IdWBStree = Service.GetById(id);

                if (IdWBStree != null && IdWBStree.Type == WBS.WBS_TYPE_LOCATION)
                {
                    listguid.Add(IdWBStree.Id);
                }               
            }

            var dataBoq = _wbsBoqService.getdtafromlist(ListGuid);
         
            var data = from BoqCustom in dataBoq
                       join designator in _designatorService.GetAll()
                       on BoqCustom.IdDesignator equals designator.IdDesignator
                       select new
                       {
                           IdBoq = BoqCustom.Id,
                           IdWBStree = BoqCustom.IdWBSTree,
                           GrupHarga = BoqCustom.GrupHarga,
                           HargaJasa = BoqCustom.HargaJasa,
                           HargaMaterial = BoqCustom.HargaMaterial,
                           Qty = BoqCustom.Qty,
                           MataUang = BoqCustom.OtherInfo,
                           Deskripsi = designator.Deskripsi,
                           IdDesignator = BoqCustom.IdDesignator,
                           Satuan = designator.Satuan,

                       };

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value) ? data : data.Where(_item => _item.Deskripsi.ToLower().Contains(request.Search.Value.ToLower()) || _item.IdBoq.ToString().ToLower().Contains(request.Search.Value.ToLower()));

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request, data.Count(), filteredData.Count(), dataPage);

            return new DataTablesJsonResult(response, true);
        }

        [HttpDelete]
        [Route("delete/{id}")]
        public override IActionResult Delete(Guid id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var getbyid = Service.GetById(id);
            
            Service.Delete(getbyid);

            return Ok(id);
        }
        [HttpGet]
        [Route("testlemon")]
        public IActionResult testlemon()
        {
            List<LemonViewModel> listdata = new List<LemonViewModel>();
            ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
            var data = context.GetAllUser(_DefaultEmailEndPoint);
            return Ok(data);
        }

    }
}
