﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace App.Web.Areas.Admin.Controllers.Api
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/tests")]
    public class TestApiController : Controller
    {
        public IActionResult Post([FromBody] dynamic data)
        {
            var request = Request;
            return Ok(data);
        }
    }
}
