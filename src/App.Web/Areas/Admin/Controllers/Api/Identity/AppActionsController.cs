﻿using App.Domain.DTO.Identity;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace App.Web.Areas.Admin.Controllers.Api.Identity
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/appactions")]
    [Authorize(Roles = ApplicationRole.AdminAndSuperadmin)]
    public class AppActionsController : BaseCacheApiController<AppAction, IAppActionService, AppActionDto, string>
    {
        public AppActionsController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            IAppActionService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
    }
}
