﻿using App.Domain.DTO.Identity;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Identity;
using App.Web.Helper;
using App.Web.Models.ViewModels.Lemon;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DataTables.AspNet.Core;
using DataTables.AspNet.AspNetCore;
using App.Services.Utils;
using System.Linq.Dynamic.Core;

namespace App.Web.Areas.Admin.Controllers.Api.Identity
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/users")]
    [Authorize(Roles = ApplicationRole.AdminAndSuperadmin)]
    public class UsersController : BaseApiController<ApplicationUser, IUserService, ApplicationUserDto, string>
    {
        private readonly ConfigHelper _configHelper;
        private readonly string _DefaultEmailEndPoint;
        public UsersController(IHttpContextAccessor httpContextAccessor,
            IUserService userService,
            IMapper mapper,
            IUserService service,
            ConfigHelper configHelper,
            IUserHelper userHelper)
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            Includes = new Expression<Func<ApplicationUser, object>>[1];
            Includes[0] = (x => x.UserProfile);
            _configHelper = configHelper;
            _DefaultEmailEndPoint = _configHelper.GetConfig("lemon.defaut.email");
        }

        [HttpGet]
        [Route("getuserfromlemon")]
        public IActionResult userlemonTelkom(string query)
        {
            List<LemonViewModel> listdata = new List<LemonViewModel>();
            ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
            var data = context.GetAllUser(_DefaultEmailEndPoint);
            if (!string.IsNullOrWhiteSpace(query))
            {
                var user = data.Where(x => x.nik.ToLower().Contains(query.ToLower()) || x.nama.ToLower().Contains(query.ToLower()) || x.Jabatan.ToLower().Contains(query.ToLower())).Take(10);
                return Ok(user.OrderBy(x => x.nama));
            }
            else
            {
                var user = data.Take(10).OrderBy(x=>x.nama);
                return Ok(user);
            }
            // return Ok(data);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("useraprovalTelkom")]
        public IActionResult useraprovalTelkom(string term)
        {
            List<LemonViewModel> listdata = new List<LemonViewModel>();
            ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
            var user2 = context.GetAllUser(_DefaultEmailEndPoint);
            Includes = new Expression<Func<ApplicationUser, object>>[1];
            Includes[0] = (x => x.UserProfile);
            var data = from datauserdb in Service.GetAll(Includes)
                           //join userlemon2 in user2 on datauserdb.UserName equals userlemon2.nik
                       select new
                       {
                          id= datauserdb.UserName,
                           text = context.GetbyNikUser(datauserdb.UserName).Count == 0 ? "(" + datauserdb.UserName + ") - (" + datauserdb.UserProfile.Name + ")" : "(" + datauserdb.UserName + ") - (" + datauserdb.UserProfile.Name + ") - (" + context.GetbyNikUser(datauserdb.UserName).First().Jabatan + ")"

                       };
            if (string.IsNullOrWhiteSpace(term))
            {
                return Ok(data.Take(10).OrderBy(x => x.text));
            }
            return Ok(data.Where(x => x.text.ToLower().Contains(term.ToLower())).Take(10).OrderBy(x=>x.text));
        }
        [AllowAnonymous]
        public override IActionResult PostDataTables(IDataTablesRequest request)
        {
            List<LemonViewModel> listdata = new List<LemonViewModel>();
            ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
            var user = context.GetAllUser(_DefaultEmailEndPoint);
            Includes = new Expression<Func<ApplicationUser, object>>[1];
            Includes[0] = (x => x.UserProfile);
            
            var data = from datauserdb in Service.GetAll(Includes)
                       select new
                       {
                           datauserdb.Id,
                           datauserdb.UserName,
                           datauserdb.Email,
                           datauserdb.EmailConfirmed,
                           name = datauserdb.UserProfile.Name,
                           jabatan = context.GetbyNikUser(datauserdb.UserName).Count == 0 ? "-" : context.GetbyNikUser(datauserdb.UserName).First().Jabatan

                       };

            var filteredData = data.AsQueryable();
            var searchQuery = request.Search.Value;
            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            return new DataTablesJsonResult(DataTablesResponse.Create(request, filteredData.Count(), filteredData.Count(), dataPage), true);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("GetDataFromNIK")]
        public IActionResult GetDataFromNIK(IDataTablesRequest request)
        {

            List<LemonViewModel> listdata = new List<LemonViewModel>();
            ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
            var user = context.GetAllUser(_DefaultEmailEndPoint);
            Includes = new Expression<Func<ApplicationUser, object>>[1];
            Includes[0] = (x => x.UserProfile);
            int n;
            var data = from datauserdb in Service.GetAll(Includes).Where(x => int.TryParse(x.UserName, out n) == true)
                       select new
                       {
                           id= datauserdb.UserName,
                           datauserdb.Email,
                           datauserdb.EmailConfirmed,
                           name = datauserdb.UserProfile.Name,
                           jabatan= context.GetbyNikUser(datauserdb.UserName).Count==0 ? "-" : context.GetbyNikUser(datauserdb.UserName).First().Jabatan
                       };
            var filteredData = data.Where(x=>x.jabatan != "-").AsQueryable();
            var searchQuery = request.Search.Value;
            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            return new DataTablesJsonResult(DataTablesResponse.Create(request, filteredData.Count(), filteredData.Count(), dataPage), true);

        }

        public IActionResult GetDataFromJabatan(IDataTablesRequest request)
        {

            List<LemonViewModel> listdata = new List<LemonViewModel>();
            ContextLemon context = HttpContext.RequestServices.GetService(typeof(Helper.ContextLemon)) as ContextLemon;
            var user = context.GetAllUser(_DefaultEmailEndPoint);
            Includes = new Expression<Func<ApplicationUser, object>>[1];
            Includes[0] = (x => x.UserProfile);
            int n;
            var data = from datauserdb in Service.GetAll(Includes).Where(x => int.TryParse(x.UserName, out n) == true)
                       select new
                       {
                           Id = context.GetbyNikUser(datauserdb.UserName).Count == 0 ? "-" : context.GetbyNikUser(datauserdb.UserName).First().kdjbatan.ToString(),
                           datauserdb.Email,
                           datauserdb.EmailConfirmed,
                           name = datauserdb.UserProfile.Name,
                           jabatan = context.GetbyNikUser(datauserdb.UserName).Count == 0 ? "-" : context.GetbyNikUser(datauserdb.UserName).First().Jabatan
                       };
            var filteredData = data.Where(x => x.jabatan != "-").AsQueryable();
            var searchQuery = request.Search.Value;
            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            return new DataTablesJsonResult(DataTablesResponse.Create(request, filteredData.Count(), filteredData.Count(), dataPage), true);

        }
    }
}
