﻿using App.Domain.Models.Identity;
using App.Services.Identity;
using App.Web.Models.ViewModels.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace App.Web.Areas.Admin.Controllers.Api.Identity
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/claims")]
    [Authorize(Roles = ApplicationRole.AdminAndSuperadmin)]
    public class ClaimsController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IAppActionService _appActionService;
        private readonly IMasterClaimService _masterClaimService;

        public ClaimsController(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IAppActionService appActionService,
            IMasterClaimService masterClaimService)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _appActionService = appActionService;
            _masterClaimService = masterClaimService;
        }

        [HttpPost]
        [Route("AssignClaim")]
        public async Task<IActionResult> AssignClaim(AssignClaimForm model)
        {
            var request = Request;
            try
            {
                if (model.Type == ClaimType.User)
                {
                    var user = await _userManager.FindByIdAsync(model.Id);

                    if (user == null)
                        return Ok(new
                        {
                            Assign = "failed",
                            Data = "User not found"
                        });

                    var currentClaims = await _userManager.GetClaimsAsync(user);

                    await AddRemoveClaim(currentClaims, model, user);

                    return Ok(new
                    {
                        Assign = "success",
                        Data = await _userManager.GetClaimsAsync(user)
                    });
                }
                else if (model.Type == ClaimType.Role)
                {
                    var role = await _roleManager.FindByIdAsync(model.Id);

                    if (role == null)
                        return Ok(new
                        {
                            Assign = "failed",
                            Data = "Role not found"
                        });

                    var currentClaims = await _roleManager.GetClaimsAsync(role);

                    await AddRemoveClaim(currentClaims, model, role);

                    return Ok(new
                    {
                        Assign = "success",
                        Data = await _roleManager.GetClaimsAsync(role)
                    });
                }
                else
                {
                    var appAction = _appActionService.GetById(model.Id);

                    if (appAction == null)
                        return Ok(new
                        {
                            Assign = "failed",
                            Data = "AppAction not found"
                        });

                    var currentClaims = _appActionService
                        .GetClaims(model.Id)
                        .Select(x => new Claim(x.ClaimType, x.ClaimValue))
                        .ToList();

                    await AddRemoveClaim(currentClaims, model, appAction);

                    return Ok(new
                    {
                        Assign = "success",
                        Data = _appActionService
                            .GetClaims(model.Id)
                            .Select(x => new Claim(x.ClaimType, x.ClaimValue))
                            .ToList()
                    });
                }
            }
            catch (Exception ex)
            {
                return Ok(new
                {
                    Assign = "failed",
                    Data = "Internal server error",
                    Exception = ex
                });
            }
        }

        private async Task AddRemoveClaim(IList<Claim> currentClaims, AssignClaimForm model, object data)
        {
            foreach (var claim in currentClaims
                .Where(x => x.Type.StartsWith(MasterClaimService.CLAIM_TEXT))
                .ToList())
            {
                if (model.Claims != null && model.Claims.Any(x => x.Type == claim.Type && x.Value == claim.Value))
                {
                    model.Claims.RemoveAll(x => x.Type == claim.Type && x.Value == claim.Value);
                }
                else
                {
                    switch (model.Type)
                    {
                        case ClaimType.User:
                            await _userManager.RemoveClaimAsync((ApplicationUser)data, claim);
                            break;
                        case ClaimType.Role:
                            await _roleManager.RemoveClaimAsync((ApplicationRole)data, claim);
                            break;
                        default:
                            _appActionService.RemoveClaim((AppAction)data, claim);
                            break;
                    }
                }
            }

            if (model.Claims != null)
            {
                foreach (var claim in model.Claims)
                {
                    var item = new Claim(claim.Type, claim.Value);
                    switch (model.Type)
                    {
                        case ClaimType.User:
                            await _userManager.AddClaimAsync((ApplicationUser)data, item);
                            break;
                        case ClaimType.Role:
                            await _roleManager.AddClaimAsync((ApplicationRole)data, item);
                            break;
                        default:
                            _appActionService.AddClaim((AppAction)data, item);
                            break;
                    }
                }
            }
        }
    }
}
