using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Core;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace App.Web.Areas.Admin.Controllers.Api.Core
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/log")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class LogController : BaseApiController<Log, ILogService, LogDto, int>
    {
        public LogController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper,
            ILogService service,
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
    }
}