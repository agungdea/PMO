using App.Domain.DTO.Core;
using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using App.Helper;
using App.Infrastructure;
using App.Services.Core;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace App.Web.Areas.Admin.Controllers.Api.Core
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/emails")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class EmailsController : BaseApiController<EmailArchieve, IEmailArchieveService, EmailArchieveDto, Guid>
    {
        private readonly MailingHelper _mailHelper;

        public EmailsController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper,
            IEmailArchieveService service,
            IUserHelper userHelper,
            MailingHelper mailHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            _mailHelper = mailHelper;
        }

        [HttpPost]
        [Route("SendEmail/{id}")]
        public IActionResult SendEmail(Guid id)
        {
            var email = Service.GetById(id);
            
            if(email == null)
            {
                return NotFound();
            }

            var result = _mailHelper.SendEmail(email).Result;

            return Ok(result);
        }
    }
}