﻿using App.Domain.DTO.Workflow;
using App.Domain.Models.Identity;
using App.Domain.Models.Workflow;
using App.Helper;
using App.Infrastructure;
using App.Services.Identity;
using App.Services.Workflow;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace App.Web.Areas.Admin.Controllers.Api.Workflow
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/wfactivityviews")]
    [Authorize(Roles = ApplicationRole.Administrator)]
    public class WfActivityViewsController  :
         BaseApiController<WfActivityView, IWfActivityViewService, WfActivityViewDto, string>
    {
        public WfActivityViewsController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            IWfActivityViewService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
        }
    }
}
