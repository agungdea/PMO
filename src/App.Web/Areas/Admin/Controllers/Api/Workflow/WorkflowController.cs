﻿using App.Domain.Models.App;
using App.Domain.Models.Identity;
using App.Helper;
using App.Services.App;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Areas.Admin.Controllers.Api.Workflow
{
    [Area("Admin")]
    [Produces("application/json")]
    [Route("admin/api/workflow")]
    public class WorkflowController : Controller
    {
        private readonly MailingHelper _emailHelper;
        private readonly ConfigHelper _config;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJustificationService _justificationService;
        private readonly string _justificationDefId;

        public WorkflowController(MailingHelper emailHelper,
            ConfigHelper config,
            UserManager<ApplicationUser> userManager,
            IJustificationService justificationService)
        {
            _emailHelper = emailHelper;
            _config = config;
            _userManager = userManager;
            _justificationService = justificationService;

            _justificationDefId = _config.GetConfig("joget.wf.justification.process.def.id");
        }

        [Route("postnotification")]
        public IActionResult PostNotification([FromBody] dynamic data)
        {
            var request = Request;
            return Ok(data);
        }

        [Route("postnewactivity")]
        public async Task<IActionResult> PostNewActivity([FromBody] dynamic data)
        {
            var processName = data.processName.ToString();
            var processDefId = data.processDefId.ToString().Replace("#", ":");

            if(data.assignees != null)
            {
                var assigness = new List<string>();

                JToken token = JToken.Parse(JsonConvert.SerializeObject(data.assignees));

                if (token is JArray)
                {
                    assigness.AddRange(token.ToObject<List<string>>());
                }
                else
                {
                    assigness.Add(token.ToObject<string>());
                }
                
                var variables = JsonConvert.DeserializeObject<List<dynamic>>(JsonConvert.SerializeObject(data.variables));
                string docId = "";
                string docName = "";
                string callbackUrl = "";

                foreach(var variable in variables)
                {
                    if (variable.id == "documentId")
                    {
                        docId = variable.val.ToString();
                    }
                    if (variable.id == "documentName")
                    {
                        docName = variable.val.ToString();
                    }
                    if (variable.id == "callbackUrl")
                    {
                        callbackUrl = variable.val.ToString();
                    }
                }

                data.CallbackUrl = callbackUrl;

                foreach (var assignee in assigness)
                {
                    var user = await _userManager.FindByNameAsync(assignee);
                    
                    _emailHelper.CreateEmail(_config.GetConfig("smtp.from.email"),
                        user.Email,
                        $"New Document To Process [{docName}]",
                        "Emails/NotifyNewDocument",
                        data);
                }
            }

            return Ok(data);
        }

        [Route("postrejected")]
        public IActionResult PostRejected([FromBody] dynamic data)
        {
            string processName = data.processName.ToString();
            string processDefId = data.processDefId.ToString().Replace("#", ":");
            var variables = JsonConvert
                .DeserializeObject<List<dynamic>>(JsonConvert
                    .SerializeObject(data.processVariableList));
            string docId = "";

            foreach (var variable in variables)
            {
                if (variable.id == "documentId")
                {
                    docId = variable.val.ToString();
                    break;
                }
            }

            if (processDefId == _justificationDefId)
            {
                var justificationId = Guid.Parse(docId);
                var justification = _justificationService.GetById(justificationId);
                justification.JustificationStatus = Justification.STATUS_REJECTED;
                _justificationService.Update(justification);
            }

            return Ok(data);
        }

        [Route("postapproved")]
        public IActionResult PostApproved([FromBody] dynamic data)
        {
            var request = Request;
            return Ok(data);
        }

        [Route("postfinalapproved")]
        public IActionResult PostFinalApproved([FromBody] dynamic data)
        {
            string processName = data.processName.ToString();
            string processDefId = data.processDefId.ToString().Replace("#", ":");
            var variables = JsonConvert
                .DeserializeObject<List<dynamic>>(JsonConvert
                    .SerializeObject(data.processVariableList));
            string docId = "";

            foreach (var variable in variables)
            {
                if (variable.id == "documentId")
                {
                    docId = variable.val.ToString();
                    break;
                }
            }

            if (processDefId == _justificationDefId)
            {
                var justificationId = Guid.Parse(_justificationDefId);
                var justification = _justificationService.GetById(justificationId);
                justification.JustificationStatus = Justification.STATUS_APPROVED;
                _justificationService.Update(justification);
            }

            return Ok(data);
        }
    }
}
