/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';

    var width = screen.width;
    var height = screen.height;

    config.height = 300;
    config.filebrowserImageBrowseUrl = "/Admin/UserMedia";
    config.filebrowserImageWindowWidth = width;
    config.filebrowserImageWindowHeight = height;
    config.filebrowserBrowseUrl = "/Admin/UserMedia";
    config.filebrowserWindowWidth = width;
    config.filebrowserWindowHeight = height;
};
