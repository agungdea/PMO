﻿var Log = function () {
    var init = function (detailUrl, dateformat) {
        $common.setMenu("#menu-log");

        var datatableOption = {
            ajaxMethod: "POST",
            listUrl: "/admin/api/log/postdatatables",
            detailUrl: detailUrl + "/",
            columnDefs: [
                         {
                             "targets": -1,
                             "data": null,
                             "orderable": false,
                             render: function (data, type, dataObj) {
                                 var html = "<button class='btn btn-success detail' title='" + $L.get("log.button.index.detail.title") + "'><i class='fa fa-info'></i></button>";
                                 return html;
                             }
                         },
                         {
                             "targets": 0,
                             "data": "application",
                             name: "Application"
                         },
                         {
                             "targets": 1,
                             "data": "message",
                             name: "Message",
                             render: function (data) {
                                 return data;
                             }
                         },
                         {
                             "targets": 2,
                             "data": "level",
                             name: "Level"
                         },
                         {
                             "targets": 3,
                             "data": "logged",
                             name: "Logged"
                         }
            ],
            order: [[3, 'desc']]
        }

        $datatables.init("#table-log", datatableOption);
    }

    return {
        //main function to initiate the module
        init: function (detailUrl, dateformat) {
            init(detailUrl, dateformat);
        }
    };
}();