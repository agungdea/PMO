﻿var idElement = "#category-treeview";

var ajaxCall = function (form, text, elementId) {
    var $this = $(form);

    var validationContainer = $this.find('#validation-summary');
    var data = {};

    $.each($this.serializeArray(), function (index, item) {
        data[item.name] = item.value;
    });

    $.ajax({
        url: $this.attr("action"),
        method: $this.attr("method"),
        data: data,
        success: function (resp) {
            validationContainer.hide();
            $this.closest(".bootbox.modal").find(".bootbox-close-button").trigger("click");
            $alert.success($L.getFormated("category.text.form.success", [data.Name, text]));
            $(idElement).jstree(true).refresh();
        },
        error: function (resp) {
            validationContainer.html("");
            validationContainer.addClass("alert alert-danger");
            if (resp.responseJSON) {
                var obj = resp.responseJSON;
                var html = "";

                $.each(obj, function (i, item) {
                    $.each(item, function (index, text) {
                        html += text + "<br/>";
                    });
                });

                validationContainer.html(html);
            } else {
                validationContainer.html(resp.responseText);
            }
        }
    });
}

var Category = function () {
    var init = function (createUrl, elementId) {
        $('.btn-add').on('click', function (e) {
            e.preventDefault();

            var uri = createUrl;

            var modal = bootbox.dialog({
                title: $L.get("category.text.create"),
                message: '<p><i class="fa fa-spin fa-spinner"></i> ' + $L.get("common.text.loading") + '</p>'
            });
            modal.init(function () {
                $.ajax({
                    url: uri,
                    success: function (resp) {
                        modal.find('.bootbox-body').html(resp);
                        var options = {
                            success: function (form) {
                                ajaxCall(form, $L.get("category.text.create.success"), elementId);
                            }
                        }
                        FormValidation.init("#CategoryCreate", options);
                    }
                })
            });
        });
    }

    return {
        //main function to initiate the module
        init: function (createUrl) {
            init(createUrl);
        }
    };
}();

var UITree = function () {

    var getListCategory = function (elementId, url) {
        $(elementId).jstree({
            "core": {
                "themes": {
                    "responsive": false
                },
                // so that create works
                "check_callback": true,
                'data': {
                    'url': function (node) {
                        return url;
                    },
                    'data': function (node) {
                        return { 'parentid': node.id };
                    }
                }
            },
            "types": {
                "default": {
                    "icon": "fa fa-folder icon-state-warning icon-lg"
                },
                "file": {
                    "icon": "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state": { "key": "category" },
            "plugins": ["contextmenu", "types", "wholerow", "dnd", "state"],
            "contextmenu": {
                "items": function ($node) {
                    var tree = $(elementId).jstree(true);
                    return {
                        "Create": {
                            "label": $L.get("common.text.create"),
                            "action": function (data) {
                                var ref = $.jstree.reference(data.reference);
                                sel = ref.get_selected();
                                var uri = "/Admin/Category/Create";
                                var modal = bootbox.dialog({
                                    title: $L.get("category.text.create"),
                                    message: '<p><i class="fa fa-spin fa-spinner"></i> ' + $L.get("common.text.loading") + '</p>'
                                });

                                modal.init(function () {
                                    $.ajax({
                                        url: uri,
                                        success: function (resp) {
                                            modal.find('.bootbox-body').html(resp);
                                            modal.find('.bootbox-body form#CategoryCreate input#ParentId').val(sel[0]);
                                            var options = {
                                                success: function (form) {
                                                    ajaxCall(form, $L.get("category.text.create.success"), elementId);
                                                }
                                            }
                                            FormValidation.init("#CategoryCreate", options);
                                        }
                                    })
                                });
                            }
                        },
                        "Rename": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": $L.get("common.text.edit"),
                            "action": function (data) {
                                var ref = $.jstree.reference(data.reference);
                                sel = ref.get_selected();
                                var uri = "/Admin/Category/Edit/" + sel[0];
                                var modal = bootbox.dialog({
                                    title: $L.get("category.text.edit"),
                                    message: '<p><i class="fa fa-spin fa-spinner"></i> ' + $L.get("common.text.loading") + '</p>'
                                });

                                modal.init(function () {
                                    $.ajax({
                                        url: uri,
                                        success: function (resp) {
                                            modal.find('.bootbox-body').html(resp);
                                            var options = {
                                                success: function (form) {
                                                    ajaxCall(form, $L.get("category.text.edit.success"), elementId);
                                                }
                                            }
                                            FormValidation.init("#CategoryCreate", options);
                                        }
                                    })
                                });
                            }
                        },
                        "Remove": {
                            "separator_before": false,
                            "separator_after": false,
                            "label": $L.get("common.text.delete"),
                            "action": function (data) {
                                var ref = $.jstree.reference(data.reference);
                                sel = ref.get_selected();
                                bootbox.confirm($L.get("common.text.remove.item.confirmation"), function (result) {
                                    var uri = "/Admin/Api/Categories/Delete/" + sel[0];
                                    if (result === true) {
                                        $.ajax({
                                            url: uri,
                                            type: "DELETE",
                                            timeout: 60000,
                                            dataType: "json",
                                            contentType: "application/json",
                                            success: function (response) {
                                                $(elementId).jstree(true).refresh();
                                                $alert.success($L.get("category.text.delete.success"))
                                            },
                                            error: function (response) {
                                                console.log(response);
                                            },
                                            beforeSend: function () {
                                                $("#body-overlay").removeClass("hide");
                                            },
                                            complete: function () {
                                                $("#body-overlay").addClass("hide");
                                            }
                                        });
                                    };
                                });
                            }
                        }
                    };
                }
            }
        }).bind("move_node.jstree", function (e, data) {
            console.log("Drop node " + data.node.id + " to " + data.parent);
            $.ajax({
                url: "/admin/api/categories/reorder?id=" + data.node.id + "&parentId=" + data.parent + "",
                type: "POST",
                success: function (response) {
                    $(elementId).jstree(true).refresh();
                    $alert.success($L.get("category.text.move.success"))
                },
                error: function (response) {
                    $(elementId).jstree(true).refresh();
                    $alert.warning($L.get("category.text.move.failed"))
                },
            });
        });
    }

    return {
        //main function to initiate the module
        init: function (elementId, url) {
            getListCategory(elementId, url);
        }

    };

}();
