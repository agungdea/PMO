﻿var ResetPassword = function () {

    var handleResetPassword = function () {
        $('.reset-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                Email: {
                    required: true,
                    email: true
                },
                Password: {
                    required: true
                },
                ConfirmPassword: {
                    required: true
                }
            },

            messages: {
                Email: {
                    required: "Email is required."
                },
                Password: {
                    required: "Password is required."
                },
                ConfirmPassword: {
                    required: "Confirm Password is required."
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('.reset-form')).show();
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                form.submit();
            }
        });

        $('.reset-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleResetPassword();
        }
    };
}();

jQuery(document).ready(function () {
    ResetPassword.init();
});