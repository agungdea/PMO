﻿var SessionTimeout = function () {

    var handlesessionTimeout = function () {
        $.sessionTimeout({
            title: 'Session Timeout Notification',
            message: 'Your session is about to expire.',
            keepAliveUrl: '/home/CheckLoggedIn',
            redirUrl: '/account/logout',
            logoutUrl: '/account/logout',
            keepAliveInterval: 5000,
            keepAlive: true,
            warnAfter: 1000 * 60 * 20, //warn after 20 minutes
            redirAfter: (1000 * 60 * 20) + 10000, //redirect after 10 seconds,
            ignoreUserActivity: true,
            countdownMessage: 'Redirecting in {timer} seconds.',
            countdownBar: true
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handlesessionTimeout();
        }
    };

}();

jQuery(document).ready(function () {
    SessionTimeout.init();
});