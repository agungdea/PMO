﻿var $datatables = function () {
    var defaultOptions = {
        removeSuccess: function (url, table) {
            table.ajax.url(url).load();
        },
        editAction: function (e, elementId, editUrl, current) {
            e.preventDefault();
            var table = $(elementId).DataTable();
            var data = table.row(current.parents('tr')).data();
            var id = data.id;
            window.location.href = editUrl + id;
        },
        editActionMini: function (e, elementId, editUrl, current) {
            e.preventDefault();
            var table = $(elementId).DataTable();
            var data = table.row(current.parents('tr').prev()).data();
            var id = data.id;
            window.location.href = editUrl + id;
        },
        viewExcelAction: function (e, elementId, excelUrl, current) {
            e.preventDefault();
            var table = $(elementId).DataTable();
            var data = table.row(current.parents('tr')).data();
            var id = data.id;
            window.location.href = excelUrl + id;
        },
        viewpdfAction: function (e, elementId, pdfUrl, current) {
            e.preventDefault();
            var table = $(elementId).DataTable();
            var data = table.row(current.parents('tr')).data();
            var id = data.id;
            window.location.href = pdfUrl + id;
        },
        detailAction: function (e, elementId, detailUrl, current) {
            e.preventDefault();
            var table = $(elementId).DataTable();
            var data = table.row(current.parents('tr')).data();
            var id = data.id;
            window.location.href = detailUrl + id;
        },
        detailActionMini: function (e, elementId, detailUrl, current) {
            e.preventDefault();
            var table = $(elementId).DataTable();
            var data = table.row(current.parents('tr').prev()).data();
            var id = data.id;
            window.location.href = detailUrl + id;
        },
        listEventAction: function (e, elementId, listActivityUrl, current) {
            e.preventDefault();
            var table = $(elementId).DataTable();
            var data = table.row(current.parents('tr')).data();
            var id = data.id;
            window.location.href = listActivityUrl + id;
        },
        order: [[0, "asc"]]
    };

    var init = function (elementId, option) {
        option = $.extend(defaultOptions, option);

        $(elementId)
           .on('init.dt', function () {
               var table = $(this).DataTable();
               var searchField = $(elementId + "_wrapper .dataTables_filter input");
               searchField.unbind();
               searchField.keyup(
                   $.debounce(500, function () {
                       table.search(searchField.val()).draw();
                   }));
           })
           .DataTable({
               "autowidth": false,
               "processing": true,
               "rowReorder": false,
               "serverSide": true,
               "ajax": {
                   "url": option.listUrl,
                   "type": option.ajaxMethod || "GET"
               },
               "columnDefs": option.columnDefs,
               "stateSave": option.stateSave || true,
               responsive: {
                   details: {

                   }
               },
               order: option.order,
               'select': option.select,
           });

        $(elementId).on('click', 'td > .remove', function (e) {
            var table = $(elementId).DataTable();
            var data = table.row($(this).parents('tr')).data();
            var id = data.id;
            e.preventDefault();// used to stop its default behaviour
            bootbox.confirm($L.get("common.text.remove.item.confirmation"), function (result) {
                if (result === true) {
                    $.ajax({
                        url: option.deleteUrl + id,
                        type: "DELETE",
                        timeout: 60000,
                        dataType: "json",
                        contentType: "application/json",
                        success: function (response) {
                            if (response === "Justification") {
                                $alert.warning("Data Wbs Tidak Bisa dihapus Karena sudah digunakan di Justifikasi");
                            } else if (response === "Designator") {
                                $alert.warning("Data Designator Tidak Bisa dihapus Karena sudah digunakan di WBS");
                            } else {
                                $alert.success(option.deleteAlertSuccess);
                                option.removeSuccess(option.listUrl, table);
                            }

                        },
                        error: function (response) {
                            console.log(response);
                        },
                        beforeSend: function () {
                            $("#body-overlay").removeClass("hide");
                        },
                        complete: function () {
                            $("#body-overlay").addClass("hide");
                        }
                    });
                };
            });
        });

        $(elementId).on('click', 'span > .remove', function (e) {
            var table = $(elementId).DataTable();
            var data = table.row($(this).parents('tr').prev()).data();
            var id = data.id;
            e.preventDefault();// used to stop its default behaviour
            bootbox.confirm("Are you sure remove this item?", function (result) {
                if (result === true) {
                    $.ajax({
                        url: option.deleteUrl + id,
                        type: "DELETE",
                        timeout: 60000,
                        dataType: "json",
                        contentType: "application/json",
                        success: function (response) {

                            $alert.success(option.deleteAlertSuccess);
                            option.removeSuccess(option.listUrl, table);
                        },
                        error: function (response) {
                            console.log(response);
                        },
                        beforeSend: function () {
                            $("#body-overlay").removeClass("hide");
                        },
                        complete: function () {
                            $("#body-overlay").addClass("hide");
                        }
                    });
                };
            });
        });

        $(elementId).on('click', 'td > .detail', function (e) {
            option.detailAction(e, elementId, option.detailUrl, $(this));
        });

        $(elementId).on('click', 'td > .edit', function (e) {
            option.editAction(e, elementId, option.editUrl, $(this));
        });

        $(elementId).on('click', 'td > .viewexcel', function (e) {
            option.viewExcelAction(e, elementId, option.excelUrl, $(this));
        });

        $(elementId).on('click', 'td > .viewpdf', function (e) {
            option.viewpdfAction(e, elementId, option.pdfUrl, $(this));
        });

        $(elementId).on('click', 'td > .list', function (e) {
            option.listEventAction(e, elementId, option.listActivityUrl, $(this));
        });

        $(elementId).on('click', 'span > .detail', function (e) {
            option.detailActionMini(e, elementId, option.detailUrl, $(this));
        });

        $(elementId).on('click', 'span > .edit', function (e) {
            option.editActionMini(e, elementId, option.editUrl, $(this));
        });


        return $(elementId).DataTable();
    }

    return {
        init: function (id, option) {
            init(id, option);
        }
    };
}();