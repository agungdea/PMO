﻿var $L = function () {
    function format(str, arr) {
        return str.replace(/\{(\d+)\}/g, function (_, m) {
            return arr[m];
        });
    }

    function getResource(resourceName) {
        var val = RESOURCES[resourceName];
        
        return val || resourceName;
    }

    return {
        get: function (resourceName) {
            return getResource(resourceName);
        },
        getFormated: function (resourceName, values) {
            return format(getResource(resourceName), values);
        }
    };
}();