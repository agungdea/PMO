﻿namespace App.Web.Models.ViewModels.Identity
{
    public class AppActionViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class AppActionForm
    {
        public string Name { get; set; }
    }
}
