﻿using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using App.Web.Models.ViewModels.App;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.Identity
{
    public class ApplicationUserViewModel
    {
        public string Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public List<string> Roles { get; set; }

        public UserProfileViewModel UserProfile { get; set; }

        public string Directorat { get; set; }
        public string UnitBisnis { get; set; }
        public string UnitKerja { get; set; }
        public string Bagian { get; set; }
    }

    public class ApplicationUserForm
    {
        [Required(ErrorMessage = "{0} harus di isi")]
        [Display(Name = "Nik")]
        //[StringLength(6, ErrorMessage = "{0} harus memiliki {2} karakter.", MinimumLength = 6)]
       // [Range(0, 9999999999999999, ErrorMessage = "Isi Dengan Angka")]
        public string Nik { get; set; }

        [Required]
        public string Name { get; set; }

        public string Birthplace { get; set; }

        public DateTime? Birthdate { get; set; }

        public string Address { get; set; }

        [UIHint("SingleFileUploadCropper")]
        public string Photo { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string Phone { get; set; }

        [Required]
        public List<string> Roles { get; set; }

        [UIHint("UnitTemplate")]
        public string  Directorat { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitBisnis { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitKerja { get; set; }
        [UIHint("UnitTemplate")]
        public string Bagian { get; set; }
        public List<string> GrupParent { get; set; }
        public Boolean Disposisi { get; set; }
        public Boolean AllUnit { get; set; }
        public Boolean AllRegional { get; set; }
    }

    public class UpdateProfileForm
    {
        [Required]
        public string Name { get; set; }

        public string Birthplace { get; set; }

        public DateTime? Birthdate { get; set; }

        public string Address { get; set; }

        [UIHint("SingleFileUploadCropper")]
        public string Photo { get; set; }

        public string Phone { get; set; }
    }

    public class ChangePasswordForm
    {
        [Required]
        [Display(Name = "Old Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [Display(Name = "New Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string NewConfirmPassword { get; set; }
    }

    public class ApplicationUserChangePassword
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Old Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [Display(Name = "New Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string NewConfirmPassword { get; set; }
    }

    public class VerifyEmailViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }

    public static class UserExtension
    {
        public static void Update(this ApplicationUser user, ApplicationUserForm model)
        {
            user.UserName = model.Email;
            user.Email = model.Email;
            user.PhoneNumber = model.Phone;
            user.UserProfile.Name = model.Name;
            user.UserProfile.Birthdate = model.Birthdate;
            user.UserProfile.Birthplace = model.Birthplace;
            user.UserProfile.Address = model.Address;
        }

        public static void Update(this ApplicationUser user, UpdateProfileForm model)
        {
            user.PhoneNumber = model.Phone;
            user.UserProfile.Name = model.Name;
            user.UserProfile.Birthdate = model.Birthdate;
            user.UserProfile.Birthplace = model.Birthplace;
            user.UserProfile.Address = model.Address;
        }
    }
}
