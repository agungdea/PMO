﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.Identity
{
    public class MasterClaimViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string ClaimValues { get; set; }

        public string[] Values
        {
            get
            {
                return ClaimValues.Split(',');
            }
        }
    }

    public class MasterClaimForm
    {
        [Required]
        public string Type { get; set; }

        [Required]
        public string ClaimValues { get; set; }
    }
}
