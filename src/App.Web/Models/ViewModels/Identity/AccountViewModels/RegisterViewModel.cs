﻿using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.Identity.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "{0} harus di isi")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} harus di isi")]
        [Display(Name = "Nik")]
        [StringLength(6, ErrorMessage = "{0} harus memiliki {2} karakter.", MinimumLength = 6)]
        [Range(0, 9999999999999999, ErrorMessage = "Isi Dengan Angka")]
        public string Nik { get; set; }

        [Required(ErrorMessage = "{0} harus di isi")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} harus di isi")]
        [StringLength(100, ErrorMessage = "{0} minimal panjang karakter {2} dan maksimal panjang karakter {1} ", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = " password dan konfirmasi password tidak sama.")]
        public string ConfirmPassword { get; set; }
    }

    public class GenerateNewEmailConfirmed
    {

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
