﻿using App.Domain.Models.Identity;
using System.Collections.Generic;

namespace App.Web.Models.ViewModels.Identity
{
    public class AssignClaimForm
    {
        public string Id { get; set; }
        public ClaimType Type { get; set; }
        public List<ClaimItem> Claims { get; set; }
    }

    public class ClaimItem
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }
}
