﻿namespace App.Web.Models.ViewModels.Workflow
{
    public class WfApprovalViewModel
    {
        public string Status { get; set; }
        public string Note { get; set; }
    }
}
