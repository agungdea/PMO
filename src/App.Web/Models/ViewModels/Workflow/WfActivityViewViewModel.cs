﻿using App.Domain.Models.Workflow;
using System.Collections.Generic;

namespace App.Web.Models.ViewModels.Workflow
{
    public class WfActivityViewViewModel : WfActivityView
    {
    }

    public class WfActivityViewForm
    {
        public string ActivityName { get; set; }

        public string ViewName { get; set; }

        public string Variables { get; set; }
    }

    public static class WfActivityViewExtension
    {
        public static void Update(this WfActivityView item, WfActivityViewForm form)
        {
            item.Id = form.ActivityName;
            item.ViewName = form.ViewName;
            item.Variables = form.Variables;
        }
    }
}
