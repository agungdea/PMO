﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.Lemon
{
    public class LemonViewModel
    {
        public string nik { get; set; }
        public string nama { get; set; }
        public string Jabatan { get; set; }
        public Decimal kdjbatan { get; set; }
        public string Email { get; set; }
    }
}
