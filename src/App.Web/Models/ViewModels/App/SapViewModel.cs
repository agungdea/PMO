﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class SapViewModel
    {
        public USER_AUTH USER_AUTH { get; set; }
        public List<REQUISITION_ITEMS> REQUISITION_ITEMS { get; set; }
        public EXTENSIONIN EXTENSIONIN { get; set; }
        public List<REQUISITION_ACCOUNT_ASSIGNMENT> REQUISITION_ACCOUNT_ASSIGNMENT { get; set; }
        public REQUISITION_ADDRDELIVERY REQUISITION_ADDRDELIVERY { get; set; }
        public REQUISITION_CONTRACT_LIMITS REQUISITION_CONTRACT_LIMITS { get; set; }
        public REQUISITION_ITEM_TEXT REQUISITION_ITEM_TEXT { get; set; }
        public REQUISITION_LIMITS REQUISITION_LIMITS { get; set; }
        public REQUISITION_SERVICES REQUISITION_SERVICES { get; set; }
        public REQUISITION_SERVICES_TEXT REQUISITION_SERVICES_TEXT { get; set; }
        public REQUISITION_SRV_ACCASS_VALUES REQUISITION_SRV_ACCASS_VALUES { get; set; }
    }



    public class USER_AUTH
    {
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
    }

    public class REQUISITION_ITEMS
    {
        public object PREQ_NO { get; set; }
        public string PREQ_ITEM { get; set; }
        public string DOC_TYPE { get; set; }
        public string PUR_GROUP { get; set; }
        public string CREATED_BY { get; set; }
        public string PREQ_NAME { get; set; }
        public string PREQ_DATE { get; set; }
        public string SHORT_TEXT { get; set; }
        public string MATERIAL { get; set; }
        public string PUR_MAT { get; set; }
        public string PLANT { get; set; }
        public string STORE_LOC { get; set; }
        public string TRACKINGNO { get; set; }
        public string MAT_GRP { get; set; }
        public string SUPPL_PLNT { get; set; }
        public string QUANTITY { get; set; }
        public string UNIT { get; set; }
        public string DEL_DATCAT { get; set; }
        public string DELIV_DATE { get; set; }
        public string REL_DATE { get; set; }
        public string GR_PR_TIME { get; set; }
        public Decimal? C_AMT_BAPI { get; set; }
        public string PRICE_UNIT { get; set; }
        public string ITEM_CAT { get; set; }
        public string ACCTASSCAT { get; set; }
        public string DISTRIB { get; set; }
        public string PART_INV { get; set; }
        public string GR_IND { get; set; }
        public string GR_NON_VAL { get; set; }
        public string IR_IND { get; set; }
        public string DES_VENDOR { get; set; }
        public string FIXED_VEND { get; set; }
        public string PURCH_ORG { get; set; }
        public string AGREEMENT { get; set; }
        public string AGMT_ITEM { get; set; }
        public string INFO_REC { get; set; }
        public string QUOTA_ARR { get; set; }
        public string QUOTARRITM { get; set; }
        public string MRP_CONTR { get; set; }
        public string BOMEXPL_NO { get; set; }
        public string LAST_RESUB { get; set; }
        public string RESUBMIS { get; set; }
        public string NO_RESUB { get; set; }
        public string VAL_TYPE { get; set; }
        public string SPEC_STOCK { get; set; }
        public string PO_UNIT { get; set; }
        public string REV_LEV { get; set; }
        public string PCKG_NO { get; set; }
        public string KANBAN_IND { get; set; }
        public string PO_PRICE { get; set; }
        public string INT_OBJ_NO { get; set; }
        public string PROMOTION { get; set; }
        public string BATCH { get; set; }
        public string VEND_MAT { get; set; }
        public string ORDERED { get; set; }
        public string CURRENCY { get; set; }
        public string MANUF_PROF { get; set; }
        public string MANU_MAT { get; set; }
        public string MFR_NO { get; set; }
        public string MFR_NO_EXT { get; set; }
        public string DEL_DATCAT_EXT { get; set; }
        public string CURRENCY_ISO { get; set; }
        public string ITEM_CAT_EXT { get; set; }
        public string PREQ_UNIT_ISO { get; set; }
        public string PO_UNIT_ISO { get; set; }
        public string GENERAL_RELEASE { get; set; }
        public string MATERIAL_EXTERNAL { get; set; }
        public string MATERIAL_GUID { get; set; }
        public string MATERIAL_VERSION { get; set; }
        public string PUR_MAT_EXTERNAL { get; set; }
        public string PUR_MAT_GUID { get; set; }
        public string PUR_MAT_VERSION { get; set; }
        public string REQ_BLOCKED { get; set; }
        public string REASON_BLOCKING { get; set; }
        public string PROCURING_PLANT { get; set; }
        public string CMMT_ITEM { get; set; }
        public string FUNDS_CTR { get; set; }
        public string FUND { get; set; }
        public string RES_DOC { get; set; }
        public string RES_ITEM { get; set; }
        public string FUNC_AREA { get; set; }
        public string GRANT_NBR { get; set; }
        public string FUND_LONG { get; set; }
        public string BUDGET_PERIOD { get; set; }
    }

    public class EXTENSIONIN
    {
    }

    public class REQUISITION_ACCOUNT_ASSIGNMENT
    {
        public object PREQ_NO { get; set; }
        public string PREQ_ITEM { get; set; }
        public string SERIAL_NO { get; set; }
        public string DELETE_IND { get; set; }
        public string CREATED_ON { get; set; }
        public string CREATED_BY { get; set; }
        public string PREQ_QTY { get; set; }
        public string DISTR_PERC { get; set; }
        public string G_L_ACCT { get; set; }
        public string BUS_AREA { get; set; }
        public string COST_CTR { get; set; }
        public string PROJ_NO { get; set; }
        public string SD_DOC { get; set; }
        public string SDOC_ITEM { get; set; }
        public string SCHED_LINE { get; set; }
        public string ASSET_NO { get; set; }
        public string SUB_NUMBER { get; set; }
        public string ORDER_NO { get; set; }
        public string GR_RCPT { get; set; }
        public string UNLOAD_PT { get; set; }
        public string CO_AREA { get; set; }
        public string TO_COSTCTR { get; set; }
        public string TO_ORDER { get; set; }
        public string TO_PROJECT { get; set; }
        public string COST_OBJ { get; set; }
        public string PROF_SEGM { get; set; }
        public string PROFIT_CTR { get; set; }
        public string WBS_ELEM { get; set; }
        public string NETWORK { get; set; }
        public string ROUTING_NO { get; set; }
        public string RL_EST_KEY { get; set; }
        public string COUNTER { get; set; }
        public string PART_ACCT { get; set; }
        public string CMMT_ITEM { get; set; }
        public string REC_IND { get; set; }
        public string FUNDS_CTR { get; set; }
        public string FUND { get; set; }
        public string FUNC_AREA { get; set; }
        public string REF_DATE { get; set; }
        public string CHANGE_ID { get; set; }
        public string CURRENCY { get; set; }
        public string PREQ_UNIT { get; set; }
        public string WBS_ELEM_E { get; set; }
        public string PROJ_EXT { get; set; }
        public string ACTIVITY { get; set; }
        public string FUNC_AREA_LONG { get; set; }
        public string GRANT_NBR { get; set; }
        public string CMMT_ITEM_LONG { get; set; }
        public string RES_DOC { get; set; }
        public string RES_ITEM { get; set; }
        public string FUND_LONG { get; set; }
        public string BUDGET_PERIOD { get; set; }
    }

    public class REQUISITION_ADDRDELIVERY
    {
    }

    public class REQUISITION_CONTRACT_LIMITS
    {
    }

    public class REQUISITION_ITEM_TEXT
    {
    }

    public class REQUISITION_LIMITS
    {
    }

    public class REQUISITION_SERVICES
    {
    }

    public class REQUISITION_SERVICES_TEXT
    {
    }

    public class REQUISITION_SRV_ACCASS_VALUES
    {
    }

}
