﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class TransformerProgramViewModel
    {

            public string id { get; set; }
            public string btp { get; set; }
            public string businessRisk { get; set; }
            public string description { get; set; }
            public string quickWin { get; set; }
            public string riskMitigation { get; set; }
            public string title { get; set; }
            public string bodChampion_id { get; set; }
            public string initiative_id { get; set; }
            public string status { get; set; }
            public string programStatus { get; set; }
            public string workflowStatus { get; set; }
            public DateTime? date_submitted { get; set; }
            public string generator { get; set; }
            public string quantifier { get; set; }
            public DateTime? date_approved { get; set; }
            public string draft { get; set; }
            public String planamount { get; set; }
            public string businessRisk2 { get; set; }
            public string riskMitigation2 { get; set; }
            public string comments { get; set; }
            public string planuser { get; set; }
            public string weight { get; set; }
            public string revision { get; set; }
            public string programType { get; set; }
            public string organization_id { get; set; }
            public string order { get; set; }
            public string backactual { get; set; }
            public string backchgreq { get; set; }
            public string issuemgt { get; set; }
            public string progressworkflowmode_id { get; set; }
            public string enabler { get; set; }
            public string warflag { get; set; }
            public string usedependency { get; set; }
            public DateTime? closeddate { get; set; }
        
    }
}
