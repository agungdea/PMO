﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels
{
    public class LevelCategorySelectorViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
