﻿using System;
using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.App
{
    public class WBSBoQForm
    {
        [Required]
        public Guid IdWBSTree { get; set; }

        [Required]
        public string Designator { get; set; }

        [Required]
        public string GrupHarga { get; set; }

        [Required]
        public decimal HargaMaterial { get; set; }

        [Required]
        public decimal HargaJasa { get; set; }

        [Required]
        public int Qty { get; set; }
        public string MataUang { get; set; }
    }
}
