﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Web.Models.ViewModels.Workflow;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.App
{
    public class JustificationViewModel
    {
        public Guid Id { get; set; }
        public string Penyusun { get; set; }
        public string NomorDRK { get; set; }
        public decimal JumlahAnggaran { get; set; }
        public Guid? WBSId { get; set; }
        public string NamaKegiatan { get; set; }
        public string NomorDRP { get; set; }
        public string JenisKebutuhan { get; set; }
        public Guid? AKIId { get; set; }
        public string AkiName { get; set; }
        public SpendingType JenisPengeluaran { get; set; }
        public string PusatPertanggungjawaban { get; set; }
        public string KelasAsset { get; set; }
        public string LatarBelakang { get; set; }
        [UIHint("AddNote")]
        public string NoteLatarBelakang { get; set; }
        public string AspekStrategis { get; set; }
        [UIHint("AddNote")]
        public string NoteAspekStrategis { get; set; }
        public string AspekBisnis { get; set; }
        [UIHint("AddNote")]
        public string NoteAspekBisnis { get; set; }
        public string SpesifikasiTeknik { get; set; }
        [UIHint("AddNote")]
        public string NoteSpesifikasiTeknik { get; set; }
        public string JumlahKebutuhanBarangJasa { get; set; }
        public string BasicDesign { get; set; }
        public bool ShowBoQ { get; set; }
        public string TOR { get; set; }
        public string DasarKebutuhanBarangJasa { get; set; }
        public string LampiranDasarKebutuhanBarangJasa { get; set; }
        [UIHint("AddNote")]
        public string NoteJumlahKebutuhan { get; set; }
        public string RencanaPelaksanaan { get; set; }
        [UIHint("AddNote")]
        public string NoteRencanaPelaksanaan { get; set; }
        public string DistribusiPenggunaan { get; set; }
        public bool ShowWBS { get; set; }
        [UIHint("AddNote")]
        public string NoteDistribusiPenggunaan { get; set; }
        public string PosisiPersediaan { get; set; }
        [UIHint("AddNote")]
        public string NotePosisiPersediaan { get; set; }
        public string Anggaran { get; set; }
        [UIHint("AddNote")]
        public string NoteAnggaran { get; set; }
        public string Penutup { get; set; }
        [UIHint("AddNote")]
        public string NotePenutup { get; set; }
        public string JustificationStatus { get; set; }
        public string CreatedBy { get; set; }
        public string LastEditedBy { get; set; }
        //STatus Submit SAP
        public string CustomField1 { get; set; }
        //UnitId
        public string OtherInfo { get; set; }
        public List<WBSBoQJustificationDto> ListBoq { get; set; }
        public List<WBSTreeJustificationDto> ListLocation { get; set; }
    }

    public class JustificationHeaderForm
    {
        public Guid Id { get; set; }
        public string Penyusun { get; set; }
        public string NomorDRK { get; set; }
        public decimal JumlahAnggaran { get; set; }
        public Guid? WBSId { get; set; }
        public string NomorDRP { get; set; }
        public string JenisKebutuhan { get; set; }
        public Guid? AKIId { get; set; }
        public SpendingType JenisPengeluaran { get; set; }
        public string PusatPertanggungjawaban { get; set; }
        public string KelasAsset { get; set; }
        public string RedirectUrl { get; set; }
    }

    public class JustificationUraianForm
    {
        public string LatarBelakang { get; set; }
        public string AspekStrategis { get; set; }
        public string AspekBisnis { get; set; }
        public string SpesifikasiTeknik { get; set; }
        public string RedirectUrl { get; set; }
    }
    public class Justificationreturnsap
    {
        public item item { get; set; }
    }
    public class item
    {
        public string TYPE { get; set; }
        public string CODE { get; set; }
        public string MESSAGE { get; set; }
        public string LOG_NO { get; set; }
        public string LOG_MSG_NO { get; set; }
        public string MESSAGE_V1 { get; set; }
        public string MESSAGE_V2 { get; set; }
        public string MESSAGE_V3 { get; set; }
        public string MESSAGE_V4 { get; set; }
    }

    public class JustificationSapForm
    {
        public string GLAccount { get; set; }
        public string CostCenter { get; set; }
        public string KodeWBSSAP { get; set; }
        public string Plant { get; set; }
        public string Purgroup { get; set; }
        public string Purchorg { get; set; }
    }

    public class JustificationJumlahKebutuhanForm
    {
        public string JumlahKebutuhanBarangJasa { get; set; }
        public string BasicDesign { get; set; }
        public bool ShowBoQ { get; set; }
        public string TOR { get; set; }
        public string DasarKebutuhanBarangJasa { get; set; }
        public string LampiranDasarKebutuhanBarangJasa { get; set; }
        public string RedirectUrl { get; set; }
    }

    public class JustificationRencanaPelaksanaanForm
    {
        public string RencanaPelaksanaan { get; set; }
        public string DistribusiPenggunaan { get; set; }
        public bool ShowWBS { get; set; }
        public string RedirectUrl { get; set; }
    }

    public class JustificationLainLainForm
    {
        public string PosisiPersediaan { get; set; }
        public string Anggaran { get; set; }
        public string Penutup { get; set; }
        public string RedirectUrl { get; set; }
    }

    public class JustificationNoteForm
    {
        public Guid pk { get; set; }
        public string name { get; set; }
        public string value { get; set; }
    }

    public class JustificationApprovalForm : WfApprovalViewModel
    {

    }

    public static class JustificationExtension
    {
        public static void Update(this Justification justification, JustificationHeaderForm form)
        {
            justification.Id = form.Id;
            justification.Penyusun = form.Penyusun;
            justification.NomorDRK = form.NomorDRK;
            justification.JumlahAnggaran = form.JumlahAnggaran;
            justification.WBSId = form.WBSId;
            justification.NomorDRP = form.NomorDRP;
            justification.JenisKebutuhan = form.JenisKebutuhan;
            justification.AKIId = form.AKIId;
            justification.JenisPengeluaran = form.JenisPengeluaran;
            justification.PusatPertanggungjawaban = form.PusatPertanggungjawaban;
            justification.KelasAsset = form.KelasAsset;
        }

        public static void Update(this Justification justification, JustificationUraianForm form)
        {
            justification.LatarBelakang = form.LatarBelakang;
            justification.AspekStrategis = form.AspekStrategis;
            justification.AspekBisnis = form.AspekBisnis;
            justification.SpesifikasiTeknik = form.SpesifikasiTeknik;
        }

        public static void Update(this Justification justification, JustificationJumlahKebutuhanForm form)
        {
            justification.JumlahKebutuhanBarangJasa = form.JumlahKebutuhanBarangJasa;
            justification.BasicDesign = form.BasicDesign;
            justification.ShowBoQ = form.ShowBoQ;
            justification.TOR = form.TOR;
            justification.DasarKebutuhanBarangJasa = form.DasarKebutuhanBarangJasa;
            justification.LampiranDasarKebutuhanBarangJasa = form.LampiranDasarKebutuhanBarangJasa;
        }

        public static void Update(this Justification justification, JustificationRencanaPelaksanaanForm form)
        {
            justification.RencanaPelaksanaan = form.RencanaPelaksanaan;
            justification.DistribusiPenggunaan = form.DistribusiPenggunaan;
            justification.ShowWBS = form.ShowWBS;
        }

        public static void Update(this Justification justification, JustificationLainLainForm form)
        {
            justification.PosisiPersediaan = form.PosisiPersediaan;
            justification.Anggaran = form.Anggaran;
            justification.Penutup = form.Penutup;
        }

        public static bool IsJustificationEditableAndSubmitable(this Justification justification, string userId)
        {
            if ((justification.JustificationStatus == Justification.STATUS_DRAFT ||
                justification.JustificationStatus == Justification.STATUS_REJECTED) &&
                justification.CreatedBy == userId)
            {
                return true;
            }

            return false;
        }

        public static bool IsJustificationEditableAndSubmitable(this JustificationViewModel justification, string userId)
        {
            if ((justification.JustificationStatus == Justification.STATUS_DRAFT ||
                justification.JustificationStatus == Justification.STATUS_REJECTED) &&
                justification.CreatedBy == userId)
            {
                return true;
            }


            return false;
        }




    }
}
