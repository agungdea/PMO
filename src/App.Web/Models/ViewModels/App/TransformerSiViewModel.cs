﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class TransformerSiViewModel
    {

            public string id { get; set; }
            public string abbreviation { get; set; }
            public string name { get; set; }
            public object parent_id { get; set; }
    }
}
