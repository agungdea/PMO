﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class RoiViewModel
    {
        public string RoiType { get; set; }
        public int Persentase { get; set; }
        public decimal Total { get; set; }
    }    
}
