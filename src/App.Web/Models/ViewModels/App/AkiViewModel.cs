﻿using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class AkiViewModel
    {
        public Guid Id { get; set; }
        public string AkiName { get; set; }
        public string PeriodId { get; set; }
        public string PeriodName { get; set; }
        public string StrategiInitiativeId { get; set; }
        public string StrategiInitiativeName { get; set; }
        public string ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string Document { get; set; }

        public virtual AkiCoverViewModel AkiCover { get; set; }
    }

    public class AkiForm
    {
        public Guid Id { get; set; }
        public string AkiName { get; set; }
        public string PeriodId { get; set; }
        public string PeriodName { get; set; }
        public string StrategiInitiativeId { get; set; }
        public string StrategiInitiativeName { get; set; }
        public string ProgramId { get; set; }
        public string ProgramName { get; set; }
        [UIHint("SingleFileUploadAki")]
        public string Document { get; set; }
        [UIHint("SingleFileUploadNJKI")]
        //DocumentNJKI=CustomField1
        public string DocumentNJKI { get; set; }
        public virtual AkiCoverForm AkiCover { get; set; }
        public List<string> listUser { get; set; }
        [UIHint("UnitTemplate")]
        public string Directorat { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitBisnis { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitKerja { get; set; }
    }
    public class AkiFormNJKI
    {
        public Guid Id { get; set; }
        public string AkiName { get; set; }
        public string PeriodId { get; set; }
        public string PeriodName { get; set; }
        public string StrategiInitiativeId { get; set; }
        public string StrategiInitiativeName { get; set; }
        public string ProgramId { get; set; }
        public string ProgramName { get; set; }
        [UIHint("SingleFileUploadAki")]
        [Required]
        public string Document { get; set; }
        [UIHint("SingleFileUploadNJKI")]
        [Required]
        //DocumentNJKI=CustomField1
        public string DocumentNJKI { get; set; }
        public virtual AkiCoverForm AkiCover { get; set; }
        public List<string> listUser { get; set; }
        [UIHint("UnitTemplate")]
        public string Directorat { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitBisnis { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitKerja { get; set; }
    }

    public static class AkiExtension
    {
        public static void Update(this Aki aki, AkiForm form)
        {
            aki.AkiName = form.AkiName;
            aki.PeriodId = form.PeriodId;
            aki.PeriodName = form.PeriodName;
            aki.StrategiInitiativeId = form.StrategiInitiativeId;
            aki.StrategiInitiativeName = form.StrategiInitiativeName;
            aki.ProgramId = form.ProgramId;
            aki.ProgramName = form.ProgramName;
        }
        public static void UpdateNJKI(this Aki aki, AkiFormNJKI form)
        {
            aki.AkiName = form.AkiName;
            aki.PeriodId = form.PeriodId;
            aki.PeriodName = form.PeriodName;
            aki.StrategiInitiativeId = form.StrategiInitiativeId;
            aki.StrategiInitiativeName = form.StrategiInitiativeName;
            aki.ProgramId = form.ProgramId;
            aki.ProgramName = form.ProgramName;
        }
    }

    public class SelectAkiItem
    {
        public string Id { get; set; }

        public string Text { get; set; }
    }
}
