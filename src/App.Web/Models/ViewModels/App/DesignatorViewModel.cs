﻿using App.Domain.Models.App;
using System.Collections.Generic;

namespace App.Web.Models.ViewModels.App
{
    public class DesignatorViewModel
    {
        public long Id { get; set; }
        
        public string IdDesignator { get; set; }

        public string Deskripsi { get; set; }

        public string Satuan { get; set; }

        public List<HargaViewModel> Hargas { get; set; }
    }

    public class DesignatorForm
    {
        public string IdDesignator { get; set; }

        public string Deskripsi { get; set; }

        public string Satuan { get; set; }

        public List<HargaForm> Hargas { get; set; }
    }

    public static class DesignationExtension
    {
        public static void Update(this Designator item, DesignatorForm form)
        {
            item.Deskripsi = form.Deskripsi;
            item.Satuan = form.Satuan;
        }
    }
}
