﻿using System;

namespace App.Web.Models.ViewModels.App
{
    public class WBSCurrencyViewModel
    {
        public Guid WBSId { get; set; }

        public Guid CurrencyId { get; set; }

        public decimal Rate { get; set; }
    }

    public class WBSCurrencyForm
    {
        public Guid WBSId { get; set; }

        public Guid CurrencyId { get; set; }

        public string Rate { get; set; }
    }
}
