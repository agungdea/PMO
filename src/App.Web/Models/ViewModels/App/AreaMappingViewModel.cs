﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class AreaMappingViewModel
    {
        public Guid Id { get; set; }
        public string AreaRegional { get; set; }
        public string AreaWitel { get; set; }
        public string AreaDatel { get; set; }
        public string AreaStoName { get; set; }
        public string AreaSto { get; set; }
        public string AreaKode { get; set; }
    }

    public class AreaMappingForm
    {
        public string AreaRegional { get; set; }
        public string AreaWitel { get; set; }
        public string AreaDatel { get; set; }
        public string AreaStoName { get; set; }
        public string AreaSto { get; set; }
        public string AreaKode { get; set; }

    }

    public class AreaMappingUpdateForm
    {
        [Required]
        [UIHint("SingleFileUpload")]
        public string Source { get; set; }
    }

    public class AreaStoAutocomplete
    {
        [UIHint("AutoComplete")]
        public string AreaCode { get; set; }
    }
}
