﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class GroupWfViewModel
    {
        public string Nik { get; set; }
        public string GrupName { get; set; }
        public Boolean isApprover { get; set; }
        public List<SelectListItem> ListGrupName { get; set; }

    }

    public class GroupWfForm
    {
        public string Nik { get; set; }
        public string GrupName { get; set; }
        public Boolean isApprover { get; set; }
        public List<SelectListItem> ListGrupName { get; set; }
    }
}
