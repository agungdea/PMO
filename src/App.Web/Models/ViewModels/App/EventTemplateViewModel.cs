﻿using App.Domain.Models.App;
using System;
using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.App
{
    public class EventTemplateViewModel
    {

        public Guid Id { get; set; }
        public Guid IdActTmp { get; set; }

        public int ActId { get; set; }

        public int? ActParent { get; set; }

        public string NamaAct { get; set; }

        public decimal? Bobot { get; set; }

        public int? Durasi { get; set; }

        public int? Predecessor { get; set; }

        public int? Lagging { get; set; }

        public string Relasi { get; set; }

        public int? Urutan { get; set; }

        public bool ISLEAF { get; set; }
    }

    public class EventTemplateForm
    {
        public Guid IdActTmp { get; set; }

        public int ActId { get; set; }

        public int? ActParent { get; set; }

        [Required]
        public string NamaAct { get; set; }

        public decimal? Bobot { get; set; }

        public int? Durasi { get; set; }

        public int? Predecessor { get; set; }

        public int? Lagging { get; set; }

        public string Relasi { get; set; }

        public int? Urutan { get; set; }

        public bool ISLEAF { get; set; }
    }


    public static class EventTemplateExtension
    {
        public static void Update(this ActTmpChild model, EventTemplateForm form)
        {
            model.NamaAct = form.NamaAct;
            model.ActId = form.ActId;
            model.ActParent = form.ActParent;
            model.Bobot = form.Bobot;
            model.Durasi = form.Durasi;
            model.Predecessor = form.Predecessor;
            model.Lagging = form.Lagging;
            model.Relasi = form.Relasi;
            model.Urutan = form.Urutan;

            if (form.ISLEAF)
            {
                model.ISLEAF = "1";
            }
            else
            {
                model.ISLEAF = "0";
            }

            model.LastUpdateTime = DateTime.Now;
        }
    }
}
