﻿using App.Domain.Models.App;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class AkiCoverViewModel
    {

        public string Level { get; set; }
        public float? Taxes { get; set; }
        public float? InterestRate { get; set; }
        public string InvestmentCategory { get; set; }
        public string IdProposal { get; set; }
        public string ProgramName { get; set; }
        public int? JumlahLop { get; set; }
        public string IdLop { get; set; }
        public string Lokasi { get; set; }
        public string Kapasitas { get; set; }
        public string NomorAccount { get; set; }
        public string PenanggungJawab { get; set; }
        public decimal? NilaiProgram { get; set; }
        public string CapexLine { get; set; }
        public string WaktuPenyelesaian { get; set; }
        public string RencanaSelesai { get; set; }
        public string PemilihanTeknologi { get; set; }
        public string Usulan { get; set; }
        public string Alasan { get; set; }
        public decimal? IRR { get; set; }
        public decimal? NPV { get; set; }
        public decimal? BCR { get; set; }
        public string CAGR { get; set; }
        public int? PaybackPeriode { get; set; }
        public float? DiscountRate { get; set; }
        public string ARPU { get; set; }
        public string Keterangan { get; set; }
        public string Roi { get; set; }
    }

    public class AkiCoverForm
    {   [UIHint("Autocomplete")]
        public string Level { get; set; }
        //LevelValue=CustomField2
        public string LevelValue { get; set; }
        public float? Taxes { get; set; }
        public float? InterestRate { get; set; }
        public string InvestmentCategory { get; set; }
        public string IdProposal { get; set; }
        public string ProgramName { get; set; }
        public int? JumlahLop { get; set; }
        public string IdLop { get; set; }
        public string Lokasi { get; set; }
        public string Kapasitas { get; set; }
        public string NomorAccount { get; set; }
        //[Required]
        public string PenanggungJawab { get; set; }
        
        //[Required]
        //PenanggungJawabId=CustomField1
        public string PenanggungJawabvalue { get; set; }
        public decimal? NilaiProgram { get; set; }
        public string CapexLine { get; set; }
        public string WaktuPenyelesaian { get; set; }
        public string RencanaSelesai { get; set; }
        public string PemilihanTeknologi { get; set; }
        public string Usulan { get; set; }
        public string Alasan { get; set; }
        public decimal? IRR { get; set; }
        public decimal? NPV { get; set; }
        public decimal? BCR { get; set; }
        public string CAGR { get; set; }
        public int? PaybackPeriode { get; set; }
        public float? DiscountRate { get; set; }
        public string ARPU { get; set; }
        public string Keterangan { get; set; }
        public string  Roi { get; set; }
        [UIHint("RoiTemplate")]
        public virtual IList<RoiViewModel> Rois { get; set; }

        public List<SelectListItem> SourceLevelList { get; set; }
        public List<SelectListItem> SourcePenanggungjawab { get; set; }

    }

    public static class AkiCoverExtension
    {
        public static void Update(this AkiCover akicover, AkiCoverForm form)
        {
            akicover.Level = form.Level;
            akicover.KategoriInvestasi = form.InvestmentCategory;
            akicover.NilaiProgram = form.NilaiProgram;
            akicover.InterestRate = form.InterestRate;
            akicover.ProposalId = form.IdProposal;
            akicover.JumlahLop = form.JumlahLop;
            akicover.Location = form.Lokasi;
            akicover.Kapasitas = form.Kapasitas;
            akicover.PenanggungJawab = form.PenanggungJawab;
            akicover.NomorAkun = form.NomorAccount;
            akicover.CapexLine = form.CapexLine;
            akicover.Taxes = form.Taxes;
            akicover.WaktuPenyelesaian = form.WaktuPenyelesaian;
            akicover.RencanaSelesai = form.RencanaSelesai;
            akicover.PemilihanTeknologi = form.PemilihanTeknologi;
            akicover.Usulan = form.Usulan;
            akicover.Alasan = form.Alasan;
            akicover.IRR = form.IRR;
            akicover.NPV = form.NPV;
            akicover.PayBackPeriode = form.PaybackPeriode;
            akicover.BCR = form.BCR;
            akicover.CAGR = form.CAGR;
            akicover.DiscountRate = form.DiscountRate;
            akicover.ARPU = form.ARPU;
            akicover.LopId = form.IdLop;

        }
    }
}
