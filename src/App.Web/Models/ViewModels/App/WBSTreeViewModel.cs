﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.App
{
    public class WBSTreeViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid IdWBS { get; set; }
        public Guid? ParentId { get; set; }
        public string Lokasi { get; set; }
        public string NamaLokasi { get; set; }
        public string GrupHarga { get; set; }
        public string Type { get; set; }
        public string Regional { get; set; }
        public string Witel { get; set; }
        public string Datel { get; set; }
    }

    public class WBSTreeForm
    {
        [Required]
        public string Name { get; set; }
        public Guid IdWBS { get; set; }
        public Guid? ParentId { get; set; }
        public string Lokasi { get; set; }
        [UIHint("Autocomplete")]
        public string NamaLokasi { get; set; }
        public string GrupHarga { get; set; }
        public string Type { get; set; }
        public string Regional { get; set; }
        public string Witel { get; set; }
        public string Datel { get; set; }
        public bool HasKHS { get; set; }
    }

    public class WBSBoQViewModel
    {
        public WBS WBS { get; set; }
        public WBSTree Lokasi { get; set; }
        public List<WBSBoQDto> Paket { get; set; }
        [UIHint("TreeCategory")]
        public string WBSCategory { get; set; }
        [Required]
        [UIHint("SingleFileUploadBoQ")]
        public string FileBoQ { get; set; }
    }
}
