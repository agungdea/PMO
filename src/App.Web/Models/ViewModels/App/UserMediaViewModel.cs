﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class UserMediaViewModel
    {
        public Guid Id { get; set; }
        public string UnitId { get; set; }
        public string MediaDirectory { get; set; }
    }


    public class UserMediaForm
    {
        public string UnitId { get; set; }
        [UIHint("SingleFileUploadCropper")]
        public string MediaDirectory { get; set; }
    }
}
