﻿using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class UnitViewModel
    {
        public string Id { get; set; }
        public string NamaUnit { get; set; }
        public string NamaSingkat { get; set; }
        public string ParentId { get; set; }
        public string STLevel { get; set; }
    }

    public class UnitForm
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public string NamaUnit { get; set; }
        [Required]
        public string NamaSingkat { get; set; }
        public string ParentId { get; set; }
        [Required]
        public string STLevel { get; set; }
    }


    public class SelectUnitItem
    {
        public string Id { get; set; }

        public string Text { get; set; }
    }

    public static class UnitExtension
    {
        private static void Update(this Unit unit, UnitForm form)
        {
            unit.NamaUnit = form.NamaUnit;
            unit.NamaSingkat = form.NamaSingkat;
            unit.ParentId = form.ParentId;
            unit.STLevel = form.STLevel;

        }
    }
}