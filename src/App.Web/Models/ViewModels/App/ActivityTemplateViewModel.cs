﻿using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class ActivityTemplateViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Grup { get; set; }
        public int? Durasi { get; set; }

        [UIHint("UnitTemplate")]
        public string Directorat { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitBisnis { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitKerja { get; set; }
        [UIHint("UnitTemplate")]
        public string Bagian { get; set; }
    }

    public class ActivityTemplateForm
    {
        [Required]
        public string Name { get; set; }
        [UIHint("UnitTemplate")]
        public string Directorat { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitBisnis { get; set; }
        [UIHint("UnitTemplate")]
        public string UnitKerja { get; set; }
        [UIHint("UnitTemplate")]
        public string Bagian { get; set; }
        public int? Durasi { get; set; }
    }

    public static class ActivityExtension
    {
        public static void Update(this ActTemplate model, ActivityTemplateForm form)
        {
            model.Name = form.Name;

            var unit = form.UnitKerja;

            if (form.Bagian != null)
            {
                unit = form.Bagian;
            }

            model.Grup = unit;
            model.Durasi = form.Durasi;
            model.LastUpdateTime = DateTime.Now;
        }
    }
}
