﻿namespace App.Web.Models.ViewModels.App
{
    public class WBSGrandTotalViewModel
    {
        public string CurrencyName { get; set; }
        public decimal? AfterConvertion { get; set; }
        public decimal? Rate { get; set; }
        public decimal? BeforeConvertion { get; set; }

    }
}
