﻿using System.Collections.Generic;

namespace App.Web.Models.ViewModels.App
{
    public class InvestmentCategorySelectorViewModel
    {
        public InvestmentCategorySelectorViewModel(string groupName, List<string> values)
        {
            GroupName = groupName;
            Values = values;
        }

        public string GroupName { get; set; }
        public List<string> Values { get; set; }
    }
}
