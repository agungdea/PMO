﻿using App.Domain.Models.App;
using System;
using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.App
{
    public class HargaViewModel
    {
        public Guid Id { get; set; }
        
        public string IdDesignator { get; set; }
        
        public string MataUang { get; set; }
        
        public decimal HargaMaterial { get; set; }
        
        public decimal HargaJasa { get; set; }
        
        public string Lokasi { get; set; }
    }

    public class HargaForm
    {
        public Guid? Id { get; set; }

        public string IdDesignator { get; set; }

        [Required]
        public string MataUang { get; set; }

        public decimal HargaMaterial { get; set; }

        public decimal HargaJasa { get; set; }

        public string Lokasi { get; set; }
    }

    public static class HargaExtension
    {
        public static void Update(this Harga model, HargaForm form)
        {
            model.Lokasi = form.Lokasi;
            model.MataUang = form.MataUang;
            model.HargaMaterial = form.HargaMaterial;
            model.HargaJasa = form.HargaJasa;
        }
    }
}
