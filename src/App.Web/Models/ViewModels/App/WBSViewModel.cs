﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.App
{
    public class WBSViewModel
    {
        public Guid Id { get; set; }
        public string NamaKegiatan { get; set; }
        public string UnitKerja { get; set; }
        public string UnitBisnis { get; set; }
        public string Direktorat { get; set; }
    }

    public class WBSForm
    {
        public string NamaKegiatan { get; set; }
        public string KodeUnit { get; set; }

        [UIHint("WBSCurrency")]
        public List<WBSCurrencyForm> WBSCurrencies { get; set; }
    }

    public class WBSUnit
    {
        public string KodeUnit { get; set; }
        public string UnitKerja { get; set; }
        public string UnitBisnis { get; set; }
        public string Direktorat { get; set; }
        public string Bagian { get; set; }
    }

    public class SelectWBSItem
    {
        public string Id { get; set; }

        public string Text { get; set; }
    }

}
