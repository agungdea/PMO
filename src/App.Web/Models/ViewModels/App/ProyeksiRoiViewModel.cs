﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class ProyeksiRoiViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
