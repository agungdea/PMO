﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class UserUnitViewModel
    {
        public string UserId { get; set; }

        public string Direktorat { get; set; }
        public string UnitBisnis { get; set; }
        public string UnitKerja { get; set; }
        public string Bagian { get; set; }
    }
}
