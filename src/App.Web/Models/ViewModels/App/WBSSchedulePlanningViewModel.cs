﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class WBSSchedulePlanningViewModel
    {
        public Guid IdWBSTree { get; set; }
        public DateTime Start { get; set; }
        public int? Durasi { get; set; }
        public Guid IdActTmpChild { get; set; }
    }
}
