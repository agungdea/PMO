﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class WBSScheduleViewModel
    {
        public Guid IdWBSTree { get; set; }
        public int DurasiReal { get; set; }
        public DateTime Start { get; set; }
        public DateTime Finish { get; set; }
        public Guid IdActTmpChild { get; set; }
    }
}
