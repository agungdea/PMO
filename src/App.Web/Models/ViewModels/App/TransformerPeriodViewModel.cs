﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models.ViewModels.App
{
    public class TransformerPeriodViewModel
    {
        public string Period { get; set; }
        public string Display_name { get; set; }
    }
}
