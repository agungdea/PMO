﻿using App.Domain.Models.App;
using System;
using System.ComponentModel.DataAnnotations;

namespace App.Web.Models.ViewModels.App
{
    public class CurrencyViewModel
    {
        public Guid Id { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyValue { get; set; }
    }

    public class CurrencyForm
    {
        [Required]
        public string CurrencyCode { get; set; }
        [Required]
        public string CurrencyValue { get; set; }
    }

    public static class CurrencyExtension
    {
        public static void Update(this Currency currency, CurrencyForm model)
        {
            currency.CurrencyCode = model.CurrencyCode;
            currency.CurrencyValue = model.CurrencyValue;
        }
    }
}
