﻿using App.Domain.Models.App;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Controllers.ViewComponents
{
    [ViewComponent(Name = "UnitHeader")]
    public class UnitHeaderController : ViewComponent
    {
        private readonly IUnitService _unitService;

        public UnitHeaderController(IUnitService unitService)
        {
            _unitService = unitService;
        }

        public IViewComponentResult Invoke(string unitId)
        {
            if (unitId == null)
            {
                return View("_UnitHeader");
            }

            var model = new WBSUnit();

            var unitBagian = _unitService.GetById(unitId);

            if (unitBagian != null && unitBagian.STLevel == Unit.STLevelBagian)
                unitId = unitBagian.ParentId;
            else {
                unitBagian = null;
            }
            var unitKerja = _unitService.GetById(unitId);
            if (unitKerja != null && unitKerja.STLevel == Unit.STLevelUnitKerja)
                unitId = unitKerja.ParentId;

            else
            {
                unitKerja = null;
            }
            var unitBisnis = _unitService.GetById(unitId);
            var unitDirectorat = _unitService.GetById(unitBisnis.ParentId);

            if (unitDirectorat != null)
            {
                model.Direktorat = unitDirectorat.NamaUnit;
            }

            if (unitBisnis != null)
            {
                model.UnitBisnis = unitBisnis.NamaUnit;
                model.KodeUnit = unitBisnis.Id;
            }

            if (unitKerja != null)
            {
                model.UnitKerja = unitKerja.NamaUnit;
                model.KodeUnit = unitKerja.Id;
            }

            if (unitBagian != null)
            {
                model.Bagian = unitBagian.NamaUnit;
                model.KodeUnit = unitBagian.Id;
            }

            return View("_UnitHeader", model);
        }
    }
}
