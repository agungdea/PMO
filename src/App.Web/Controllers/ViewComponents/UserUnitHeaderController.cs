﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Services.App;
using App.Web.Models.ViewModels.App;
using App.Domain.Models.App;
using App.Domain.Models.Identity;
using Microsoft.AspNetCore.Identity;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Controllers.ViewComponents
{
    [ViewComponent(Name = "UserUnitHeader")]
    public class UserUnitHeaderController : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUnitService _unitService;

        public UserUnitHeaderController(IUnitService unitService,
            UserManager<ApplicationUser> userManager)
        {
            _unitService = unitService;
            _userManager = userManager;
        }

        public IViewComponentResult Invoke(string userId)
        {
            var user = _userManager.FindByIdAsync(userId).Result;

            var claims = _userManager.GetClaimsAsync(user).Result;

            var unitId = claims
                .FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;

            if (unitId == null)
            {
                return View("_UserUnitHeader");
            }

            var model = new WBSUnit();

            var unitBagian = _unitService.GetById(unitId);

            if (unitBagian != null && unitBagian.STLevel == Unit.STLevelBagian)
            {
                unitId = unitBagian.ParentId;
            }
            else
            {
                unitBagian = null;
            }
            if (unitBagian != null && unitBagian.STLevel == Unit.STLevelBagian)
                unitId = unitBagian.ParentId;
            else
            {
                unitBagian = null;
            }
            var unitKerja = _unitService.GetById(unitId);
            if (unitKerja != null && unitKerja.STLevel == Unit.STLevelUnitKerja)
                unitId = unitKerja.ParentId;

            else
            {
                unitKerja = null;
            }
            var unitBisnis = _unitService.GetById(unitId);
            var unitDirectorat = _unitService.GetById(unitBisnis.ParentId);

            if (unitDirectorat != null)
            {
                model.Direktorat = unitDirectorat.NamaUnit;
            }

            if (unitBisnis != null)
            {
                model.UnitBisnis = unitBisnis.NamaUnit;
                model.KodeUnit = unitBisnis.Id;
            }

            if (unitKerja != null)
            {
                model.UnitKerja = unitKerja.NamaUnit;
                model.KodeUnit = unitKerja.Id;
            }
            
            if (unitBagian != null)
            {
                model.Bagian = unitBagian.NamaUnit;
                model.KodeUnit = unitBagian.Id;
            }

            return View("_UserUnitHeader", model);
        }
    }
}
