﻿using App.Helper;
using App.Web.Models.ViewModels.App;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace App.Web.Controllers.ViewComponents
{
    [ViewComponent(Name = "InvestmentCategorySelector")]
    public class InvestmentCategorySelectorController : ViewComponent
    {
        private const string INVESTMENT_CONFIG_NAME = "investment.category";
        private readonly ConfigHelper _config;

        public InvestmentCategorySelectorController(ConfigHelper config)
        {
            _config = config;
        }

        public IViewComponentResult Invoke(string name = "InvestmentCategory", string selected = null)
        {
            try
            {
                var selector = new List<InvestmentCategorySelectorViewModel>();

                var con = _config.GetConfig(INVESTMENT_CONFIG_NAME);

                var groups = con.Split('\n');

                foreach (var group in groups)
                {
                    var groupName = group.Split(':')[0];
                    var values = group.Split(':')[1].Split(',').ToList();
                    selector.Add(new InvestmentCategorySelectorViewModel(groupName, values));
                }

                ViewBag.Selected = selected;
                ViewBag.Name = name;

                return View("_InvestmentCategorySelector", selector);
            }
            catch (System.Exception)
            {
                return View("_InvestmentCategorySelector", new List<InvestmentCategorySelectorViewModel>());
            }
        }
    }
}
