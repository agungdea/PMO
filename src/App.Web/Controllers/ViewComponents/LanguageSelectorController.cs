﻿using App.Services.Localization;
using App.Web.Models.ViewModels.Localization;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace App.Web.Controllers.ViewComponents
{
    [ViewComponent(Name = "LanguageSelector")]
    public class LanguageSelectorController : ViewComponent
    {
        private readonly ILanguageService _languageService;

        public LanguageSelectorController(ILanguageService languageService,
            IMapper mapper)
        {
            _languageService = languageService;
        }

        public IViewComponentResult Invoke(bool backAdmin = false)
        {
            var model = Mapper.Map<List<LanguageViewModel>>(_languageService.GetAll());

            if(backAdmin)
                return View("_LanguageSelectorBackAdmin", model);

            return View("_LanguageSelector", model);
        }
    }
}
