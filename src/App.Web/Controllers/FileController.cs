using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using FileIO = System.IO.File;
using System;
using App.Domain.Models.Core;
using App.Helper;

namespace App.Web.Areas.Admin.Controllers.Api
{
    [Produces("application/json")]
    public class FileController : Controller
    {
        private readonly IHostingEnvironment _env;
        private readonly FileHelper _fileHelper;
        private const string _uploadDir = "uploads/temp";

        public FileController(IHostingEnvironment env,
            FileHelper fileHelper)
        {
            _env = env;
            _fileHelper = fileHelper;
        }

        [HttpPost]
        public IActionResult Upload(IFormFile filedata)
        {
            var upload = Path.Combine(_env.WebRootPath, _uploadDir);
            if (!Directory.Exists(upload))
            {
                Directory.CreateDirectory(upload);
            }

            if (filedata.Length > 0)
            {
                var prefix = Guid.NewGuid().ToString("n");
                var filepath = Path.Combine(upload, prefix + filedata.FileName);
                var imageSrc = $"{_uploadDir}/{prefix}{filedata.FileName}";
                using (var filestream = new FileStream(filepath, FileMode.Create))
                {
                    filedata.CopyTo(filestream);
                }

                var result = new Attachment()
                {
                    Name = filedata.FileName,
                    FileType = _fileHelper.GetMimeTypeByName(filedata.FileName),
                    Type = Attachment.FILE_TYPE_UPLOAD,
                    Path = imageSrc,
                    Size = filedata.Length / 1024
                };
                
                return Json(new
                {
                    data = result
                });
            }

            return Json(new BadRequestResult());
        }

        [HttpPost]
        public IActionResult Delete()
        {
            var upload = Path.Combine(_env.WebRootPath, _uploadDir);
            var listFile = Directory.GetFiles(upload);
            if (listFile.Length != 0 && Directory.Exists(upload))
            {
                foreach (var item in listFile)
                {
                    if ((DateTime.Now.Subtract(FileIO.GetCreationTime(item)).TotalDays > 1 ? true : false))
                    {
                        FileIO.Delete(item);
                    }
                }
            }
            return Json(new OkResult());
        }
    }
}