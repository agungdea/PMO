﻿using App.Helper;
using DinkToPdf;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Net.Http;

namespace App.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly LocalizationHelper Localization;
        private readonly PdfHelper pdfHelper;
        private readonly IHostingEnvironment environment;


        public HomeController(LocalizationHelper localization, IHostingEnvironment environment, PdfHelper pdfHelper)
        {
            Localization = localization;
            this.pdfHelper = pdfHelper;
            this.environment = environment;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Index", "Home", new { area = "Admin" });
        }

        public IActionResult About()
        {
            var doc = new HtmlToPdfDocument()
            {
                GlobalSettings = {
        ColorMode = ColorMode.Color,
        Orientation = Orientation.Portrait,
        PaperSize = PaperKind.A4,
        Margins = new MarginSettings() { Top = 10 },
        Out = environment.WebRootPath,
    },
                Objects = {
        new ObjectSettings()
        {
            Page = "http://google.com/",
        },
    }
            };

            return View();
        }

        [HttpGet]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            GetPdf();

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        [HttpPost]
        public bool CheckLoggedIn()
        {
            if (User.Identity.IsAuthenticated)
            {
                return true;
            }

            return false;
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Localization.SetCulture(culture);

            return LocalRedirect(returnUrl);
        }

        public void GetPdf()
        {
            //using (MemoryStream ms = new MemoryStream())
            //{

            //    pdfHelper.GetPDFAttachment(ms);
            //    Response.ContentType = "pdf/application";
            //    Response.Headers.Add("content-disposition",
            //    "attachment;filename=invoice.pdf");
            //    Response.Body.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
            //    Response.Body.Flush();
            //}
        }


    }
}
