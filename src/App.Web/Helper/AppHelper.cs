﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using FileIO = System.IO.File;
using System.Linq;
using System.Threading.Tasks;
using App.Domain.Models.Core;
using ImageProcessorCore;
using System.Text;
using MimeKit.Cryptography;
using App.Domain.Models.Identity;
using System.Security.Principal;

namespace App.Web.Helper
{
    public static class AppHelper
    {
        public static bool IsAdministrator(this IPrincipal user)
        {

            if (user.IsInRole(ApplicationRole.Administrator))
            {
                return true;
            }

            return false;
        }

        public static bool IsUser(this IPrincipal user)
        {
            if (user.IsInRole(ApplicationRole.User))
            {
                return true;
            }

            return false;
        }
        public static bool IsPurchaseRequisition(this IPrincipal user)
        {
            if (user.IsInRole(ApplicationRole.PurchaseRequisition))
            {
                return true;
            }

            return false;
        }

        public static bool IsAdminRegional(this IPrincipal user)
        {
            if (user.IsInRole(ApplicationRole.AdminReg))
            {
                return true;
            }
            return false;
        }

        public static bool IsCategoryManagement(this IPrincipal user)
        {
            if (user.IsInRole(ApplicationRole.CategoryManagement))
            {
                return true;
            }
            return false;
        }

        public static bool IsSTOManagement(this IPrincipal user)
        {
            if (user.IsInRole(ApplicationRole.StoManagement))
            {
                return true;
            }
            return false;
        }
    }
}
