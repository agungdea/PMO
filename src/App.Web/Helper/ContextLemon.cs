﻿using App.Helper;
using App.Web.Models.ViewModels.Lemon;
using Pomelo.Data.MySql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Helper
{
    public class ContextLemon
    {
        public string ConnectionString { get; set; }
        public ContextLemon(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        public List<LemonViewModel> GetAllUser(string EndpointEmail)
        {
            List<LemonViewModel> list = new List<LemonViewModel>();

            using (MySqlConnection conn = GetConnection())
            {
                try
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand(" select n_nik,v_nama_karyawan,v_short_posisi,objidposisi  from hrdwh_rpt01 ", conn);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new LemonViewModel()
                            {
                                nik = reader["n_nik"].ToString(),
                                nama = reader["v_nama_karyawan"].ToString(),
                                Jabatan = reader["v_short_posisi"].ToString(),
                                kdjbatan = Convert.ToDecimal(reader["objidposisi"]),
                                Email = reader["n_nik"].ToString() + EndpointEmail

                            });
                        }
                    }

                    conn.Dispose();
                }
                catch (Exception)
                {
                    conn.Dispose();

                }
              
            }
            return list;
        }

        public List<LemonViewModel> GetbyNikUser(string Nik)
        {
            List<LemonViewModel> list = new List<LemonViewModel>();

            using (MySqlConnection conn = GetConnection())
            {
                try
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand(" select n_nik,v_nama_karyawan,v_short_posisi,objidposisi  from hrdwh_rpt01 where n_nik= '" + Nik + "' ", conn);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new LemonViewModel()
                            {
                                nik = reader["n_nik"].ToString(),
                                nama = reader["v_nama_karyawan"].ToString(),
                                Jabatan = reader["v_short_posisi"].ToString(),
                                kdjbatan = Convert.ToDecimal(reader["objidposisi"]),

                            });
                        }
                    }

                }
                catch (Exception)
                {

                   
                }
                
                conn.Dispose();
            }
            return list;
        }
    }
}
