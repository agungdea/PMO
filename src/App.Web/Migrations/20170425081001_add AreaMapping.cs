﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addAreaMapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AreaMapping",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AreaDatel = table.Column<string>(nullable: true),
                    AreaKode = table.Column<string>(nullable: true),
                    AreaRegional = table.Column<string>(nullable: true),
                    AreaSto = table.Column<string>(nullable: true),
                    AreaStoName = table.Column<string>(nullable: true),
                    AreaWitel = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AreaMapping_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AreaMapping_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AreaMapping_CreatedBy",
                table: "AreaMapping",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_AreaMapping_LastEditedBy",
                table: "AreaMapping",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AreaMapping");
        }
    }
}
