﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class updateWBStree : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_WBSTree_ParentId",
                table: "WBSTree",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_WBSTree_WBSTree_ParentId",
                table: "WBSTree",
                column: "ParentId",
                principalTable: "WBSTree",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WBSTree_WBSTree_ParentId",
                table: "WBSTree");

            migrationBuilder.DropIndex(
                name: "IX_WBSTree_ParentId",
                table: "WBSTree");
        }
    }
}
