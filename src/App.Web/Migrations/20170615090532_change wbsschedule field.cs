﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class changewbsschedulefield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Activity",
                table: "WBSSchedule");

            migrationBuilder.DropColumn(
                name: "Bobot",
                table: "WBSSchedule");

            migrationBuilder.DropColumn(
                name: "ProjectFinish",
                table: "WBSSchedule");

            migrationBuilder.DropColumn(
                name: "ProjectStart",
                table: "WBSSchedule");

            migrationBuilder.AddColumn<DateTime>(
                name: "Finish",
                table: "WBSSchedule",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "Start",
                table: "WBSSchedule",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Finish",
                table: "WBSSchedule");

            migrationBuilder.DropColumn(
                name: "Start",
                table: "WBSSchedule");

            migrationBuilder.AddColumn<string>(
                name: "Activity",
                table: "WBSSchedule",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Bobot",
                table: "WBSSchedule",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ProjectFinish",
                table: "WBSSchedule",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProjectStart",
                table: "WBSSchedule",
                nullable: true);
        }
    }
}
