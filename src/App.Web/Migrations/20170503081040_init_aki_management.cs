﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class init_aki_management : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Aki",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AkiName = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    Document = table.Column<string>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    PeriodId = table.Column<string>(nullable: false),
                    PeriodName = table.Column<string>(nullable: false),
                    ProgramId = table.Column<string>(nullable: false),
                    ProgramName = table.Column<string>(nullable: false),
                    StrategiInitiativeId = table.Column<string>(nullable: false),
                    StrategiInitiativeName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aki", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Aki_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Aki_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AkiCover",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AkiId = table.Column<Guid>(nullable: false),
                    Alasan = table.Column<string>(nullable: true),
                    CAGR = table.Column<string>(nullable: true),
                    CapexLine = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    DiscountRate = table.Column<float>(nullable: true),
                    Jumlah = table.Column<int>(nullable: true),
                    Kapasitas = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    LopId = table.Column<string>(nullable: true),
                    NilaiProgram = table.Column<decimal>(nullable: true),
                    NomorAkun = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    PemilihanTeknologi = table.Column<string>(nullable: true),
                    PenanggungJawab = table.Column<string>(nullable: true),
                    ProposalId = table.Column<string>(nullable: true),
                    RencanaSelesai = table.Column<string>(nullable: true),
                    Roi = table.Column<string>(nullable: true),
                    Usulan = table.Column<string>(nullable: true),
                    WaktuPenyelesaian = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AkiCover", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AkiCover_Aki_AkiId",
                        column: x => x.AkiId,
                        principalTable: "Aki",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AkiCover_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AkiCover_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AkiValuation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AddBackDepresiation = table.Column<decimal>(nullable: true),
                    AkiId = table.Column<Guid>(nullable: false),
                    BCR = table.Column<decimal>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CumulativeNetCashFlow = table.Column<decimal>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    Depreciation = table.Column<decimal>(nullable: true),
                    Ebit = table.Column<decimal>(nullable: true),
                    Ebitda = table.Column<decimal>(nullable: true),
                    Expenses = table.Column<decimal>(nullable: true),
                    IRR = table.Column<decimal>(nullable: true),
                    InterestRate = table.Column<float>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    NPV = table.Column<decimal>(nullable: true),
                    NetCashFlow = table.Column<decimal>(nullable: true),
                    NetIncome = table.Column<decimal>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    PayBackPeriode = table.Column<int>(nullable: true),
                    Revenue = table.Column<decimal>(nullable: true),
                    Taxes = table.Column<decimal>(nullable: true),
                    Year = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AkiValuation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AkiValuation_Aki_AkiId",
                        column: x => x.AkiId,
                        principalTable: "Aki",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AkiValuation_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AkiValuation_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Aki_CreatedBy",
                table: "Aki",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Aki_LastEditedBy",
                table: "Aki",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_AkiCover_AkiId",
                table: "AkiCover",
                column: "AkiId");

            migrationBuilder.CreateIndex(
                name: "IX_AkiCover_CreatedBy",
                table: "AkiCover",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_AkiCover_LastEditedBy",
                table: "AkiCover",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_AkiValuation_AkiId",
                table: "AkiValuation",
                column: "AkiId");

            migrationBuilder.CreateIndex(
                name: "IX_AkiValuation_CreatedBy",
                table: "AkiValuation",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_AkiValuation_LastEditedBy",
                table: "AkiValuation",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AkiCover");

            migrationBuilder.DropTable(
                name: "AkiValuation");

            migrationBuilder.DropTable(
                name: "Aki");
        }
    }
}
