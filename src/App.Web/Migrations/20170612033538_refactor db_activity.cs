﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class refactordb_activity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActTemplate",
                columns: table => new
                {
                    KD_ACT_TEMP = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    DURASI = table.Column<int>(nullable: true),
                    GRUP = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    NM_ACT_TEMP = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActTemplate", x => x.KD_ACT_TEMP);
                    table.ForeignKey(
                        name: "FK_ActTemplate_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActTemplate_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActTmpChild",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ACT_ID = table.Column<int>(nullable: false),
                    ACT_PARENT = table.Column<int>(nullable: true),
                    BOBOT = table.Column<decimal>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    DURASI = table.Column<int>(nullable: true),
                    ISLEAF = table.Column<string>(nullable: true),
                    KD_ACT_TEMP = table.Column<Guid>(nullable: false),
                    LAGGING = table.Column<int>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    NM_ACT = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    PREDECESSOR = table.Column<int>(nullable: true),
                    RELASI = table.Column<string>(nullable: true),
                    URUTAN = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActTmpChild", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActTmpChild_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActTmpChild_ActTemplate_KD_ACT_TEMP",
                        column: x => x.KD_ACT_TEMP,
                        principalTable: "ActTemplate",
                        principalColumn: "KD_ACT_TEMP",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActTmpChild_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActTemplate_CreatedBy",
                table: "ActTemplate",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ActTemplate_LastEditedBy",
                table: "ActTemplate",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ActTmpChild_CreatedBy",
                table: "ActTmpChild",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ActTmpChild_KD_ACT_TEMP",
                table: "ActTmpChild",
                column: "KD_ACT_TEMP");

            migrationBuilder.CreateIndex(
                name: "IX_ActTmpChild_LastEditedBy",
                table: "ActTmpChild",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActTmpChild");

            migrationBuilder.DropTable(
                name: "ActTemplate");
        }
    }
}
