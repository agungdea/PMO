﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class init_wbs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WBSCurrency",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CurrencyCode = table.Column<string>(nullable: true),
                    CurrencyValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSCurrency", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WBS",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CurrencyId = table.Column<Guid>(nullable: false),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    NamaKegiatan = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    Unit = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBS", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBS_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBS_WBSCurrency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "WBSCurrency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WBS_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WBSTree",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IdWBS = table.Column<Guid>(nullable: false),
                    Lokasi = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSTree", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBSTree_WBS_IdWBS",
                        column: x => x.IdWBS,
                        principalTable: "WBS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WBS_CreatedBy",
                table: "WBS",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBS_CurrencyId",
                table: "WBS",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_WBS_LastEditedBy",
                table: "WBS",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSTree_IdWBS",
                table: "WBSTree",
                column: "IdWBS");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WBSTree");

            migrationBuilder.DropTable(
                name: "WBS");

            migrationBuilder.DropTable(
                name: "WBSCurrency");
        }
    }
}
