﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class emailArchieve : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "SystemSetting",
                table: "WebSetting",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "EmailArchieve",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Bcc = table.Column<string>(nullable: true),
                    Cc = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    ExceptionSendingMessage = table.Column<string>(nullable: true),
                    From = table.Column<string>(nullable: false),
                    FromName = table.Column<string>(nullable: true),
                    HtmlMessage = table.Column<string>(nullable: true),
                    IsSent = table.Column<bool>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastTrySentDate = table.Column<DateTime>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    PlainMessage = table.Column<string>(nullable: true),
                    ReplyTo = table.Column<string>(nullable: true),
                    ReplyToName = table.Column<string>(nullable: true),
                    SentDate = table.Column<DateTime>(nullable: true),
                    Subject = table.Column<string>(nullable: false),
                    Tos = table.Column<string>(nullable: false),
                    TrySentCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailArchieve", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmailArchieve_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmailArchieve_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmailArchieve_CreatedBy",
                table: "EmailArchieve",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_EmailArchieve_LastEditedBy",
                table: "EmailArchieve",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmailArchieve");

            migrationBuilder.DropColumn(
                name: "SystemSetting",
                table: "WebSetting");
        }
    }
}
