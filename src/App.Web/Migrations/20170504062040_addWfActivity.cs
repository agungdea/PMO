﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addWfActivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ActivityId",
                table: "WfProcess",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrevActivityId",
                table: "WfProcess",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WfActivity",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    WfProcessId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WfActivity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WfActivity_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WfActivity_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WfActivity_WfProcess_WfProcessId",
                        column: x => x.WfProcessId,
                        principalTable: "WfProcess",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WfProcess_ActivityId",
                table: "WfProcess",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_WfProcess_PrevActivityId",
                table: "WfProcess",
                column: "PrevActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_WfActivity_CreatedBy",
                table: "WfActivity",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WfActivity_LastEditedBy",
                table: "WfActivity",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WfActivity_WfProcessId",
                table: "WfActivity",
                column: "WfProcessId");

            migrationBuilder.AddForeignKey(
                name: "FK_WfProcess_WfActivity_ActivityId",
                table: "WfProcess",
                column: "ActivityId",
                principalTable: "WfActivity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WfProcess_WfActivity_PrevActivityId",
                table: "WfProcess",
                column: "PrevActivityId",
                principalTable: "WfActivity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WfProcess_WfActivity_ActivityId",
                table: "WfProcess");

            migrationBuilder.DropForeignKey(
                name: "FK_WfProcess_WfActivity_PrevActivityId",
                table: "WfProcess");

            migrationBuilder.DropTable(
                name: "WfActivity");

            migrationBuilder.DropIndex(
                name: "IX_WfProcess_ActivityId",
                table: "WfProcess");

            migrationBuilder.DropIndex(
                name: "IX_WfProcess_PrevActivityId",
                table: "WfProcess");

            migrationBuilder.DropColumn(
                name: "ActivityId",
                table: "WfProcess");

            migrationBuilder.DropColumn(
                name: "PrevActivityId",
                table: "WfProcess");
        }
    }
}
