﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addisdeleteonbasemodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Cms_Page",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "WfProcess",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "WfActivityView",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "WfActivity",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "LocaleResource",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Language",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "WebSetting",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "EmailArchieve",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "WBS",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Justification",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Category",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "AreaMapping",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "AkiValuation",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "AkiCover",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                table: "Aki",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Cms_Page");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "WfProcess");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "WfActivityView");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "WfActivity");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "LocaleResource");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Language");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "WebSetting");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "EmailArchieve");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "WBS");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Justification");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Category");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "AreaMapping");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "AkiValuation");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                table: "Aki");
        }
    }
}
