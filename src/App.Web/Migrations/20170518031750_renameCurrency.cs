﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class renameCurrency : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WBS_WBSCurrency_CurrencyId",
                table: "WBS");

            migrationBuilder.DropTable(
                name: "WBSCurrency");

            migrationBuilder.DropIndex(
                name: "IX_WBS_CurrencyId",
                table: "WBS");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "WBS");

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CurrencyCode = table.Column<string>(nullable: true),
                    CurrencyValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.AddColumn<Guid>(
                name: "CurrencyId",
                table: "WBS",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "WBSCurrency",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CurrencyCode = table.Column<string>(nullable: true),
                    CurrencyValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSCurrency", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WBS_CurrencyId",
                table: "WBS",
                column: "CurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_WBS_WBSCurrency_CurrencyId",
                table: "WBS",
                column: "CurrencyId",
                principalTable: "WBSCurrency",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
