﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addWbsBoqGrantTotal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WBSBoQGrantTotal",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BoQBeforeConverstion = table.Column<decimal>(nullable: true),
                    BoQTotalConversion = table.Column<decimal>(nullable: false),
                    CurrencyId = table.Column<Guid>(nullable: false),
                    IdWBStree = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSBoQGrantTotal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBSBoQGrantTotal_Currency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBSBoQGrantTotal_WBSTree_IdWBStree",
                        column: x => x.IdWBStree,
                        principalTable: "WBSTree",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WBSBoQGrantTotal_CurrencyId",
                table: "WBSBoQGrantTotal",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_WBSBoQGrantTotal_IdWBStree",
                table: "WBSBoQGrantTotal",
                column: "IdWBStree");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WBSBoQGrantTotal");
        }
    }
}
