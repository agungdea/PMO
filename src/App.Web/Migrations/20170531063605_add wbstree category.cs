﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addwbstreecategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WBStreeCategory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false),
                    WBStreeId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBStreeCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBStreeCategory_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBStreeCategory_WBSTree_WBStreeId",
                        column: x => x.WBStreeId,
                        principalTable: "WBSTree",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WBStreeCategory_CategoryId",
                table: "WBStreeCategory",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_WBStreeCategory_WBStreeId",
                table: "WBStreeCategory",
                column: "WBStreeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WBStreeCategory");
        }
    }
}
