﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace App.Web.Migrations
{
    public partial class AddWfProcess : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "WfProcessId",
                table: "Justification",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WfProcess",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    ProcessDefinitionId = table.Column<string>(nullable: true),
                    ProcessId = table.Column<string>(nullable: true),
                    Requester = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WfProcess", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WfProcess_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WfProcess_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Justification_WfProcessId",
                table: "Justification",
                column: "WfProcessId");

            migrationBuilder.CreateIndex(
                name: "IX_WfProcess_CreatedBy",
                table: "WfProcess",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WfProcess_LastEditedBy",
                table: "WfProcess",
                column: "LastEditedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_Justification_WfProcess_WfProcessId",
                table: "Justification",
                column: "WfProcessId",
                principalTable: "WfProcess",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Justification_WfProcess_WfProcessId",
                table: "Justification");

            migrationBuilder.DropTable(
                name: "WfProcess");

            migrationBuilder.DropIndex(
                name: "IX_Justification_WfProcessId",
                table: "Justification");

            migrationBuilder.DropColumn(
                name: "WfProcessId",
                table: "Justification");
        }
    }
}
