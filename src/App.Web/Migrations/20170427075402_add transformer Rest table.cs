﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addtransformerResttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TransformerPeriod",
                columns: table => new
                {
                    Period = table.Column<string>(nullable: false),
                    Display_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransformerPeriod", x => x.Period);
                });

            migrationBuilder.CreateTable(
                name: "TransformerProgram",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Backactual = table.Column<string>(nullable: true),
                    Backchgreq = table.Column<string>(nullable: true),
                    BodChampion_id = table.Column<string>(nullable: true),
                    Btp = table.Column<string>(nullable: true),
                    BusinessRisk = table.Column<string>(nullable: true),
                    BusinessRisk2 = table.Column<string>(nullable: true),
                    Closeddate = table.Column<DateTime>(nullable: true),
                    Comments = table.Column<string>(nullable: true),
                    Date_approved = table.Column<DateTime>(nullable: false),
                    Date_submitted = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Draft = table.Column<string>(nullable: true),
                    Generator = table.Column<string>(nullable: true),
                    Initiative_id = table.Column<string>(nullable: true),
                    Issuemgt = table.Column<string>(nullable: true),
                    Order = table.Column<string>(nullable: true),
                    Organization_id = table.Column<string>(nullable: true),
                    Planamount = table.Column<string>(nullable: true),
                    Planuser = table.Column<string>(nullable: true),
                    ProgramStatus = table.Column<string>(nullable: true),
                    ProgramType = table.Column<string>(nullable: true),
                    Progressworkflowmode_id = table.Column<string>(nullable: true),
                    Quantifier = table.Column<string>(nullable: true),
                    QuickWin = table.Column<string>(nullable: true),
                    Revision = table.Column<string>(nullable: true),
                    RiskMitigation = table.Column<string>(nullable: true),
                    RiskMitigation2 = table.Column<string>(nullable: true),
                    SiId = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Usedependency = table.Column<string>(nullable: true),
                    Warflag = table.Column<string>(nullable: true),
                    Weight = table.Column<string>(nullable: true),
                    WorkflowStatus = table.Column<string>(nullable: true),
                    enabler = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransformerProgram", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransformerSi",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Abbreviation = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Parent_id = table.Column<string>(nullable: true),
                    PeriodId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransformerSi", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransformerPeriod");

            migrationBuilder.DropTable(
                name: "TransformerProgram");

            migrationBuilder.DropTable(
                name: "TransformerSi");
        }
    }
}
