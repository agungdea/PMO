﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addJustification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Justification",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AKIId = table.Column<Guid>(nullable: true),
                    Anggaran = table.Column<string>(nullable: true),
                    AspekBisnis = table.Column<string>(nullable: true),
                    AspekStrategis = table.Column<string>(nullable: true),
                    BasicDesign = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    DasarKebutuhanBarangJasa = table.Column<string>(nullable: true),
                    DistribusiPenggunaan = table.Column<string>(nullable: true),
                    JenisKebutuhan = table.Column<string>(nullable: true),
                    JenisPengeluaran = table.Column<int>(nullable: false),
                    JumlahAnggaran = table.Column<decimal>(nullable: false),
                    JumlahKebutuhanBarangJasa = table.Column<string>(nullable: true),
                    JustificationStatus = table.Column<string>(nullable: true),
                    KelasAsset = table.Column<string>(nullable: true),
                    LampiranDasarKebutuhanBarangJasa = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    LatarBelakang = table.Column<string>(nullable: true),
                    NomorDRK = table.Column<string>(nullable: true),
                    NomorDRP = table.Column<string>(nullable: true),
                    NoteAnggaran = table.Column<string>(nullable: true),
                    NoteAspekBisnis = table.Column<string>(nullable: true),
                    NoteAspekStrategis = table.Column<string>(nullable: true),
                    NoteDistribusiPenggunaan = table.Column<string>(nullable: true),
                    NoteJumlahKebutuhan = table.Column<string>(nullable: true),
                    NoteLatarBelakang = table.Column<string>(nullable: true),
                    NotePenutup = table.Column<string>(nullable: true),
                    NotePosisiPersediaan = table.Column<string>(nullable: true),
                    NoteRencanaPelaksanaan = table.Column<string>(nullable: true),
                    NoteSpesifikasiTeknik = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    Penutup = table.Column<string>(nullable: true),
                    Penyusun = table.Column<string>(nullable: true),
                    PosisiPersediaan = table.Column<string>(nullable: true),
                    PusatPertanggungjawaban = table.Column<string>(nullable: true),
                    RencanaPelaksanaan = table.Column<string>(nullable: true),
                    ShowBoQ = table.Column<bool>(nullable: false),
                    ShowWBS = table.Column<bool>(nullable: false),
                    SpesifikasiTeknik = table.Column<string>(nullable: true),
                    TOR = table.Column<string>(nullable: true),
                    WBSId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Justification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Justification_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Justification_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Justification_CreatedBy",
                table: "Justification",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Justification_LastEditedBy",
                table: "Justification",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Justification");
        }
    }
}
