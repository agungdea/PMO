﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class recratetableplanning : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WBSSchedulePlanning",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    Finish = table.Column<DateTime>(nullable: false),
                    IdActTmpChild = table.Column<Guid>(nullable: false),
                    IdWBSTree = table.Column<Guid>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    Start = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSSchedulePlanning", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBSSchedulePlanning_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WBSSchedulePlanning_ActTmpChild_IdActTmpChild",
                        column: x => x.IdActTmpChild,
                        principalTable: "ActTmpChild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBSSchedulePlanning_WBSTree_IdWBSTree",
                        column: x => x.IdWBSTree,
                        principalTable: "WBSTree",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBSSchedulePlanning_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedulePlanning_CreatedBy",
                table: "WBSSchedulePlanning",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedulePlanning_IdActTmpChild",
                table: "WBSSchedulePlanning",
                column: "IdActTmpChild");

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedulePlanning_IdWBSTree",
                table: "WBSSchedulePlanning",
                column: "IdWBSTree");

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedulePlanning_LastEditedBy",
                table: "WBSSchedulePlanning",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WBSSchedulePlanning");
        }
    }
}
