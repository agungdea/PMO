﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addingWbsBoqCurrencyFixingWbsTree : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomField1",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomField2",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomField3",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GrupHarga",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastEditedBy",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdateTime",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "WBSTree",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "OtherInfo",
                table: "WBSTree",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WBSBoQ",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    GrupHarga = table.Column<string>(nullable: true),
                    HargaJasa = table.Column<decimal>(nullable: false),
                    HargaMaterial = table.Column<decimal>(nullable: false),
                    IdDesignator = table.Column<string>(nullable: true),
                    IdWBSTree = table.Column<Guid>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    Qty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSBoQ", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBSBoQ_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WBSBoQ_WBSTree_IdWBSTree",
                        column: x => x.IdWBSTree,
                        principalTable: "WBSTree",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBSBoQ_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WBSCurrency",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CurrencyId = table.Column<Guid>(nullable: false),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    Rate = table.Column<decimal>(nullable: false),
                    WBSId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSCurrency", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBSCurrency_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WBSCurrency_Currency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBSCurrency_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WBSCurrency_WBS_WBSId",
                        column: x => x.WBSId,
                        principalTable: "WBS",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WBSTree_CreatedBy",
                table: "WBSTree",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSTree_LastEditedBy",
                table: "WBSTree",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSBoQ_CreatedBy",
                table: "WBSBoQ",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSBoQ_IdWBSTree",
                table: "WBSBoQ",
                column: "IdWBSTree");

            migrationBuilder.CreateIndex(
                name: "IX_WBSBoQ_LastEditedBy",
                table: "WBSBoQ",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSCurrency_CreatedBy",
                table: "WBSCurrency",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSCurrency_CurrencyId",
                table: "WBSCurrency",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_WBSCurrency_LastEditedBy",
                table: "WBSCurrency",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSCurrency_WBSId",
                table: "WBSCurrency",
                column: "WBSId");

            migrationBuilder.AddForeignKey(
                name: "FK_WBSTree_AspNetUsers_CreatedBy",
                table: "WBSTree",
                column: "CreatedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WBSTree_AspNetUsers_LastEditedBy",
                table: "WBSTree",
                column: "LastEditedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WBSTree_AspNetUsers_CreatedBy",
                table: "WBSTree");

            migrationBuilder.DropForeignKey(
                name: "FK_WBSTree_AspNetUsers_LastEditedBy",
                table: "WBSTree");

            migrationBuilder.DropTable(
                name: "WBSBoQ");

            migrationBuilder.DropTable(
                name: "WBSCurrency");

            migrationBuilder.DropIndex(
                name: "IX_WBSTree_CreatedBy",
                table: "WBSTree");

            migrationBuilder.DropIndex(
                name: "IX_WBSTree_LastEditedBy",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "CustomField1",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "CustomField2",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "CustomField3",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "GrupHarga",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "LastEditedBy",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "LastUpdateTime",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "WBSTree");

            migrationBuilder.DropColumn(
                name: "OtherInfo",
                table: "WBSTree");
        }
    }
}
