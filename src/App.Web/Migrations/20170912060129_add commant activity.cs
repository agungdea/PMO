﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace App.Web.Migrations
{
    public partial class addcommantactivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "currentApproval",
                table: "WfProcess",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "totalApproval",
                table: "WfProcess",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "WfActivityAki",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "WfActivityAki",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomField1",
                table: "WfActivityAki",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomField2",
                table: "WfActivityAki",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomField3",
                table: "WfActivityAki",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastEditedBy",
                table: "WfActivityAki",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastUpdateTime",
                table: "WfActivityAki",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherInfo",
                table: "WfActivityAki",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GroupWf",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    GrupName = table.Column<int>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    Nik = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupWf", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GroupWf_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GroupWf_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WfCommantActivity",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Comment = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    IsAggree = table.Column<bool>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    NIK = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    ProcessId = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WfCommantActivity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WfCommantActivity_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WfCommantActivity_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WfActivityAki_CreatedBy",
                table: "WfActivityAki",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WfActivityAki_LastEditedBy",
                table: "WfActivityAki",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_GroupWf_CreatedBy",
                table: "GroupWf",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_GroupWf_LastEditedBy",
                table: "GroupWf",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WfCommantActivity_CreatedBy",
                table: "WfCommantActivity",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WfCommantActivity_LastEditedBy",
                table: "WfCommantActivity",
                column: "LastEditedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_WfActivityAki_AspNetUsers_CreatedBy",
                table: "WfActivityAki",
                column: "CreatedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WfActivityAki_AspNetUsers_LastEditedBy",
                table: "WfActivityAki",
                column: "LastEditedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WfActivityAki_AspNetUsers_CreatedBy",
                table: "WfActivityAki");

            migrationBuilder.DropForeignKey(
                name: "FK_WfActivityAki_AspNetUsers_LastEditedBy",
                table: "WfActivityAki");

            migrationBuilder.DropTable(
                name: "GroupWf");

            migrationBuilder.DropTable(
                name: "WfCommantActivity");

            migrationBuilder.DropIndex(
                name: "IX_WfActivityAki_CreatedBy",
                table: "WfActivityAki");

            migrationBuilder.DropIndex(
                name: "IX_WfActivityAki_LastEditedBy",
                table: "WfActivityAki");

            migrationBuilder.DropColumn(
                name: "currentApproval",
                table: "WfProcess");

            migrationBuilder.DropColumn(
                name: "totalApproval",
                table: "WfProcess");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "WfActivityAki");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "WfActivityAki");

            migrationBuilder.DropColumn(
                name: "CustomField1",
                table: "WfActivityAki");

            migrationBuilder.DropColumn(
                name: "CustomField2",
                table: "WfActivityAki");

            migrationBuilder.DropColumn(
                name: "CustomField3",
                table: "WfActivityAki");

            migrationBuilder.DropColumn(
                name: "LastEditedBy",
                table: "WfActivityAki");

            migrationBuilder.DropColumn(
                name: "LastUpdateTime",
                table: "WfActivityAki");

            migrationBuilder.DropColumn(
                name: "OtherInfo",
                table: "WfActivityAki");
        }
    }
}
