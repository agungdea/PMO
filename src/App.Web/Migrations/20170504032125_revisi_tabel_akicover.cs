﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class revisi_tabel_akicover : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BCR",
                table: "AkiValuation");

            migrationBuilder.DropColumn(
                name: "IRR",
                table: "AkiValuation");

            migrationBuilder.DropColumn(
                name: "InterestRate",
                table: "AkiValuation");

            migrationBuilder.DropColumn(
                name: "NPV",
                table: "AkiValuation");

            migrationBuilder.DropColumn(
                name: "PayBackPeriode",
                table: "AkiValuation");

            migrationBuilder.RenameColumn(
                name: "Jumlah",
                table: "AkiCover",
                newName: "PayBackPeriode");

            migrationBuilder.AddColumn<string>(
                name: "ARPU",
                table: "AkiCover",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BCR",
                table: "AkiCover",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "IRR",
                table: "AkiCover",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "InterestRate",
                table: "AkiCover",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "JumlahLop",
                table: "AkiCover",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "KategoriInvestasi",
                table: "AkiCover",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Level",
                table: "AkiCover",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "NPV",
                table: "AkiCover",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "Taxes",
                table: "AkiCover",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ARPU",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "BCR",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "IRR",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "InterestRate",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "JumlahLop",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "KategoriInvestasi",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "Level",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "NPV",
                table: "AkiCover");

            migrationBuilder.DropColumn(
                name: "Taxes",
                table: "AkiCover");

            migrationBuilder.RenameColumn(
                name: "PayBackPeriode",
                table: "AkiCover",
                newName: "Jumlah");

            migrationBuilder.AddColumn<decimal>(
                name: "BCR",
                table: "AkiValuation",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "IRR",
                table: "AkiValuation",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "InterestRate",
                table: "AkiValuation",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "NPV",
                table: "AkiValuation",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PayBackPeriode",
                table: "AkiValuation",
                nullable: true);
        }
    }
}
