﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class updatewbsshcdule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Durasi",
                table: "WBSSchedule");

            migrationBuilder.DropColumn(
                name: "Leg",
                table: "WBSSchedule");

            migrationBuilder.RenameColumn(
                name: "Prede",
                table: "WBSSchedule",
                newName: "DurasiReal");

            migrationBuilder.AddColumn<Guid>(
                name: "IdActTmpChild",
                table: "WBSSchedule",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedule_IdActTmpChild",
                table: "WBSSchedule",
                column: "IdActTmpChild");

            migrationBuilder.AddForeignKey(
                name: "FK_WBSSchedule_ActTmpChild_IdActTmpChild",
                table: "WBSSchedule",
                column: "IdActTmpChild",
                principalTable: "ActTmpChild",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WBSSchedule_ActTmpChild_IdActTmpChild",
                table: "WBSSchedule");

            migrationBuilder.DropIndex(
                name: "IX_WBSSchedule_IdActTmpChild",
                table: "WBSSchedule");

            migrationBuilder.DropColumn(
                name: "IdActTmpChild",
                table: "WBSSchedule");

            migrationBuilder.RenameColumn(
                name: "DurasiReal",
                table: "WBSSchedule",
                newName: "Prede");

            migrationBuilder.AddColumn<int>(
                name: "Durasi",
                table: "WBSSchedule",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Leg",
                table: "WBSSchedule",
                nullable: false,
                defaultValue: 0);
        }
    }
}
