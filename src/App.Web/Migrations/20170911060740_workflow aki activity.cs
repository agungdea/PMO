﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class workflowakiactivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WfActivityAki",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AkiId = table.Column<Guid>(nullable: false),
                    AssigmentTo = table.Column<string>(nullable: true),
                    dateCreated = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    serviceLevelMonitor = table.Column<string>(nullable: true),
                    state = table.Column<string>(nullable: true),
                    wf_processId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WfActivityAki", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WfActivityAki_Aki_AkiId",
                        column: x => x.AkiId,
                        principalTable: "Aki",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WfActivityAki_AkiId",
                table: "WfActivityAki",
                column: "AkiId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WfActivityAki");
        }
    }
}
