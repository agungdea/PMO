﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addWfActivityView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ActivityDefId",
                table: "WfActivity",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "WfActivityView",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    Variables = table.Column<string>(nullable: true),
                    ViewName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WfActivityView", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WfActivityView_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WfActivityView_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WfActivity_ActivityDefId",
                table: "WfActivity",
                column: "ActivityDefId");

            migrationBuilder.CreateIndex(
                name: "IX_WfActivityView_CreatedBy",
                table: "WfActivityView",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WfActivityView_LastEditedBy",
                table: "WfActivityView",
                column: "LastEditedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_WfActivity_WfActivityView_ActivityDefId",
                table: "WfActivity",
                column: "ActivityDefId",
                principalTable: "WfActivityView",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WfActivity_WfActivityView_ActivityDefId",
                table: "WfActivity");

            migrationBuilder.DropTable(
                name: "WfActivityView");

            migrationBuilder.DropIndex(
                name: "IX_WfActivity_ActivityDefId",
                table: "WfActivity");

            migrationBuilder.DropColumn(
                name: "ActivityDefId",
                table: "WfActivity");
        }
    }
}
