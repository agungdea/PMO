﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Unit",
                columns: table => new
                {
                    GRUP = table.Column<string>(nullable: false),
                    NM_SINGKAT = table.Column<string>(nullable: true),
                    NM_UNIT = table.Column<string>(nullable: true),
                    GRUP_PARENT = table.Column<string>(nullable: true),
                    ST_LEVEL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Unit", x => x.GRUP);
                    table.ForeignKey(
                        name: "FK_Unit_Unit_GRUP_PARENT",
                        column: x => x.GRUP_PARENT,
                        principalTable: "Unit",
                        principalColumn: "GRUP",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Unit_GRUP_PARENT",
                table: "Unit",
                column: "GRUP_PARENT");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Unit");
        }
    }
}
