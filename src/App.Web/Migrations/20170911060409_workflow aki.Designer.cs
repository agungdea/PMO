﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using App.Data.DAL;
using App.Domain.Models.App;

namespace App.Web.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170911060409_workflow aki")]
    partial class workflowaki
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("App.Domain.Models.App.ActTemplate", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("KD_ACT_TEMP");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<int?>("Durasi")
                        .HasColumnName("DURASI");

                    b.Property<string>("Grup")
                        .HasColumnName("GRUP");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("Name")
                        .HasColumnName("NM_ACT_TEMP");

                    b.Property<string>("OtherInfo");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("ActTemplate");
                });

            modelBuilder.Entity("App.Domain.Models.App.ActTmpChild", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ActId")
                        .HasColumnName("ACT_ID");

                    b.Property<int?>("ActParent")
                        .HasColumnName("ACT_PARENT");

                    b.Property<decimal?>("Bobot")
                        .HasColumnName("BOBOT");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<int?>("Durasi")
                        .HasColumnName("DURASI");

                    b.Property<string>("ISLEAF")
                        .HasColumnName("ISLEAF");

                    b.Property<Guid>("IdActTmp")
                        .HasColumnName("KD_ACT_TEMP");

                    b.Property<int?>("Lagging")
                        .HasColumnName("LAGGING");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("NamaAct")
                        .HasColumnName("NM_ACT");

                    b.Property<string>("OtherInfo");

                    b.Property<int?>("Predecessor")
                        .HasColumnName("PREDECESSOR");

                    b.Property<string>("Relasi")
                        .HasColumnName("RELASI");

                    b.Property<int?>("Urutan")
                        .HasColumnName("URUTAN");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("IdActTmp");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("ActTmpChild");
                });

            modelBuilder.Entity("App.Domain.Models.App.Aki", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AkiName")
                        .IsRequired();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("Document")
                        .IsRequired();

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("PeriodId")
                        .IsRequired();

                    b.Property<string>("PeriodName")
                        .IsRequired();

                    b.Property<string>("ProgramId")
                        .IsRequired();

                    b.Property<string>("ProgramName")
                        .IsRequired();

                    b.Property<string>("StrategiInitiativeId")
                        .IsRequired();

                    b.Property<string>("StrategiInitiativeName")
                        .IsRequired();

                    b.Property<string>("wf_processId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("Aki");
                });

            modelBuilder.Entity("App.Domain.Models.App.AkiCover", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ARPU");

                    b.Property<Guid>("AkiId");

                    b.Property<string>("Alasan");

                    b.Property<decimal?>("BCR");

                    b.Property<string>("CAGR");

                    b.Property<string>("CapexLine");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<float?>("DiscountRate");

                    b.Property<decimal?>("IRR");

                    b.Property<float?>("InterestRate");

                    b.Property<int?>("JumlahLop");

                    b.Property<string>("Kapasitas");

                    b.Property<string>("KategoriInvestasi");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("Level");

                    b.Property<string>("Location");

                    b.Property<string>("LopId");

                    b.Property<decimal?>("NPV");

                    b.Property<decimal?>("NilaiProgram");

                    b.Property<string>("NomorAkun");

                    b.Property<string>("OtherInfo");

                    b.Property<int?>("PayBackPeriode");

                    b.Property<string>("PemilihanTeknologi");

                    b.Property<string>("PenanggungJawab");

                    b.Property<string>("ProposalId");

                    b.Property<string>("RencanaSelesai");

                    b.Property<string>("Roi");

                    b.Property<float?>("Taxes");

                    b.Property<string>("Usulan");

                    b.Property<string>("WaktuPenyelesaian");

                    b.HasKey("Id");

                    b.HasIndex("AkiId");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("AkiCover");
                });

            modelBuilder.Entity("App.Domain.Models.App.AkiValuation", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("AddBackDepresiation");

                    b.Property<Guid>("AkiId");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<decimal?>("CumulativeNetCashFlow");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<decimal?>("Depreciation");

                    b.Property<decimal?>("Ebit");

                    b.Property<decimal?>("Ebitda");

                    b.Property<decimal?>("Expenses");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<decimal?>("NetCashFlow");

                    b.Property<decimal?>("NetIncome");

                    b.Property<string>("OtherInfo");

                    b.Property<decimal?>("Revenue");

                    b.Property<decimal?>("Taxes");

                    b.Property<int?>("Year");

                    b.HasKey("Id");

                    b.HasIndex("AkiId");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("AkiValuation");
                });

            modelBuilder.Entity("App.Domain.Models.App.AreaMapping", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AreaDatel");

                    b.Property<string>("AreaKode");

                    b.Property<string>("AreaRegional");

                    b.Property<string>("AreaSto");

                    b.Property<string>("AreaStoName");

                    b.Property<string>("AreaWitel");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("AreaMapping");
                });

            modelBuilder.Entity("App.Domain.Models.App.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("DeletedBy");

                    b.Property<DateTime?>("DeletedDate");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("OtherInfo");

                    b.Property<int?>("ParentId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("Category");
                });

            modelBuilder.Entity("App.Domain.Models.App.Currency", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CurrencyCode");

                    b.Property<string>("CurrencyValue");

                    b.HasKey("Id");

                    b.ToTable("Currency");
                });

            modelBuilder.Entity("App.Domain.Models.App.Designator", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ID")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

                    b.Property<string>("Deskripsi")
                        .HasColumnName("DESKRIPSI");

                    b.Property<string>("IdDesignator")
                        .HasColumnName("PART_NUMBER");

                    b.Property<string>("Satuan")
                        .HasColumnName("SATUAN");

                    b.HasKey("Id");

                    b.ToTable("Designator");
                });

            modelBuilder.Entity("App.Domain.Models.App.Harga", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("HargaJasa")
                        .HasColumnName("UNIT_PRICE_SERVICES");

                    b.Property<decimal>("HargaMaterial")
                        .HasColumnName("UNIT_PRICE_MATERIALS");

                    b.Property<string>("IdDesignator")
                        .IsRequired()
                        .HasColumnName("PART_NUMBER");

                    b.Property<string>("Lokasi")
                        .HasColumnName("LOKASI");

                    b.Property<string>("MataUang")
                        .HasColumnName("PK_MTU");

                    b.HasKey("Id");

                    b.ToTable("Harga");
                });

            modelBuilder.Entity("App.Domain.Models.App.Justification", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AKIId");

                    b.Property<string>("Anggaran");

                    b.Property<string>("AspekBisnis");

                    b.Property<string>("AspekStrategis");

                    b.Property<string>("BasicDesign");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("DasarKebutuhanBarangJasa");

                    b.Property<string>("DistribusiPenggunaan");

                    b.Property<string>("JenisKebutuhan");

                    b.Property<int>("JenisPengeluaran");

                    b.Property<decimal>("JumlahAnggaran");

                    b.Property<string>("JumlahKebutuhanBarangJasa");

                    b.Property<string>("JustificationStatus");

                    b.Property<string>("KelasAsset");

                    b.Property<string>("LampiranDasarKebutuhanBarangJasa");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("LatarBelakang");

                    b.Property<string>("NomorDRK");

                    b.Property<string>("NomorDRP");

                    b.Property<string>("NoteAnggaran");

                    b.Property<string>("NoteAspekBisnis");

                    b.Property<string>("NoteAspekStrategis");

                    b.Property<string>("NoteDistribusiPenggunaan");

                    b.Property<string>("NoteJumlahKebutuhan");

                    b.Property<string>("NoteLatarBelakang");

                    b.Property<string>("NotePenutup");

                    b.Property<string>("NotePosisiPersediaan");

                    b.Property<string>("NoteRencanaPelaksanaan");

                    b.Property<string>("NoteSpesifikasiTeknik");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("Penutup");

                    b.Property<string>("Penyusun");

                    b.Property<string>("PosisiPersediaan");

                    b.Property<string>("PusatPertanggungjawaban");

                    b.Property<string>("RencanaPelaksanaan");

                    b.Property<bool>("ShowBoQ");

                    b.Property<bool>("ShowWBS");

                    b.Property<string>("SpesifikasiTeknik");

                    b.Property<string>("TOR");

                    b.Property<Guid?>("WBSId");

                    b.Property<long?>("WfProcessId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.HasIndex("WfProcessId");

                    b.ToTable("Justification");
                });

            modelBuilder.Entity("App.Domain.Models.App.TransformerPeriod", b =>
                {
                    b.Property<string>("Period")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Display_name");

                    b.HasKey("Period");

                    b.ToTable("TransformerPeriod");
                });

            modelBuilder.Entity("App.Domain.Models.App.TransformerProgram", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Backactual");

                    b.Property<string>("Backchgreq");

                    b.Property<string>("BodChampion_id");

                    b.Property<string>("Btp");

                    b.Property<string>("BusinessRisk");

                    b.Property<string>("BusinessRisk2");

                    b.Property<DateTime?>("Closeddate");

                    b.Property<string>("Comments");

                    b.Property<DateTime>("Date_approved");

                    b.Property<DateTime>("Date_submitted");

                    b.Property<string>("Description");

                    b.Property<string>("Draft");

                    b.Property<string>("Generator");

                    b.Property<string>("Initiative_id");

                    b.Property<string>("Issuemgt");

                    b.Property<string>("Order");

                    b.Property<string>("Organization_id");

                    b.Property<string>("Planamount");

                    b.Property<string>("Planuser");

                    b.Property<string>("ProgramStatus");

                    b.Property<string>("ProgramType");

                    b.Property<string>("Progressworkflowmode_id");

                    b.Property<string>("Quantifier");

                    b.Property<string>("QuickWin");

                    b.Property<string>("Revision");

                    b.Property<string>("RiskMitigation");

                    b.Property<string>("RiskMitigation2");

                    b.Property<string>("SiId");

                    b.Property<string>("Status");

                    b.Property<string>("Title");

                    b.Property<string>("Usedependency");

                    b.Property<string>("Warflag");

                    b.Property<string>("Weight");

                    b.Property<string>("WorkflowStatus");

                    b.Property<string>("enabler");

                    b.HasKey("Id");

                    b.ToTable("TransformerProgram");
                });

            modelBuilder.Entity("App.Domain.Models.App.TransformerSi", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Abbreviation");

                    b.Property<string>("Name");

                    b.Property<string>("Parent_id");

                    b.Property<string>("PeriodId");

                    b.HasKey("Id");

                    b.ToTable("TransformerSi");
                });

            modelBuilder.Entity("App.Domain.Models.App.Unit", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("GRUP");

                    b.Property<string>("DeletedBy");

                    b.Property<DateTime?>("DeletedDate");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("NamaSingkat")
                        .HasColumnName("NM_SINGKAT");

                    b.Property<string>("NamaUnit")
                        .HasColumnName("NM_UNIT");

                    b.Property<string>("ParentId")
                        .HasColumnName("GRUP_PARENT");

                    b.Property<string>("STLevel")
                        .HasColumnName("ST_LEVEL");

                    b.HasKey("Id");

                    b.HasIndex("ParentId");

                    b.ToTable("Unit");
                });

            modelBuilder.Entity("App.Domain.Models.App.UserMedia", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("MediaDirectory");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("UnitId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("UserMedia");
                });

            modelBuilder.Entity("App.Domain.Models.App.UserUnit", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("UnitId");

                    b.HasKey("UserId", "UnitId");

                    b.HasAlternateKey("UnitId");


                    b.HasAlternateKey("UnitId", "UserId");

                    b.ToTable("UserUnit");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBS", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("NamaKegiatan");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("Unit");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("WBS");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSBoQ", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("GrupHarga");

                    b.Property<decimal>("HargaJasa");

                    b.Property<decimal>("HargaMaterial");

                    b.Property<string>("IdDesignator");

                    b.Property<Guid>("IdWBSTree");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<int>("Qty");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("IdWBSTree");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("WBSBoQ");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSBoQGrantTotal", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("BoQBeforeConverstion");

                    b.Property<decimal>("BoQTotalConversion");

                    b.Property<Guid>("CurrencyId");

                    b.Property<Guid>("IdWBStree");

                    b.HasKey("Id");

                    b.HasIndex("CurrencyId");

                    b.HasIndex("IdWBStree");

                    b.ToTable("WBSBoQGrantTotal");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSCurrency", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<Guid>("CurrencyId");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<decimal>("Rate");

                    b.Property<Guid>("WBSId");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("CurrencyId");

                    b.HasIndex("LastEditedBy");

                    b.HasIndex("WBSId");

                    b.ToTable("WBSCurrency");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSSchedule", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<int>("DurasiReal");

                    b.Property<DateTime>("Finish");

                    b.Property<Guid>("IdActTmpChild");

                    b.Property<Guid>("IdWBSTree");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<DateTime>("Start");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("IdActTmpChild");

                    b.HasIndex("IdWBSTree");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("WBSSchedule");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSSchedulePlanning", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<DateTime>("Finish");

                    b.Property<Guid>("IdActTmpChild");

                    b.Property<Guid>("IdWBSTree");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<DateTime>("Start");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("IdActTmpChild");

                    b.HasIndex("IdWBSTree");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("WBSSchedulePlanning");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSScheduleTemp", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<int>("DurasiReal");

                    b.Property<DateTime>("Finish");

                    b.Property<DateTime>("FinishPerkiraan");

                    b.Property<DateTime>("FinishRencana");

                    b.Property<Guid>("IdActTmpChild");

                    b.Property<Guid>("IdWBSTree");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<DateTime>("Start");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("IdActTmpChild");

                    b.HasIndex("IdWBSTree");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("WBSScheduleTemp");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSTree", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("GrupHarga");

                    b.Property<Guid>("IdWBS");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("Lokasi");

                    b.Property<string>("Name");

                    b.Property<int>("Order");

                    b.Property<string>("OtherInfo");

                    b.Property<Guid?>("ParentId");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("IdWBS");

                    b.HasIndex("LastEditedBy");

                    b.HasIndex("ParentId");

                    b.ToTable("WBSTree");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBStreeCategory", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryId");

                    b.Property<Guid>("WBStreeId");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("WBStreeId");

                    b.ToTable("WBStreeCategory");
                });

            modelBuilder.Entity("App.Domain.Models.Core.EmailArchieve", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Bcc");

                    b.Property<string>("Cc");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("ExceptionSendingMessage");

                    b.Property<string>("From")
                        .IsRequired();

                    b.Property<string>("FromName");

                    b.Property<string>("HtmlMessage");

                    b.Property<bool>("IsSent");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastTrySentDate");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("PlainMessage");

                    b.Property<string>("ReplyTo");

                    b.Property<string>("ReplyToName");

                    b.Property<DateTime?>("SentDate");

                    b.Property<string>("Subject")
                        .IsRequired();

                    b.Property<string>("Tos")
                        .IsRequired();

                    b.Property<int>("TrySentCount");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("EmailArchieve");
                });

            modelBuilder.Entity("App.Domain.Models.Core.EntityRouting", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("EntityId");

                    b.Property<int>("EntityRoutingTypeId");

                    b.Property<string>("Name");

                    b.Property<string>("Slug");

                    b.HasKey("Id");

                    b.HasIndex("EntityRoutingTypeId");

                    b.ToTable("EntityRouting");
                });

            modelBuilder.Entity("App.Domain.Models.Core.EntityRoutingType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

                    b.Property<string>("Name");

                    b.Property<string>("RoutingAction");

                    b.Property<string>("RoutingController");

                    b.HasKey("Id");

                    b.ToTable("EntityRoutingType");
                });

            modelBuilder.Entity("App.Domain.Models.Core.Log", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Application");

                    b.Property<string>("CallSite");

                    b.Property<string>("Exception");

                    b.Property<string>("Level");

                    b.Property<string>("Logged");

                    b.Property<string>("Logger");

                    b.Property<string>("Message");

                    b.HasKey("Id");

                    b.ToTable("Log");
                });

            modelBuilder.Entity("App.Domain.Models.Core.WebSetting", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("OtherInfo");

                    b.Property<bool>("SystemSetting");

                    b.Property<string>("Value")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("WebSetting");
                });

            modelBuilder.Entity("App.Domain.Models.Identity.AppAction", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("AppAction");
                });

            modelBuilder.Entity("App.Domain.Models.Identity.AppActionClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

                    b.Property<string>("ActionId")
                        .IsRequired();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.HasKey("Id");

                    b.HasIndex("ActionId");

                    b.ToTable("AppActionClaim");
                });

            modelBuilder.Entity("App.Domain.Models.Identity.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("App.Domain.Models.Identity.UserProfile", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

                    b.Property<string>("Address");

                    b.Property<string>("ApplicationUserId")
                        .HasColumnName("UserId");

                    b.Property<DateTime?>("Birthdate");

                    b.Property<string>("Birthplace");

                    b.Property<string>("Name")
                        .HasMaxLength(200);

                    b.Property<string>("Photo");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserId")
                        .IsUnique();

                    b.ToTable("UserProfile");
                });

            modelBuilder.Entity("App.Domain.Models.Localization.Language", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<bool>("DefaultLanguage");

                    b.Property<string>("Flag")
                        .IsRequired();

                    b.Property<string>("LanguageCulture")
                        .IsRequired();

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("Order");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("UniqueSeoCode")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("Language");
                });

            modelBuilder.Entity("App.Domain.Models.Localization.LocaleResource", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<int>("LanguageId");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("ResourceName")
                        .IsRequired();

                    b.Property<string>("ResourceValue")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LanguageId");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("LocaleResource");
                });

            modelBuilder.Entity("App.Domain.Models.Workflow.WfActivity", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ActivityDefId");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("State");

                    b.Property<long>("WfProcessId");

                    b.HasKey("Id");

                    b.HasIndex("ActivityDefId");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.HasIndex("WfProcessId");

                    b.ToTable("WfActivity");
                });

            modelBuilder.Entity("App.Domain.Models.Workflow.WfActivityView", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("Variables");

                    b.Property<string>("ViewName");

                    b.HasKey("Id");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.ToTable("WfActivityView");
                });

            modelBuilder.Entity("App.Domain.Models.Workflow.WfProcess", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

                    b.Property<string>("ActivityId");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("CreatedBy");

                    b.Property<string>("CustomField1");

                    b.Property<string>("CustomField2");

                    b.Property<string>("CustomField3");

                    b.Property<string>("LastEditedBy");

                    b.Property<DateTime?>("LastUpdateTime");

                    b.Property<string>("OtherInfo");

                    b.Property<string>("PrevActivityId");

                    b.Property<string>("ProcessDefinitionId");

                    b.Property<string>("ProcessId");

                    b.Property<string>("Requester");

                    b.Property<string>("State");

                    b.HasKey("Id");

                    b.HasIndex("ActivityId");

                    b.HasIndex("CreatedBy");

                    b.HasIndex("LastEditedBy");

                    b.HasIndex("PrevActivityId");

                    b.ToTable("WfProcess");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");

                    b.HasDiscriminator<string>("Discriminator").HasValue("IdentityRole");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("App.Domain.Models.Identity.ApplicationRole", b =>
                {
                    b.HasBaseType("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole");

                    b.Property<string>("Description");

                    b.Property<string>("OtherInfo");

                    b.ToTable("ApplicationRole");

                    b.HasDiscriminator().HasValue("ApplicationRole");
                });

            modelBuilder.Entity("App.Domain.Models.App.ActTemplate", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.ActTmpChild", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.App.ActTemplate", "ActTemplates")
                        .WithMany("ActivityTempChild")
                        .HasForeignKey("IdActTmp")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.Aki", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.AkiCover", b =>
                {
                    b.HasOne("App.Domain.Models.App.Aki", "Akis")
                        .WithMany()
                        .HasForeignKey("AkiId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.AkiValuation", b =>
                {
                    b.HasOne("App.Domain.Models.App.Aki", "Akis")
                        .WithMany("AkiValuations")
                        .HasForeignKey("AkiId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.AreaMapping", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.Category", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.Justification", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");

                    b.HasOne("App.Domain.Models.Workflow.WfProcess", "WfProcess")
                        .WithMany()
                        .HasForeignKey("WfProcessId");
                });

            modelBuilder.Entity("App.Domain.Models.App.Unit", b =>
                {
                    b.HasOne("App.Domain.Models.App.Unit", "Parent")
                        .WithMany("Children")
                        .HasForeignKey("ParentId");
                });

            modelBuilder.Entity("App.Domain.Models.App.UserMedia", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.UserUnit", b =>
                {
                    b.HasOne("App.Domain.Models.App.Unit", "Unit")
                        .WithMany()
                        .HasForeignKey("UnitId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("App.Domain.Models.App.WBS", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSBoQ", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.App.WBSTree", "WBSTree")
                        .WithMany()
                        .HasForeignKey("IdWBSTree")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSBoQGrantTotal", b =>
                {
                    b.HasOne("App.Domain.Models.App.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.App.WBSTree", "WBSTree")
                        .WithMany()
                        .HasForeignKey("IdWBStree")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSCurrency", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.App.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");

                    b.HasOne("App.Domain.Models.App.WBS", "WBS")
                        .WithMany()
                        .HasForeignKey("WBSId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSSchedule", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.App.ActTmpChild", "ActTmpChild")
                        .WithMany()
                        .HasForeignKey("IdActTmpChild")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.App.WBSTree", "WBSTree")
                        .WithMany()
                        .HasForeignKey("IdWBSTree")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSSchedulePlanning", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.App.ActTmpChild", "ActTmpChild")
                        .WithMany()
                        .HasForeignKey("IdActTmpChild")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.App.WBSTree", "WBSTree")
                        .WithMany()
                        .HasForeignKey("IdWBSTree")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSScheduleTemp", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.App.ActTmpChild", "ActTmpChild")
                        .WithMany()
                        .HasForeignKey("IdActTmpChild")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.App.WBSTree", "WBSTree")
                        .WithMany()
                        .HasForeignKey("IdWBSTree")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBSTree", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.App.WBS", "WBS")
                        .WithMany()
                        .HasForeignKey("IdWBS")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");

                    b.HasOne("App.Domain.Models.App.WBSTree", "Parent")
                        .WithMany("Children")
                        .HasForeignKey("ParentId");
                });

            modelBuilder.Entity("App.Domain.Models.App.WBStreeCategory", b =>
                {
                    b.HasOne("App.Domain.Models.App.Category", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.App.WBSTree", "WBSTree")
                        .WithMany()
                        .HasForeignKey("WBStreeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("App.Domain.Models.Core.EmailArchieve", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.Core.EntityRouting", b =>
                {
                    b.HasOne("App.Domain.Models.Core.EntityRoutingType", "EntityRoutingType")
                        .WithMany()
                        .HasForeignKey("EntityRoutingTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("App.Domain.Models.Core.WebSetting", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.Identity.AppActionClaim", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.AppAction", "Action")
                        .WithMany("ActionClaims")
                        .HasForeignKey("ActionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("App.Domain.Models.Identity.UserProfile", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "ApplicationUser")
                        .WithOne("UserProfile")
                        .HasForeignKey("App.Domain.Models.Identity.UserProfile", "ApplicationUserId");
                });

            modelBuilder.Entity("App.Domain.Models.Localization.Language", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.Localization.LocaleResource", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Localization.Language", "Language")
                        .WithMany()
                        .HasForeignKey("LanguageId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.Workflow.WfActivity", b =>
                {
                    b.HasOne("App.Domain.Models.Workflow.WfActivityView", "WfActivityView")
                        .WithMany()
                        .HasForeignKey("ActivityDefId");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");

                    b.HasOne("App.Domain.Models.Workflow.WfProcess", "WfProcess")
                        .WithMany("Activities")
                        .HasForeignKey("WfProcessId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("App.Domain.Models.Workflow.WfActivityView", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");
                });

            modelBuilder.Entity("App.Domain.Models.Workflow.WfProcess", b =>
                {
                    b.HasOne("App.Domain.Models.Workflow.WfActivity", "Activity")
                        .WithMany()
                        .HasForeignKey("ActivityId");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Creator")
                        .WithMany()
                        .HasForeignKey("CreatedBy");

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser", "Editor")
                        .WithMany()
                        .HasForeignKey("LastEditedBy");

                    b.HasOne("App.Domain.Models.Workflow.WfActivity", "PrevActivity")
                        .WithMany()
                        .HasForeignKey("PrevActivityId");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("App.Domain.Models.Identity.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("App.Domain.Models.Identity.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
