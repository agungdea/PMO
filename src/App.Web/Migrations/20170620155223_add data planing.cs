﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class adddataplaning : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinishPerkiraan",
                table: "WBSSchedule");

            migrationBuilder.DropColumn(
                name: "FinishRencana",
                table: "WBSSchedule");

            migrationBuilder.CreateTable(
                name: "WBSSchedulePlaning",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    DurasiReal = table.Column<int>(nullable: false),
                    Finish = table.Column<DateTime>(nullable: false),
                    IdActTmpChild = table.Column<Guid>(nullable: false),
                    IdWBSTree = table.Column<Guid>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    Start = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSSchedulePlaning", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBSSchedulePlaning_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WBSSchedulePlaning_ActTmpChild_IdActTmpChild",
                        column: x => x.IdActTmpChild,
                        principalTable: "ActTmpChild",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBSSchedulePlaning_WBSTree_IdWBSTree",
                        column: x => x.IdWBSTree,
                        principalTable: "WBSTree",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBSSchedulePlaning_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedulePlaning_CreatedBy",
                table: "WBSSchedulePlaning",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedulePlaning_IdActTmpChild",
                table: "WBSSchedulePlaning",
                column: "IdActTmpChild");

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedulePlaning_IdWBSTree",
                table: "WBSSchedulePlaning",
                column: "IdWBSTree");

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedulePlaning_LastEditedBy",
                table: "WBSSchedulePlaning",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WBSSchedulePlaning");

            migrationBuilder.AddColumn<DateTime>(
                name: "FinishPerkiraan",
                table: "WBSSchedule",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "FinishRencana",
                table: "WBSSchedule",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
