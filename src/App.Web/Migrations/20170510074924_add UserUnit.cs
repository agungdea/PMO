﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addUserUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserUnit",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    UnitId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserUnit", x => new { x.UserId, x.UnitId });
                    table.UniqueConstraint("AK_UserUnit_UnitId", x => x.UnitId);
                    table.UniqueConstraint("AK_UserUnit_UnitId_UserId", x => new { x.UnitId, x.UserId });
                    table.ForeignKey(
                        name: "FK_UserUnit_Unit_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Unit",
                        principalColumn: "GRUP",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserUnit_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserUnit");
        }
    }
}
