﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace App.Web.Migrations
{
    public partial class addtabletemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActTemplate",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    DURASI = table.Column<int>(nullable: false),
                    GRUP = table.Column<double>(nullable: false),
                    KD_ACT_TEMP = table.Column<double>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    NM_ACT_TEMP = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActTemplate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActTemplate_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActTemplate_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActTmpChild",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ACT_ID = table.Column<double>(nullable: true),
                    ACT_PARENT = table.Column<double>(nullable: true),
                    BOBOT = table.Column<decimal>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    DURASI = table.Column<int>(nullable: true),
                    ISLEAF = table.Column<string>(nullable: true),
                    KD_ACT_TEMP = table.Column<double>(nullable: false),
                    LAGGING = table.Column<int>(nullable: true),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    NM_ACT = table.Column<string>(nullable: true),
                    OtherInfo = table.Column<string>(nullable: true),
                    PREDECESSOR = table.Column<int>(nullable: true),
                    RELASI = table.Column<string>(nullable: true),
                    URUTAN = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActTmpChild", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActTmpChild_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ActTmpChild_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActTemplate_CreatedBy",
                table: "ActTemplate",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ActTemplate_LastEditedBy",
                table: "ActTemplate",
                column: "LastEditedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ActTmpChild_CreatedBy",
                table: "ActTmpChild",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ActTmpChild_LastEditedBy",
                table: "ActTmpChild",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActTemplate");

            migrationBuilder.DropTable(
                name: "ActTmpChild");
        }
    }
}
