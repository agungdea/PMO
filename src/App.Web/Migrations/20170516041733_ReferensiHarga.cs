﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace App.Web.Migrations
{
    public partial class ReferensiHarga : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Designation",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DESKRIPSI = table.Column<string>(nullable: true),
                    PART_NUMBER = table.Column<string>(nullable: true),
                    SATUAN = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Designation", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Harga",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UNIT_PRICE_SERVICES = table.Column<decimal>(nullable: false),
                    UNIT_PRICE_MATERIALS = table.Column<decimal>(nullable: false),
                    PART_NUMBER = table.Column<string>(nullable: false),
                    LOKASI = table.Column<string>(nullable: true),
                    PK_MTU = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Harga", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Designation");

            migrationBuilder.DropTable(
                name: "Harga");
        }
    }
}
