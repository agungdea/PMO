﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace App.Web.Migrations
{
    public partial class addWBSSchedule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WBSSchedule",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Activity = table.Column<string>(nullable: true),
                    Bobot = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CustomField1 = table.Column<string>(nullable: true),
                    CustomField2 = table.Column<string>(nullable: true),
                    CustomField3 = table.Column<string>(nullable: true),
                    Durasi = table.Column<int>(nullable: false),
                    IdWBSTree = table.Column<Guid>(nullable: false),
                    LastEditedBy = table.Column<string>(nullable: true),
                    LastUpdateTime = table.Column<DateTime>(nullable: true),
                    Leg = table.Column<int>(nullable: false),
                    OtherInfo = table.Column<string>(nullable: true),
                    Prede = table.Column<int>(nullable: false),
                    ProjectFinish = table.Column<string>(nullable: true),
                    ProjectStart = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WBSSchedule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WBSSchedule_AspNetUsers_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WBSSchedule_WBSTree_IdWBSTree",
                        column: x => x.IdWBSTree,
                        principalTable: "WBSTree",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WBSSchedule_AspNetUsers_LastEditedBy",
                        column: x => x.LastEditedBy,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedule_CreatedBy",
                table: "WBSSchedule",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedule_IdWBSTree",
                table: "WBSSchedule",
                column: "IdWBSTree");

            migrationBuilder.CreateIndex(
                name: "IX_WBSSchedule_LastEditedBy",
                table: "WBSSchedule",
                column: "LastEditedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WBSSchedule");
        }
    }
}
