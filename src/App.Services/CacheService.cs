﻿using App.Data.Repository;
using App.Domain;
using App.Services.Caching;
using App.Services.Utils;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Modular.Core.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;

namespace App.Services
{
    public class CacheService<T, TKey, TRepo> : ICacheService<T, TKey>
        where T : class, ICacheEntity<TKey>
        where TRepo : IRepository<T>
    {
        public List<Expression<Func<T, object>>> Includes { get; set; }
        protected readonly TRepo _repository;
        protected readonly ICacheManager _cacheManager;
        protected int _cacheInterval;
        protected string _cacheKey;
        protected Func<T, string> _keyItem;

        public CacheService(TRepo repository,
            ICacheManager cacheManager)
        {
            _repository = repository;
            _cacheManager = cacheManager;
            Includes = new List<Expression<Func<T, object>>>();
        }

        #region Utils

        protected virtual Dictionary<string, T> GetCache()
        {
            var items = _cacheManager.GetEntityCache<T>(_cacheKey);

            if (items == null)
                InitCache();

            items = _cacheManager.GetEntityCache<T>(_cacheKey);

            return items;
        }

        protected virtual void SetCache(T model)
        {
            if (_cacheManager.GetEntityCache<T>(_cacheKey) == null)
                InitCache();

            _cacheManager.SetEntityCache<T, TKey>(_cacheKey, _keyItem(model), model, _cacheInterval);
        }

        protected virtual void RemoveCache(T model)
        {
            if (_cacheManager.GetEntityCache<T>(_cacheKey) == null)
                InitCache();

            _cacheManager.RemoveEntityCache<T, TKey>(_cacheKey, _keyItem(model), model, _cacheInterval);
        }

        protected virtual void InitCache()
        {
            var query = _repository
                .GetAll(Includes.ToArray());

            var items = query
                .ToList()
                .ToDictionary(_keyItem, x => x);

            _cacheManager.Set(_cacheKey, items, _cacheInterval);
        }

        #endregion

        public virtual T Add(T model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _repository.Add(model);

            SetCache(model);

            return model;
        }

        public virtual T Update(T model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _repository.Update(model);

            SetCache(model);

            return model;
        }

        public virtual T Delete(T model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _repository.Delete(model);

            RemoveCache(model);

            return model;
        }

        public virtual IList<T> GetAll(params Expression<Func<T, object>>[] includes)
        {
            return _repository.GetAll(includes)
                 .ToList();
        }

        public virtual T GetById(params object[] ids)
        {
            return _repository.GetSingle(ids);
        }

        public virtual Dictionary<string, T> GetFromCache()
        {
            return GetCache();
        }

        public virtual DataTablesResponse GetDataTablesResponseFromCache<TDto>(IDataTablesRequest request, 
            IMapper mapper, 
            string where = null)
        {
            var data = GetFromCache()
                .Select(x => x.Value)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(where))
            {
                data = data.Where(where);
            }

            return GetDataTablesResponseByQuery<TDto>(request, mapper, data);
        }

        public virtual DataTablesResponse GetDataTablesResponse<TDto>(
            IDataTablesRequest request,
            IMapper mapper,
            string where = null,
            params Expression<Func<T, object>>[] includes)
        {
            var data = _repository.GetAll(includes);

            if (!string.IsNullOrWhiteSpace(where))
            {
                data = data.Where(where);
            }

            return GetDataTablesResponseByQuery<TDto>(request, mapper, data);
        }

        public virtual DataTablesResponse GetDataTablesResponseByQuery<TDto>(
            IDataTablesRequest request,
            IMapper mapper,
            IQueryable data)
        {
            var searchQuery = request.Search.Value;

            var filteredData = data.AsQueryable();

            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request,
                data.Count(),
                filteredData.Count(),
                mapper.Map<List<TDto>>(dataPage));

            return response;
        }

        protected virtual IQueryable<T> RelationQuery(IQueryable<T> data, string query)
        {
            return data;
        }
    }


    public class CacheService<T, TRepo> : CacheService<T, Guid, TRepo>
        where T : class, ICacheEntity<Guid>
        where TRepo : IRepository<T>
    {
        public CacheService(TRepo repository, ICacheManager cacheManager) 
            : base(repository, cacheManager)
        {
        }
    }
}
