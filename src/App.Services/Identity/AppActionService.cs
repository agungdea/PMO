﻿using System;
using System.Collections.Generic;
using App.Data.Repository;
using App.Domain.Models.Identity;
using Modular.Core;
using Modular.Core.Caching;
using System.Linq;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;

namespace App.Services.Identity
{
    public class AppActionService : 
        CacheService<AppAction, string, IRepository<AppAction>>, 
        IAppActionService
    {
        private readonly IRepository<AppActionClaim> _actionClaimRepo;
        private const string APPACTION_ALL = "app.action.all";

        public AppActionService(IRepository<AppAction> repository, 
            IRepository<AppActionClaim> actionClaimRepo,
            ICacheManager cacheManager) 
            : base(repository, cacheManager)
        {
            _actionClaimRepo = actionClaimRepo;
            _cacheKey = APPACTION_ALL;
            _keyItem = x => x.Id;
            _cacheInterval = 60 * 24;

            Includes.Add(x => x.ActionClaims);

            if (!GlobalConfiguration.CachingKeys.Contains(APPACTION_ALL))
                GlobalConfiguration.CachingKeys.Add(APPACTION_ALL);
        }

        public List<AppActionClaim> GetClaims(string id)
        {
            var result = _actionClaimRepo
                .GetAll()
                .Where(x => x.ActionId == id)
                .ToList();

            return result;
        }

        public void AddClaim(AppAction action, Claim claim)
        {
            var item = new AppActionClaim
            {
                ActionId = action.Id,
                ClaimType = claim.Type,
                ClaimValue = claim.Value
            };

            _actionClaimRepo.Add(item);

            action.ActionClaims = GetClaims(action.Id);

            SetCache(action);
        }

        public void RemoveClaim(AppAction action, Claim claim)
        {
            var claimToDelete = _actionClaimRepo
                .Table
                .Where(x => x.ActionId == action.Id &&
                    x.ClaimType == claim.Type &&
                    x.ClaimValue == claim.Value)
                .ToList();

            foreach(var item in claimToDelete)
            {
                _actionClaimRepo.Delete(item);
            }

            action.ActionClaims = GetClaims(action.Id);

            SetCache(action);
        }

        public AppAction GetByName(string name)
        {
            var item = _repository
                .Table
                .Include(x => x.ActionClaims)
                .FirstOrDefault(x => x.Name == name);

            if(item != null)
            {
                if (GetCache().ContainsKey(item.Id))
                {
                    var result = GetCache()[item.Id];
                    if (result.ActionClaims == null || !result.ActionClaims.Any())
                    {
                        result = item;
                        SetCache(result);
                    }
                    return result;
                }
            }

            return item;
        }
    }
}
