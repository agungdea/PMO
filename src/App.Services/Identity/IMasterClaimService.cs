﻿using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace App.Services.Identity
{
    public interface IMasterClaimService : IService<MasterClaim>
    {
        IList<MasterClaim> GetAll();
    }
}
