﻿using App.Data.Repository;
using App.Domain.Models.Core;
using App.Domain.Models.Identity;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Modular.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace App.Services.Identity
{
    public class MasterClaimService : IMasterClaimService
    {
        public const string CLAIM_TEXT = "claim.";

        private readonly IRepository<WebSetting> _repository;
        private readonly IMapper _mapper;

        public MasterClaimService(IRepository<WebSetting> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        #region Utils

        private string GetClaimName(MasterClaim model)
        {
            var name = $"{CLAIM_TEXT}{model.Type.ToUrlFriendly()}";
            var i = 2;

            while (true)
            {
                var item = _repository.TableNoTracking.FirstOrDefault(x => x.Name == name && 
                    x.Id != model.Id);
                if (item != null)
                {
                    name = string.Format("{0}-{1}", name, i);
                    i++;
                }
                else
                {
                    break;
                }
            }

            return name;
        }

        #endregion

        public MasterClaim GetById(params object[] ids)
        {
            var result = _repository.GetSingle(ids);

            if (result == null)
                return null;

            return _mapper.Map<MasterClaim>(result);
        }

        public MasterClaim Add(MasterClaim model)
        {
            var setting = _mapper.Map<WebSetting>(model);

            setting.Name = GetClaimName(model);

            _repository.Add(setting);

            return model;
        }

        public MasterClaim Update(MasterClaim model)
        {
            var setting = _repository.GetSingle(model.Id);

            setting.Name = GetClaimName(model);
            setting.Value = model.Value;
            setting.CustomField1 = model.ClaimValues;
            setting.LastEditedBy = model.LastEditedBy;
            setting.LastUpdateTime = model.LastUpdateTime;

            _repository.Update(setting);

            return model;
        }

        public MasterClaim Delete(MasterClaim model)
        {
            var setting = _repository.GetSingle(model.Id);

            _repository.Delete(setting);

            return model;
        }

        public IList<MasterClaim> GetAll()
        {
            var settings = _repository.GetAll()
                .Where(x => x.Name.StartsWith(CLAIM_TEXT))
                .ToList();

            return _mapper.Map<List<MasterClaim>>(settings);
        }

        public DataTablesResponse GetDataTablesResponse<TDto>(IDataTablesRequest request, 
            IMapper mapper, 
            string where = null, 
            params Expression<Func<MasterClaim, object>>[] includes)
        {
            throw new NotImplementedException();
        }

        public DataTablesResponse GetDataTablesResponseByQuery<TDto>(IDataTablesRequest request, 
            IMapper mapper, 
            IQueryable data)
        {
            throw new NotImplementedException();
        }

        public IList<MasterClaim> GetAll(params Expression<Func<MasterClaim, object>>[] includes)
        {
            return GetAll();
        }
    }
}
