﻿using App.Domain.Models.Identity;
using System.Collections.Generic;
using System.Security.Claims;

namespace App.Services.Identity
{
    public interface IAppActionService : ICacheService<AppAction, string>
    {
        List<AppActionClaim> GetClaims(string id);
        AppAction GetByName(string name);
        void AddClaim(AppAction action, Claim claim);
        void RemoveClaim(AppAction action, Claim claim);
    }
}
