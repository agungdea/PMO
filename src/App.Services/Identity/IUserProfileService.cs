﻿using App.Domain.Models.Identity;

namespace App.Services.Identity
{
    public interface IUserProfileService : IService<UserProfile>
    {
        UserProfile GetByUserId(string id);
    }
}
