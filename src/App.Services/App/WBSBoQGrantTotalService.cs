﻿using App.Data.Repository;
using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Services.Utils;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class WBSBoQGrantTotalService : BaseService<WBSBoQGrantTotal, IRepository<WBSBoQGrantTotal>>, IWBSBoQGrantTotalService
    {
        private readonly IRepository<WBSTree> _wbsTreeRepository;
        private readonly IRepository<AreaMapping> _stoRepository;
        private readonly IRepository<WBS> _wbsRepository;
        private readonly IRepository<Currency> _currencyRepository;

        private readonly IRepository<WBStreeCategory> _wbsTreeCategoryService;
        public WBSBoQGrantTotalService(
            IRepository<WBSBoQGrantTotal> repository,
            IRepository<WBSTree> wbsTreeRepository,
            IRepository<AreaMapping> stoRepository,
            IRepository<WBS> wbsRepository,
            IRepository<Currency> currencyRepository,
            IRepository<WBStreeCategory> wbsTreeCategoryService

            ) : base(repository)
        {
            _wbsTreeRepository = wbsTreeRepository;
            _stoRepository = stoRepository;
            _wbsRepository = wbsRepository;
            _currencyRepository = currencyRepository;
            _wbsTreeCategoryService = wbsTreeCategoryService;
        }
        public DataTablesResponse GetTotalPerCurrency(Guid WbsId, IDataTablesRequest request)
        {
            var tmpdatalokasi = new List<Guid>();
            var datalistLokasi = _wbsTreeRepository.Table.Where(x => x.IdWBS.Equals(WbsId) && x.Type.Equals(WBS.WBS_TYPE_LOCATION)).ToList();
            foreach (var wbstreelokasitmp in datalistLokasi)
            {
                tmpdatalokasi.Add(wbstreelokasitmp.Id);
            }
            var datatotal = from dataBoqGrandtotal in _repository.Table.Where(x => tmpdatalokasi.Contains(x.IdWBStree))
                            join datawbstree in _wbsTreeRepository.Table on
                            dataBoqGrandtotal.IdWBStree equals datawbstree.Id
                            join currency in _currencyRepository.Table on
                            dataBoqGrandtotal.CurrencyId equals currency.Id
                            select new
                            {
                                WbsId = datawbstree.IdWBS,
                                CurreniesId = dataBoqGrandtotal.CurrencyId,
                                currencies = currency.CurrencyValue,
                                dataBoqGrandtotal.BoQBeforeConverstion,
                            };

            var querydata = from total in datatotal
                            group total by new
                            {
                                total.currencies,
                                total.WbsId
                            } into g
                            select new
                            {
                                Currencies = g.Key.currencies,
                                WbsId = g.Key.WbsId,
                                Total = g.Sum(x => x.BoQBeforeConverstion)
                            };

            var filteredData = querydata;
            var searchQuery = request.Search.Value;
            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            return DataTablesResponse.Create(request, querydata.Count(), filteredData.Count(), dataPage);
        }

        public List<WBSBoQGrantTotalDto> GetWBsPerlocationlist(Guid? WbsId)
        {
            var tmpdatalokasi = new List<Guid>();
            var datalistLokasi = _wbsTreeRepository.Table.Where(x => x.IdWBS.Equals(WbsId) && x.Type.Equals(WBS.WBS_TYPE_LOCATION)).ToList();
            var kamusHeader = new Dictionary<Guid, Guid?>();

            foreach (var wbstreelokasitmp in datalistLokasi)
            {
                tmpdatalokasi.Add(wbstreelokasitmp.Id);
                GetHeader(wbstreelokasitmp.Id, wbstreelokasitmp.Id, kamusHeader);
            }
            var dataheader = new List<JustificationGetTotalDto>();
            foreach (var headerdata in kamusHeader)
            {
                var header = new JustificationGetTotalDto();
                header.IdWBStree = headerdata.Key;
                header.Parent = headerdata.Value;
                dataheader.Add(header);

            }

            var datatotal = from dataTotal in _repository.Table.Where(x => tmpdatalokasi.Contains(x.IdWBStree))
                            join datawbstree in _wbsTreeRepository.Table on
                            dataTotal.IdWBStree equals datawbstree.Id
                            join lokasi in _stoRepository.Table
                            on datawbstree.Lokasi equals lokasi.AreaSto + "|" + lokasi.AreaKode
                            join currency in _currencyRepository.Table
                            on dataTotal.CurrencyId equals currency.Id
                            join header in dataheader on datawbstree.Id equals header.IdWBStree
                            join datawbstreeparent in _wbsTreeRepository.Table on header.Parent equals datawbstreeparent.Id
                            join datawbscategory in _wbsTreeCategoryService.Table on datawbstree.Id equals datawbscategory.WBStreeId
                            select new WBSBoQGrantTotalDto
                            {
                                CurrencyId = dataTotal.CurrencyId,
                                IdWBS = datawbstree.IdWBS,
                                IdWBStree = datawbstree.Id,
                                Text = datawbstree.Name + (lokasi != null ? $" ({lokasi.AreaRegional} - {lokasi.AreaWitel} - {lokasi.AreaDatel} - {lokasi.AreaStoName})" : ""),
                                Total = dataTotal.BoQBeforeConverstion,
                                CurrencyValue = currency.CurrencyValue,
                                CurrencyCode = currency.CurrencyCode,
                                Parent = datawbstree.ParentId,
                                Header = datawbstreeparent.Name,
                                Category = datawbscategory.Category.Name
                            };
            return (datatotal.ToList());
        }
        public List<WBSTreeJustificationDto> GetWBsPerlocation(Guid WbsId)
        {
            var tmpdatalokasi = new List<Guid>();
            var datalistLokasi = _wbsTreeRepository.Table.Where(x => x.IdWBS.Equals(WbsId) && x.Type.Equals(WBS.WBS_TYPE_LOCATION)).ToList();
            var kamusHeader = new Dictionary<Guid, Guid?>();

            foreach (var wbstreelokasitmp in datalistLokasi)
            {
                tmpdatalokasi.Add(wbstreelokasitmp.Id);
                GetHeader(wbstreelokasitmp.Id, wbstreelokasitmp.Id, kamusHeader);
            }
            var dataheader = new List<JustificationGetTotalDto>();
            foreach (var headerdata in kamusHeader)
            {
                var header = new JustificationGetTotalDto();
                header.IdWBStree = headerdata.Key;
                header.Parent = headerdata.Value;
                dataheader.Add(header);

            }

            var datatotal = from datawbstree in _wbsTreeRepository.Table
                            join lokasi in _stoRepository.Table
                            on datawbstree.Lokasi equals lokasi.AreaSto + "|" + lokasi.AreaKode
                            join header in dataheader on datawbstree.Id equals header.IdWBStree
                            join datawbstreeparent in _wbsTreeRepository.Table on header.Parent equals datawbstreeparent.Id
                            select new WBSTreeJustificationDto
                            {
                                IdWBS = datawbstree.IdWBS,
                                IdWBStree = datawbstree.Id,
                                Text = datawbstree.Name + (lokasi != null ? $" ({lokasi.AreaRegional} - {lokasi.AreaWitel} - {lokasi.AreaDatel} - {lokasi.AreaStoName})" : ""),
                                Parent = datawbstree.ParentId,
                                Header = datawbstreeparent.Name

                            };
            return datatotal.ToList();
        }
        
        public DataTablesResponse GetWBsPerlocation(Guid WbsId, IDataTablesRequest request)
        {
            var tmpdatalokasi = new List<Guid>();
            var datalistLokasi = _wbsTreeRepository.Table.Where(x => x.IdWBS.Equals(WbsId) && x.Type.Equals(WBS.WBS_TYPE_LOCATION)).ToList();
            var kamusHeader = new Dictionary<Guid, Guid?>();

            foreach (var wbstreelokasitmp in datalistLokasi)
            {
                tmpdatalokasi.Add(wbstreelokasitmp.Id);
                GetHeader(wbstreelokasitmp.Id, wbstreelokasitmp.Id, kamusHeader);
            }
            var dataheader = new List<JustificationGetTotalDto>();
            foreach (var headerdata in kamusHeader)
            {
                var header = new JustificationGetTotalDto();
                header.IdWBStree = headerdata.Key;
                header.Parent = headerdata.Value;
                dataheader.Add(header);

            }

            var datatotal = from datawbstree in _wbsTreeRepository.Table
                            join lokasi in _stoRepository.Table
                            on datawbstree.Lokasi equals lokasi.AreaSto + "|" + lokasi.AreaKode
                            join header in dataheader on datawbstree.Id equals header.IdWBStree
                            join datawbstreeparent in _wbsTreeRepository.Table on header.Parent equals datawbstreeparent.Id
                            select new
                            {
                                IdWBS = datawbstree.IdWBS,
                                IdWBStree = datawbstree.Id,
                                Text = datawbstree.Name + (lokasi != null ? $" ({lokasi.AreaRegional} - {lokasi.AreaWitel} - {lokasi.AreaDatel} - {lokasi.AreaStoName})" : ""),
                                Parent = datawbstree.ParentId,
                                Header = datawbstreeparent.Name

                            };


            var searchQuery = request.Search.Value;

            var filteredData = datatotal;

            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            return DataTablesResponse.Create(request, datatotal.Count(), filteredData.Count(), dataPage);
        }

        public DataTablesResponse GetWBStotalPerlocation(Guid WbsId, IDataTablesRequest request)
        {
            var tmpdatalokasi = new List<Guid>();
            var datalistLokasi = _wbsTreeRepository.Table.Where(x => x.IdWBS.Equals(WbsId) && x.Type.Equals(WBS.WBS_TYPE_LOCATION)).ToList();
            var kamusHeader = new Dictionary<Guid, Guid?>();

            foreach (var wbstreelokasitmp in datalistLokasi)
            {
                tmpdatalokasi.Add(wbstreelokasitmp.Id);
                GetHeader(wbstreelokasitmp.Id, wbstreelokasitmp.Id, kamusHeader);
            }
            var dataheader = new List<JustificationGetTotalDto>();
            foreach (var headerdata in kamusHeader)
            {
                var header = new JustificationGetTotalDto();
                header.IdWBStree = headerdata.Key;
                header.Parent = headerdata.Value;
                dataheader.Add(header);

            }

            var datatotal = from dataTotal in _repository.Table.Where(x => tmpdatalokasi.Contains(x.IdWBStree))
                            join datawbstree in _wbsTreeRepository.Table on
                            dataTotal.IdWBStree equals datawbstree.Id
                            join lokasi in _stoRepository.Table
                            on datawbstree.Lokasi equals lokasi.AreaSto + "|" + lokasi.AreaKode
                            join currency in _currencyRepository.Table
                            on dataTotal.CurrencyId equals currency.Id
                            join header in dataheader on datawbstree.Id equals header.IdWBStree
                            join datawbstreeparent in _wbsTreeRepository.Table on header.Parent equals datawbstreeparent.Id
                            join datawbscategory in _wbsTreeCategoryService.Table on datawbstree.Id equals datawbscategory.WBStreeId
                            select new
                            {
                                CurrencyId = dataTotal.CurrencyId,
                                IdWBS = datawbstree.IdWBS,
                                IdWBStree = datawbstree.Id,
                                Text = datawbstree.Name + (lokasi != null ? $" ({lokasi.AreaRegional} - {lokasi.AreaWitel} - {lokasi.AreaDatel} - {lokasi.AreaStoName})" : ""),
                                Total = dataTotal.BoQBeforeConverstion,
                                CurrencyValue = currency.CurrencyValue,
                                Parent = datawbstree.ParentId,
                                Header = datawbstreeparent.Name,
                                Category = datawbscategory.Category.Name
                            };


            var searchQuery = request.Search.Value;

            var filteredData = datatotal;

            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            return DataTablesResponse.Create(request, datatotal.Count(), filteredData.Count(), dataPage);

        }
        private Dictionary<Guid, Guid?> GetHeader(Guid wbsTreeId, Guid? parents, Dictionary<Guid, Guid?> kamus)
        {

            var getheader = _wbsTreeRepository.Table.FirstOrDefault(x => x.Id.Equals(parents));
            if (getheader != null)
            {
                if (getheader.ParentId != null)
                {
                    var Data = GetHeader(wbsTreeId, getheader.ParentId, kamus);
                }
                else
                {
                    kamus.Add(wbsTreeId, getheader.Id);
                }
            }
            return kamus;
        }




    }
}
