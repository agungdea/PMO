﻿using App.Data.Repository;
using App.Domain.Models.App;
using App.Domain.Models.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class UserUnitService : BaseService<UserUnit, IRepository<UserUnit>>, IUserUnitService
    {
        public UserUnitService(IRepository<UserUnit> repository) : base(repository)
        {
        }

        public void AddUserUnit(ApplicationUser user, List<string> unit)
        {
            foreach (string UnitId in unit)
            {
                _repository.Add(new UserUnit
                {
                    UserId = user.Id,
                    UnitId = UnitId
                });
            }
        }

        public UserUnit GetUserUnitByIdUserAndLevel(string id, string level)
        {
            return _repository.Table.Include(x => x.Unit).Where(x => x.UserId == id && x.Unit.STLevel == level).FirstOrDefault();
        }
      
    }
}
