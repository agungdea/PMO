﻿using System;
using System.Collections.Generic;
using App.Data.Repository;
using App.Domain.Models.App;
using System.Linq;

namespace App.Services.App
{
    public class ActivityTemplateService : BaseService<ActTemplate, IRepository<ActTemplate>>, IActivityTemplateService
    {

        private readonly IUnitService _unitService;
        public ActivityTemplateService(IRepository<ActTemplate> repository,
           IUnitService unitService)
            : base(repository)
        {
            _unitService = unitService;
        }
        private List<string> getGrup(string unitId, List<string> grup)
        {

            var unit = _unitService.GetAll().Where(x=>x.ParentId==unitId);
            foreach (var dataunit in unit)
            {
                grup.Add(dataunit.Id);
                getGrup(dataunit.Id, grup);
            }

            //if (unit.STLevel != Unit.STLevelBagian)
            //{
            //    grup = _unitService.GetChild(unit.Id);
            //}
            //else
            //{
            //    grup.Add(unit.Id);
            //}


            return grup;

        }
        public List<ActTemplate> Query(string unitId, string query, int take = 10, int page = 1)
        {

            var templates = _repository
                 .Table
                 .Where(x => x.Name.ToLower().Contains(query.ToLower()))
                 .OrderBy(x => x.Name)
                 .Skip((page - 1) * take)
                 .Take(take);

            if (unitId != null)
            {
                var unitBagian = _unitService.GetById(unitId);

                if (unitBagian != null && unitBagian.STLevel == Unit.STLevelBagian)
                {
                    List<string> grup = new List<string>();
                    grup.Add(unitBagian.ParentId);
                    var datagrup = _unitService.GetAll().Where(x => x.ParentId == unitBagian.ParentId);
                    foreach (var grup_list in datagrup)
                    {
                        grup.Add(grup_list.Id);
                        grup = getGrup(grup_list.Id, grup);
                    }
                    var conten = grup;

                    templates = _repository
                                     .Table
                                     .Where(x => x.Name.ToLower().Contains(query.ToLower()))
                                     .OrderBy(x => x.Name)
                                     .Skip((page - 1) * take)
                                     .Take(take);

                    return templates.Where(x => conten.Contains(x.Grup)).ToList();
                    // templates = _repository
                    //.Table
                    //.Where(x => x.Name.ToLower().Contains(query.ToLower()))
                    //.OrderBy(x => x.Name)
                    //.Skip((page - 1) * take)
                    //.Take(take);

                    // return templates.Where(x => x.Grup.Equals(unitId)).ToList();
                }
                else
                {
                    List<string> grup = new List<string>();
                    grup.Add(unitId);
                    var datagrup=_unitService.GetAll().Where(x => x.ParentId == unitId);
                    foreach (var grup_list in datagrup)
                    {
                        grup.Add(grup_list.Id);
                        grup= getGrup(grup_list.Id, grup);
                    }
                    var conten = grup;

                    templates = _repository
                                     .Table
                                     .Where(x => x.Name.ToLower().Contains(query.ToLower()))
                                     .OrderBy(x => x.Name)
                                     .Skip((page - 1) * take)
                                     .Take(take);

                    return templates.Where(x => conten.Contains(x.Grup)).ToList();
                }
            }
            return templates.ToList();
        }
    }
}
