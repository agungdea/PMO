﻿using App.Data.Repository;
using App.Domain.Models.App;
using App.Services.Core;
using Modular.Core.Caching;
using System;

namespace App.Services.App
{
    public class AkiValuationService : CacheService<AkiValuation, Guid, IRepository<AkiValuation>>, IAkiValuationService
    {
        private readonly IWebSettingService _webSettingService;
        private const string AKIVALUATION_ALL = "app.akivaluation.all";
        public AkiValuationService(IRepository<AkiValuation> repository,
            IWebSettingService webSettingService,
            ICacheManager cacheManager)
            : base(repository, cacheManager)
        {
            _webSettingService = webSettingService;
            _cacheKey = AKIVALUATION_ALL;
            _keyItem = x => x.Id.ToString();

            int.TryParse(_webSettingService.GetByName("cache.akivaluation.interval")?.Value, out _cacheInterval);
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;
        }
    }
}
