﻿using System;
using System.Collections.Generic;
using App.Data.Repository;
using App.Domain.Models.App;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace App.Services.App
{
    public class UnitService : SoftDeleteBaseService<Unit, IRepository<Unit>>, IUnitService
    {
        public UnitService(IRepository<Unit> repository) : base(repository)
        {
        }

        public List<SelectListItem> GetSelectListItem()
        {
            var getall = _repository.GetAll();
            var tmp = getall.Select(y => new
            {
                id = y.Id,
                name = y.NamaUnit,
                parent = y.ParentId != null ? y.ParentId : "-"
            });
            return tmp.Where(y => y.parent.Substring(0, 2) == "01").Select(x => new SelectListItem
            {
                Text = x.name,
                Value = x.id,
            }).OrderBy(x => x.Value).ToList();
        }
        public List<Unit> GetUnitByParameters(string level, string parentId)
        {

            List<Unit> result = new List<Unit>();

            if (string.IsNullOrWhiteSpace(parentId))
            {
                result = _repository.Table.Where(x => x.STLevel.Equals(level)).ToList();

                return result;
            }

            result = _repository.Table.Where(x => x.STLevel.Equals(level) && x.ParentId == parentId).ToList();

            return result;
        }
        public string DeleteChildData(string id)
        {
            var getbyid = this.GetAll().Where(x => x.ParentId == id);
            if (getbyid.Count() > 0)
            {
                foreach (var data in getbyid)
                {
                    this.DeleteChildData(data.Id);
                    this.Delete(data);
                }
            }
            return id;
        }

        public List<string> GetChild(string id)
        {
            var child = new List<string>();

            var getbyid = this.GetAll().Where(x => x.ParentId == id).Select(x => x.Id).ToList();

            if (getbyid.Count() > 0)
            {
                foreach (var data in getbyid)
                {
                    child.Add(data);
                }
            }

            return child;
        }

        public List<string> GetUnitLevel(string unitId)
        {
            List<string> grup = new List<string>();
            var unitBagian = this.GetById(unitId);

            if (unitBagian != null)
            {
                if (unitBagian != null && unitBagian.STLevel == Unit.STLevelBagian)
                {
                    grup.Add(unitBagian.Id);
                }
                else
                {
                    var level2 = _repository.Table.Where(x => x.ParentId.Equals(unitId) && x.STLevel == Unit.STLevelUnitBisnis).Select(X => X.Id).ToList();
                    if (level2.Count > 0)
                    {
                        var level3 = _repository.Table.Where(x => x.ParentId.Equals(unitId) ).Select(X => X.Id).ToList();
                        if (level3.Count > 0)
                        {
                            level2.AddRange(level3);
                            var level4 = _repository.Table.Where(x => level3.Contains(x.ParentId)).Select(X => X.Id).ToList();
                            if (level4.Count > 0)
                            {
                                level2.AddRange(level4);
                                var level5 = _repository.Table.Where(x => level4.Contains(x.ParentId)).Select(X => X.Id).ToList();
                                if (level5.Count > 0)
                                {
                                    level2.AddRange(level5);
                                }
                            }
                        }
                        level2.Add(unitId);
                        return level2;
                    }
                    else
                    {
                        var level3 = _repository.Table.Where(x => x.ParentId.Equals(unitId) && x.STLevel == Unit.STLevelUnitKerja).Select(X => X.Id).ToList();
                        if (level3.Count > 0)
                        {
                            var level4 = _repository.Table.Where(x => level3.Contains(x.ParentId)).Select(X => X.Id).ToList();
                            if (level4.Count > 0)
                            {
                                level3.AddRange(level4);
                                var level5 = _repository.Table.Where(x => level4.Contains(x.ParentId)).Select(X => X.Id).ToList();
                                if (level5.Count > 0)
                                {
                                    level3.AddRange(level5);
                                }
                            }
                            level3.Add(unitId);
                            return level3;
                        }
                        else
                        {

                            var level4 = _repository.Table.Where(x => x.ParentId.Equals(unitId) && x.STLevel == Unit.STLevelBagian).Select(X => X.Id).ToList();
                            if (level4.Count > 0)
                            {
                                var level5 = _repository.Table.Where(x => level3.Contains(x.ParentId)).Select(X => X.Id).ToList();
                                if (level5.Count > 0)
                                {
                                    level4.AddRange(level5);
                                }
                                level4.Add(unitId);
                                return level4;
                            }
                        }
                    }
                   
                    //grup.Add(unitId);
                    //var datagrup = this.GetAll().Where(x => x.ParentId == unitId);
                    //foreach (var grup_list in datagrup)
                    //{
                    //    grup.Add(grup_list.Id);
                    //    grup = getGrup(grup_list.Id, grup);
                    //}
                }
            }

            return grup;
        }
        private List<string> getGrup(string unitId, List<string> grup)
        {
            var unit = this.GetAll().Where(x => x.ParentId == unitId);
            foreach (var dataunit in unit)
            {
                grup.Add(dataunit.Id);
                getGrup(dataunit.Id, grup);
            }
            return grup;
        }

        public bool ValidateUnit(string id)
        {
            var unit = _repository.Table.FirstOrDefault(x => x.Id == id);

            if (null == unit)
            {
                return false;
            }

            return true;
        }
    }
}
