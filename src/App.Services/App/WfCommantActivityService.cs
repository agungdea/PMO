﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class WfCommantActivityService : BaseService<WfCommantActivity, IRepository<WfCommantActivity>>,IWfCommantActivityService
    {
        public WfCommantActivityService(IRepository<WfCommantActivity> repository) : base(repository)
        {
        }
    }
}
