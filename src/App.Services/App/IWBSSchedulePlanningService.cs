﻿using App.Domain.Models.App;
using System;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IWBSSchedulePlanningService : IService<WBSSchedulePlanning>
    {
        Dictionary<DateTime, Decimal?> getCurvaSPlanning(Guid id);
    }
}
