﻿using App.Domain.Models.App;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IActivityTemplateChildService : IService<ActTmpChild>
    {
        int GenerateActivityId();

        List<string> QueryActivity(string query, int take = 10, int page = 1);

        int GetActivityId(string activityName);
    }
}
