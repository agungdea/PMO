﻿using App.Data.Repository;
using App.Domain.Models.App;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using App.Data.DAL;
using App.Domain.DTO.App;
using App.Services.Utils;
using System.Linq.Dynamic.Core;
using System;

namespace App.Services.App
{
    public class HargaService : BaseService<Harga, IRepository<Harga>>, IHargaService
    {
        private readonly ApplicationDbContext _context;

        public HargaService(IRepository<Harga> repository,
            ApplicationDbContext context) : base(repository)
        {
            _context = context;
        }

        public List<Harga> GetByIdDesignator(string idDesignator)
        {
            var hargas = _repository
                .Table
                .Where(x => x.IdDesignator == idDesignator)
                .ToList();

            return hargas;
        }

        public List<Harga> DeleteByIdDesignator(string idDesignator)
        {
            var hargas = _repository
                .Table
                .Where(x => x.IdDesignator == idDesignator)
                .ToList();

            _repository.Delete(hargas);

            return hargas;
        }

        public List<string> QueryGrup(string query, int take = 10, int page = 1)
        {
            var groups = _repository
                .Table
                .Where(x => x.Lokasi.ToLower().Contains(query.ToLower()))
                .Select(x => x.Lokasi)
                .Distinct()
                .OrderBy(x => x)
                .Skip((page - 1) * take)
                .Take(take);

            return groups.ToList();
        }

        public List<Harga> QueryGrupByDesignator(string idDesignator, string query, int take = 10, int page = 1)
        {
            var groups = _repository
                .Table
                .Where(x => x.IdDesignator == idDesignator 
                    && x.Lokasi.ToLower().Contains(query.ToLower()))
                .OrderBy(x => x.Lokasi)
                .Skip((page - 1) * take)
                .Take(take);

            return groups.ToList();
        }

        public List<string> QueryMataUang(string query, int take = 10, int page = 1)
        {
            var groups = _repository
                .Table
                .Where(x => x.MataUang.ToLower().Contains(query.ToLower()))
                .Select(x => x.MataUang)
                .Distinct()
                .OrderBy(x => x)
                .Skip((page - 1) * take)
                .Take(take);

            return groups.ToList();
        }

        public DataTablesResponse GetDataTablesResponsePaket(IDataTablesRequest request, IMapper mapper, string paket)
        {
            var data = (from harga in _context.Hargas
                        join designator in _context.Designators
                            on harga.IdDesignator equals designator.IdDesignator
                        select new HargaDto
                        {
                            Id = designator.Id,
                            IdDesignator = harga.IdDesignator,
                            Deskripsi = designator.Deskripsi,
                            HargaJasa = harga.HargaJasa,
                            HargaMaterial = harga.HargaMaterial,
                            Lokasi = harga.Lokasi,
                            MataUang = harga.MataUang,
                            Satuan = designator.Satuan
                        });

            if (paket != null && paket != "null")
                data = data.Where(x => x.Lokasi == paket);

            var searchQuery = request.Search.Value;

            var filteredData = data;

            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request,
                data.Count(),
                filteredData.Count(),
                dataPage);

            return response;
        }

        public List<HargaDto> GetByPaket(string paket)
        {
            var items = (from harga in _context.Hargas
                         join designator in _context.Designators
                             on harga.IdDesignator equals designator.IdDesignator
                         where harga.Lokasi == paket
                         select new HargaDto
                         {
                             Id = designator.Id,
                             IdDesignator = harga.IdDesignator,
                             Deskripsi = designator.Deskripsi,
                             HargaJasa = harga.HargaJasa,
                             HargaMaterial = harga.HargaMaterial,
                             Lokasi = harga.Lokasi,
                             MataUang = harga.MataUang,
                             Satuan = designator.Satuan
                         }).ToList();

            return items;
        }

        public bool Exist(string idDesignator, string paket)
        {
            return _repository
                .Table
                .Any(x => x.IdDesignator == idDesignator && x.Lokasi == paket);
        }
    }
}
