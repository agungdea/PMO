﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class GroupWfService : BaseService<GroupWf, IRepository<GroupWf>>, IGroupWfService
    {
        public GroupWfService(IRepository<GroupWf> repository) : base(repository)
        {
        }
    }
}
