﻿using App.Data.Repository;
using App.Domain.Models.App;

namespace App.Services.App
{
    public class UserMediaService : BaseService<UserMedia, IRepository<UserMedia>>, IUserMediaService
    {
        public UserMediaService(IRepository<UserMedia> repository) : base(repository)
        {
        }
    }
}
