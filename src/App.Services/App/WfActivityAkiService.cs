﻿using App.Data.Repository;
using App.Domain.Models.App;
using App.Domain.Models.Identity;
using App.Domain.Models.Workflow;
using App.Services.Utils;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class WfActivityAkiService : BaseService<WfActivityAki, IRepository<WfActivityAki>>, IWfActivityAkiService
    {
        private IRepository<WfProcess> _WfProcessRepo;
        private IRepository<Aki> _akiRepo;
        private IRepository<ApplicationUser> _userManagerRepo;
        public WfActivityAkiService(IRepository<WfActivityAki> repository,
             IRepository<WfProcess> WfProcessRepo,
             IRepository<Aki> akiRepo,
             IRepository<ApplicationUser> userManagerRepo
            ) : base(repository)
        {
            _WfProcessRepo = WfProcessRepo;
            _akiRepo = akiRepo;
            _userManagerRepo = userManagerRepo;
        }

        public DataTablesResponse DataTablesMyTask(String Nik, Boolean iscapex, IDataTablesRequest request)
        {
           
            var includes = new Expression<Func<ApplicationUser, object>>[1];
            includes[0] = (x => x.UserProfile);
        
            if (iscapex == true)
            {
                var data = from actwf in _repository.Table.Where(x => x.AssigmentTo.Equals(Nik) && x.state.Equals(WfActivityAki.OPEN_ACTIVITY) || x.AssigmentTo.Equals(GroupWf.GROUP_WF_Capex) && x.state.Equals(WfActivityAki.OPEN_ACTIVITY))
                       join _aki in _akiRepo.Table on actwf.AkiId equals _aki.Id
                       join wfProcess in _WfProcessRepo.Table on actwf.wf_processId equals wfProcess.ProcessId
                       join usr in _userManagerRepo.GetAll(includes) on wfProcess.Requester equals usr.UserName
                       select new
                       {
                           actwf,
                           wfProcess.Requester,
                           RequsterName = usr.UserName + " - " + usr.UserProfile.Name,
                           _aki.AkiName,
                           _aki.Id
                       };
                var filteredData = data;

                var searchQuery = request.Search.Value;
                var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

                if (!string.IsNullOrEmpty(globalFilter))
                    filteredData = filteredData.Where(globalFilter);

                var filter = request.Columns.GetFilter();

                if (!string.IsNullOrEmpty(filter))
                    filteredData = filteredData.Where(filter);

                var sort = request.Columns.GetSort();

                if (!string.IsNullOrEmpty(sort))
                    filteredData = filteredData.OrderBy(sort);

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);
                return DataTablesResponse.Create(request, filteredData.Count(), filteredData.Count(), dataPage);

            }
            else {
                var data = from actwf in _repository.Table.Where(x => x.AssigmentTo.Equals(Nik) && x.state.Equals(WfActivityAki.OPEN_ACTIVITY))
                           join _aki in _akiRepo.Table on actwf.AkiId equals _aki.Id
                           join wfProcess in _WfProcessRepo.Table on actwf.wf_processId equals wfProcess.ProcessId
                           join usr in _userManagerRepo.GetAll(includes) on wfProcess.Requester equals usr.UserName
                           select new
                           {
                               actwf,
                               wfProcess.Requester,
                               RequsterName = usr.UserName + " - " + usr.UserProfile.Name,
                               _aki.AkiName,
                               _aki.Id
                           };
                var filteredData = data;

                var searchQuery = request.Search.Value;
                var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

                if (!string.IsNullOrEmpty(globalFilter))
                    filteredData = filteredData.Where(globalFilter);

                var filter = request.Columns.GetFilter();

                if (!string.IsNullOrEmpty(filter))
                    filteredData = filteredData.Where(filter);

                var sort = request.Columns.GetSort();

                if (!string.IsNullOrEmpty(sort))
                    filteredData = filteredData.OrderBy(sort);

                var dataPage = filteredData.Skip(request.Start).Take(request.Length);
                return DataTablesResponse.Create(request, filteredData.Count(), filteredData.Count(), dataPage);

            }
           
           
        }
    }
}
