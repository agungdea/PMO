﻿using App.Domain.Models.App;
using System;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IAreaMappingService : ICacheService<AreaMapping, Guid>
    {
        List<AreaMapping> GetListAreaByCodeArea(string codeArea);
        List<string> GetRegional(string search);
        List<string> GetWitel(string regional, string search);
        List<string> GetDatel(string witel, string search);
        List<AreaMapping> GetSTO(string datel, string search);
    }
}
