﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IHargaService : IService<Harga>
    {
        bool Exist(string idDesignator, string paket);
        List<Harga> GetByIdDesignator(string idDesignator);
        List<Harga> DeleteByIdDesignator(string idDesignator);
        List<string> QueryGrup(string query, int take = 10, int page = 1);
        List<Harga> QueryGrupByDesignator(string idDesignator, string query, int take = 10, int page = 1);
        List<string> QueryMataUang(string query, int take = 10, int page = 1);
        List<HargaDto> GetByPaket(string paket);
        DataTablesResponse GetDataTablesResponsePaket(IDataTablesRequest request, IMapper mapper, string paket);
    }
}
