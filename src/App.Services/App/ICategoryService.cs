﻿using App.Domain.Models.App;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface ICategoryService : IService<Category>
    {
        int DeleteChildData(int id);
        IEnumerable GetAlldata(Guid Id);
        IEnumerable GetChildData(int? parentid);
    }

}
