﻿using App.Data.Repository;
using App.Domain.DTO.App;
using App.Domain.Models.App;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Services.App
{
    public class WBSScheduleTempService : BaseService<WBSScheduleTemp, IRepository<WBSScheduleTemp>>, IWBSScheduleTempService
    {
        private readonly IActivityTemplateChildService _activityTemplateChildService;
        private readonly IRepository<ActTmpChild> _RepositoryActivityTemplatechild;
        private readonly IRepository<ActTemplate> _RepositoryActivityTemplate;

        public WBSScheduleTempService(IRepository<WBSScheduleTemp> repository,
            IActivityTemplateChildService activityTemplateChildService,
             IRepository<ActTmpChild> RepositoryActivityTemplatechild,
             IRepository<ActTemplate> RepositoryActivityTemplate
            ) 
            : base(repository)
        {
            _RepositoryActivityTemplatechild = RepositoryActivityTemplatechild;
            _activityTemplateChildService = activityTemplateChildService;
            _RepositoryActivityTemplate = RepositoryActivityTemplate;
        }
        public DataTablesResponse GetWBSScheduleTemp(
            Guid Wbstreeid,Guid IdActTmp,
            IDataTablesRequest request,IMapper mapper,
            bool other = false,
            bool preview = false
        )
        {
            var scheduletemp = from tempchild in _RepositoryActivityTemplatechild.Table
                               join temp in _RepositoryActivityTemplate.Table
                                   on tempchild.IdActTmp equals temp.Id
                               //join wbstem in _repository.Table
                               //     on new { tes=tempchild.Id, Id = Wbstreeid } equals
                               //         new { tes =wbstem.IdActTmpChild, Id = wbstem.IdWBSTree } into wbstem2
                               //from wbstem in wbstem2.DefaultIfEmpty()
                               select new 
                               {
                                   IdActTmpChild= tempchild.Id,
                                   ActId= tempchild.ActId,
                                   ActParent = tempchild.ActParent,
                                   NamaAct = tempchild.NamaAct,
                                   Bobot = tempchild.Bobot,
                                   Durasi = tempchild.Durasi,
                                   Predecessor = tempchild.Predecessor,
                                   ISLEAF= tempchild.ISLEAF,
                                   IdActTmp = tempchild.IdActTmp,
                                    //DurasiReal = wbstem.DurasiReal,
                                   //Start= wbstem.Start,
                                   //Finish= wbstem.Finish ,
                                   //IdWBSTree= wbstem.IdWBSTree,
                                   //OtherInfo= _RepositoryActivityTemplatechild.Table.Where(z=>z.ActId.Equals(tempchild.ActParent)).First().NamaAct
                               };
            //if (other)
            //{
            //    scheduletemp = scheduletemp.Where(x => x.IdActTmp != IdActTmp && x.Start !=null);
            //}
            //else if (preview)
            //{
            //    scheduletemp = scheduletemp.Where(x => x.IdActTmp == IdActTmp && x.Start != null);
            //}
            //else
            //{
                scheduletemp = scheduletemp.Where(x => x.IdActTmp == IdActTmp);
            //}

            var filteredData = String.IsNullOrWhiteSpace(request.Search.Value) ? scheduletemp : scheduletemp.Where(_item => _item.NamaAct.ToLower().Contains(request.Search.Value.ToLower()) || _item.IdActTmpChild.ToString().ToLower().Contains(request.Search.Value.ToLower()));
            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
           return  DataTablesResponse.Create(request, scheduletemp.Count(), filteredData.Count(), dataPage);
            
        }

 }
}
