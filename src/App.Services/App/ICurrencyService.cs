﻿using App.Domain.Models.App;

namespace App.Services.App
{
    public interface ICurrencyService : IService<Currency>
    {
    }
}
