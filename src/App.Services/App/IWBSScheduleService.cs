﻿using App.Domain.Models.App;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface IWBSScheduleService: IService<WBSSchedule>
    {
        Dictionary<DateTime, Decimal?> getCurvaS(Guid id);
        DataTablesResponse GetWBSScheduleTemp(Guid Wbstreeid, Guid IdActTmp,IDataTablesRequest request);
        DataTablesResponse GetWBSScheduleById(Guid Wbstreeid,IDataTablesRequest request);
    }
}
