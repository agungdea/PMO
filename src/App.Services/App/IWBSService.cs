﻿using App.Domain.Models.App;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IWBSService : IService<WBS>
    {
        DataTablesResponse GetDataTablesWBS(List<string> Contain, IDataTablesRequest request,Boolean IsAdmin );


    }
}
