﻿using App.Data.Repository;
using App.Domain.Models.App;
using App.Services.Utils;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace App.Services.App
{
    public class WBSService : BaseService<WBS, IRepository<WBS>>, IWBSService
    {
        private readonly IUnitService _unitService;
        private readonly IRepository<Unit> _Unitrepository;
        public WBSService(
            IRepository<WBS> repository,
            IRepository<Unit> Unitrepository,
             IUnitService unitService
            ) : base(repository)
        {
            _unitService = unitService;
            _Unitrepository = Unitrepository;
        }
        
        private List<string> getGrup(string unitId, List<string> grup)
        {
            var unit = _unitService.GetAll().Where(x => x.ParentId == unitId);
            foreach (var dataunit in unit)
            {
                grup.Add(dataunit.Id);
                getGrup(dataunit.Id, grup);
            }
            return grup;
        }
        public DataTablesResponse GetDataTablesWBS(List<string> Contain, IDataTablesRequest request, Boolean IsAdmin)
        {
            var data = from wbses in _repository.Table
                       join unites in _Unitrepository.Table on wbses.Unit equals unites.Id
                       select new
                       {
                           wbses.Id,
                           wbses.NamaKegiatan,
                           wbses.Unit,
                           unites.NamaUnit,
                           CreatedAt= string.Format("{0:d}", wbses.CreatedAt)
                          
                       };
            var filteredData = data;
            if (IsAdmin == false)
            {
                filteredData= filteredData.Where(x => Contain.Contains(x.Unit));
            }
            var searchQuery = request.Search.Value;
            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            return DataTablesResponse.Create(request, data.Count(), filteredData.Count(), dataPage);
        }
    }
}
