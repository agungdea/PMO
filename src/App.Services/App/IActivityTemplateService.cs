﻿using App.Domain.Models.App;
using System;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IActivityTemplateService : IService<ActTemplate>
    {
        List<ActTemplate> Query(string unitId, string query, int take = 10, int page = 1);
    }
}
