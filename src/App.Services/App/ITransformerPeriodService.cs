﻿using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface ITransformerPeriodService : IService<TransformerPeriod>
    {
        List<TransformerPeriod> GetExistingTransformerPeriod(String Period);
    }
}
