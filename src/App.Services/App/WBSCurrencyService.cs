﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Linq;

namespace App.Services.App
{
    public class WBSCurrencyService : BaseService<WBSCurrency, IRepository<WBSCurrency>>, IWBSCurrencyService
    {
        private readonly IRepository<Currency> _currenciesRepository;
        private readonly IRepository<WBSCurrency> _wbscurrenciesRepository;
        private readonly IRepository<WBSTree> _wbstreeRepository;
        public WBSCurrencyService(
            IRepository<WBSCurrency> repository,
            IRepository<Currency> currenciesRepository,
            IRepository<WBSCurrency> wbscurrenciesRepository,
            IRepository<WBSTree> wbstreeRepository
            ) : base(repository)
        {
            _currenciesRepository = currenciesRepository;
            _wbscurrenciesRepository = wbscurrenciesRepository;
            _wbstreeRepository = wbstreeRepository;
        }
        public bool CekBoq(Guid WbsTreeId, string matauang)
        {

            if (matauang != Currency.CurrencyRp)
            {
                var datamatauang = _currenciesRepository.Table.First(x => x.CurrencyValue == matauang);

                if (datamatauang != null)
                {
                    var datawbsid = _wbstreeRepository.Table.First(x => x.Id == WbsTreeId);
                    if (datawbsid != null)
                    {
                        var datawbscurrency = _wbscurrenciesRepository.Table.Where(x => x.CurrencyId.Equals(datamatauang.Id) && x.WBSId.Equals(datawbsid.IdWBS));
                        if (datawbscurrency.Count() != 0)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }
    }
}
