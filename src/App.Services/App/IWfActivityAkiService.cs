﻿using App.Domain.Models.App;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface IWfActivityAkiService : IService<WfActivityAki>
    {
        DataTablesResponse DataTablesMyTask(String Nik, Boolean iscapex, IDataTablesRequest request);


    }
}
