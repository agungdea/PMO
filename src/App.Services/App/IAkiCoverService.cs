﻿using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface IAkiCoverService : ICacheService<AkiCover, Guid>
    {
        AkiCover GetAkiCoverByIdAki(Guid id);

        AkiCover GetAllAboutAkiByIdAki(Guid id);
    }
}
