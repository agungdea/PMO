﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class TranformerProgramService : BaseService<TransformerProgram, IRepository<TransformerProgram>>, ITranformerProgramService
    {
        public TranformerProgramService(IRepository<TransformerProgram> repository) : base(repository)
        {
        }
    }
}
