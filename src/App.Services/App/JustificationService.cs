﻿using System;
using App.Data.Repository;
using App.Domain.Models.App;
using App.Services.Core;
using Modular.Core.Caching;
using App.Services.Workflow;
using System.Threading.Tasks;
using Newtonsoft.Json;
using App.Domain.Models.Workflow;
using System.Collections.Generic;
using DataTables.AspNet.Core;
using DataTables.AspNet.AspNetCore;
using System.Linq;
using App.Services.Utils;
using System.Linq.Dynamic.Core;
using App.Domain.Models.Identity;
using Microsoft.EntityFrameworkCore;

namespace App.Services.App
{
    public class JustificationService :
        BaseService<Justification, IRepository<Justification>>,
        IJustificationService
    {
        private readonly ICacheManager _cacheManager;
        private readonly IWebSettingService _webSettingService;
        private readonly IWfProcessService _wfProcessService;
        private const string JUSTIFICATION_CACHE_KEY = "justification-{0}";
        private readonly string _jogetAppId;
        private readonly string _jogetProcessDefId;

        public JustificationService(IRepository<Justification> repository,
            ICacheManager cacheManager,
            IWebSettingService webSettingService,
            IWfProcessService wfProcessService)
            : base(repository)
        {
            _cacheManager = cacheManager;
            _webSettingService = webSettingService;
            _wfProcessService = wfProcessService;

            _jogetAppId = _webSettingService.GetByName("joget.wf.justification.app.id")?.Value;
            _jogetProcessDefId = _webSettingService.GetByName("joget.wf.justification.process.def.id")?.Value;
        }

        public void SetToCache(Justification justification)
        {
            var key = string.Format(JUSTIFICATION_CACHE_KEY, justification.CreatedBy);
            _cacheManager.Set(key, justification, 60 * 24);
        }

        public Justification GetFromCache(string creator)
        {
            var key = string.Format(JUSTIFICATION_CACHE_KEY, creator);
            var result = _cacheManager.Get<Justification>(key);
            return result;
        }

        public bool DeleteCache(string creator)
        {
            var key = string.Format(JUSTIFICATION_CACHE_KEY, creator);
            _cacheManager.Remove(key);
            return !_cacheManager.IsSet(key);
        }

        public override Justification GetById(params object[] ids)
        {
            var justification = base.GetById(ids);

            if (justification == null)
                return null;

            if (justification.WfProcessId.HasValue)
            {
                justification.WfProcess = _wfProcessService.GetById(justification.WfProcessId);
            }

            return justification;
        }
        //public override Justification Add(Justification model)
        //{
        //    var item = CreateWf(model).Result;

        //    return base.Add(item);
        //}

        //public override Justification Delete(Justification model)
        //{
        //    model = base.Delete(model);

        //    var wf = _wfProcessService.GetById(model.WfProcessId);

        //    _wfProcessService.Delete(wf);

        //    return model;
        //}

        public async Task<Justification> CreateWf(Justification model)
        {
            if (model == null)
                return model;

            if (model.WfProcessId.HasValue)
                return model;

            var wf = await _wfProcessService.StartProcess(_jogetProcessDefId);

            var activities = await _wfProcessService.ListActivity(wf.ProcessId, null);

            wf.Activities = activities;

            _wfProcessService.Add(wf);

            wf.ActivityId = activities[0].Id;

            _wfProcessService.Update(wf);

            model.WfProcessId = wf.Id;

            return model;
        }

        public async Task<Justification> Submit(Guid id, string callbackUrl)
        {
            var justification = GetById(id);

            if (justification == null)
                return null;

            if (!justification.WfProcessId.HasValue)
            {
                justification = await CreateWf(justification);
            }

            var wf = _wfProcessService.GetById(justification.WfProcessId);

            var parameter = new Dictionary<string, string>();

            parameter.Add("var_totalBudget", justification.JumlahAnggaran.ToString());
            parameter.Add("var_documentId", justification.Id.ToString());
            parameter.Add("var_documentName", justification.Penyusun.ToString());
            parameter.Add("var_callbackUrl", callbackUrl);

            wf = await _wfProcessService.CompleteActivity(wf.ActivityId, parameter);

            justification.JustificationStatus = Justification.STATUS_SUBMITTED;

            Update(justification);

            return justification;
        }

        public async Task<Justification> Approve(Guid id, Dictionary<string, string> parameter)
        {
            var justification = GetById(id);

            if (justification == null)
                return null;

            if (!justification.WfProcessId.HasValue)
                return null;

            var wf = _wfProcessService.GetById(justification.WfProcessId);

            wf = await _wfProcessService.CompleteActivity(wf.ActivityId, parameter);

            if (string.IsNullOrWhiteSpace(wf.ActivityId))
            {
                justification.JustificationStatus = Justification.STATUS_APPROVED;
                Update(justification);
            }

            return justification;
        }
        public DataTablesResponse GetDataJustification(List<string> UnitId, IDataTablesRequest request, Boolean IsAdmin, Boolean IsPurchaseRequisition, Boolean IsUser)
        {
            var data = from justification in _repository.Table
                       select new
                       {
                           justification.Id,
                           justification.Penyusun,
                           justification.OtherInfo,
                           justification.CreatedAt,
                           justification.CreatedBy,
                           justification.LastEditedBy,
                           justification.JustificationStatus,
                           justification.CustomField1
                       };
            var filteredData = data;
            if (IsAdmin == true)
            {
                filteredData = data;
            }
            if (IsPurchaseRequisition == true)
            {
                filteredData = data.Where(x => (UnitId.Contains(x.OtherInfo) && x.JustificationStatus.Equals(Justification.STATUS_NEED_REVIEW) && x.CustomField1.Equals(Justification.STATUS_SAP_BEFORE_SUBMIT)) ||
                                               (UnitId.Contains(x.OtherInfo) && x.JustificationStatus.Equals(Justification.STATUS_APPROVED) && x.CustomField1.Equals(Justification.STATUS_SAP_BEFORE_SUBMIT)));
            }
            if (IsPurchaseRequisition == false && IsAdmin == false && IsUser == false)
            {
                filteredData = data.Where(x => UnitId.Contains(x.OtherInfo));
            }

            var searchQuery = request.Search.Value;
            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            return DataTablesResponse.Create(request, data.Count(), filteredData.Count(), dataPage);
        }

    }
}
