﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface IWBSBoQGrantTotalService : IService<WBSBoQGrantTotal>
    {
        DataTablesResponse GetWBStotalPerlocation(Guid WbsId, IDataTablesRequest request);
        DataTablesResponse GetTotalPerCurrency(Guid WbsId, IDataTablesRequest request);
        DataTablesResponse GetWBsPerlocation(Guid WbsId, IDataTablesRequest request);
        List<WBSBoQGrantTotalDto> GetWBsPerlocationlist (Guid? WbsId);
        List<WBSTreeJustificationDto> GetWBsPerlocation(Guid WbsId);
    }
}
