﻿using App.Domain.Models.App;
using App.Domain.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface IUserUnitService: IService<UserUnit>
    {
        void AddUserUnit(ApplicationUser user, List<string> unit);
        UserUnit GetUserUnitByIdUserAndLevel(string id, string level);
    }
}
