﻿using App.Domain.Models.App;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IUnitService : IService<Unit>
    {
        List<SelectListItem> GetSelectListItem();
        List<Unit> GetUnitByParameters(string level, string parentId);
        string DeleteChildData(string id);
        List<string> GetChild(string id);
        List<string> GetUnitLevel(string unitId);
        bool ValidateUnit(string id);
    }
}
