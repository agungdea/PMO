﻿using App.Domain.Models.App;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
   public interface IWBSScheduleTempService : IService<WBSScheduleTemp>
    {
        DataTablesResponse GetWBSScheduleTemp(
           Guid Wbstreeid, Guid IdActTmp,
           IDataTablesRequest request, IMapper mapper,
           bool other = false,
           bool preview = false
       );
    }
}
