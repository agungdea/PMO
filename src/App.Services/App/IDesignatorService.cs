﻿using App.Domain.Models.App;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IDesignatorService : IService<Designator>
    {
        bool CheckDuplicate(string id);
        List<Designator> QueryDesignator(string query, int take = 10, int page = 1);
    }
}
