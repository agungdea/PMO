﻿using App.Data.Repository;
using App.Domain.Models.App;
using App.Services.Core;
using Modular.Core.Caching;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Services.App
{
    public class AreaMappingService : CacheService<AreaMapping, Guid, IRepository<AreaMapping>>, IAreaMappingService
    {
        private readonly IWebSettingService _webSettingService;
        private const string STO_ALL = "app.sto.all";
        public AreaMappingService(IRepository<AreaMapping> repository,
            IWebSettingService webSettingService,
            ICacheManager cacheManager)
            : base(repository, cacheManager)
        {
            _webSettingService = webSettingService;
            _cacheKey = STO_ALL;
            _keyItem = x => x.Id.ToString();

            int.TryParse(_webSettingService.GetByName("cache.sto.interval")?.Value, out _cacheInterval);
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;
        }

        public List<AreaMapping> GetListAreaByCodeArea(string codeArea)
        {
            return _repository.GetAll().Where(x => x.AreaKode == codeArea).ToList();
        }

        public List<string> GetRegional(string search)
        {
            var query = _repository
                .GetAll()
                .Select(x => x.AreaRegional)
                .Distinct();

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(x => x.ToLower().Contains(search.ToLower()));
            }

            return query
                .OrderBy(x => x)
                .ToList();
        }

        public List<string> GetWitel(string regional, string search)
        {
            var query = _repository
                .GetAll()
                .Where(x => x.AreaRegional == regional)
                .Select(x => x.AreaWitel)
                .Distinct();

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(x => x.ToLower().Contains(search.ToLower()));
            }

            return query
                .OrderBy(x => x)
                .ToList();
        }

        public List<string> GetDatel(string witel, string search)
        {
            var query = _repository
                .GetAll()
                .Where(x => x.AreaWitel == witel)
                .Select(x => x.AreaDatel)
                .Distinct();

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(x => x.ToLower().Contains(search.ToLower()));
            }

            return query
                .OrderBy(x => x)
                .ToList();
        }

        public List<AreaMapping> GetSTO(string datel, string search)
        {
            var query = _repository
                .GetAll()
                .Where(x => x.AreaDatel == datel);

            if (!string.IsNullOrWhiteSpace(search))
            {
                query = query.Where(x => x.AreaStoName.ToLower().Contains(search.ToLower()));
            }

            return query
                .OrderBy(x => x.AreaStoName)
                .ToList();
        }
    }
}