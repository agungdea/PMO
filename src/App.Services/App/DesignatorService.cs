﻿using App.Data.Repository;
using App.Domain.Models.App;
using App.Services.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Services.App
{
    public class DesignatorService :
        BaseService<Designator, IRepository<Designator>>,
        IDesignatorService
    {
        private readonly IWebSettingService _webSettingService;
        private readonly IHargaService _hargaService;
        private readonly string KodeDesignator;
        private const int KODELENGTH = 6;

        public DesignatorService(IRepository<Designator> repository,
            IWebSettingService webSettingService,
            IHargaService hargaService) 
            : base(repository)
        {
            _webSettingService = webSettingService;
            _hargaService = hargaService;

            KodeDesignator = _webSettingService.GetByName("kode.designator")?.Value ?? "DST-{0}-{1}";
        }

        #region Utils

        private string GenerateCode()
        {
            var year = DateTime.Now.Year.ToString();
            var last = GetLastCode(year);
            var lastCounter = int.Parse(last?.Substring(last.Length - KODELENGTH) ?? "0");
            lastCounter++;
            var codeGenerated = string.Format(KodeDesignator, year, 
                lastCounter.ToString().PadLeft(KODELENGTH, '0'));

            return codeGenerated;
        }

        private string GetLastCode(string year)
        {
            var code = string.Format(KodeDesignator, year, "");
            var max = _repository
                .Table
                .Where(x => x.IdDesignator.StartsWith(code))
                .Max(x => x.IdDesignator);

            return max;
        }

        public bool CheckDuplicate(string id)
        {
            return !(GetByIdDesignator(id) == null);
        }

        #endregion

        public Designator GetByIdDesignator(string id)
        {
            return _repository
                .Table
                .FirstOrDefault(x => x.IdDesignator == id);
        }

        public override Designator GetById(params object[] ids)
        {
            var item = base.GetById(ids);

            item.Hargas = _hargaService.GetByIdDesignator(item.IdDesignator);

            return item;
        }

        public override Designator Add(Designator model)
        {
            if (string.IsNullOrWhiteSpace(model.IdDesignator))
                model.IdDesignator = GenerateCode();

            if (CheckDuplicate(model.IdDesignator))
                throw new Exception("Duplicate Designator Id");

            return base.Add(model);
        }

        public override Designator Delete(Designator model)
        {
            _hargaService.DeleteByIdDesignator(model.IdDesignator);

            return base.Delete(model);
        }

        public List<Designator> QueryDesignator(string query, int take = 10, int page = 1)
        {
            var groups = _repository
                .Table
                .Where(x => x.IdDesignator.ToLower().Contains(query.ToLower()))
                .OrderBy(x => x.IdDesignator)
                .Skip((page - 1) * take)
                .Take(take);

            return groups.ToList();
        }
    }
}
