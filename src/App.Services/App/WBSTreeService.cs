﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Collections;
using System.Linq;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System.Collections.Generic;

namespace App.Services.App
{
    public class WBSTreeService : BaseService<WBSTree, IRepository<WBSTree>>, IWBSTreeService
    {
        private readonly IRepository<AreaMapping> _repositoryLokasi;

        public WBSTreeService(IRepository<WBSTree> repository,
            IRepository<AreaMapping> repositoryLokasi) : base(repository)
        {
            _repositoryLokasi = repositoryLokasi;
        }

        public IEnumerable GetPerentDataWbs(Guid Idwbs)
        {
            var x = (from parent in _repository.Table
                     where parent.ParentId == null && parent.IdWBS.Equals(Idwbs)
                     select new
                     {
                         Children = _repository
                             .Table
                             .Any(c => c.ParentId == parent.Id),
                         icon = parent.Type == WBS.WBS_TYPE_WBS
                             || parent.Type == WBS.WBS_TYPE_WBS_HAS_LOCATION
                             || parent.Type == WBS.WBS_TYPE_WBS_HAS_WBS
                             ? "fa fa-square icon-state-warning icon-lg"
                             : parent.Type == WBS.WBS_TYPE_WP
                                 ? "fa fa-circle icon-state-warning icon-lg"
                                 : "fa fa-map-pin icon-state-success",
                         Id = parent.Id,
                         Text = parent.Name,
                         ParentId = parent.ParentId,
                         Type = parent.Type
                     }).ToList();

            return x.ToList().OrderBy(c => c.Id);

        }
        public IEnumerable GetLocation(Guid Perentid)
        {
            var data = from child in _repository.Table
                       join lokasi in _repositoryLokasi.Table
                           on child.Lokasi equals lokasi.AreaSto + "|" + lokasi.AreaKode into cl
                       from lokasi in cl.DefaultIfEmpty()
                       where child.ParentId.Equals(Perentid)
                       && child.Type == WBS.WBS_TYPE_LOCATION
                       select new
                       {
                           id = child.Id,
                           text = child.Name + (lokasi != null
                            ? $" ({lokasi.AreaRegional} - {lokasi.AreaWitel} - {lokasi.AreaDatel} - {lokasi.AreaStoName})"
                            : "")
                       };
            return data.ToArray().OrderBy(x => x.text);

        }

        public IEnumerable GetchildDataWbs(Guid Perentid)
        {
            var x = from child in _repository.Table
                    join lokasi in _repositoryLokasi.Table
                        on child.Lokasi equals lokasi.AreaSto + "|" + lokasi.AreaKode into cl
                    from lokasi in cl.DefaultIfEmpty()
                    where child.ParentId.Equals(Perentid)
                    select new
                    {
                        children = _repository
                            .Table
                            .Any(c => c.ParentId.Equals(child.Id)),
                        icon = child.Type == WBS.WBS_TYPE_WBS
                            || child.Type == WBS.WBS_TYPE_WBS_HAS_LOCATION
                            || child.Type == WBS.WBS_TYPE_WBS_HAS_WBS
                            ? "fa fa-square icon-state-warning icon-lg"
                            : child.Type == WBS.WBS_TYPE_WP
                                ? "fa fa-circle icon-state-warning icon-lg"
                                : "fa fa-map-pin icon-state-success",
                        id = child.Id,
                        text = child.Name + (lokasi != null
                            ? $" ({lokasi.AreaRegional} - {lokasi.AreaWitel} - {lokasi.AreaDatel} - {lokasi.AreaStoName})"
                            : ""),
                        ParentId = child.ParentId,
                        type = child.Type
                    };

            return x.ToArray().OrderBy(c => c.id);
        }

        public override WBSTree GetById(params object[] ids)
        {
            var item = base.GetById(ids);
            if (item == null)
            {
                return item;
            }

            if (!string.IsNullOrWhiteSpace(item.Lokasi))
            {
                var lokasi = _repositoryLokasi
                    .Table
                    .FirstOrDefault(x => x.AreaSto + "|" + x.AreaKode == item.Lokasi);

                item.AreaMapping = lokasi;
            }

            return item;
        }

        public string GetTreeIcon(string type)
        {
            var icon = "";
            switch (type)
            {
                case WBS.WBS_TYPE_WBS:
                    icon = "fa fa-square icon-state-warning icon-lg";
                    break;
                case WBS.WBS_TYPE_WP:
                    icon = "fa fa-circle icon-state-warning icon-lg";
                    break;
                case WBS.WBS_TYPE_LOCATION:
                    icon = "fa fa-map-pin icon-state-success";
                    break;
            }

            return icon;
        }
        private List<WBSTree> GetDescendants(WBSTree model, int level, int? maxLevel = null)
        {
            if (maxLevel.HasValue && level == maxLevel)
                return null;

            var child = _repository
                .GetAll()
                .Where(x => x.ParentId == model.Id && x.Type != WBS.WBS_TYPE_LOCATION)
                .ToList();

            foreach (var item in child)
            {
                item.Children = GetDescendants(item, level + 1, maxLevel);
            }

            return child;
        }

        private List<Guid> GetDescendantIds(WBSTree model, int level, int? maxLevel = null)
        {
            if (maxLevel.HasValue && level == maxLevel)
                return null;

            var result = new List<Guid>()
            {
                model.Id
            };

            if (model.Children != null && model.Children.Any())
            {
                foreach (var child in model.Children)
                {
                    result.AddRange(GetDescendantIds(child, level + 1, maxLevel));
                }
            }

            return result;
        }

        private List<WBSTree> GetDescendantNamed(WBSTree model, int level, int? maxLevel = null)
        {
            if (maxLevel.HasValue && level == maxLevel)
                return null;

            var result = new List<WBSTree>();
            var prefix = "";
            for (var i = 0; i < level; i++)
            {
                prefix += "—";
            }

            model.Name = string.Format("{0} {1}", prefix, model.Name);

            result.Add(model);

            if (model.Children != null && model.Children.Any())
            {
                foreach (var child in model.Children)
                {
                    foreach (var item in GetDescendantNamed(child, level + 1, maxLevel))
                    {
                        result.Add(item);
                    }
                }
            }

            return result;
        }

        private Dictionary<Guid, string> GetDescendantsName(WBSTree model, int level, int? maxLevel = null)
        {
            if (maxLevel.HasValue && level == maxLevel)
                return null;

            var result = new Dictionary<Guid, string>();
            var prefix = "";
            for (var i = 0; i < level; i++)
            {
                prefix += "—";
            }

            result.Add(model.Id, string.Format("{0} {1}", prefix, model.Name));

            if (model.Children != null && model.Children.Any())
            {
                foreach (var item in model.Children)
                {
                    if (item.Type != WBS.WBS_TYPE_LOCATION)
                    {
                        foreach (var temp in GetDescendantsName(item, level + 1))
                        {
                            result.Add(temp.Key, temp.Value);
                        }
                    }
                }
            }

            return result;
        }
        public IList<WBSTree> GetTree(Guid WBSid, int? maxLevel = null)
        {
            var roots = GetRoot(WBSid);

            foreach (var item in roots)
            {
                item.Children = GetDescendants(item, 0, maxLevel);
            }

            return roots;
        }



        public Dictionary<Guid, string> GetLeveledName(Guid WBSid, int? maxLevel = null)
        {
            var result = new Dictionary<Guid, string>();

            var trees = GetTree(WBSid, maxLevel);

            foreach (var item in trees)
            {
                foreach (var temp in GetDescendantsName(item, 0, maxLevel))
                {
                    result.Add(temp.Key, temp.Value);
                }
            }

            return result;
        }

        public List<Guid> getListguid(Guid? perentsId, List<Guid> data)
        {
   
          var datarepo=  _repository.Table.Where(x => x.ParentId.Equals(perentsId));
            if (datarepo.Count() > 0)
            {
                var datax = datarepo.ToList();
                for (var x = 0; x < datax.Count(); x++)
                {
                    if (datax[x].Type != WBS.WBS_TYPE_LOCATION)
                    {
                        getListguid(datax[x].Id, data);
                    }
                    else {
                        data.Add(datax[x].Id);
                    }
                }
            }
            return data;
        }
      



        public IList<WBSTree> GetRoot(Guid WBSid)
        {
            var roots = _repository
                   .GetAll()
                   .Where(x => !x.ParentId.HasValue && x.IdWBS.Equals(WBSid))
                   .ToList();

            return roots;
        }
    }
}
