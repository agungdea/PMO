﻿using App.Domain.Models.App;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface IJustificationService : IService<Justification>
    {
        void SetToCache(Justification justification);
        Justification GetFromCache(string creator);
        bool DeleteCache(string creator);
        Task<Justification> CreateWf(Justification model);
        Task<Justification> Submit(Guid id, string callbackUrl);
        Task<Justification> Approve(Guid id, Dictionary<string, string> param);
        DataTablesResponse GetDataJustification(List<string> UnitId, IDataTablesRequest request, Boolean IsAdmin, Boolean IsPurchaseRequisition, Boolean IsUser);
    }
}
