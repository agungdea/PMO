﻿using App.Data.Repository;
using App.Domain.Models.App;
using App.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class TransformerPeriodService : BaseService<TransformerPeriod, IRepository<TransformerPeriod>>, ITransformerPeriodService
    {
        public TransformerPeriodService(IRepository<TransformerPeriod> repository) : base(repository)
        {
        }
          public  List<TransformerPeriod> GetExistingTransformerPeriod(String Period) {

            return _repository.GetAll().Where(x => x.Period == Period).ToList();

        }
    }
}
