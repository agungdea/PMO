﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using Modular.Core.Caching;
using App.Services.Core;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace App.Services.App
{
    public class AkiCoverService : CacheService<AkiCover, Guid, IRepository<AkiCover>>, IAkiCoverService
    {
        private readonly IWebSettingService _webSettingService;
        private const string AKICOVER_ALL = "app.akicover.all";
        public AkiCoverService(IRepository<AkiCover> repository,
            IWebSettingService webSettingService,
            ICacheManager cacheManager)
            : base(repository, cacheManager)
        {
            _webSettingService = webSettingService;
            _cacheKey = AKICOVER_ALL;
            _keyItem = x => x.Id.ToString();

            int.TryParse(_webSettingService.GetByName("cache.akicover.interval")?.Value, out _cacheInterval);
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;
        }

        public AkiCover GetAkiCoverByIdAki(Guid id)
        {
            return _repository.Table.FirstOrDefault(x => x.AkiId == id);
        }

        public AkiCover GetAllAboutAkiByIdAki(Guid id)
        {
            return _repository.Table.Include(x => x.Akis).Include(x => x.Akis.AkiValuations).FirstOrDefault(x => x.AkiId == id);
        }
    }
}
