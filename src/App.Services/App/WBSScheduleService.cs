﻿using App.Data.Repository;
using App.Domain.DTO.App;
using App.Domain.Models.App;
using App.Services.Utils;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class WBSScheduleService : BaseService<WBSSchedule, IRepository<WBSSchedule>>, IWBSScheduleService
    {
        private readonly IActivityTemplateChildService _activityTemplateChildService;
        private readonly IRepository<ActTmpChild> _RepositoryActivityTemplatechild;
        private readonly IRepository<ActTemplate> _RepositoryActivityTemplate;
        private readonly IRepository<WBSSchedulePlanning> _RepositorWBSSchedulePlanning;

        public WBSScheduleService(
            IRepository<WBSSchedule> repository,
            IActivityTemplateChildService activityTemplateChildService,
             IRepository<ActTmpChild> RepositoryActivityTemplatechild,
             IRepository<ActTemplate> RepositoryActivityTemplate,
             IRepository<WBSSchedulePlanning> RepositorWBSSchedulePlanning

            ) : base(repository)
        {
            _RepositoryActivityTemplatechild = RepositoryActivityTemplatechild;
            _activityTemplateChildService = activityTemplateChildService;
            _RepositoryActivityTemplate = RepositoryActivityTemplate;
            _RepositorWBSSchedulePlanning = RepositorWBSSchedulePlanning;
        }


        public Dictionary<DateTime, Decimal?> getCurvaS(Guid id)
        {
            var dic = new Dictionary<DateTime, Decimal?>();
            var mindate = this.GetAll().Where(x => x.IdWBSTree.Equals(id)).Select(x => x.Start).Min();
            var maxdate = this.GetAll().Where(x => x.IdWBSTree.Equals(id)).Select(x => x.Finish).Max();
            var totalday = (maxdate - mindate).TotalDays + 1;
            for (var x = 0; x < totalday; x++)
            {
                var data2 = from wbsschedule in this.GetAll().Where(z => z.Finish >= mindate.AddDays(x) && z.Start <= mindate.AddDays(x))
                            join wbstemplatechild in _activityTemplateChildService.GetAll() on wbsschedule.IdActTmpChild equals wbstemplatechild.Id
                            where wbsschedule.IdWBSTree.Equals(id)
                            select new
                            {
                                id = wbsschedule.Id,
                                date = mindate.AddDays(x),
                                value = wbstemplatechild.Bobot / (Decimal.Parse(((wbsschedule.Finish - wbsschedule.Start).TotalDays.ToString())) + 1)
                            };
                var datasum = from datatotal in data2
                              group
                               datatotal by new
                               {
                                   datatotal.date

                               } into g
                              select new
                              {
                                  Total = g.Sum(y => y.value),
                                  currency = g.Key.date
                              };
                    dic.Add(mindate.AddDays(x), datasum.First().Total);
               

            }
            return dic;
        }

        public DataTablesResponse GetWBSScheduleTemp(
                Guid Wbstreeid, Guid IdActTmp,
                IDataTablesRequest request
            )
        {

            var schedule = from tempchild in _RepositoryActivityTemplatechild.Table.Where(c => c.ActParent.HasValue || c.Bobot.HasValue)

                           select new WBSScheduleTempDto
                           {
                               Id = tempchild.Id,
                               ActId = tempchild.ActId,
                               ActParent = tempchild.ActParent,
                               NamaAct = tempchild.NamaAct,
                               Bobot = tempchild.Bobot,
                               DurasiReal = _repository.Table.Where(u => u.ActTmpChild.Equals(tempchild.Id) && u.IdWBSTree.Equals(Wbstreeid)).Count() == 0 ? tempchild.Durasi : _repository.Table.Where(u => u.ActTmpChild.Equals(tempchild.Id) && u.IdWBSTree.Equals(Wbstreeid)).First().DurasiReal,
                               Durasi = tempchild.Durasi,
                               Predecessor = tempchild.Predecessor,
                               ISLEAF = tempchild.ISLEAF,
                               IdActTmp = tempchild.IdActTmp,
                               OtherInfo = tempchild.ActParent.HasValue == true ? _RepositoryActivityTemplatechild.Table.Where(u => u.ActId.Equals(tempchild.ActParent)).First().NamaAct : "",
                               Start = _repository.Table.Where(u => u.ActTmpChild.Equals(tempchild.Id) && u.IdWBSTree.Equals(Wbstreeid)).Count() == 0 ? "" : string.Format("{0:MM/dd/yyyy}", _repository.Table.Where(u => u.ActTmpChild.Equals(tempchild.Id) && u.IdWBSTree.Equals(Wbstreeid)).First().Start),
                               Finish = _repository.Table.Where(u => u.ActTmpChild.Equals(tempchild.Id) && u.IdWBSTree.Equals(Wbstreeid)).Count() == 0 ? "" : string.Format("{0:MM/dd/yyyy}", _repository.Table.Where(u => u.ActTmpChild.Equals(tempchild.Id) && u.IdWBSTree.Equals(Wbstreeid)).First().Finish),
                               Urutan = tempchild.Urutan
                           };

            schedule = schedule.Where(x => x.IdActTmp == IdActTmp).OrderBy(x => x.Urutan);

            var searchQuery = request.Search.Value;

            var filteredData = schedule;

            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            return DataTablesResponse.Create(request, schedule.Count(), filteredData.Count(), dataPage);

        }

        public DataTablesResponse GetWBSScheduleById(Guid Wbstreeid, IDataTablesRequest request)
        {
            var data = from tempData in _repository.Table.Include(x => x.ActTmpChild).Where(c => c.IdWBSTree == Wbstreeid).OrderBy(x => x.ActTmpChild.Urutan)

                       select new WBSScheduleTempDto
                       {
                           Id = tempData.Id,
                           ActId = tempData.ActTmpChild.ActId,
                           ActParent = tempData.ActTmpChild.ActParent,
                           NamaAct = tempData.ActTmpChild.NamaAct,
                           Bobot = tempData.ActTmpChild.Bobot,
                           DurasiReal = tempData.DurasiReal,
                           Predecessor = tempData.ActTmpChild.Predecessor,
                           ISLEAF = tempData.ActTmpChild.ISLEAF,
                           IdActTmp = tempData.ActTmpChild.IdActTmp,
                           OtherInfo = tempData.ActTmpChild.ActParent.HasValue == true ? _RepositoryActivityTemplatechild.Table.Where(u => u.ActId.Equals(tempData.ActTmpChild.ActParent)).First().NamaAct : "",
                           Start = tempData.Start.ToString(),
                           Finish = tempData.Finish.ToString(),

                       };

            var searchQuery = request.Search.Value;

            var filteredData = data;

            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            return DataTablesResponse.Create(request, data.Count(), filteredData.Count(), dataPage);
        }
    }
}
