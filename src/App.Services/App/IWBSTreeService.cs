﻿using App.Domain.Models.App;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace App.Services.App
{
    public interface IWBSTreeService : IService<WBSTree>
    {
        IEnumerable GetPerentDataWbs(Guid idwbs);
        IEnumerable GetchildDataWbs(Guid Perentid);
        IEnumerable GetLocation(Guid Perentid);
        List<Guid> getListguid(Guid? perentsId, List<Guid> data = null);
        Dictionary<Guid, string> GetLeveledName(Guid WBSid, int? maxLevel = null);
    }
}
