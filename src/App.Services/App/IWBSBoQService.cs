﻿using App.Domain.DTO.App;
using App.Domain.Models.App;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;

namespace App.Services.App
{
    public interface IWBSBoQService : IService<WBSBoQ>
    {
        WBSBoQ AddOrUpdate(WBSBoQ item);
        bool Exist(Guid idWBSTree, string designator, string grupharga);
        List<WBSBoQDto> GetBoQWithPaket(Guid idWBSTree);
        List<WBSBoQ> getdtafromlist(List<Guid> data);
        DataTablesResponse GetBoQs(Guid idWBSTree,
            IDataTablesRequest request,
            IMapper mapper,
            bool other = false,
            bool preview = false);
        DataTablesResponse GetBoqPerWbs(Guid WbsId, IDataTablesRequest request);
        List<WBSBoQJustificationDto> GetBoqPerWbs(Guid WbsId);
        bool CheckStatusBoq(Guid IdWBSTree);
    }
}
