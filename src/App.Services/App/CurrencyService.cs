﻿using App.Data.Repository;
using App.Domain.Models.App;

namespace App.Services.App
{
    public class CurrencyService : BaseService<Currency, IRepository<Currency>>, ICurrencyService
    {
        public CurrencyService(IRepository<Currency> repository) : base(repository)
        {
        }
    }
}
