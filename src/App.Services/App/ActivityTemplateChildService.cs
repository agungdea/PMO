﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class ActivityTemplateChildService : BaseService<ActTmpChild, IRepository<ActTmpChild>>, IActivityTemplateChildService
    {
        public ActivityTemplateChildService(IRepository<ActTmpChild> repository) : base(repository)
        {
        }

        public int GenerateActivityId()
        {
            int Id = _repository.Table.Max(x => x.ActId) + 1;

            return Id;
        }

        public int GetActivityId(string activityName)
        {
            return _repository.Table.Where(x => x.NamaAct.ToUpper() == activityName.ToUpper()).Select(x => x.ActId).FirstOrDefault();
        }

        public List<string> QueryActivity(string query, int take = 10, int page = 1)
        {
            var activity = _repository
                .Table
                .Where(x => x.NamaAct.ToLower().Contains(query.ToLower()))
                .Select(x => x.NamaAct)
                .Distinct()
                .OrderBy(x => x)
                .Skip((page - 1) * take)
                .Take(take);

            return activity.ToList();
        }
    }
}
