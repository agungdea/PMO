﻿using App.Data.Repository;
using App.Domain.Models.App;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class WBStreeCategoryService : BaseService<WBStreeCategory, IRepository<WBStreeCategory>>, IWBStreeCategoryService
    {
        private readonly ICategoryService _categoryservice;
        public WBStreeCategoryService(
            IRepository<WBStreeCategory> repository,
            ICategoryService categoryservice
            ) : base(repository)
        {
            _categoryservice = categoryservice;
        }

        public bool CheckExistWBSTreeCategory(int categoryId)
        {
            var result = _repository.Table.FirstOrDefault(x => x.CategoryId == categoryId);

            if (result == null)
            {
                return false;
            }

            return true;
        }
        public int? getCategoryById(Guid WBSId)
        {
            var result = _repository.Table.Where(x => x.WBStreeId == WBSId).Select(x => x.CategoryId).FirstOrDefault();

            if (result.ToString() == null)
            {
                return null;
            }

            return result;
        }
        public IEnumerable GetChildData(Guid WBSTreeId, int? parentid)
        {
            string statestrue = "{\"selected\":true}";
            string statesfalse = "{\"selected\":false}";

            if (!parentid.HasValue)
            {
                var x = from servicecategory in _categoryservice.GetAll()
                        where servicecategory.ParentId.Equals(null)
                        select new
                        {
                            children = _categoryservice.GetAll().Where(c => c.ParentId.Equals(servicecategory.Id)).Count() == 0 ? false : true,
                            icon = "fa fa-folder icon-state-warning icon-lg",
                            id = servicecategory.Id.ToString(),
                            text = servicecategory.Name,
                            type = "root",
                            ParentId = servicecategory.ParentId,
                            state = this.GetAll().Where(c => c.WBStreeId.Equals(WBSTreeId) && c.CategoryId.Equals(servicecategory.Id)).Count() == 0
                            ? JsonConvert.DeserializeObject<dynamic>(statesfalse) : JsonConvert.DeserializeObject<dynamic>(statestrue)
                        };

                return x.ToArray().OrderBy(c => c.id);
            }
            else
            {
                var x = from servicecategory in _categoryservice.GetAll()
                        where servicecategory.ParentId.Equals(parentid)
                        select new
                        {
                            children = _categoryservice.GetAll().Where(c => c.ParentId.Equals(servicecategory.Id)).Count() == 0 ? false : true,
                            icon = _categoryservice.GetAll().Where(c => c.ParentId.Equals(servicecategory.Id)).Count() == 0 ? "fa fa-file icon-state-success" : "fa fa-folder icon-state-warning icon-lg",
                            id = servicecategory.Id.ToString(),
                            text = servicecategory.Name,
                            ParentId = servicecategory.ParentId,
                            state = this.GetAll().Where(c => c.WBStreeId.Equals(WBSTreeId) && c.CategoryId.Equals(servicecategory.Id)).Count() == 0
                            ? JsonConvert.DeserializeObject<dynamic>(statesfalse) : JsonConvert.DeserializeObject<dynamic>(statestrue)
                        };

                return x.ToArray().OrderBy(c => c.id);

            }

        }


        public WBStreeCategory GetByWBSIdAndCategoryId(Guid WBSId, int categoryId)
        {
            return _repository.Table.Where(x => x.WBStreeId == WBSId && x.CategoryId == categoryId).FirstOrDefault();
        }

        public List<int> GetCategoryIdByWBSId(Guid WBSId)
        {
            List<int> temp = new List<int>();

            var categoryId = _repository.Table.Where(x => x.WBStreeId == WBSId).Select(x => x.CategoryId).ToList();

            if (categoryId != null)
            {
                foreach (var item in categoryId)
                {
                    temp.Add(item);
                }
            }

            return temp;
        }
    }
}
