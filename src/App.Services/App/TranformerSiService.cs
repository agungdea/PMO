﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class TranformerSiService : BaseService<TransformerSi, IRepository<TransformerSi>>, ITranformerSiService
    {
        public TranformerSiService(IRepository<TransformerSi> repository) : base(repository)
        {
        }
    }
}
