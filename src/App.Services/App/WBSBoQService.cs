﻿using App.Data.Repository;
using App.Domain.DTO.App;
using App.Domain.Models.App;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using App.Services.Utils;
using System.Linq.Dynamic.Core;

namespace App.Services.App
{
    public class WBSBoQService : BaseService<WBSBoQ, IRepository<WBSBoQ>>, IWBSBoQService
    {
        private readonly IRepository<Designator> _designatorRepository;
        private readonly IRepository<Harga> _hargaRepository;
        private readonly IWBSTreeService _wbsTreeService;
        private readonly IRepository<WBSTree> _wbstreeRepository;
        private readonly IRepository<WBSBoQGrantTotal> _grantTotalRepository;
        private readonly IRepository<Currency> _currencyRepository;
        private readonly IRepository<WBSCurrency> _wbscurrencyRepository;
        public WBSBoQService(IRepository<WBSBoQ> repository,
            IRepository<Designator> designatorRepository,
            IRepository<Harga> hargaRepository,
            IRepository<WBSBoQGrantTotal> grantTotalRepository,
            IRepository<WBSTree> wbstreeRepository,
            IRepository<Currency> currencyRepository,
            IRepository<WBSCurrency> wbscurrencyRepository,
            IWBSTreeService wbsTreeService)
            : base(repository)
        {
            _designatorRepository = designatorRepository;
            _hargaRepository = hargaRepository;
            _grantTotalRepository = grantTotalRepository;
            _currencyRepository = currencyRepository;
            _wbsTreeService = wbsTreeService;
            _wbstreeRepository = wbstreeRepository;
            _wbscurrencyRepository = wbscurrencyRepository;
        }

        private Boolean SumGrandTotal(WBSBoQ boq)
        {
            if (!string.IsNullOrWhiteSpace(boq.OtherInfo))
            {
                if (_currencyRepository.Table.Where(x => x.CurrencyValue.ToLower().Equals(boq.OtherInfo.ToLower())).Count() > 0)
                {
                    var data = _currencyRepository.Table.First(x => x.CurrencyValue.ToLower().Equals(boq.OtherInfo.ToLower()));
                    if (data == null)
                    {
                        return false;
                    }
                    if (data.CurrencyValue == Currency.CurrencyRp)
                    {
                        var totalharga = _repository.Table.Where(x => x.IdWBSTree.Equals(boq.IdWBSTree) && x.OtherInfo.Equals(data.CurrencyValue)).Sum(x => (x.HargaJasa + x.HargaMaterial) * x.Qty);
                        var datagranttotal = new WBSBoQGrantTotal()
                        {
                            CurrencyId = data.Id,
                            IdWBStree = boq.IdWBSTree,
                            BoQTotalConversion = totalharga,
                            BoQBeforeConverstion = totalharga,
                        };
                        var dataTotalService = _grantTotalRepository.Table.Where(x => x.CurrencyId.Equals(datagranttotal.CurrencyId) && x.IdWBStree.Equals(datagranttotal.IdWBStree));
                        if (dataTotalService.Count() > 0)
                        {
                            var UpadatedataGrantTotalService = _grantTotalRepository.Table.Where(x => x.CurrencyId.Equals(datagranttotal.CurrencyId) && x.IdWBStree.Equals(datagranttotal.IdWBStree)).FirstOrDefault();

                            if (totalharga == 0)
                            {
                                _grantTotalRepository.Delete(UpadatedataGrantTotalService);
                            }
                            else
                            {
                                UpadatedataGrantTotalService.BoQTotalConversion = totalharga;
                                UpadatedataGrantTotalService.BoQBeforeConverstion = totalharga;
                                _grantTotalRepository.Update(UpadatedataGrantTotalService);
                            }

                        }
                        else
                        {
                            _grantTotalRepository.Add(datagranttotal);
                        }

                    }
                    else
                    {
                        var dataparentcurrency = _wbsTreeService.GetById(boq.IdWBSTree);
                        if (dataparentcurrency == null)
                        {

                            return false;
                        }
                        var listdatawbscurrency = _wbscurrencyRepository.Table.Where(x => x.CurrencyId == data.Id && x.WBSId == dataparentcurrency.IdWBS);
                        if (listdatawbscurrency!= null)
                        {
                            var datawbscurrency = listdatawbscurrency.FirstOrDefault();
                            var totalbeforeharga = _repository.Table.Where(x => x.IdWBSTree.Equals(boq.IdWBSTree) && x.OtherInfo.Equals(data.CurrencyValue)).Sum(x => (x.HargaJasa + x.HargaMaterial) * x.Qty);
                            var totalharga = totalbeforeharga * datawbscurrency.Rate;
                            var datagranttotal = new WBSBoQGrantTotal()
                            {
                                CurrencyId = data.Id,
                                IdWBStree = boq.IdWBSTree,
                                BoQBeforeConverstion = totalbeforeharga,
                                BoQTotalConversion = totalharga
                            };
                            var dataTotalService = _grantTotalRepository.Table.Where(x => x.CurrencyId.Equals(datagranttotal.CurrencyId) && x.IdWBStree.Equals(datagranttotal.IdWBStree));
                            if (dataTotalService.Count() > 0)
                            {
                                var UpadatedataGrantTotalService = _grantTotalRepository.Table.Where(x => x.CurrencyId.Equals(datagranttotal.CurrencyId) && x.IdWBStree.Equals(datagranttotal.IdWBStree)).FirstOrDefault();

                                if (totalbeforeharga == 0)
                                {
                                    _grantTotalRepository.Delete(UpadatedataGrantTotalService);
                                }
                                else
                                {
                                    UpadatedataGrantTotalService.BoQTotalConversion = totalharga;
                                    UpadatedataGrantTotalService.BoQBeforeConverstion = totalbeforeharga;
                                    _grantTotalRepository.Update(UpadatedataGrantTotalService);
                                }
                            }
                            else
                            {
                                _grantTotalRepository.Add(datagranttotal);
                            }
                        }

                    }
                }
                return true;
            }
            return false;
        }
        public DataTablesResponse GetBoqPerWbs(Guid WbsId, IDataTablesRequest request)
        {
            var ListBoq = new List<WBSBoQDto>();
            var Wbstreeid = from wbstree in _wbstreeRepository.Table.Where(x => x.IdWBS.Equals(WbsId) && x.Type.Equals(WBS.WBS_TYPE_LOCATION))
                            join Boq in _repository.Table on wbstree.Id equals Boq.IdWBSTree
                            join Designatorr in _designatorRepository.Table on Boq.IdDesignator equals Designatorr.IdDesignator
                            select new
                            {
                                WbstreeId = wbstree.Id,
                                BoqId = Boq.Id,
                                LocationName = wbstree.Name,
                                DesignatorDeskripsi = Designatorr.Deskripsi,
                                DesignatorId = Designatorr.Id,
                                Currencies = Boq.OtherInfo,
                                Boq.HargaJasa,
                                Boq.HargaMaterial,
                                Boq.Qty,
                                Boq.GrupHarga,
                                Jumlah = (Boq.HargaMaterial + Boq.HargaJasa) * Boq.Qty
                            };

            var filteredData = Wbstreeid;

            var searchQuery = request.Search.Value;
            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);
            return DataTablesResponse.Create(request, filteredData.Count(), filteredData.Count(), dataPage);


        }

        public List<WBSBoQJustificationDto> GetBoqPerWbs(Guid WbsId)
        {
            var ListBoq = new List<WBSBoQJustificationDto>();
            var Wbstreeid = from wbstree in _wbstreeRepository.Table.Where(x => x.IdWBS.Equals(WbsId) && x.Type.Equals(WBS.WBS_TYPE_LOCATION))
                            join Boq in _repository.Table on wbstree.Id equals Boq.IdWBSTree
                            join Designatorr in _designatorRepository.Table on Boq.IdDesignator equals Designatorr.IdDesignator
                            select new WBSBoQJustificationDto
                            {
                                WbstreeId = wbstree.Id,
                                BoqId = Boq.Id,
                                LocationName = wbstree.Name,
                                DesignatorDeskripsi = Designatorr.Deskripsi,
                                DesignatorId = Designatorr.Id,
                                Currencies = Boq.OtherInfo,
                                HargaJasa = Boq.HargaJasa,
                                HargaMaterial = Boq.HargaMaterial,
                                Qty = Boq.Qty,
                                GrupHarga = Boq.GrupHarga,
                                Jumlah = (Boq.HargaMaterial + Boq.HargaJasa) * Boq.Qty
                            };
            return Wbstreeid.ToList();
        }

        public WBSBoQ AddOrUpdate(WBSBoQ item)
        {
            var boq = item;

            if (Exist(item.IdWBSTree, item.IdDesignator, item.GrupHarga))
            {
                boq = _repository
                    .Table
                    .First(x => x.IdWBSTree == item.IdWBSTree
                        && x.IdDesignator == item.IdDesignator
                        && x.GrupHarga == item.GrupHarga);
                boq.Qty = item.Qty;
                _repository.Update(boq);
            }
            else
            {
                _repository.Add(boq);
            }
            SumGrandTotal(item);


            return boq;
        }



        public override WBSBoQ Delete(WBSBoQ model)
        {
            if (Exist(model.IdWBSTree, model.IdDesignator, model.GrupHarga))
            {
                var item = _repository
                    .Table
                    .First(x => x.IdWBSTree == model.IdWBSTree
                        && x.IdDesignator == model.IdDesignator
                        && x.GrupHarga == model.GrupHarga);

                _repository.Delete(item);

                SumGrandTotal(model);
                return item;
            }

            return null;
        }

        public bool Exist(Guid idWBSTree, string designator, string grupharga)
        {
            return _repository
                .Table
                .Any(x => x.IdWBSTree == idWBSTree
                    && x.IdDesignator == designator
                    && x.GrupHarga == grupharga);
        }

        public List<WBSBoQDto> GetBoQWithPaket(Guid idWBSTree)
        {
            var wbsTree = _wbsTreeService.GetById(idWBSTree);

            if (wbsTree == null)
                return null;

            var boqs = from harga in _hargaRepository.Table
                       join designator in _designatorRepository.Table
                            on harga.IdDesignator equals designator.IdDesignator
                       join boq in _repository.Table
                            on new { harga.IdDesignator, Grup = harga.Lokasi, Id = idWBSTree } equals
                               new { boq.IdDesignator, Grup = boq.GrupHarga, Id = boq.IdWBSTree } into boqHarga
                       from boq in boqHarga.DefaultIfEmpty()
                       where harga.Lokasi == wbsTree.GrupHarga
                            || (harga.Lokasi != wbsTree.GrupHarga && boq != null)
                       select new WBSBoQDto
                       {
                           IdDesignator = harga.IdDesignator,
                           GrupHarga = harga.Lokasi,
                           Deskripsi = designator.Deskripsi,
                           Satuan = designator.Satuan,
                           MataUang = harga.MataUang,
                           HargaMaterial = harga.HargaMaterial,
                           HargaJasa = harga.HargaJasa,
                           IdWBSTree = idWBSTree,
                           Qty = boq == null ? (int?)null : boq.Qty
                       };

            return boqs.ToList();
        }
        public List<WBSBoQ> getdtafromlist(List<Guid> data)
        {
            var datalistboq = new List<WBSBoQ>();
            if (data.Count() > 0)
            {
                for (var x = 0; x < data.Count(); x++)
                {
                    var databoq = _repository.Table.Where(d => d.IdWBSTree.Equals(data[x]));
                    foreach (var databoqtmp in databoq)
                    {
                        datalistboq.Add(databoqtmp);
                    }
                }
            }
            return datalistboq;
        }
        public DataTablesResponse GetBoQs(Guid idWBSTree,
            IDataTablesRequest request,
            IMapper mapper,
            bool other = false,
            bool preview = false)
        {
            var wbsTree = _wbsTreeService.GetById(idWBSTree);

            if (wbsTree == null)
                return null;

            var boqs = from harga in _hargaRepository.Table
                       join designator in _designatorRepository.Table
                            on harga.IdDesignator equals designator.IdDesignator
                       join boq in _repository.Table
                            on new { harga.IdDesignator, Grup = harga.Lokasi, Id = idWBSTree } equals
                               new { boq.IdDesignator, Grup = boq.GrupHarga, Id = boq.IdWBSTree } into boqHarga
                       from boq in boqHarga.DefaultIfEmpty()
                       select new WBSBoQDto
                       {
                           IdDesignator = harga.IdDesignator,
                           GrupHarga = harga.Lokasi,
                           Deskripsi = designator.Deskripsi,
                           Satuan = designator.Satuan,
                           MataUang = harga.MataUang,
                           HargaMaterial = harga.HargaMaterial,
                           HargaJasa = harga.HargaJasa,
                           IdWBSTree = idWBSTree,
                           TotalItemBoq = boq == null ? 0 : (harga.HargaJasa + harga.HargaMaterial) * boq.Qty,
                           Qty = boq == null ? (int?)null : boq.Qty

                       };

            if (other)
            {
                boqs = boqs.Where(x => x.GrupHarga != wbsTree.GrupHarga && x.Qty.HasValue);
            }
            else if (preview)
            {
                boqs = boqs.Where(x => x.GrupHarga == wbsTree.GrupHarga && x.Qty.HasValue);
            }
            else
            {
                boqs = boqs.Where(x => x.GrupHarga == wbsTree.GrupHarga);
            }

            return GetDataTablesResponseByQuery<WBSBoQDto>(request, mapper, boqs);
        }

        public bool CheckStatusBoq(Guid IdWBSTree)
        {
            var result = _repository.Table.Where(x => x.IdWBSTree == IdWBSTree).FirstOrDefault();

            if (result != null)
            {
                return true;
            }

            return false;
        }
    }
}
