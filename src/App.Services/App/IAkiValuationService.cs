﻿using App.Domain.Models.App;
using System;

namespace App.Services.App
{
    public interface IAkiValuationService : ICacheService<AkiValuation, Guid>
    {
    }
}
