﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public class WBSSchedulePlanningService : BaseService<WBSSchedulePlanning, IRepository<WBSSchedulePlanning>>, IWBSSchedulePlanningService
    {
        private readonly IActivityTemplateChildService _activityTemplateChildService;
        public WBSSchedulePlanningService(IRepository<WBSSchedulePlanning> repository,
            IActivityTemplateChildService activityTemplateChildService) : base(repository)
        {
            _activityTemplateChildService = activityTemplateChildService;
        }

        public Dictionary<DateTime, decimal?> getCurvaSPlanning(Guid id)
        {
            var dic = new Dictionary<DateTime, Decimal?>();
            var mindate = this.GetAll().Where(x => x.IdWBSTree.Equals(id)).Select(x => x.Start).Min();
            var maxdate = this.GetAll().Where(x => x.IdWBSTree.Equals(id)).Select(x => x.Finish).Max();
            var totalday = (maxdate - mindate).TotalDays + 1;
            for (var x = 0; x < totalday; x++)
            {
                var data2 = from wbsschedule in this.GetAll().Where(z => z.Finish >= mindate.AddDays(x) && z.Start <= mindate.AddDays(x))
                            join wbstemplatechild in _activityTemplateChildService.GetAll() on wbsschedule.IdActTmpChild equals wbstemplatechild.Id
                            where wbsschedule.IdWBSTree.Equals(id)
                            select new
                            {
                                id = wbsschedule.Id,
                                date = mindate.AddDays(x),
                                value = wbstemplatechild.Bobot / (Decimal.Parse(((wbsschedule.Finish - wbsschedule.Start).TotalDays.ToString())) + 1)
                            };
                var datasum = from datatotal in data2
                              group
                               datatotal by new
                               {
                                   datatotal.date

                               } into g
                              select new
                              {
                                  Total = g.Sum(y => y.value),
                                  currency = g.Key.date
                              };
                if (datasum.Count() != 0)
                {
                    dic.Add(mindate.AddDays(x), datasum.First().Total);
                }

            }
            return dic;
        }
    }
}
