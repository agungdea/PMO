﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Modular.Core.Caching;
using App.Services.Core;
using System.Collections;
using App.Domain.DTO.App;
using Newtonsoft.Json;

namespace App.Services.App
{
    public class CategoryService : SoftDeleteBaseService<Category, IRepository<Category>>, ICategoryService
    {
        private readonly IRepository<WBStreeCategory> _categoryrepository;
        public CategoryService(IRepository<Category> repository,
            IRepository<WBStreeCategory> categoryrepository
            ) : base(repository)
        {
            _categoryrepository = categoryrepository;
        }

        public int DeleteChildData(int id)
        {
            var getbyid = this.GetAll().Where(x => x.ParentId == id);
            if (getbyid.Count() > 0)
            {
                foreach (var data in getbyid)
                {
                    this.DeleteChildData(data.Id);
                    this.Delete(data);
                }
            }
            return id;
        }
        public IEnumerable GetAlldata(Guid Id)
        {
            string statestrue = "{\"selected\":true}";
            string statesfalse = "{\"selected\":false}";

            var listCategory = new List<DataCategoryDto>();
            var x = this.GetAll().Where(y => !y.ParentId.HasValue);
            foreach (var data in x)
            {
                var listCategorychild = new List<DataCategoryDto>();
                var datacategory = new DataCategoryDto();
                var child = getChildAll( Id, data.Id, listCategorychild);
                datacategory.id = data.Id.ToString();
                datacategory.ParentId = data.ParentId.ToString();
                datacategory.type = "root";
                datacategory.icon = "fa fa-folder icon-state-warning icon-lg";
                datacategory.text = data.Name;
                datacategory.children = child;
                datacategory.state = _categoryrepository.Table.Where(c => c.WBStreeId.Equals(Id) && c.CategoryId.Equals(data.Id)).Count() == 0
                            ? JsonConvert.DeserializeObject<dynamic>(statesfalse) : JsonConvert.DeserializeObject<dynamic>(statestrue);
                listCategory.Add(datacategory);
            }
            return listCategory;
        }

        private List<DataCategoryDto> getChildAll(Guid Id,int id, List<DataCategoryDto> listCategory)
        {
            string statestrue = "{\"selected\":true}";
            string statesfalse = "{\"selected\":false}";
            var x = this.GetAll().Where(y => y.ParentId.Equals(id));

            foreach (var data in x)
            {
                List<DataCategoryDto> newCategory = new List<DataCategoryDto>();
                var datacategory = new DataCategoryDto();
                var child = getChildAll( Id, data.Id, newCategory);
                datacategory.id = data.Id.ToString();
                datacategory.ParentId = data.ParentId.ToString();
                datacategory.type = "child";
                datacategory.icon = datacategory.icon = this.GetAll().Where(c => c.ParentId.Equals(data.Id)).Count() == 0 ? "fa fa-file icon-state-success" : "fa fa-folder icon-state-warning icon-lg";
                datacategory.text = data.Name;
                datacategory.children = child;
                datacategory.state = _categoryrepository.Table.Where(c => c.WBStreeId.Equals(Id) && c.CategoryId.Equals(data.Id)).Count() == 0
                           ? JsonConvert.DeserializeObject<dynamic>(statesfalse) : JsonConvert.DeserializeObject<dynamic>(statestrue);

                listCategory.Add(datacategory);


            }
            return listCategory;
        }

        public IEnumerable GetChildData(int? parentid)
        {

            if (!parentid.HasValue)
            {
                var x = from servicecategory in this.GetAll()
                        where servicecategory.ParentId.Equals(null)
                        select new
                        {
                            children = this.GetAll().Where(c => c.ParentId.Equals(servicecategory.Id)).Count() == 0 ? false : true,
                            icon = "fa fa-folder icon-state-warning icon-lg",
                            id = servicecategory.Id.ToString(),
                            text = servicecategory.Name,
                            type = "root",
                            ParentId = servicecategory.ParentId
                        };

                return x.ToArray().OrderBy(c => c.id);
            }
            else
            {
                var x = from servicecategory in this.GetAll()
                        where servicecategory.ParentId.Equals(parentid)
                        select new
                        {
                            children = this.GetAll().Where(c => c.ParentId.Equals(servicecategory.Id)).Count() == 0 ? false : true,
                            icon = this.GetAll().Where(c => c.ParentId.Equals(servicecategory.Id)).Count() == 0 ? "fa fa-file icon-state-success" : "fa fa-folder icon-state-warning icon-lg",
                            id = servicecategory.Id.ToString(),
                            text = servicecategory.Name,
                            ParentId = servicecategory.ParentId
                        };

                return x.ToArray().OrderBy(c => c.id);

            }

        }
    }
}
