﻿using App.Domain.Models.App;
using System;

namespace App.Services.App
{
    public interface IWBSCurrencyService : IService<WBSCurrency>
    {
        bool CekBoq(Guid WbsTreeId, string matauang);
    }
}
