﻿using App.Domain.Models.App;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.App
{
    public interface IWBStreeCategoryService : IService<WBStreeCategory>
    {
        IEnumerable GetChildData(Guid WBSTreeId, int? parentid);
        List<int> GetCategoryIdByWBSId(Guid WBSId);
        bool CheckExistWBSTreeCategory(int categoryId);

        WBStreeCategory GetByWBSIdAndCategoryId(Guid WBSId, int categoryId);
        int? getCategoryById(Guid WBSId);
    }
}
