﻿using App.Data.Repository;
using App.Domain.Models.App;
using System;
using Modular.Core.Caching;
using App.Services.Core;

namespace App.Services.App
{
    public class AkiService : CacheService<Aki, Guid, IRepository<Aki>>, IAkiService
    {
        private readonly IWebSettingService _webSettingService;
        private const string AKI_ALL = "app.aki.all";
        private readonly string _jogetProcessDefId;
        public AkiService(IRepository<Aki> repository,
            IWebSettingService webSettingService,
            ICacheManager cacheManager)
            : base(repository, cacheManager)
        {
            _webSettingService = webSettingService;
            _cacheKey = AKI_ALL;
            _keyItem = x => x.Id.ToString();

            int.TryParse(_webSettingService.GetByName("cache.aki.interval")?.Value, out _cacheInterval);
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;
            _jogetProcessDefId = _webSettingService.GetByName("joget.wf.justification.process.def.id")?.Value;
        }


    }
}
