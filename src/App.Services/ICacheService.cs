﻿using App.Domain;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace App.Services
{
    public interface ICacheService<T, TKey> : IService<T>
        where T : class, ICacheEntity<TKey>
    {
        List<Expression<Func<T, object>>> Includes { get; set; }
        Dictionary<string, T> GetFromCache();
        DataTablesResponse GetDataTablesResponseFromCache<TDto>(
            IDataTablesRequest request,
            IMapper mapper,
            string where = null);
    }

    public interface ICacheService<T> : ICacheService<T, Guid>
        where T : class, ICacheEntity<Guid>
    {
    }
}
