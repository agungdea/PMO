﻿using App.Domain.Models.Localization;
using System;
using System.Collections.Generic;

namespace App.Services.Localization
{
    public interface ILocaleResourceService : ICacheService<LocaleResource>
    {
        string GetResourceValue(string resourceName, string culture);
        List<LocaleResource> GetResources(string resourceName);
        List<LocaleResource> GetAllByCulture(string culture);
        void GenerateResourceJson(string culture);
    }
}
