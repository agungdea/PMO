﻿using App.Data.Repository;
using App.Domain.Models.Localization;
using App.Services.Core;
using Microsoft.AspNetCore.Hosting;
using Modular.Core;
using Modular.Core.Caching;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace App.Services.Localization
{
    public class LocaleResourceService : CacheService<LocaleResource, 
        IRepository<LocaleResource>>, 
        ILocaleResourceService
    {
        #region Fields

        private readonly ILanguageService _languageService;
        private readonly IHostingEnvironment _env;
        private readonly IWebSettingService _webSettingService;
        private readonly string _defaultCulture;
        private const string LOCALSTRINGRESOURCES_ALL = "app.lsr.all";

        #endregion

        #region Ctor

        public LocaleResourceService(IRepository<LocaleResource> repository,
            ILanguageService languageService,
            IHostingEnvironment env,
            IWebSettingService webSettingService,
            ICacheManager cacheManager) 
            : base(repository, cacheManager)
        {
            _languageService = languageService;
            _defaultCulture = _languageService.GetDefaultLanguage().LanguageCulture;
            _env = env;
            _webSettingService = webSettingService;
            _keyItem = x => $"{x.Language.LanguageCulture}--{x.ResourceName}";

            _cacheKey = LOCALSTRINGRESOURCES_ALL;
            
            Includes.Add(x => x.Language);

            int.TryParse(_webSettingService.GetByName("cache.resources.interval")?.Value, out _cacheInterval);
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;

            if (!GlobalConfiguration.CachingKeys.Contains(LOCALSTRINGRESOURCES_ALL))
                GlobalConfiguration.CachingKeys.Add(LOCALSTRINGRESOURCES_ALL);
        }

        #endregion

        #region Utils

        private bool CheckDuplicate(string resourceName, int languageId)
        {
            return _repository
                .GetAll()
                .Any(x => x.ResourceName.ToLower() == resourceName.ToLower() && 
                    x.LanguageId == languageId);
        }

        #endregion

        #region Methods

        public override LocaleResource Add(LocaleResource model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.ResourceName = model.ResourceName.ToLower();

            if (CheckDuplicate(model.ResourceName, model.LanguageId))
                throw new Exception("Duplicate Resource Name");

            _repository.Add(model);

            model.Language = _languageService.GetById(model.LanguageId);

            SetCache(model);

            return model;
        }

        public override LocaleResource Update(LocaleResource model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.ResourceName = model.ResourceName.ToLower();

            var current = _repository
                .GetSingle(model.Id);

            if (current.ResourceName != model.ResourceName && CheckDuplicate(model.ResourceName, model.LanguageId))
                throw new Exception("Duplicate Resource Name");
            
            _repository.Update(model);

            current.Language = _languageService.GetById(model.LanguageId);

            SetCache(current);

            return current;
        }

        public string GetResourceValue(string resourceName, string culture)
        {
            LocaleResource resource = null;

            if (GetCache().ContainsKey($"{culture}--{resourceName}"))
                resource = GetCache()[$"{culture}--{resourceName}"];

            if (resource == null && culture != _defaultCulture)
                if (GetCache().ContainsKey($"{culture}--{_defaultCulture}"))
                    resource = GetCache()[$"{culture}--{_defaultCulture}"];

            if (resource != null)
                return resource.ResourceValue;

            return "";
        }

        public List<LocaleResource> GetResources(string resourceName)
        {
            return _repository
                .GetAll(x => x.Language)
                .Where(x => x.ResourceName == resourceName)
                .ToList();
        }

        public List<LocaleResource> GetAllByCulture(string culture)
        {
            return _repository
                .GetAll(x => x.Language)
                .Where(x => x.Language.LanguageCulture == culture)
                .ToList();
        }

        public void GenerateResourceJson(string culture)
        {
            var path = _env.WebRootPath + $"/resources/{culture}.js";
            var resources = GetAllByCulture(culture);

            var json = "var RESOURCES = {};";

            foreach (var resource in resources)
            {
                json += $"RESOURCES[\"{resource.ResourceName}\"] = \"{resource.ResourceValue}\";";
            }

            File.WriteAllText(path, json);
        }

        #endregion
    }
}
