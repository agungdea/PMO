﻿using App.Domain.Models.Localization;
using System.Collections.Generic;

namespace App.Services.Localization
{
    public interface ILanguageService : ICacheService<Language, int>
    {
        Language GetDefaultLanguage();

        Language GetByName(string name);

        Language GetBySeoName(string seoName);

        Language GetByCulture(string culture);
    }
}
