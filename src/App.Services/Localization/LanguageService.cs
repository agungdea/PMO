﻿using App.Data.Repository;
using App.Domain.Models.Localization;
using App.Services.Core;
using Modular.Core;
using Modular.Core.Caching;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Linq.Expressions;

namespace App.Services.Localization
{
    public class LanguageService : CacheService<Language, int, IRepository<Language>>, ILanguageService
    {
        private readonly IWebSettingService _webSettingService;
        private const string LANGUAGE_ALL = "app.lang.all";

        public LanguageService(IRepository<Language> repository,
            IWebSettingService webSettingService,
            ICacheManager cacheManager)
            : base(repository, cacheManager)
        {
            _webSettingService = webSettingService;
            _cacheKey = LANGUAGE_ALL;
            _keyItem = x => x.LanguageCulture;

            int.TryParse(_webSettingService.GetByName("cache.language.interval")?.Value, out _cacheInterval);
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;

            if (!GlobalConfiguration.CachingKeys.Contains(LANGUAGE_ALL))
                GlobalConfiguration.CachingKeys.Add(LANGUAGE_ALL);
        }

        public Language GetDefaultLanguage()
        {
            var defaultLang = GetCache()
                .Where(x => x.Value.DefaultLanguage)
                .FirstOrDefault();

            if (defaultLang.Value == null)
                defaultLang = GetCache().FirstOrDefault();

            return defaultLang.Value;
        }

        public Language GetByName(string name)
        {
            return GetCache()
                .Where(x => x.Value.Name == name)
                .SingleOrDefault()
                .Value;
        }

        public Language GetBySeoName(string seoName)
        {
            return GetCache()
                .Where(x => x.Value.UniqueSeoCode == seoName)
                .SingleOrDefault()
                .Value;
        }

        public Language GetByCulture(string culture)
        {
            return GetCache()[culture];
        }

        public override IList<Language> GetAll(params Expression<Func<Language, object>>[] includes)
        {
            return GetCache()
                .Select(x => x.Value)
                .ToList();
        }

        public override Language Add(Language model)
        {
            if (model.DefaultLanguage)
            {
                var others = _repository
                    .GetAll()
                    .Where(x => x.DefaultLanguage)
                    .ToList();

                foreach (var other in others)
                {
                    other.DefaultLanguage = false;
                    Update(other);
                    SetCache(other);
                }
            }

            var result = base.Add(model);

            SetCache(model);

            return result;
        }

        public override Language Update(Language model)
        {
            if (model.DefaultLanguage)
            {
                var others = _repository
                    .GetAll()
                    .Where(x => x.DefaultLanguage && x.Id != model.Id)
                    .ToList();

                foreach (var other in others)
                {
                    other.DefaultLanguage = false;
                    Update(other);
                    SetCache(other);
                }
            }

            var result = base.Update(model);

            SetCache(model);

            return result;
        }
    }
}
