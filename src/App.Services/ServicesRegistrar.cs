﻿using App.Services.App;
using App.Services.Core;
using App.Services.Identity;
using App.Services.Localization;
using App.Services.Messages;
using App.Services.Utils;
using App.Services.Workflow;
using Microsoft.Extensions.DependencyInjection;

namespace App.Services
{
    public static class ServicesRegistrar
    {
        public static void Register(IServiceCollection services)
        {
            #region Identity

            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserProfileService, UserProfileService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IMasterClaimService, MasterClaimService>();
            services.AddTransient<IAppActionService, AppActionService>();

            services.AddTransient<IMailSenderService, MailSenderService>();
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<ISmtpOptionsService, SmtpOptionsService>();

            #endregion

            #region Core

            services.AddTransient<IWebSettingService, WebSettingService>();
            services.AddTransient<IEmailArchieveService, EmailArchieveService>();
            services.AddTransient<ILogService, LogService>();
            services.AddTransient<IEntityRoutingService, EntityRoutingService>();

            #endregion

            #region Localization

            services.AddTransient<ILanguageService, LanguageService>();
            services.AddTransient<ILocaleResourceService, LocaleResourceService>();

            #endregion

            #region App
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IAreaMappingService, AreaMappingService>();
            services.AddTransient<IAkiService, AkiService>();
            services.AddTransient<IAkiCoverService, AkiCoverService>();
            services.AddTransient<IAkiValuationService, AkiValuationService>();
            services.AddTransient<IDesignatorService, DesignatorService>();
            services.AddTransient<IHargaService, HargaService>();
            services.AddTransient<IJustificationService, JustificationService>();
            services.AddTransient<ITranformerProgramService, TranformerProgramService>();
            services.AddTransient<ITranformerSiService, TranformerSiService>();
            services.AddTransient<ITransformerPeriodService, TransformerPeriodService>();
            services.AddTransient<IUserUnitService, UserUnitService>();
            services.AddTransient<IUnitService, UnitService>();
            services.AddTransient<ICurrencyService, CurrencyService>();
            services.AddTransient<IWBSService, WBSService>();
            services.AddTransient<IWBSTreeService, WBSTreeService>();
            services.AddTransient<IWBSBoQService, WBSBoQService>();
            services.AddTransient<IWBSCurrencyService, WBSCurrencyService>();
            services.AddTransient<IWBStreeCategoryService, WBStreeCategoryService>();
            services.AddTransient<IWBSBoQGrantTotalService, WBSBoQGrantTotalService>();
            services.AddTransient<IActivityTemplateService, ActivityTemplateService>();
            services.AddTransient<IActivityTemplateChildService, ActivityTemplateChildService>();
            services.AddTransient<IWBSScheduleService, WBSScheduleService>();
            services.AddTransient<IWBSScheduleTempService, WBSScheduleTempService>();
            services.AddTransient<IWBSSchedulePlanningService, WBSSchedulePlanningService>();
            services.AddTransient<IUserMediaService, UserMediaService>();
            services.AddTransient<IWfActivityAkiService, WfActivityAkiService>();
            services.AddTransient<IGroupWfService, GroupWfService>();
            services.AddTransient<IWfCommantActivityService, WfCommantActivityService>();
            #endregion

            #region Workflow
            services.AddTransient<IWfProcessService, WfProcessService>();
            services.AddTransient<IWfActivityService, WfActivityService>();
            services.AddTransient<IWfActivityViewService, WfActivityViewService>();
         
            #endregion
        }
    }
}
