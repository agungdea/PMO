﻿using App.Domain;
using Modular.Core.Caching;
using System.Collections.Generic;

namespace App.Services.Caching
{
    public static class CacheManagerExtension
    {
        public static Dictionary<string, T> GetEntityCache<T>(this ICacheManager cache, string key) where T : IBaseCacheEntity
        {
            var list = cache.Get<Dictionary<string, T>>(key);
            return list;
        }

        public static void SetEntityCache<T, TKey>(this ICacheManager cache, string key, string cacheKey, T model, int interval) where T : ICacheEntity<TKey>
        {
            var list = cache.Get<Dictionary<string, T>>(key);
            if (list.ContainsKey(cacheKey))
            {
                list.Remove(cacheKey);
            }
            list.Add(cacheKey, model);
            cache.Set(key, list, interval);
        }

        public static void RemoveEntityCache<T, TKey>(this ICacheManager cache, string key, string cacheKey, T model, int interval) where T : ICacheEntity<TKey>
        {
            var list = cache.Get<Dictionary<string, T>>(key);
            if (list.ContainsKey(cacheKey))
            {
                list.Remove(cacheKey);
            }
            cache.Set(key, list, interval);
        }
    }
}
