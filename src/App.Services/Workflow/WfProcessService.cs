﻿using App.Data.Repository;
using App.Domain.Models.Workflow;
using App.Services.Core;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace App.Services.Workflow
{
    public class WfProcessService :
        BaseService<WfProcess, IRepository<WfProcess>>,
        IWfProcessService
    {
        private readonly string _jogetApiUrl;
        private readonly string _jogetHash;
        private readonly string _jogetUsername;
        private readonly string _jogetPassword;

        private const string JOGET_PROCESS_START_URL = "workflow/process/start/{0}";
        private const string JOGET_PROCESS_VIEW_URL = "monitoring/process/view/{0}";
        private const string JOGET_PROCESS_ABORT_URL = "workflow/process/abort/{0}";
        private const string JOGET_ACTIVITY_LIST_URL = "monitoring/activity/list?processId={0}";
        private const string JOGET_ACTIVITY_VIEW_URL = "monitoring/activity/view/{0}";
        private const string JOGET_ACTIVITY_ABORT_URL = "monitoring/activity/abort/{0}/{1}";
        private const string JOGET_ASSIGNMENT_COMPLETE_WITH_VARIABLE_URL = "workflow/assignment/completeWithVariable/{0}";
        private const string JOGET_ASSIGNMENT_VARIABLE_URL = " web/json/workflow/assignment/variable/{0}";
        private const string JOGET_ASSIGNMENT_COMPLETE_URL = "workflow/assignment/complete/{0}";
        private const string JOGET_ASSIGNMENT_view_URL = "workflow/assignment/view";
        private const string JOGET_DIREKTORI_user_URL = "directory/admin/user/list?name={0}";
        private const string JOGET_MONITORING_REASSIGN_URL = "monitoring/running/activity/reassign?processDefId={0}";
        //?/json/monitoring/activity/reassign?
        private readonly IWebSettingService _webSettingService;
        private readonly IWfActivityService _wfActivityService;
        private readonly IWfActivityViewService _wfActivityViewService;
        private readonly HttpClient _client;
        private readonly Dictionary<string, string> _query;

        public WfProcessService(IRepository<WfProcess> repository,
            IWebSettingService webSettingService,
            IWfActivityService wfActivityService,
            IWfActivityViewService wfActivityViewService)
            : base(repository)
        {
            _webSettingService = webSettingService;
            _wfActivityService = wfActivityService;
            _wfActivityViewService = wfActivityViewService;

            _jogetApiUrl = _webSettingService.GetByName("joget.api.url")?.Value;
            _jogetHash = _webSettingService.GetByName("joget.hash")?.Value;
            _jogetUsername = _webSettingService.GetByName("joget.username")?.Value;
            _jogetPassword = _webSettingService.GetByName("joget.password")?.Value;

            _query = new Dictionary<string, string>();

            _query.Add("j_username", _jogetUsername);
            _query.Add("hash", _jogetHash);

            _client = new HttpClient();
            _client.BaseAddress = new Uri(_jogetApiUrl);
            _client.DefaultRequestHeaders.Add("origin", "http://localhost");
            _client.DefaultRequestHeaders.Add("referer", "http://localhost");
        }

        private string MapQueryToUrl(string url, Dictionary<string, string> data)
        {
            if (data == null)
                data = new Dictionary<string, string>();

            foreach (var item in _query)
            {
                data.Add(item.Key, item.Value);
            }

            url = QueryHelpers.AddQueryString(url, data);

            return url;
        }

        private async Task<dynamic> Post(string url, Dictionary<string, string> data = null)
        {
            var resp = await PostWf(url, data);

            return JsonConvert.DeserializeObject<dynamic>(resp);
        }
        public async Task<dynamic> Start(string processDefId, string loginAs)
        {
            var url = string.Format(JOGET_PROCESS_START_URL, processDefId);
            var data = new Dictionary<string, string>();
            if (!string.IsNullOrWhiteSpace(loginAs))
            {
                data.Add("loginAs", loginAs);
            }
            url = MapQueryToUrl(url, data);

            var caller = await _client.PostAsync(url, null);

            var resp = await caller.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<dynamic>(resp);
        }


        public async Task<dynamic> listActivity(string processId)
        {
            var Url = string.Format(JOGET_ACTIVITY_LIST_URL, processId);
            var data = new Dictionary<string, string>();

            Url = MapQueryToUrl(Url, data);

            var caller = await _client.PostAsync(Url, null);

            var resp = await caller.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<dynamic>(resp);

        }

        public async Task<dynamic> CekUser(string name)
        {
            var Url = string.Format(JOGET_DIREKTORI_user_URL, name);
            var data = new Dictionary<string, string>();

            Url = MapQueryToUrl(Url, data);

            var caller = await _client.PostAsync(Url, null);

            var resp = await caller.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<dynamic>(resp);

        }
        public async Task<dynamic> Assigment(string processDefId, Dictionary<string, string> data)
        {
            var Url= string.Format(JOGET_MONITORING_REASSIGN_URL,processDefId);

            Url = MapQueryToUrl(Url, data);
            var caller = await _client.PostAsync(Url, null);

            var resp = await caller.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<dynamic>(resp);
        }

       //  activityId = 841_206_crm_process1_send_proposal&username=admin&replaceUser=cat


        public async Task<dynamic> CompleteActivitySubmit(string activity, Dictionary<string, string> data)
        {
            var Url = string.Format(JOGET_ASSIGNMENT_COMPLETE_WITH_VARIABLE_URL, activity);
          
            Url = MapQueryToUrl(Url, data);

            var caller = await _client.PostAsync(Url, null);

            var resp = await caller.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<dynamic>(resp);

        }

        public async Task<dynamic> ViewProcessDynamic(string processId)
        {
            var Url = string.Format(JOGET_PROCESS_VIEW_URL, processId);
            Url = MapQueryToUrl(Url, null);

            var caller = await _client.PostAsync(Url, null);

            var resp = await caller.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<dynamic>(resp);
        }




        public async Task<WfProcess> StartProcess(string processDefId)
        {
            var url = string.Format(JOGET_PROCESS_START_URL, processDefId);

            var respObj = await Post(url);

            var wf = await ViewProcess(respObj.processId.ToString());

            return wf;
        }

        public async Task<WfProcess> ViewProcess(string processId)
        {
            var url = string.Format(JOGET_PROCESS_VIEW_URL, processId);

            var respObj = await Post(url);

            var wf = new WfProcess()
            {
                ProcessId = respObj.processId,
                ProcessDefinitionId = respObj.processDefId.ToString().Replace("#", ":"),
                Requester = respObj.requester,
                State = JsonConvert.SerializeObject(respObj.states),
                OtherInfo = JsonConvert.SerializeObject(respObj)
            };

            return wf;
        }

        public async Task<WfProcess> AbortProcess(string processId)
        {
            var url = string.Format(JOGET_PROCESS_ABORT_URL, processId);

            var wf = GetByProcessId(processId);

            await Post(url);

            var newWf = await ViewProcess(processId);

            wf.State = newWf.State;
            wf.OtherInfo = newWf.OtherInfo;

            _repository.Update(wf);

            return wf;
        }

        public async Task<List<WfActivity>> ListActivity(string processId, Dictionary<string, string> data)
        {
            var url = string.Format(JOGET_ACTIVITY_LIST_URL, processId);

            //if (data == null)
            //{
            //    data = new Dictionary<string, string>()
            //    {

            //        {"loginAs", "cat" },
            //    };
            //}

            url = MapQueryToUrl(url, data);

            var respObj = await Post(url);

            JToken token = JToken.Parse(JsonConvert.SerializeObject(respObj.data));

            var activities = new List<WfActivity>();

            if (token is JArray)
            {
                var dataList = token.ToObject<List<dynamic>>();

                foreach (var item in dataList)
                {
                    var activity = new WfActivity(item.id.ToString(), item.state.ToString())
                    {
                        CreatedAt = item.dateCreated
                    };

                    var temp = await ViewActivity(item.id.ToString());

                    activity.OtherInfo = JsonConvert.SerializeObject(temp);
                    string activityDefId = temp.activityDefId.ToString();
                    if (_wfActivityViewService.GetById(activityDefId) != null)
                    {
                        activity.ActivityDefId = temp.activityDefId;
                    }

                    activities.Add(activity);
                }
            }
            else
            {
                var activity = new WfActivity(respObj.data.id.ToString(), respObj.data.state.ToString())
                {
                    CreatedAt = respObj.data.dateCreated
                };

                var temp = await ViewActivity(respObj.data.id.ToString());

                activity.OtherInfo = JsonConvert.SerializeObject(temp);
                string activityDefId = temp.activityDefId.ToString();
                if (_wfActivityViewService.GetById(activityDefId) != null)
                {
                    activity.ActivityDefId = temp.activityDefId;
                }

                activities.Add(activity);
            }

            return activities;
        }

        public async Task<dynamic> ViewActivity(string activityId)
        {
            var url = string.Format(JOGET_ACTIVITY_VIEW_URL, activityId);

            var respObj = await Post(url);

            return respObj;
        }

        //private async Task<WfProcess> assigmentVariable(string activityId, string url = null)
        //{

        //}

        private async Task<WfProcess> CompleteActivity(string activityId, string url = null)
        {
            if (url == null)
                url = string.Format(JOGET_ASSIGNMENT_COMPLETE_URL, activityId);

            var param = new Dictionary<string, string>();

            var prevActivity = _wfActivityService.GetById(activityId);

            if (prevActivity.OtherInfo != null)
            {
                var prevActivityData = JsonConvert.DeserializeObject<dynamic>(prevActivity.OtherInfo);

                if (prevActivityData.assignee != null)
                {
                    param.Add("loginAs", prevActivityData.assignee.ToString());
                }
            }

            var respObj = await Post(url, param);

            WfProcess wf = GetByProcessId(respObj.processId.ToString());

            prevActivity.State = WfActivity.ACTIVITY_COMPLETED;
            prevActivity.OtherInfo = JsonConvert.SerializeObject(await ViewActivity(activityId));
            _wfActivityService.Update(prevActivity);

            var activities = await ListActivity(wf.ProcessId, null);

            WfActivity nextActivity;

            if (respObj.nextActivityId != null)
            {
                nextActivity = new WfActivity(respObj.nextActivityId.ToString(), WfActivity.ACTIVITY_NOTRUNNING);
            }
            else
            {
                nextActivity = activities.FirstOrDefault(x => x.State == WfActivity.ACTIVITY_NOTRUNNING);
            }

            if (nextActivity != null)
            {
                var temp = await ViewActivity(nextActivity.Id);
                nextActivity.WfProcessId = wf.Id;
                nextActivity.OtherInfo = JsonConvert.SerializeObject(temp);
                string activityDefId = temp.activityDefId.ToString();
                if (_wfActivityViewService.GetById(activityDefId) != null)
                {
                    nextActivity.ActivityDefId = activityDefId;
                }
                _wfActivityService.Add(nextActivity);
                wf.ActivityId = nextActivity.Id;
            }
            else
            {
                wf.ActivityId = null;
            }

            wf.PrevActivityId = activityId;

            var newWfInfo = await ViewProcess(wf.ProcessId);

            wf.State = newWfInfo.State;
            wf.OtherInfo = newWfInfo.OtherInfo;

            Update(wf);

            var currentActivities = _wfActivityService.GetByProcess(wf.Id);

            var unregisteredActivities = activities
                .Where(x => !currentActivities.Any(y => y.Id == x.Id));

            foreach (var activity in unregisteredActivities)
            {
                activity.WfProcessId = wf.Id;
                activity.OtherInfo = JsonConvert.SerializeObject(await ViewActivity(activity.Id));
                _wfActivityService.Add(activity);
            }

            return wf;
        }

        public async Task<WfProcess> CompleteActivity(string activityId)
        {
            return await CompleteActivity(activityId, url: null);
        }

        public async Task<WfProcess> CompleteActivity(string activityId, Dictionary<string, string> data)
        {
            var url = string.Format(JOGET_ASSIGNMENT_COMPLETE_WITH_VARIABLE_URL, activityId);

            url = MapQueryToUrl(url, data);

            return await CompleteActivity(activityId, url);
        }

        public async Task<WfProcess> UpdateParameter(string activityId, Dictionary<string, string> data)
        {
            var url = string.Format(JOGET_ASSIGNMENT_VARIABLE_URL, activityId);

            url = MapQueryToUrl(url, data);

            return await CompleteActivity(activityId, url);
        }

        public async Task<WfActivity> AbortActivity(string activityId)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetWf(string url, Dictionary<string, string> data)
        {
            url = MapQueryToUrl(url, data);

            var caller = await _client.GetAsync(url);

            return await caller.Content.ReadAsStringAsync();
        }

        public async Task<string> PostWf(string url, Dictionary<string, string> data)
        {
            url = MapQueryToUrl(url, data);

            var caller = await _client.PostAsync(url, null);

            return await caller.Content.ReadAsStringAsync();
        }

        public WfProcess GetByProcessId(string processId)
        {
            return _repository
                .Table
                .FirstOrDefault(x => x.ProcessId == processId);
        }

        public List<string> GetCurrentAssignees(WfProcess wfProcess)
        {
            if (string.IsNullOrWhiteSpace(wfProcess.ActivityId))
            {
                return null;
            }

            return _wfActivityService.GetAssignees(wfProcess.ActivityId);
        }

        public List<string> GetCurrentAssignees(long id)
        {
            var wf = GetById(id);

            return GetCurrentAssignees(wf);
        }

        public List<string> GetCurrentAssignees(string processId)
        {
            var wf = GetByProcessId(processId);

            return GetCurrentAssignees(wf);
        }

        public Dictionary<string, string> GetCurrentVariables(WfProcess wfProcess)
        {
            if (string.IsNullOrWhiteSpace(wfProcess.ActivityId))
            {
                return null;
            }

            return _wfActivityService.GetVariables(wfProcess.ActivityId);
        }

        public Dictionary<string, string> GetCurrentVariables(long id)
        {
            var wf = GetById(id);

            return GetCurrentVariables(wf);
        }

        public Dictionary<string, string> GetCurrentVariables(string processId)
        {
            var wf = GetByProcessId(processId);

            return GetCurrentVariables(wf);
        }

        public WfActivity GetCurrentActivity(string processId)
        {
            var wfProcess = GetByProcessId(processId);

            return GetCurrentActivity(wfProcess);
        }

        public WfActivity GetCurrentActivity(long id)
        {
            var wfProcess = GetById(id);

            return GetCurrentActivity(wfProcess);
        }

        public WfActivity GetCurrentActivity(WfProcess wfProcess)
        {
            if (string.IsNullOrWhiteSpace(wfProcess.ActivityId))
                return null;

            var activity = _wfActivityService.GetById(wfProcess.ActivityId);

            return activity;
        }
    }
}
