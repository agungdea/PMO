﻿using App.Domain.Models.Workflow;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Services.Workflow
{
    public interface IWfProcessService : IService<WfProcess>
    {
        Task<dynamic> Start(string processDefId, string loginAs);
        Task<dynamic> listActivity(string processId);
        Task<dynamic> CompleteActivitySubmit(string activity, Dictionary<string, string> data);
        Task<dynamic> ViewProcessDynamic(string processId);
        Task<dynamic> CekUser(string name);
        Task<dynamic> Assigment(string processDefId, Dictionary<string, string> data);

        Task<WfProcess> StartProcess(string processDefId);
        Task<WfProcess> ViewProcess(string processId);
        Task<WfProcess> AbortProcess(string processId);
        Task<List<WfActivity>> ListActivity(string processId, Dictionary<string, string> data);
        Task<dynamic> ViewActivity(string activityId);
        Task<WfProcess> CompleteActivity(string activityId);
        Task<WfProcess> CompleteActivity(string activityId, Dictionary<string, string> data);
        Task<WfActivity> AbortActivity(string activityId);
        Task<string> GetWf(string url, Dictionary<string, string> data);
        Task<string> PostWf(string url, Dictionary<string, string> data);
        WfProcess GetByProcessId(string processId);
        WfActivity GetCurrentActivity(WfProcess wfProcess);
        WfActivity GetCurrentActivity(long id);
        WfActivity GetCurrentActivity(string processId);
        List<string> GetCurrentAssignees(WfProcess wfProcess);
        List<string> GetCurrentAssignees(long id);
        List<string> GetCurrentAssignees(string processId);
        Dictionary<string, string> GetCurrentVariables(WfProcess wfProcess);
        Dictionary<string, string> GetCurrentVariables(long id);
        Dictionary<string, string> GetCurrentVariables(string processId);
    }
}
