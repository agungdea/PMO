﻿using System.Collections.Generic;
using App.Data.Repository;
using App.Domain.Models.Workflow;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace App.Services.Workflow
{
    public class WfActivityService :
        BaseService<WfActivity, IRepository<WfActivity>>,
        IWfActivityService
    {
        private readonly IWfActivityViewService _wfActivityViewService;

        public WfActivityService(IRepository<WfActivity> repository,
            IWfActivityViewService wfActivityViewService) 
            : base(repository)
        {
            _wfActivityViewService = wfActivityViewService;
        }

        public List<string> GetAssignees(string id)
        {
            var activity = GetById(id);
            var activityInfo = JsonConvert.DeserializeObject<dynamic>(activity.OtherInfo);

            JToken token;

            if (activityInfo.assignees == null)
            {
                token = JToken.Parse(JsonConvert.SerializeObject(activityInfo.assignee));
            }
            else
            {
                token = JToken.Parse(JsonConvert.SerializeObject(activityInfo.assignees));
            }

            if (token is JArray)
            {
                var assignees = token.ToObject<List<string>>();
                return assignees;
            }
            else
            {
                var assignee = token.ToObject<string>();
                return new List<string> { assignee };
            }
        }

        public override WfActivity GetById(params object[] ids)
        {
            var result = base.GetById(ids);

            if (result == null)
                return result;

            result.WfActivityView = _wfActivityViewService.GetById(result.ActivityDefId);

            return result;
        }

        public List<WfActivity> GetByProcess(string id)
        {
            return _repository
                .Table
                .Include(x => x.WfProcess)
                .Where(x => x.WfProcess.ProcessId == id)
                .ToList();
        }

        public List<WfActivity> GetByProcess(long id)
        {
            return _repository
                .Table
                .Where(x => x.WfProcessId == id)
                .ToList();
        }

        public Dictionary<string, string> GetVariables(string id)
        {
            var activity = GetById(id);
            var activityInfo = JsonConvert.DeserializeObject<dynamic>(activity.OtherInfo);

            JToken token;

            if (activityInfo.variables == null)
            {
                token = JToken.Parse(JsonConvert.SerializeObject(activityInfo.variable));
            }
            else
            {
                token = JToken.Parse(JsonConvert.SerializeObject(activityInfo.variables));
            }

            var variables = token.ToObject<List<Dictionary<string, string>>>();
            return variables.SelectMany(x => x)
                            .ToDictionary(pair => pair.Key, pair => pair.Value);
        }
    }
}
