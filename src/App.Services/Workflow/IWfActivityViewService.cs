﻿using App.Domain.Models.Workflow;

namespace App.Services.Workflow
{
    public interface IWfActivityViewService : IService<WfActivityView>
    {
    }
}
