﻿using App.Data.Repository;
using App.Domain.Models.Workflow;

namespace App.Services.Workflow
{
    public class WfActivityViewService : 
        BaseService<WfActivityView, IRepository<WfActivityView>>,
        IWfActivityViewService
    {
        public WfActivityViewService(IRepository<WfActivityView> repository) : base(repository)
        {
        }
    }
}
