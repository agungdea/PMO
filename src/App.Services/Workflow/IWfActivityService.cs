﻿using App.Domain.Models.Workflow;
using System.Collections.Generic;

namespace App.Services.Workflow
{
    public interface IWfActivityService : IService<WfActivity>
    {
        List<WfActivity> GetByProcess(long id);
        List<WfActivity> GetByProcess(string id);
        List<string> GetAssignees(string id);
        Dictionary<string, string> GetVariables(string id);
    }
}
