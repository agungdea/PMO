﻿using App.Data.Repository;
using App.Domain.Models.Core;
using Modular.Core;
using Modular.Core.Caching;
using System;
using System.Linq;

namespace App.Services.Core
{
    public class WebSettingService : CacheService<WebSetting, IRepository<WebSetting>>, IWebSettingService
    {
        private const string WEBSETTING_ALL = "app.web.setting.all";

        public WebSettingService(IRepository<WebSetting> repository,
            ICacheManager cacheManager)
            : base(repository, cacheManager)
        {
            _cacheKey = WEBSETTING_ALL;
            _keyItem = x => x.Name;

            int.TryParse(GetCacheTime()?.Value, out _cacheInterval);
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;

            if (!GlobalConfiguration.CachingKeys.Contains(WEBSETTING_ALL))
                GlobalConfiguration.CachingKeys.Add(WEBSETTING_ALL);
        }

        #region Utils

        private WebSetting GetCacheTime()
        {
            var settingKey = "cache.web.setting.interval";

            if (GetCache() != null && GetCache().ContainsKey(settingKey))
                return GetCache()[settingKey];

            return _repository
                .Table
                .Where(x => x.Name == settingKey)
                .SingleOrDefault();
        }

        #endregion

        public WebSetting GetByName(string name)
        {
            if(GetCache().ContainsKey(name))
                return GetCache()[name];

            return null;
        }

        public override WebSetting Add(WebSetting model)
        {
            if (GetCache().ContainsKey(model.Name))
                return model;

            return base.Add(model);
        }

        public override WebSetting Update(WebSetting model)
        {
            if (GetCache().ContainsKey(model.Name) && GetCache()[model.Name].Id != model.Id)
                throw new Exception("Duplicate Setting Name");

            return base.Update(model);
        }

        public override WebSetting Delete(WebSetting model)
        {
            if (model.SystemSetting)
                throw new Exception("Cannot delete system setting.");

            return base.Delete(model);
        }
    }
}
