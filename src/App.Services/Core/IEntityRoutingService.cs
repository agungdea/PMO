﻿using App.Domain.Models.Core;
using System;
using System.Collections.Generic;

namespace App.Services.Core
{
    public interface IEntityRoutingService
    {
        string ToSafeSlug(string slug, Guid entityId, int entityRoutingTypeId);

        EntityRouting Get(Guid entityId, int entityRoutingTypeId);

        void Add(string name, string slug, Guid entityId, int entityRoutingTypeId);

        void Update(string newName, string newSlug, Guid entityId, int entityRoutingTypeId);

        void Remove(Guid entityId, int entityRoutingTypeId);

        Dictionary<string, EntityRouting> GetFromCache();
    }
}
