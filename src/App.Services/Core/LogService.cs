﻿using App.Data.Repository;
using App.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Services.Core
{
    public class LogService : BaseService<Log, IRepository<Log>>,
        ILogService
    {
        public LogService(IRepository<Log> repository) : base(repository)
        {
        }
    }
}
