﻿using App.Domain.Models.Core;
using System.Collections.Generic;

namespace App.Services.Core
{
    public interface IWebSettingService : ICacheService<WebSetting>
    {
        WebSetting GetByName(string name);
    }
}
