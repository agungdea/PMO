﻿using App.Domain.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System.Linq.Expressions;

namespace App.Services.Core
{
    public interface ILogService : IService<Log>
    {
    }
}
