﻿using System;
using System.Collections.Generic;
using App.Data.Repository;
using App.Domain.Models.Core;
using System.Linq;

namespace App.Services.Core
{
    public class EmailArchieveService : BaseService<EmailArchieve, IRepository<EmailArchieve>>, 
        IEmailArchieveService
    {
        public EmailArchieveService(IRepository<EmailArchieve> repository) 
            : base(repository)
        {
        }

        public List<EmailArchieve> GetUnsentEmail(int? maxTry = null)
        {
            var emails = _repository
                .Table
                .Where(x => !x.IsSent);

            if (maxTry.HasValue)
            {
                emails = emails.Where(x => x.TrySentCount < maxTry);
            }

            return emails.ToList();
        }
    }
}
