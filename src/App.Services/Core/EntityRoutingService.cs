﻿using App.Data.Repository;
using App.Domain.Models.Core;
using App.Services.Caching;
using Microsoft.EntityFrameworkCore;
using Modular.Core;
using Modular.Core.Caching;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Services.Core
{
    public class EntityRoutingService : IEntityRoutingService
    {
        private readonly IRepository<EntityRouting> _repository;
        private readonly IRepository<EntityRoutingType> _entityRoutingTypeRepo;
        private readonly IWebSettingService _webSettingService;
        private readonly ICacheManager _cacheManager;
        private int _cacheInterval;
        private const string ROUTING_ALL = "app.routing.all";
        private Func<EntityRouting, string> _keyItem;

        public EntityRoutingService(IRepository<EntityRouting> repository,
            IRepository<EntityRoutingType> entityRoutingTypeRepo,
            IWebSettingService webSettingService,
            ICacheManager cacheManager)
        {
            _repository = repository;
            _entityRoutingTypeRepo = entityRoutingTypeRepo;
            _webSettingService = webSettingService;
            _cacheManager = cacheManager;
            _keyItem = x => x.Slug.ToLowerInvariant();

            int.TryParse(_webSettingService.GetByName("cache.routing.interval")?.Value, out _cacheInterval);
            _cacheInterval = _cacheInterval > 0 ? _cacheInterval : 60;

            if (!GlobalConfiguration.CachingKeys.Contains(ROUTING_ALL))
                GlobalConfiguration.CachingKeys.Add(ROUTING_ALL);
        }

        #region Utils

        private Dictionary<string, EntityRouting> GetCache()
        {
            var entityRoutings = _cacheManager.GetEntityCache<EntityRouting>(ROUTING_ALL);

            if (entityRoutings == null)
                InitCache();

            entityRoutings = _cacheManager.GetEntityCache<EntityRouting>(ROUTING_ALL);

            return entityRoutings;
        }

        private void SetCache(EntityRouting model)
        {
            _cacheManager.SetEntityCache<EntityRouting, Guid>(ROUTING_ALL, _keyItem(model), model, _cacheInterval);
        }

        private void RemoveCache(EntityRouting model)
        {
            _cacheManager.RemoveEntityCache<EntityRouting, Guid>(ROUTING_ALL, _keyItem(model), model, _cacheInterval);
        }

        private void InitCache()
        {
            var entityRoutings = _repository
                .TableNoTracking
                .Include(x => x.EntityRoutingType)
                .ToList()
                .ToDictionary(_keyItem, x => x);

            _cacheManager.Set(ROUTING_ALL, entityRoutings, _cacheInterval);
        }

        #endregion

        public string ToSafeSlug(string slug, Guid entityId, int entityRoutingTypeId)
        {
            var i = 2;
            slug = slug.ToUrlFriendly();
            while (true)
            {
                var entity = _repository.TableNoTracking.FirstOrDefault(x => x.Slug == slug);
                if (entity != null && 
                    !(entity.EntityId == entityId && 
                        entity.EntityRoutingTypeId == entityRoutingTypeId))
                {
                    slug = string.Format("{0}-{1}", slug, i);
                    i++;
                }
                else
                {
                    break;
                }
            }

            return slug;
        }

        public EntityRouting Get(Guid entityId, int entityRoutingTypeId)
        {
            return _repository.Table.FirstOrDefault(x => x.EntityId == entityId &&
                x.EntityRoutingTypeId == entityRoutingTypeId);
        }

        public void Add(string name, string slug, Guid entityId, int entityRoutingTypeId)
        {
            var entity = new EntityRouting
            {
                Name = name,
                Slug = slug,
                EntityId = entityId,
                EntityRoutingTypeId = entityRoutingTypeId
            };

            _repository.Add(entity);

            entity.EntityRoutingType = _entityRoutingTypeRepo.GetSingle(entityRoutingTypeId);

            SetCache(entity);
        }

        public void Update(string newName, string newSlug, Guid entityId, int entityRoutingTypeId)
        {
            var entity = _repository
                .Table
                .Include(x => x.EntityRoutingType)
                .First(x => x.EntityId == entityId 
                    && x.EntityRoutingTypeId == entityRoutingTypeId);
            entity.Name = newName;
            entity.Slug = newSlug;

            _repository.Update(entity);

            SetCache(entity);
        }

        public void Remove(Guid entityId, int entityRoutingTypeId)
        {
            var entity = _repository
                .Table
                .FirstOrDefault(x => x.EntityId == entityId && 
                    x.EntityRoutingTypeId == entityRoutingTypeId);
            if (entity != null)
            {
                _repository.Delete(entity);

                RemoveCache(entity);
            }
        }

        public Dictionary<string, EntityRouting> GetFromCache()
        {
            return GetCache();
        }
    }
}
