﻿using App.Domain.Models.Core;
using System.Collections.Generic;

namespace App.Services.Core
{
    public interface IEmailArchieveService : IService<EmailArchieve>
    {
        List<EmailArchieve> GetUnsentEmail(int? maxTry = null);
    }
}
