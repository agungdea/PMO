﻿using App.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using App.Data.Repository;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using System.Linq.Dynamic.Core;
using App.Services.Utils;

namespace App.Services
{
    public class SoftDeleteBaseService<T, TRepo> : IService<T>
        where T : class, ISoftDeleteEntity
        where TRepo : IRepository<T>
    {
        protected readonly TRepo _repository;

        public SoftDeleteBaseService(TRepo repository)
        {
            _repository = repository;
        }

        public T Add(T model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            model.IsDeleted = false;
            _repository.Add(model);

            return model;
        }
        public T Update(T model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            _repository.Update(model);

            return model;
        }
        public T Delete(T model)
        {
            if (model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }
            else
            {
                model.IsDeleted = true;
                model.DeletedDate = DateTime.Now;
                model.DeletedBy = "";

            }
           
            _repository.Update(model);

            return model;
        }

        public IList<T> GetAll(params Expression<Func<T, object>>[] includes)
        {
            return _repository.GetAll(includes).Where(x=>x.IsDeleted.Equals(false))
                  .ToList();
        }

        public T GetById(params object[] ids)
        {
            var data = _repository.GetSingle(ids);

            if (data.IsDeleted.Equals(false))
            {
                return _repository.GetSingle(ids);
            }
            else {
                throw new ArgumentNullException(nameof(data));
            }
       
        }

        public T GetByIdSoftDelete(Expression<Func<T, bool>> predicate)
        {
            return _repository.GetAll().Where(predicate)
                  .FirstOrDefault();
        }

        public virtual DataTablesResponse GetDataTablesResponse<TDto>(
             IDataTablesRequest request,
             IMapper mapper,
             string where = null,
             params Expression<Func<T, object>>[] includes)
        {
            var data = _repository.GetAll(includes).Where(x=>x.IsDeleted.Equals(false));

            var searchQuery = request.Search.Value;

            var filteredData = data.AsQueryable();
            if (!string.IsNullOrWhiteSpace(where))
            {
                filteredData = filteredData.Where(where);
            }

            return GetDataTablesResponseByQuery<TDto>(request, mapper, data);
        }

        public virtual DataTablesResponse GetDataTablesResponseByQuery<TDto>(
            IDataTablesRequest request,
            IMapper mapper,
            IQueryable data)
        {
            var searchQuery = request.Search.Value;

            var filteredData = data.AsQueryable();

            var globalFilter = request.Columns.GetFilter(searchQuery, "OR");

            if (!string.IsNullOrEmpty(globalFilter))
                filteredData = filteredData.Where(globalFilter);

            var filter = request.Columns.GetFilter();

            if (!string.IsNullOrEmpty(filter))
                filteredData = filteredData.Where(filter);

            var sort = request.Columns.GetSort();

            if (!string.IsNullOrEmpty(sort))
                filteredData = filteredData.OrderBy(sort);

            var dataPage = filteredData.Skip(request.Start).Take(request.Length);

            var response = DataTablesResponse.Create(request,
                data.Count(),
                filteredData.Count(),
                mapper.Map<List<TDto>>(dataPage));

            return response;
        }

        protected virtual IQueryable<T> RelationQuery(IQueryable<T> data, string query)
        {
            return data;
        }


    }
}
