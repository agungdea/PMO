﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Caching.Distributed;
using System;

namespace Modular.Core.Caching
{
    public partial class MemoryCacheManager : ICacheManager
    {
        private readonly IMemoryCache _cache;

        public MemoryCacheManager(IMemoryCache cache)
        {
            _cache = cache;
        }

        public virtual T Get<T>(string key)
        {
            return _cache.Get<T>(key);
        }
        
        public virtual void Set(string key, object data, int cacheTime)
        {
            if (data == null)
                return;

            var policy = new MemoryCacheEntryOptions();
            policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(cacheTime);
            _cache.Set(key, data, policy);
        }
        
        public virtual bool IsSet(string key)
        {
            return (_cache.Get(key) != null);
        }
        
        public virtual void Remove(string key)
        {
            _cache.Remove(key);
        }
        
        public virtual void Dispose()
        {
        }
    }
}
