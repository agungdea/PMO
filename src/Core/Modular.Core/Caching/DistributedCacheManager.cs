﻿using Microsoft.Extensions.Caching.Distributed;
using System;

namespace Modular.Core.Caching
{
    public partial class DistributedCacheManager : ICacheManager
    {
        private readonly IDistributedCache _cache;

        public DistributedCacheManager(IDistributedCache cache)
        {
            _cache = cache;
        }

        public virtual T Get<T>(string key)
        {
            var data = _cache.Get(key);

            var result = data.ConvertFromBytes<T>();

            return result;
        }

        public virtual void Set(string key, object data, int cacheTime)
        {
            if (data == null)
                return;

            var policy = new DistributedCacheEntryOptions();
            policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(cacheTime) + TimeSpan.FromHours(7);
            _cache.Set(key, data.ToByteArray(), policy);
        }

        public virtual bool IsSet(string key)
        {
            return (_cache.Get(key) != null);
        }

        public virtual void Remove(string key)
        {
            _cache.Remove(key);
        }

        public virtual void Dispose()
        {
        }
    }
}
