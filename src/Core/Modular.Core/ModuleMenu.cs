﻿using System.Collections.Generic;

namespace Modular.Core
{
    public class ModuleMenu
    {
        public string Id { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Area { get; set; }
        public string Text { get; set; }
        public string Icon { get; set; }
        public int Order { get; set; }
        public bool UseDictionary { get; set; }
        public List<ModuleMenu> Children { get; set; }
    }
}
