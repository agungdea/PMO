﻿using System.Collections.Generic;

namespace Modular.Core
{
    public static class GlobalConfiguration
    {
        static GlobalConfiguration()
        {
            Modules = new List<ModuleInfo>();
            ModuleMenus = new List<ModuleMenu>();
            CachingKeys = new List<string>();
        }

        public static IList<ModuleInfo> Modules { get; set; }

        public static IList<ModuleMenu> ModuleMenus { get; set; }

        public static IList<string> CachingKeys { get; set; }

        public static string WebRootPath { get; set; }

        public static string ContentRootPath { get; set; }
    }
}
