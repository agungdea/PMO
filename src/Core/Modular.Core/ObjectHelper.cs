﻿using Newtonsoft.Json;
using System;
using System.Text;

namespace Modular.Core
{
    public static class ObjectHelper
    {
        public static byte[] ToByteArray(this object item)
        {
            var str = JsonConvert.SerializeObject(item, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            var bytes = Encoding.UTF8.GetBytes(str);

            return bytes;
        }

        public static T ConvertFromBytes<T>(this byte[] bytes)
        {
            try
            {
                var str = Encoding.UTF8.GetString(bytes);
                var obj = JsonConvert.DeserializeObject<T>(str);

                return obj;
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}
