﻿using App.Domain.DTO.App;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;

namespace App.Helper
{
    public class PdfHelper
    {
        private readonly IHostingEnvironment environment;
        public PdfHelper(IHostingEnvironment environment)
        {
            this.environment = environment;
        }

        public void GetPDFAttachment(MemoryStream ms, JustificationPdfHeaderDto texts)
        {
            BaseFont times_roman = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BaseFont times_bold = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            Document document = new Document(PageSize.A4, 25, 25, 30, 30);
            PdfWriter writer = PdfWriter.GetInstance(document, ms);
            document.Open();
            PdfContentByte cb = writer.DirectContent;
            #region doc
            cb.BeginText();
            //cb.Rectangle(50, 520, 491, 280);
            //cb.Stroke();
            cb.SetFontAndSize(times_roman, 12);
            cb.SetTextMatrix(250, 775); cb.ShowText("JUSTIFIKASI KEBUTUHAN");
            cb.SetFontAndSize(times_roman, 12);
            cb.SetTextMatrix(250, 764); cb.ShowText("BARANG DAN/ATAU JASA");
            cb.SetFontAndSize(times_roman, 8);
            cb.SetTextMatrix(255, 754); cb.ShowText("(disusun dan ditetapkan oleh " + texts.Penyusun + ")");
            cb.Stroke();
            cb.SetColorFill(BaseColor.Black);
            cb.SetFontAndSize(times_roman, 12);
            cb.SetTextMatrix(60, 700); cb.ShowText("Nomor DRK");
            cb.SetTextMatrix(200, 700); cb.ShowText(":");
            cb.SetTextMatrix(210, 700); cb.ShowText(texts.NomorDRK);
            cb.SetTextMatrix(60, 685); cb.ShowText("Jumlah Anggaran");
            cb.SetTextMatrix(200, 685); cb.ShowText(":");
            cb.SetTextMatrix(210, 685); cb.ShowText(texts.JumlahAnggaran.ToString());
            cb.SetTextMatrix(60, 670); cb.ShowText("Nama Kegiatan");
            cb.SetTextMatrix(200, 670); cb.ShowText(":");
            cb.SetTextMatrix(210, 670); cb.ShowText(texts.NamaKegiatan);
            cb.SetTextMatrix(60, 655); cb.ShowText("Nomor DRP");
            cb.SetTextMatrix(200, 655); cb.ShowText(":");
            cb.SetTextMatrix(210, 655); cb.ShowText(texts.NomorDRP);
            cb.SetTextMatrix(60, 640); cb.ShowText("Jenis Kebutuhan");
            cb.SetTextMatrix(200, 640); cb.ShowText(":");
            cb.SetTextMatrix(210, 640); cb.ShowText(texts.JenisKebutuhan);
            cb.SetTextMatrix(60, 625); cb.ShowText("Pusat Pertanggung Jawaban");
            cb.SetTextMatrix(200, 625); cb.ShowText(":");
            cb.SetTextMatrix(210, 625); cb.ShowText(texts.PusatPertanggungjawaban);
            cb.SetTextMatrix(60, 610); cb.ShowText("Kelas Aset");
            cb.SetTextMatrix(200, 610); cb.ShowText(":");
            cb.SetTextMatrix(210, 610); cb.ShowText(texts.KelasAsset);
            cb.SetFontAndSize(times_roman, 8);
            cb.SetTextMatrix(60, 600); cb.ShowText("(sesuai kelas aset yang berlaku)");

            float lastLine = document.PageSize.Height - document.TopMargin;
            AddParagraphWithHeader(texts.Content, document, cb, out lastLine);

            #region Table
            //TABLE
            var totalColumn = 7;
            PdfPTable table = new PdfPTable(totalColumn);
            var colorTableWhite = BaseColor.White;
            var colorTableBlack = BaseColor.Black;
            var fontTableArialBlack = FontFactory.GetFont(FontFactory.TIMES_ROMAN, 10, colorTableBlack);
            table.HorizontalAlignment = 0;
            table.TotalWidth = 470;
            table.LockedWidth = true;
            float[] widths = new float[] { 100, 100, 100, 100, 100, 100, 100 };
            table.SetWidths(widths);
            //TABLE HEADER
            // cell.Colspan = 3;

            List<JustificationBOQTable> list = new List<JustificationBOQTable>();
            for (int i = 0; i < 30; i++)
            {
                list.Add(new JustificationBOQTable() { GroupHarga = "Group Harga " + i, Item = "Item", MataUang = "Mata Uang", Kuantitas = "Kuantitas", HargaJasa = "Harga Jasa", HargaMaterial = "Harga Material", Total = "Total" });
            }
            AddRow(list, table, cb, document, totalColumn, lastLine);

            //FOOTER
            //PdfPCell total = new PdfPCell(new Paragraph("Total", fontTableArialBlack));
            //total.VerticalAlignment = PdfCell.ALIGN_MIDDLE;
            //total.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            //total.BackgroundColor = BaseColor.White;
            //total.BorderWidth = 1;
            //total.BorderColor = BaseColor.LightGray;
            //PdfPCell totalValue = new PdfPCell(new Paragraph("592.236.544", fontTableArialBlack));
            //totalValue.VerticalAlignment = PdfCell.ALIGN_MIDDLE;
            //totalValue.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            //totalValue.BackgroundColor = BaseColor.Yellow;
            //totalValue.BorderWidth = 1;
            //totalValue.BorderColor = BaseColor.LightGray;
            //total.Colspan = 3;
            //totalValue.Colspan = 2;
            //table.AddCell(total);
            //table.AddCell(totalValue);
            #endregion


            cb.EndText();
            #endregion
            document.Close();
            writer.Close();
        }

        /// <summary>
        /// Add Rows to Table Including Header and Footer
        /// </summary>
        /// <param name="listBOQ"></param>
        /// <param name="table"></param>
        /// <param name="cb"></param>
        /// <param name="document"></param>
        /// <param name="totalColumn"></param>
        /// <param name="startLine"></param>
        private void AddRow(List<JustificationBOQTable> listBOQ, PdfPTable table, PdfContentByte cb, Document document, int totalColumn, float? startLine)
        {
            int CurrentRow = 0;
            float DefaultStartLine = document.PageSize.Height - document.TopMargin;
            if (startLine == null) startLine = DefaultStartLine;
            foreach (var BOQ in listBOQ)
            {
                for (int i = 0; i < totalColumn; i++)
                {
                    var text = "";
                    switch (i + 1)
                    {
                        case 1:
                            text = BOQ.GroupHarga;
                            break;
                        case 2:
                            text = BOQ.Item;
                            break;
                        case 3:
                            text = BOQ.MataUang;
                            break;
                        case 4:
                            text = BOQ.Kuantitas;
                            break;
                        case 5:
                            text = BOQ.HargaJasa;
                            break;
                        case 6:
                            text = BOQ.HargaMaterial;
                            break;
                        case 7:
                            text = BOQ.Total;
                            break;
                        default:
                            text = "";
                            break;
                    }
                    PdfPCell Cell = new PdfPCell(new Paragraph(text, FontFactory.GetFont(FontFactory.TIMES_ROMAN, 10, BaseColor.Black)));
                    Cell.VerticalAlignment = PdfCell.ALIGN_MIDDLE;
                    Cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                    Cell.BackgroundColor = BaseColor.White;
                    Cell.BorderWidth = 1;
                    Cell.BorderColor = BaseColor.Black;
                    table.AddCell(Cell);
                }

                var rowHeight = table.GetRowHeight(CurrentRow);
                if (startLine <= rowHeight)
                {
                    cb.EndText();
                    document.NewPage();
                    cb.BeginText();
                    startLine = DefaultStartLine;
                    startLine = table.WriteSelectedRows(0, 1, 60, (float)startLine, cb);
                    startLine = table.WriteSelectedRows(CurrentRow, CurrentRow + 1, 60, (float)startLine, cb);
                }
                else
                {
                    startLine = table.WriteSelectedRows(CurrentRow, CurrentRow + 1, 60, (float)startLine, cb);
                }
                CurrentRow++;
            }
        }

        /// <summary>
        /// Add Paragraph(s) with Header. Header font will be BOLD.
        /// </summary>
        /// <param name="texts">List of Header and Paragraph(s).</param>
        /// <param name="document">Document Object.</param>
        /// <param name="cb">PdfContentByte Object.</param>
        /// <param name="spaceBetweenParagraf">Spacing Between Paragraphs.</param>
        /// <param name="spaceBeforeAdd">Space before adding Paragraph with Header.</param>
        /// <param name="fontSize">Font Size.</param>
        /// <param name="leading">Spacing Between Line text.</param>
        private void AddParagraphWithHeader(List<JustificationPdfDto> texts, Document document, PdfContentByte cb, out float lastLine, float spaceBetweenParagraf = 20, float spaceBetweenHeaderAndParagraf = 10, float spaceBeforeAdd = 20, float fontSize = 12, float leading = 15)
        {
            lastLine = 0;
            if (texts != null && texts.Count > 0)
            {
                var startLine = cb.Ytlm - spaceBeforeAdd;
                var DefaultstartLine = document.PageSize.Height - document.TopMargin;
                var font = FontFactory.GetFont(FontFactory.TIMES_ROMAN, fontSize, Font.NORMAL);
                var availableWidth = document.PageSize.Width - (document.LeftMargin + document.RightMargin);
                var defaultAlign = Element.ALIGN_JUSTIFIED;
                var align = defaultAlign;
                bool isHeader;


                ColumnText[] columns = new ColumnText[texts.Count * 2];

                for (int la = 0; la < texts.Count; la++)
                {
                    for (int lb = 0; lb < 2; lb++)
                    {
                        isHeader = false;
                        if (lb == 0)
                        {
                            align = Element.ALIGN_LEFT;
                            font = FontFactory.GetFont(FontFactory.TIMES_ROMAN, fontSize + 2, Font.BOLD);
                            isHeader = true;

                            //Check if next text (Body) can fill current page
                            if (la + 1 != texts.Count)
                            {
                                columns[la + 1] = new ColumnText(cb);
                                columns[la + 1].SetSimpleColumn(new Phrase(texts[la].Content, font), 60, startLine, 530, 36, leading, align);
                                var resultNext = columns[la + 1].Go(true);
                                if (ColumnText.HasMoreText(resultNext))
                                {
                                    cb.EndText();
                                    document.NewPage();
                                    cb.BeginText();
                                    startLine = DefaultstartLine;
                                }
                            }
                        }
                        else
                        {
                            align = defaultAlign;
                            font = FontFactory.GetFont(FontFactory.TIMES_ROMAN, fontSize, Font.NORMAL);
                        }

                        columns[la] = new ColumnText(cb);
                        columns[la].SetSimpleColumn(new Phrase(isHeader ? texts[la].Header : texts[la].Content, font), 60, startLine, 530, 36, leading, align);
                        var result = columns[la].Go(true);
                        if (ColumnText.HasMoreText(result))
                        {
                            cb.EndText();
                            document.NewPage();
                            cb.BeginText();
                            startLine = DefaultstartLine;
                        }
                        columns[la] = new ColumnText(cb);
                        columns[la].SetSimpleColumn(new Phrase(isHeader ? texts[la].Header : texts[la].Content, font), 60, startLine, 530, 36, leading, align);
                        columns[la].Go();

                        startLine -= columns[la].LinesWritten * leading + (isHeader ? spaceBetweenHeaderAndParagraf : spaceBetweenParagraf);
                        lastLine = startLine;
                    }
                }
            }
        }

    }
}
