﻿using App.Domain.Models.Core;
using App.Services.Core;
using MailKit.Net.Smtp;
using MimeKit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Helper
{
    public class MailingHelper
    {
        private readonly ViewRender _viewRender;
        private readonly IEmailArchieveService _emailArchieveService;
        private readonly ConfigHelper _config;
        private readonly CommonHelper _commonHelper;
        private readonly string _smtpUsername;
        private readonly string _smtpPassword;
        private readonly string _smtpServer;
        private readonly int _smtpPort;
        private readonly bool _smtpSsl;
        private readonly bool _smtpRequireAuthentication;
        private readonly int _maxTrySend;

        public MailingHelper(ViewRender viewRender,
            IEmailArchieveService emailArchieveService,
            ConfigHelper config,
            CommonHelper commonHelper)
        {
            _viewRender = viewRender;
            _emailArchieveService = emailArchieveService;
            _config = config;
            _commonHelper = commonHelper;
            _smtpUsername = _config.GetConfig("smtp.username");
            _smtpPassword = _config.GetConfig("smtp.password");
            _smtpServer = _config.GetConfig("smtp.server");
            _smtpPort = _config.GetConfigAsInt("smtp.port");
            _smtpSsl = _config.GetConfigAsBool("smtp.ssl");
            _smtpRequireAuthentication = _config.GetConfigAsBool("smtp.requires.authentication");
            _maxTrySend = _config.GetConfigAsInt("email.scheduler.max.send.try");
        }

        public EmailArchieve CreateEmail<TModel>(string from,
            string tos, 
            string subject, 
            string viewName, 
            TModel model,
            Dictionary<string, string> additionalData = null,
            string fromName = null,
            string replyTo = null,
            string replyToName = null,
            string plainMessage = null,
            string cc = null,
            string bcc = null)
        {
            var messages = _viewRender.Render(viewName, model, additionalData);

            var email = new EmailArchieve(from, tos, subject, messages, DateTime.Now)
            {
                FromName = fromName,
                Cc = cc,
                Bcc = bcc,
                ReplyTo = replyTo,
                ReplyToName = replyToName
            };

            var hasPlainText = !string.IsNullOrWhiteSpace(plainMessage);

            if (hasPlainText)
                email.PlainMessage = plainMessage;

            var result = _emailArchieveService.Add(email);

            return result;
        }

        public async Task<EmailArchieve> SendEmail(EmailArchieve email, bool resend = false)
        {
            if (email.IsSent && !resend)
                throw new Exception(string.Format("Email already sent at {0}",
                    _commonHelper.ToLongString(email.SentDate.Value)));

            //if(!email.IsSent && email.TrySentCount > _maxTrySend)
            //    throw new Exception($"Maximum try sending email excedeed, max try is {_maxTrySend}.");

            if (string.IsNullOrWhiteSpace(email.Tos))
                throw new ArgumentException("No to address provided");

            if (string.IsNullOrWhiteSpace(email.From))
                throw new ArgumentException("no from address provided");

            if (string.IsNullOrWhiteSpace(email.Subject))
                throw new ArgumentException("no subject provided");

            var hasPlainText = !string.IsNullOrWhiteSpace(email.PlainMessage);
            var hasHtml = !string.IsNullOrWhiteSpace(email.HtmlMessage);
            var fromName = email.FromName ?? "";
            var replyToName = email.ReplyToName ?? "";
            var tos = email.Tos.Split(',');
            var hasCc = !string.IsNullOrWhiteSpace(email.Cc);
            var hasBcc = !string.IsNullOrWhiteSpace(email.Bcc);

            if (!hasPlainText && !hasHtml)
                throw new ArgumentException("no message provided");

            var m = new MimeMessage();

            m.From.Add(new MailboxAddress(fromName, email.From));

            if (!string.IsNullOrWhiteSpace(email.ReplyTo))
                m.ReplyTo.Add(new MailboxAddress(replyToName, email.ReplyTo));

            foreach (var to in tos)
                m.To.Add(new MailboxAddress("", to));

            if (hasCc)
            {
                var ccs = email.Cc.Split(',');
                foreach (var cc in ccs)
                    m.Cc.Add(new MailboxAddress("", cc));
            }

            if (hasBcc)
            {
                var bccs = email.Bcc.Split(',');
                foreach (var bcc in bccs)
                    m.Bcc.Add(new MailboxAddress("", bcc));
            }

            m.Subject = email.Subject;

            var bodyBuilder = new BodyBuilder();

            if (hasPlainText)
                bodyBuilder.TextBody = email.PlainMessage;

            if (hasHtml)
                bodyBuilder.HtmlBody = email.HtmlMessage;

            m.Body = bodyBuilder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                try
                {
                    await client.ConnectAsync(
                            _smtpServer,
                            _smtpPort,
                            _smtpSsl)
                        .ConfigureAwait(false);

                    // Note: since we don't have an OAuth2 token, disable
                    // the XOAUTH2 authentication mechanism.
                    client.AuthenticationMechanisms.Remove("XOAUTH2");

                    // Note: only needed if the SMTP server requires authentication
                    if (_smtpRequireAuthentication)
                    {
                        await client.AuthenticateAsync(_smtpUsername, _smtpPassword)
                            .ConfigureAwait(false);
                    }

                    await client.SendAsync(m).ConfigureAwait(false);
                    await client.DisconnectAsync(true).ConfigureAwait(false);

                    email.SentDate = DateTime.Now;
                    email.IsSent = true;
                }
                catch (Exception ex)
                {
                    email.ExceptionSendingMessage = JsonConvert.SerializeObject(ex);
                    email.IsSent = false;
                }
                finally
                {
                    email.TrySentCount += 1;
                    email.LastTrySentDate = DateTime.Now;
                    email.LastUpdateTime = DateTime.Now;
                }
            }

            var result = _emailArchieveService.Update(email);

            return result;
        }

        public async Task<EmailArchieve> SendEmail(Guid id, bool resend = false)
        {
            var email = _emailArchieveService.GetById(id);

            if (email == null)
                throw new Exception("Email archieve not found");

            return await SendEmail(email, resend);
        }
    }
}
