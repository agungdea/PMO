﻿using App.Domain.Models.App;
using App.Domain.Models.Localization;
using App.Services.App;
using App.Services.Localization;
using Microsoft.AspNetCore.Hosting;
using Npgsql;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Linq;
using OfficeOpenXml.Style;
using App.Data.Repository;

namespace App.Helper
{
    public class ExcelHelper
    {
        private readonly ConfigHelper _config;
        private readonly CommonHelper _commonHelper;
        private readonly FileHelper _fileHelper;
        private readonly IHostingEnvironment _env;
        private readonly ILanguageService _languageService;
        private readonly IAreaMappingService _areaMappingService;
        private readonly IAkiCoverService _akicoverservice;
        private readonly IAkiValuationService _akivaluationservice;
        private readonly IHargaService _harga;
        private readonly IDesignatorService _designator;
        private readonly string _AkiName;
        private readonly string _Akipath;
        private readonly IWBSTreeService _wbstreeservice;
        private readonly IWBSBoQService _wbsboqservice;
        private readonly IRepository<Designator> _designatorRepository;
        private readonly IRepository<Harga> _hargaRepository;
        private readonly IRepository<WBSBoQ> _wbsboqrepo;
        private readonly IWBSCurrencyService _wbsCurrencyService;
        private readonly ICurrencyService _currencyservice;
        private readonly IWBSBoQGrantTotalService _wbsBoQGrantTotalService;
        private readonly IRepository<Currency> _currenciesRepository;
        private readonly IRepository<WBSCurrency> _wbscurrenciesRepository;
        private readonly IRepository<WBSTree> _wbstreeRepository;

        public ExcelHelper(ConfigHelper config,
            CommonHelper commonHelper,
            FileHelper fileHelper,
            IHostingEnvironment env,
            IAkiCoverService akicoverservice,
            IWBSTreeService wbstreeservice,
            IWBSBoQService wbsboqservice,
            IAkiValuationService akivaluationservice,
            IDesignatorService designator,
            IHargaService harga,
            IAreaMappingService areaMappingService,
            IRepository<Designator> designatorRepository,
            IRepository<Harga> hargaRepository,
            IWBSCurrencyService wbsCurrencyService,
            ICurrencyService currencyservice,
            IRepository<Currency> currenciesRepository,
            IRepository<WBSBoQ> wbsboqrepo,
            IWBSBoQGrantTotalService wbsBoQGrantTotalService,
             IRepository<WBSCurrency> wbscurrenciesRepository,
            IRepository<WBSTree> wbstreeRepository,
             ILanguageService languageService)
        {
            _config = config;
            _harga = harga;
            _designator = designator;
            _commonHelper = commonHelper;
            _akicoverservice = akicoverservice;
            _fileHelper = fileHelper;
            _env = env;
            _wbsBoQGrantTotalService = wbsBoQGrantTotalService;
            _currencyservice = currencyservice;
            _wbsboqrepo = wbsboqrepo;
            _wbstreeservice = wbstreeservice;
            _wbsboqservice = wbsboqservice;
            _languageService = languageService;
            _akivaluationservice = akivaluationservice;
            _areaMappingService = areaMappingService;
            _designatorRepository = designatorRepository;
            _hargaRepository = hargaRepository;
            _wbsCurrencyService = wbsCurrencyService;
            _Akipath = _config.GetConfig("aki.upload.directory");
            _AkiName = _config.GetConfig("aki.file.name");
            _currenciesRepository = currenciesRepository;
            _wbscurrenciesRepository = wbscurrenciesRepository;
            _wbstreeRepository = wbstreeRepository;
        }
        public string rewrite()
        {
            //read and write file jika diperlukan  
            var sWebRootFolder = _env.WebRootPath;
            var sFileName = @"assets/AKITelkomERP.xlsx";
            var file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            var package = new ExcelPackage(file);
            var ws = package.Workbook.Worksheets[1];
            ws.Cells[5, 6].Value = "Program KB";
            var ws2 = package.Workbook.Worksheets[6];
            ws2.Cells[1, 1].Value = "Coba dari sini";
            package.SaveAs(file);
            return "ok";
        }

        public string Export(string connString,
            string command,
            Dictionary<string, int> widths = null)
        {
            using (var conn = new NpgsqlConnection(connString))
            {
                conn.Open();

                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;

                    cmd.CommandText = command;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var sWebRootFolder = _env.WebRootPath;
                        var sFileName = @"upload/demo.xlsx";
                        var file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));

                        if (file.Exists)
                        {
                            file.Delete();
                            file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
                        }

                        using (var package = new ExcelPackage(file))
                        {
                            var columns = reader.GetColumnSchema();

                            var worksheet = package.Workbook.Worksheets.Add("sheet1");
                            var columnIndex = 1;
                            var rowIndex = 1;
                            var totalColumn = columns.Count;

                            foreach (var column in columns)
                            {
                                worksheet.Cells[rowIndex, columnIndex].Value = column.ColumnName;
                                if (widths != null)
                                {
                                    var width = 50;
                                    widths.TryGetValue(column.ColumnName, out width);
                                    worksheet.Column(columnIndex).Width = width;
                                }
                                columnIndex++;
                            }

                            rowIndex++;

                            while (reader.Read())
                            {
                                for (var i = 0; i < totalColumn; i++)
                                {
                                    columnIndex = i + 1;
                                    if (reader.IsDBNull(i))
                                    {
                                        worksheet.Cells[rowIndex, columnIndex].Value = "";
                                    }
                                    else
                                    {
                                        try
                                        {
                                            switch (reader.GetFieldType(i).FullName)
                                            {
                                                case "System.Int16":
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        reader.GetInt16(i);
                                                    break;
                                                case "System.Int32":
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        reader.GetInt32(i);
                                                    break;
                                                case "System.Int64":
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        reader.GetInt64(i);
                                                    break;
                                                case "System.Double":
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        reader.GetDouble(i);
                                                    break;
                                                case "System.Decimal":
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        reader.GetDecimal(i);
                                                    break;
                                                case "System.Float":
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        reader.GetFloat(i);
                                                    break;
                                                case "System.Boolean":
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        reader.GetBoolean(i);
                                                    break;
                                                case "System.DateTime":
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        _commonHelper
                                                            .ToLongString(reader.GetDateTime(i));
                                                    break;
                                                default:
                                                    worksheet.Cells[rowIndex, columnIndex].Value =
                                                        reader.GetString(i);
                                                    break;
                                            }
                                        }
                                        catch (System.Exception)
                                        {
                                            worksheet.Cells[rowIndex, columnIndex].Value =
                                                "Failed to parsing data";
                                        }
                                    }
                                }

                                rowIndex++;
                            }
                            worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                            package.Save();
                        }
                    }
                }
            }

            return "";
        }

        public ExcelPackage OpenExcel(string source, bool isFullPath = false)
        {
            if (isFullPath)
            {
                return new ExcelPackage(new FileInfo(source));
            }

            source = source.StartsWith("/") ? source.Substring(1) : source;
            var sWebRootFolder = _env.WebRootPath;

            var file = new FileInfo(Path.Combine(sWebRootFolder, source));

            var package = new ExcelPackage(file);
            return package;
        }

        public List<LocaleResource> GetResources(string source, bool isFullPath = false)
        {
            var resources = new List<LocaleResource>();

            using (var package = OpenExcel(source, isFullPath))
            {
                var ws = package.Workbook.Worksheets[1];
                var rowCount = ws.Dimension.Rows;
                var colCount = ws.Dimension.Columns;

                var languages = new Dictionary<int, int>();

                for (var col = 3; col <= colCount; col++)
                {
                    var langCulture = ws.Cells[1, col].Value.ToString();
                    var language = _languageService.GetByCulture(langCulture);
                    if (language == null)
                        continue;

                    languages.Add(col, language.Id);
                }

                for (var row = 2; row <= rowCount; row++)
                {
                    var name = ws.Cells[row, 2].Value.ToString();

                    for (var col = 3; col <= colCount; col++)
                    {
                        if (!languages.ContainsKey(col))
                            continue;

                        var item = new LocaleResource()
                        {
                            LanguageId = languages[col],
                            ResourceName = name,
                            ResourceValue = ws.Cells[row, col].Value.ToString(),
                            CreatedAt = DateTime.Now
                        };

                        resources.Add(item);
                    }
                }
            }

            return resources;
        }
        public AkiCover UploadExcel(string source)
        {
            var termsList = new AkiCover();
            if (!string.IsNullOrWhiteSpace(source))
            {
                var excelSource = Path.Combine(_env.WebRootPath, source);


                using (var package = OpenExcel(excelSource, false))
                {
                    var ws = package.Workbook.Worksheets[1];
                    var wsValuation = package.Workbook.Worksheets[6];
                    var rowCount = ws.Dimension.Rows;
                    var colCount = ws.Dimension.Columns;
                    if (ws.Cells[5, 5].Value != null)
                    {
                        termsList.ProposalId = ws.Cells[5, 5].Value.ToString();
                    }
                    if (ws.Cells[7, 5].Value != null)
                    {
                        int Lop;
                        var JumlahLop = Regex.Match(ws.Cells[7, 5].Value.ToString(), @"\d+");
                        string JumlahLopAfterCheck = int.TryParse(JumlahLop.ToString(), out Lop) == true ? JumlahLop.ToString() : "0";
                        termsList.JumlahLop = int.Parse(JumlahLopAfterCheck);
                    }
                    if (ws.Cells[8, 5].Value != null)
                    {
                        termsList.LopId = ws.Cells[8, 5].Value.ToString();
                    }
                    if (ws.Cells[9, 5].Value != null)
                    {
                        termsList.Location = ws.Cells[9, 5].Value.ToString();
                    }
                    if (ws.Cells[10, 5].Value != null)
                    {
                        termsList.Kapasitas = ws.Cells[10, 5].Value.ToString();
                    }
                    if (ws.Cells[11, 5].Value != null)
                    {
                        termsList.PenanggungJawab = ws.Cells[11, 5].Value.ToString();
                    }
                    if (ws.Cells[12, 5].Value != null)
                    {
                        termsList.NomorAkun = ws.Cells[12, 5].Value.ToString();
                    }
                    if (ws.Cells[13, 5].Value != null)
                    {
                        Decimal var;
                        var NilaiProgram = Decimal.TryParse(ws.Cells[13, 5].Value.ToString(), out var) == true ? ws.Cells[13, 5].Value.ToString() : "0";
                        termsList.NilaiProgram = Decimal.Parse(NilaiProgram.ToString());
                    }
                    if (ws.Cells[14, 5].Value != null)
                    {
                        termsList.CapexLine = ws.Cells[14, 5].Value.ToString();
                    }
                    if (ws.Cells[15, 5].Value != null)
                    {
                        termsList.WaktuPenyelesaian = ws.Cells[15, 5].Value.ToString();
                    }
                    if (ws.Cells[16, 5].Value != null)
                    {
                        termsList.RencanaSelesai = ws.Cells[16, 5].Value.ToString();
                    }
                    if (ws.Cells[17, 5].Value != null)
                    {
                        termsList.PemilihanTeknologi = ws.Cells[17, 5].Value.ToString();
                    }
                    if (ws.Cells[18, 5].Value != null)
                    {
                        termsList.Usulan = ws.Cells[18, 5].Value.ToString();
                    }
                    if (ws.Cells[19, 5].Value != null)
                    {
                        termsList.Alasan = ws.Cells[19, 5].Value.ToString();
                    }
                    if (ws.Cells[20, 5].Value != null)
                    {
                        Decimal IRR;
                        var IRRvalue = Decimal.TryParse(ws.Cells[20, 5].Value.ToString(), out IRR) == true ? ws.Cells[20, 5].Value.ToString() : "0";
                        decimal afterIRR = Math.Round(decimal.Parse(IRRvalue.ToString()), 2, MidpointRounding.AwayFromZero);
                        termsList.IRR = afterIRR;
                        if (afterIRR < 1)
                        {
                            termsList.IRR = afterIRR * 100;
                        }

                    }
                    if (ws.Cells[21, 5].Value != null)
                    {
                        decimal value;
                        var NPV = Decimal.TryParse(ws.Cells[21, 5].Value.ToString(), out value) == true ? ws.Cells[21, 5].Value.ToString() : "0";
                        termsList.NPV = Math.Round(Decimal.Parse(NPV), MidpointRounding.AwayFromZero);
                    }
                    if (wsValuation.Cells[22, 3].Value != null)
                    {
                        Decimal paybackcekvalue;
                        var payback = Regex.Match(wsValuation.Cells[22, 3].Value.ToString(), @"\d+");
                        string payaftervalue = Decimal.TryParse(payback.ToString(), out paybackcekvalue) == true ? payback.ToString() : "0";
                        termsList.PayBackPeriode = int.Parse(payaftervalue.ToString());
                    }
                    if (wsValuation.Cells[12, 2].Value != null)
                    {
                        int taxesvar;
                        var taxes = Regex.Match(wsValuation.Cells[12, 2].Value.ToString(), @"\d+");
                        string taxesAftervalue = int.TryParse(taxes.ToString(), out taxesvar) == true ? taxes.ToString() : "0";
                        termsList.Taxes = int.Parse(taxesAftervalue.ToString());
                    }
                    if (ws.Cells[23, 5].Value != null)
                    {
                        decimal BCR;
                        string BCRcheckvalue = Decimal.TryParse(ws.Cells[23, 5].Value.ToString(), out BCR) == true ? ws.Cells[23, 5].Value.ToString() : "0";
                        decimal BCRaftervalue = Math.Round(decimal.Parse(BCRcheckvalue), 2, MidpointRounding.AwayFromZero);
                        termsList.BCR = BCRaftervalue;
                    }
                    if (ws.Cells[24, 5].Value != null)
                    {
                        termsList.CAGR = ws.Cells[24, 5].Value.ToString();
                    }
                    if (ws.Cells[25, 5].Value != null)
                    {
                        float DiscountRate;
                        var DiscountRateval = float.TryParse(ws.Cells[25, 5].Value.ToString(), out DiscountRate) == true ? ws.Cells[25, 5].Value.ToString() : "0";
                        termsList.DiscountRate = (float)Math.Round(float.Parse(DiscountRateval.ToString()) * 100f) / 100f;
                        if (termsList.DiscountRate < 1)
                        {
                            termsList.DiscountRate = termsList.DiscountRate * 100;
                        }
                    }

                    if (ws.Cells[26, 5].Value != null)
                    {
                        termsList.ARPU = ws.Cells[26, 5].Value.ToString();
                    }
                }
                return termsList;
            }

            return termsList;
        }

        public List<AreaMapping> GetArea(string source, bool isFullPath = false)
        {
            var areaSto = new List<AreaMapping>();

            using (var package = OpenExcel(source, isFullPath))
            {
                var ws = package.Workbook.Worksheets[1];
                var rowCount = ws.Dimension.Rows;
                var colCount = ws.Dimension.Columns;

                for (var row = 2; row <= rowCount; row++)
                {
                    var item = new AreaMapping()
                    {

                        AreaRegional = ws.Cells[row, 1].Value.ToString(),
                        AreaWitel = ws.Cells[row, 2].Value.ToString(),
                        AreaDatel = ws.Cells[row, 3].Value.ToString(),
                        AreaStoName = ws.Cells[row, 4].Value.ToString(),
                        AreaSto = ws.Cells[row, 5].Value.ToString(),
                        AreaKode = ws.Cells[row, 6].Value.ToString(),
                        CreatedAt = DateTime.Now
                    };

                    areaSto.Add(item);
                }
            }

            return areaSto;
        }
        public string Validateexcel(string source)
        {


            var valuation = new List<AkiValuation>();
            var file = Path.Combine(_env.WebRootPath, source);
            using (var package = OpenExcel(file, false))
            {

                var ws1 = package.Workbook.Worksheets[1];
                var ws6 = package.Workbook.Worksheets[6];
                #region CekNull
                if (package.Workbook.Worksheets.Count() != 7)
                {
                    return ("Jumlah Worksheet tidak seseuai ketentuan, harap download tamplate");
                }
                if (ws1.Cells[5, 2].Value == null || ws1.Cells[6, 2].Value == null || ws1.Cells[7, 2].Value == null || ws1.Cells[8, 2].Value == null)
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws1.Cells[9, 2].Value == null || ws1.Cells[10, 2].Value == null || ws1.Cells[11, 2].Value == null || ws1.Cells[12, 2].Value == null)
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws1.Cells[13, 2].Value == null || ws1.Cells[14, 2].Value == null || ws1.Cells[15, 2].Value == null || ws1.Cells[16, 2].Value == null)
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws1.Cells[17, 2].Value == null || ws1.Cells[18, 2].Value == null || ws1.Cells[19, 2].Value == null || ws1.Cells[20, 3].Value == null)
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws1.Cells[21, 3].Value == null || ws1.Cells[22, 3].Value == null || ws1.Cells[23, 3].Value == null || ws1.Cells[24, 3].Value == null || ws1.Cells[25, 3].Value == null || ws1.Cells[26, 3].Value == null)
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws6.Cells[7, 2].Value == null || ws6.Cells[8, 2].Value == null || ws6.Cells[9, 2].Value == null || ws6.Cells[10, 2].Value == null)
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                if (ws6.Cells[11, 2].Value == null || ws6.Cells[12, 2].Value == null || ws6.Cells[13, 2].Value == null || ws6.Cells[14, 2].Value == null)
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                if (ws6.Cells[15, 2].Value == null || ws6.Cells[16, 2].Value == null || ws6.Cells[17, 2].Value == null || ws6.Cells[18, 2].Value == null)
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                if (ws6.Cells[19, 2].Value == null || ws6.Cells[20, 2].Value == null || ws6.Cells[21, 2].Value == null || ws6.Cells[22, 2].Value == null)
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                #endregion
                #region Cek Value Content
                if (ws1.Cells[5, 2].Value.ToString() != "ID Proposal" || ws1.Cells[6, 2].Value.ToString() != "Nama Program" || ws1.Cells[7, 2].Value.ToString() != "Jumlah LOP" || ws1.Cells[8, 2].Value.ToString() != "ID LOP (sesuai jumlah LOP)")
                {

                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }

                if (ws1.Cells[9, 2].Value.ToString() != "Lokasi / Site Program" || ws1.Cells[10, 2].Value.ToString() != "Kapasitas" || ws1.Cells[11, 2].Value.ToString() != "Penanggung Jawab" || ws1.Cells[12, 2].Value.ToString() != "Nomor Account")
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws1.Cells[13, 2].Value.ToString() != "Nilai Program" || ws1.Cells[14, 2].Value.ToString() != "Capex / Line" || ws1.Cells[15, 2].Value.ToString() != "Waktu Penyelesaian" || ws1.Cells[16, 2].Value.ToString() != "Rencana Selesai")
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws1.Cells[17, 2].Value.ToString() != "Pemilihan Teknologi" || ws1.Cells[18, 2].Value.ToString() != "Usulan pola kemitraan" || ws1.Cells[19, 2].Value.ToString() != "Alasan keberadaan proyek" || ws1.Cells[20, 3].Value.ToString() != "IRR")
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws1.Cells[21, 3].Value.ToString() != "NPV" || ws1.Cells[22, 3].Value.ToString() != "PBP" || ws1.Cells[23, 3].Value.ToString() != "BCR" || ws1.Cells[24, 3].Value.ToString() != "CAGR" || ws1.Cells[25, 3].Value.ToString() != "Discount rate" || ws1.Cells[26, 3].Value.ToString() != "ARPU")
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (ws6.Cells[7, 2].Value.ToString() != "Revenue" || ws6.Cells[8, 2].Value.ToString() != "Expenses (w/o depreciation)" || ws6.Cells[9, 2].Value.ToString() != "EBITDA" || ws6.Cells[10, 2].Value.ToString() != "Depreciation")
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                if (ws6.Cells[11, 2].Value.ToString() != "EBIT" || ws6.Cells[12, 2].Value.ToString() != "Taxes (30%)" || ws6.Cells[13, 2].Value.ToString() != "NET INCOME" || ws6.Cells[14, 2].Value.ToString() != "Add back Depresiation")
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                if (ws6.Cells[15, 2].Value.ToString() != "Invesment" || ws6.Cells[16, 2].Value.ToString() != "Net Cash flow" || ws6.Cells[17, 2].Value.ToString() != "Cumulative Net Cash flow" || ws6.Cells[18, 2].Value.ToString() != "Interest Rate")
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                if (ws6.Cells[19, 2].Value.ToString() != "NPV" || ws6.Cells[20, 2].Value.ToString() != "IRR" || ws6.Cells[21, 2].Value.ToString() != "BCR" || ws6.Cells[22, 2].Value.ToString() != "Payback Periode")
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                #endregion
                #region Cek Row Count
                var wscount1 = ws1.Dimension.End.Row;
                var wscount6 = ws6.Dimension.End.Row;

                if (wscount1 != 28)
                {
                    return ("Aki Cover tidak sesuai Tamplate , harap download template");
                }
                if (wscount6 != 24)
                {
                    return ("Aki Valuation tidak sesuai Tamplate , harap download template");
                }
                #endregion
            }

            return "Ok";
        }

        public string ExportBOQ(Guid WbsTreeId)
        {
            var sWebRootFolder = _env.WebRootPath;
            var sFileName = @"download\" + WbsTreeId.ToString() + ".xlsx";
            var file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            if (file.Exists)
            {
                file.Delete();
                file = new FileInfo(Path.Combine(sWebRootFolder, sFileName));
            }
            using (ExcelPackage package = new ExcelPackage(file))
            {
                var listdatatree = new List<WBSTree>();
                var datatrees = getchild(WbsTreeId, listdatatree);
                if (datatrees == null)
                {
                    return "not ok";
                }
                if (datatrees.Count() <= 0)
                {
                    return "not ok";
                }
                var a = 1;
                foreach (var datatree in datatrees)
                {
                    if (datatree.GrupHarga == null)
                    {
                        continue;
                    }
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(datatree.Lokasi + "_Sheet" + a);
                    var data = from harga in _hargaRepository.Table
                               join designator in _designatorRepository.Table
                                    on harga.IdDesignator equals designator.IdDesignator
                               join boq in _wbsboqrepo.Table
                                    on new { harga.IdDesignator, Grup = harga.Lokasi, Id = datatree.Id } equals
                                       new { boq.IdDesignator, Grup = boq.GrupHarga, Id = boq.IdWBSTree } into boqHarga
                               from boq in boqHarga.DefaultIfEmpty()
                               where harga.Lokasi == datatree.GrupHarga
                                    || (harga.Lokasi != datatree.GrupHarga && boq != null)
                               select new
                               {
                                   designator = harga.IdDesignator,
                                   grupharga = harga.Lokasi,
                                   deskripsi = designator.Deskripsi,
                                   satuan = designator.Satuan,
                                   MataUang = harga.MataUang,
                                   material = harga.HargaMaterial,
                                   jasa = harga.HargaJasa,
                                   idwbstree = datatree.Id,
                                   jumlah = boq == null ? 0 : boq.Qty
                               };
                    var datax = data.ToList();
                    worksheet.Cells["A1"].Value = "WBS ID :";
                    worksheet.Cells["B1"].Value = datatree.Id;
                    for (var x = 0; x < datax.Count(); x++)
                    {
                        worksheet.Cells[x + 5, 2].Value = datax[x].designator;
                        worksheet.Cells[x + 5, 3].Value = datax[x].deskripsi;
                        worksheet.Cells[x + 5, 4].Value = datax[x].satuan;
                        worksheet.Cells[x + 5, 5].Value = datax[x].material;
                        worksheet.Cells[x + 5, 6].Value = datax[x].jasa;
                        worksheet.Cells[x + 5, 7].Value = datax[x].MataUang;
                        worksheet.Cells[x + 5, 8].Value = datax[x].jumlah;
                    }
                    worksheet.Cells["B3:B4"].Merge = true;
                    worksheet.Cells["B3:B4"].Value = "Designator";
                    worksheet.Cells["C3:C4"].Merge = true;
                    worksheet.Cells["C3:C4"].Value = "Deskripsi";
                    worksheet.Cells["D3:D4"].Merge = true;
                    worksheet.Cells["D3:D4"].Value = "Satuan";
                    worksheet.Cells["E3:F3"].Merge = true;
                    worksheet.Cells["E3:F3"].Value = datatree.GrupHarga;
                    worksheet.Cells["E4"].Value = "Material";
                    worksheet.Cells["F4"].Value = "Jasa";
                    worksheet.Cells["G3:G4"].Merge = true;
                    worksheet.Cells["G3:G4"].Value = "Mata Uang";
                    worksheet.Cells["H3:H4"].Merge = true;
                    worksheet.Cells["H3:H4"].Value = "Jumlah";
                    var sheetdatatable = datax.Count() + 4;
                    worksheet.Cells["B3:H" + sheetdatatable.ToString()].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    worksheet.Cells["B3:H" + sheetdatatable.ToString()].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells["B3:H" + sheetdatatable.ToString()].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells["B3:H" + sheetdatatable.ToString()].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells["B3:H" + sheetdatatable.ToString()].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells["B3:H" + sheetdatatable.ToString()].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["B3:H" + sheetdatatable.ToString()].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#f0f3f5"));
                    a++;
                }
                package.Save();
            }

            return @"/download/" + WbsTreeId.ToString() + ".xlsx";
        }
        private List<WBSTree> getchild(Guid wbsid, List<WBSTree> data)
        {
            //   var data = new List<WBSTree>();
            var datatmp = _wbstreeservice.GetAll().Where(x => x.ParentId == wbsid);
            if (datatmp.Count() > 0)
            {
                var datatmpchild = datatmp.ToList();
                for (var x = 0; x < datatmpchild.Count(); x++)
                {
                    if (datatmpchild[x].Type == WBS.WBS_TYPE_LOCATION)
                    {
                        data.Add(datatmpchild[x]);
                    }
                    else
                    {
                        this.getchild(datatmpchild[x].Id, data);
                    }
                }
            }
            return data;
        }
        public List<string> ValidasiUploadMataUangBOQ(string source)
        {
            var listmatauang = new List<string>();
            var file = Path.Combine(_env.WebRootPath, source);
            var jumlahcuency = 0;
            using (var package = OpenExcel(file, false))
            {
                if (package.Workbook.Worksheets == null)
                {
                    return listmatauang;

                }
                for (var sheet = 1; sheet <= package.Workbook.Worksheets.Count(); sheet++)
                {
                    var worksheet = package.Workbook.Worksheets[sheet];
                    worksheet.Cells["E3:F3"].Merge = true;
                    var rowCount = worksheet.Dimension.Rows;
                    var colCount = worksheet.Dimension.End.Column;
                    if (rowCount == 0)
                    {
                        return listmatauang;

                    }
                    for (var row = 5; row <= rowCount; row++)
                    {
                        if (worksheet.Cells[row, 7].Value != null)
                        {
                            var matauang = worksheet.Cells[row, 7].Value.ToString();

                            if (matauang != Currency.CurrencyRp)
                            {
                                var datamatauang = _currenciesRepository.Table.First(x => x.CurrencyValue == matauang);

                                if (datamatauang != null)
                                {

                                    if (worksheet.Cells["B1"].Value != null)
                                    {
                                        Guid newGuid;
                                        if (Guid.TryParse(worksheet.Cells["B1"].Value.ToString(), out newGuid) == true)
                                        {
                                            var WbsTreeId = Guid.Parse(worksheet.Cells["B1"].Value.ToString());
                                            var datawbsid = _wbstreeRepository.Table.First(x => x.Id == WbsTreeId);
                                            if (datawbsid != null)
                                            {
                                                var datawbscurrency = _wbscurrenciesRepository.Table.Where(x => x.CurrencyId.Equals(datamatauang.Id) && x.WBSId.Equals(datawbsid.IdWBS));

                                                if (worksheet.Cells[row, 8].Value != null)
                                                {
                                                    if (worksheet.Cells[row, 8].Value.ToString() != "0")
                                                    {
                                                        if (datawbscurrency.Count() == 0)
                                                        {
                                                            if (listmatauang.Count() > 0)
                                                            {
                                                             
                                                                foreach (var item in listmatauang)
                                                                {
                                                                    if (item != matauang)
                                                                    {
                                                                        jumlahcuency++;
                                                                    }
                                                                }
                                                                if (!listmatauang.Contains(matauang))
                                                                {
                                                                    listmatauang.Add(matauang);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                listmatauang.Add(matauang);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return listmatauang;
          
        }
      

        public Boolean ValidasiImportBOQ(string source, string userId)
        {
            var file = Path.Combine(_env.WebRootPath, source);
            using (var package = OpenExcel(file, false))
            {
                if (package.Workbook.Worksheets == null)
                {
                    return false;

                }
                for (var sheet = 1; sheet <= package.Workbook.Worksheets.Count(); sheet++)
                {
                    var worksheet = package.Workbook.Worksheets[sheet];
                    worksheet.Cells["E3:F3"].Merge = true;
                    var rowCount = worksheet.Dimension.Rows;
                    var colCount = worksheet.Dimension.End.Column;
                    if (rowCount == 0)
                    {
                        return false;
                    }
                    for (var row = 5; row <= rowCount; row++)
                    {
                        if (worksheet.Cells["B1"].Value == null)
                        {
                            return false;
                        }
                        Guid newGuid;
                        if (Guid.TryParse(worksheet.Cells["B1"].Value.ToString(), out newGuid) == false)
                        {
                            return false;
                        }
                        if (worksheet.Cells[row, 5].Value == null)
                        {
                            return false;
                        }
                        Decimal newdecimal1;
                        if (Decimal.TryParse(worksheet.Cells[row, 5].Value.ToString(), out newdecimal1) == false)
                        {
                            return false;
                        }
                        Decimal newdecimal2;
                        if (worksheet.Cells[row, 6].Value == null)
                        {
                            return false;
                        }
                        if (Decimal.TryParse(worksheet.Cells[row, 6].Value.ToString(), out newdecimal2) == false)
                        {
                            return false;
                        }
                        int newint;
                        if (worksheet.Cells[row, 8].Value == null)
                        {
                            return false;
                        }
                        if (int.TryParse(worksheet.Cells[row, 8].Value.ToString(), out newint) == false)
                        {
                            return false;
                        }
                    }
                }

            }

            return true;
        }
        public string ImportBOQ(string source, string userId)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return "no";
            }
            var file = Path.Combine(_env.WebRootPath, source);
            using (var package = OpenExcel(file, false))
            {
                for (var sheet = 1; sheet <= package.Workbook.Worksheets.Count(); sheet++)
                {

                    var worksheet = package.Workbook.Worksheets[sheet];
                    worksheet.Cells["E3:F3"].Merge = true;
                    var rowCount = worksheet.Dimension.Rows;
                    var colCount = worksheet.Dimension.End.Column;
                    for (var row = 5; row <= rowCount; row++)
                    {
                        var item = new WBSBoQ()
                        {
                            IdWBSTree = worksheet.Cells["B1"].Value == null ? new Guid() : Guid.Parse(worksheet.Cells["B1"].Value.ToString()),
                            IdDesignator = worksheet.Cells[row, 2].Value == null ? null : worksheet.Cells[row, 2].Value.ToString(),
                            GrupHarga = worksheet.Cells[3, 5].Value == null ? null : worksheet.Cells[3, 5].Value.ToString(),
                            CreatedAt = DateTime.Now,
                            HargaMaterial = worksheet.Cells[row, 5].Value == null ? 0 : Decimal.Parse(worksheet.Cells[row, 5].Value.ToString()),
                            HargaJasa = worksheet.Cells[row, 6].Value == null ? 0 : Decimal.Parse(worksheet.Cells[row, 6].Value.ToString()),
                            OtherInfo = worksheet.Cells[row, 7].Value == null ? null : worksheet.Cells[row, 7].Value.ToString(),
                            Qty = worksheet.Cells[row, 8].Value == null ? 0 : int.Parse(worksheet.Cells[row, 8].Value.ToString()),

                        };
                        if (worksheet.Cells[row, 8].Value == null || item.Qty == 0 || item.GrupHarga == null || worksheet.Cells["B1"].Value == null)
                        {
                            if (item.Qty == 0)
                            {
                                if (_wbsboqservice.Exist(item.IdWBSTree, item.IdDesignator, item.GrupHarga))
                                {
                                    try
                                    {
                                        var dataitemwbsboq = _wbsboqrepo
                                            .Table
                                            .First(x => x.IdWBSTree == item.IdWBSTree
                                                && x.IdDesignator == item.IdDesignator
                                                && x.GrupHarga == item.GrupHarga);
                                        _wbsboqrepo.Delete(dataitemwbsboq);
                                    }
                                    catch (Exception)
                                    {
                                        continue;
                                    }

                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                item.CreatedBy = userId;
                                _wbsboqservice.AddOrUpdate(item);
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }
                    }
                }
            }
            return "ok";

        }
        public List<AkiValuation> GetExcelAkivaluation(string source)
        {

            var valuation = new List<AkiValuation>();
            var file = Path.Combine(_env.WebRootPath, source);
            using (var package = OpenExcel(file, false))
            {
                var ws = package.Workbook.Worksheets[6];
                var rowCount = ws.Dimension.Rows;
                var colCount = ws.Dimension.Columns;
                int yearvar;
                for (var coll = 3; coll <= 13; coll++)
                {
                    decimal globalcekvar, pen;
                    string ratapendapatanstring = ws.Cells[13, coll].Value == null ? null : ws.Cells[13, coll].Value.ToString();
                    string depresiasistring = ws.Cells[10, coll].Value == null ? null : ws.Cells[10, coll].Value.ToString();
                    var depresiasi = decimal.TryParse(depresiasistring, out pen) == false ? (decimal?)null : decimal.Parse(depresiasistring);
                    var rataratapendapatan = decimal.TryParse(ratapendapatanstring, out pen) == false ? (decimal?)null : decimal.Parse(ratapendapatanstring);
                    var item = new AkiValuation()
                    {

                        Year = ws.Cells[6, coll].Value == null ? (int?)null : int.TryParse(ws.Cells[6, coll].Value.ToString(), out yearvar) == false ? (int?)null : int.Parse(ws.Cells[6, coll].Value.ToString()),
                        Revenue = ws.Cells[7, coll].Value == null ? (decimal?)null : decimal.TryParse(ws.Cells[7, coll].Value.ToString(), out globalcekvar) == false ? (decimal?)null : decimal.Parse(ws.Cells[7, coll].Value.ToString()),
                        Expenses = ws.Cells[8, coll].Value == null ? (decimal?)null : decimal.TryParse(ws.Cells[8, coll].Value.ToString(), out globalcekvar) == false ? (decimal?)null : decimal.Parse(ws.Cells[8, coll].Value.ToString()),
                        Ebitda = ws.Cells[9, coll].Value == null ? (decimal?)null : decimal.TryParse(ws.Cells[9, coll].Value.ToString(), out globalcekvar) == false ? (decimal?)null : decimal.Parse(ws.Cells[9, coll].Value.ToString()),
                        Depreciation = depresiasi,
                        Ebit = ws.Cells[11, coll].Value == null ? (decimal?)null : decimal.TryParse(ws.Cells[11, coll].Value.ToString(), out globalcekvar) == false ? (decimal?)null : decimal.Parse(ws.Cells[11, coll].Value.ToString()),
                        Taxes = ws.Cells[12, coll].Value == null ? (decimal?)null : decimal.TryParse(ws.Cells[12, coll].Value.ToString(), out globalcekvar) == false ? (decimal?)null : decimal.Parse(ws.Cells[12, coll].Value.ToString()),
                        NetIncome = rataratapendapatan,
                        AddBackDepresiation = ws.Cells[14, coll].Value == null ? (decimal?)null : decimal.TryParse(ws.Cells[14, coll].Value.ToString(), out globalcekvar) == false ? (decimal?)null : decimal.Parse(ws.Cells[14, coll].Value.ToString()),
                        NetCashFlow = ws.Cells[16, coll].Value == null ? (decimal?)null : decimal.TryParse(ws.Cells[16, coll].Value.ToString(), out globalcekvar) == false ? (decimal?)null : decimal.Parse(ws.Cells[16, coll].Value.ToString()),
                        CumulativeNetCashFlow = ws.Cells[17, coll].Value == null ? (decimal?)null : decimal.TryParse(ws.Cells[17, coll].Value.ToString(), out globalcekvar) == false ? (decimal?)null : decimal.Parse(ws.Cells[17, coll].Value.ToString()),
                    };

                    if (item.Year != (int?)null)
                    {
                        if (ws.Cells[6, coll].Value != null)
                        {
                            valuation.Add(item);
                        }
                    }

                }

            }


            return valuation;
        }
        public Boolean UpdateExcelAkivaluation(string source, Guid idAki)
        {
            var getallvaluation = _akivaluationservice.GetAll().Where(x => x.AkiId.Equals(idAki));
            if (getallvaluation.Count() < 0)
            {
                return false;
            }
            foreach (var val in getallvaluation)
            {
                _akivaluationservice.Delete(val);
            }
            var valuation = new List<AkiValuation>();
            var file = Path.Combine(_env.WebRootPath, source);
            using (var package = OpenExcel(file, false))
            {
                var ws = package.Workbook.Worksheets[6];
                var rowCount = ws.Dimension.Rows;
                var colCount = ws.Dimension.End.Column;
                int yearvar;

                for (var coll = 3; coll <= 13; coll++)
                {
                    decimal globalcekvar, pen;
                    string ratapendapatanstring = ws.Cells[13, coll].Value == null ? "0" : ws.Cells[13, coll].Value.ToString();
                    string depresiasistring = ws.Cells[10, coll].Value == null ? "0" : ws.Cells[10, coll].Value.ToString();
                    var depresiasi = decimal.TryParse(depresiasistring, out pen) == false ? 0 : decimal.Parse(depresiasistring);
                    var rataratapendapatan = decimal.TryParse(ratapendapatanstring, out pen) == false ? 0 : decimal.Parse(ratapendapatanstring);


                    var item = new AkiValuation()
                    {

                        Year = ws.Cells[6, coll].Value == null ? 0 : int.TryParse(ws.Cells[6, coll].Value.ToString(), out yearvar) == false ? 0 : int.Parse(ws.Cells[6, coll].Value.ToString()),
                        Revenue = ws.Cells[7, coll].Value == null ? 0 : Decimal.TryParse(ws.Cells[7, coll].Value.ToString(), out globalcekvar) == false ? 0 : decimal.Parse(ws.Cells[7, coll].Value.ToString()),
                        Expenses = ws.Cells[8, coll].Value == null ? 0 : Decimal.TryParse(ws.Cells[8, coll].Value.ToString(), out globalcekvar) == false ? 0 : decimal.Parse(ws.Cells[8, coll].Value.ToString()),
                        Ebitda = ws.Cells[9, coll].Value == null ? 0 : Decimal.TryParse(ws.Cells[9, coll].Value.ToString(), out globalcekvar) == false ? 0 : decimal.Parse(ws.Cells[9, coll].Value.ToString()),
                        Depreciation = depresiasi,
                        Ebit = ws.Cells[11, coll].Value == null ? 0 : Decimal.TryParse(ws.Cells[11, coll].Value.ToString(), out globalcekvar) == false ? 0 : decimal.Parse(ws.Cells[11, coll].Value.ToString()),
                        Taxes = ws.Cells[12, coll].Value == null ? 0 : Decimal.TryParse(ws.Cells[12, coll].Value.ToString(), out globalcekvar) == false ? 0 : decimal.Parse(ws.Cells[12, coll].Value.ToString()),
                        NetIncome = rataratapendapatan,
                        AddBackDepresiation = ws.Cells[14, coll].Value == null ? 0 : Decimal.TryParse(ws.Cells[14, coll].Value.ToString(), out globalcekvar) == false ? 0 : decimal.Parse(ws.Cells[14, coll].Value.ToString()),
                        NetCashFlow = ws.Cells[16, coll].Value == null ? 0 : Decimal.TryParse(ws.Cells[16, coll].Value.ToString(), out globalcekvar) == false ? 0 : decimal.Parse(ws.Cells[16, coll].Value.ToString()),
                        CumulativeNetCashFlow = ws.Cells[17, coll].Value == null ? 0 : Decimal.TryParse(ws.Cells[17, coll].Value.ToString(), out globalcekvar) == false ? 0 : decimal.Parse(ws.Cells[17, coll].Value.ToString()),
                    };

                    if (item.Year != 0)
                    {
                        item.AkiId = idAki;
                        item.LastUpdateTime = DateTime.Now;
                        _akivaluationservice.Add(item);
                    }

                }

            }
            return true;
        }

    }
}