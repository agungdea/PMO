﻿using App.Domain.Models.Identity;
using App.Services.Identity;
using Microsoft.AspNetCore.Identity;
using Modular.Core.Caching;
using System.Security.Claims;
using System;
using System.Linq;

namespace App.Helper
{
    public class UserHelper : IUserHelper
    {
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _roleManager;
        private IUserService _userService;
        private ConfigHelper _configHelper;
        private readonly ICacheManager _cacheManager;
        private const string USER_KEY = "user.{0}";

        public UserHelper(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IUserService userService,
            ConfigHelper configHelper,
            ICacheManager cacheManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _userService = userService;
            _configHelper = configHelper;
            _cacheManager = cacheManager;
        }

        public ApplicationUser GetCache(string id)
        {
            var cache = _cacheManager.Get<ApplicationUser>(string.Format(USER_KEY, id));
            if (cache == null)
            {
                var user = _userService.GetByIdNoTracked(id);
                SetCache(user);

                return user;
            }

            return cache;
        }

        public ApplicationUser GetCache(ClaimsPrincipal user)
        {
            var userId = GetUserId(user);

            if (string.IsNullOrWhiteSpace(userId))
                return null;

            return GetCache(userId);
        }

        public void SetCache(ApplicationUser user)
        {
            _cacheManager.Set(string.Format(USER_KEY, user.Id), user, 60);
        }

        public string GetUserId(ClaimsPrincipal user)
        {
            if (user == null)
            {
                return "";
            }

            //var userId = user.FindFirstValue(ClaimTypes.NameIdentifier);
            var userId = user.FindFirstValue("sub");
            return userId;
        }
        public string GetUserUnit(string Id)
        {
            if (string.IsNullOrWhiteSpace(Id))
            {

                return null;
            }

            var user = _userService.GetById(Id);
            var claim = _userManager.GetClaimsAsync(user).Result;
            var unitId = claim.FirstOrDefault(x => x.Type == ApplicationUser.UNIT_CLAIMS)?.Value;

            return unitId;

        }
        public string GetUserUnitBefore(string Id)
        {
            if (string.IsNullOrWhiteSpace(Id))
            {

                return null;
            }

            var user = _userService.GetById(Id);
            var claim = _userManager.GetClaimsAsync(user).Result;
            var unitId = claim.FirstOrDefault(x => x.Type == ApplicationUser.UNIT_DISPO_CLAIMS)?.Value;

            return unitId;

        }

        public ApplicationUser GetUser(string id)
        {
            var user = _userService.GetById(id);
            return user;
        }

        public ApplicationUser GetUser(ClaimsPrincipal user)
        {
            var userId = GetUserId(user);

            if (string.IsNullOrWhiteSpace(userId))
                return null;

            return GetUser(userId);
        }

        public string GetAvatar(string id)
        {
            var user = GetCache(id);
            var attachment = user.UserProfile.Photo.ConvertToAttachment();
            if (attachment == null)
                return _configHelper.GetConfig("user.no.avatar.url");

            return "/" + attachment.CropedPath ?? attachment.Path + "?_t=" + DateTime.Now.Ticks;
        }

        public string GetAvatar(ClaimsPrincipal user)
        {
            var userId = GetUserId(user);

            if (string.IsNullOrWhiteSpace(userId))
                return _configHelper.GetConfig("user.no.avatar.url");

            return GetAvatar(userId);
        }
    }
}
