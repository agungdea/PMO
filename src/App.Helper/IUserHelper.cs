﻿using App.Domain.Models.Identity;
using System.Security.Claims;

namespace App.Helper
{
    public interface IUserHelper
    {
        ApplicationUser GetCache(string id);
        ApplicationUser GetCache(ClaimsPrincipal user);
        void SetCache(ApplicationUser user);
        string GetUserId(ClaimsPrincipal user);
        ApplicationUser GetUser(string id);
        ApplicationUser GetUser(ClaimsPrincipal user);
        string GetAvatar(string id);
        string GetAvatar(ClaimsPrincipal user);
        string GetUserUnit(string Id);
        string GetUserUnitBefore(string Id);
    }
}
