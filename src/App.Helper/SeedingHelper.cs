﻿using App.Domain.Models.Core;
using App.Services.App;
using App.Services.Core;
using App.Services.Localization;
using Modular.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace App.Helper
{
    public class SeedingHelper
    {
        private readonly ConfigHelper _config;
        private readonly FileHelper _fileHelper;
        private readonly ExcelHelper _excelHelper;
        private readonly ILocaleResourceService _resourceService;
        private readonly IWebSettingService _webSettingService;
        private readonly IAreaMappingService _areaMappingService;

        public SeedingHelper(ConfigHelper config,
            FileHelper fileHelper,
            ExcelHelper excelHelper,
            ILocaleResourceService resourceService,
            IAreaMappingService areaMappingService,
            IWebSettingService webSettingService)
        {
            _config = config;
            _fileHelper = fileHelper;
            _excelHelper = excelHelper;
            _resourceService = resourceService;
            _webSettingService = webSettingService;
            _areaMappingService = areaMappingService;
        }

        public void SeedWebSetting(IList<ModuleInfo> modules = null)
        {
            var webSettingJson = _fileHelper.LoadJson("/seeds/WebSetting.json");

            var data = JsonConvert.DeserializeObject<List<WebSetting>>(webSettingJson);

            foreach (var item in data)
            {
                var exist = _webSettingService.GetByName(item.Name);
                if (exist == null)
                    _webSettingService.Add(item);
            }

            if (modules == null)
                return;

            foreach (var module in modules)
            {
                var wwwrootDir = new DirectoryInfo(Path.Combine(module.Path, "wwwroot"));
                if (!wwwrootDir.Exists)
                    continue;

                var seedsDir = new DirectoryInfo(Path.Combine(wwwrootDir.FullName, "seeds"));
                if (!seedsDir.Exists)
                    continue;

                var jsonPath = Path.Combine(seedsDir.FullName, "WebSetting.json");
                if (!File.Exists(jsonPath))
                    continue;

                webSettingJson = _fileHelper.LoadJson(jsonPath, true);

                data = JsonConvert.DeserializeObject<List<WebSetting>>(webSettingJson);

                foreach (var item in data)
                {
                    var exist = _webSettingService.GetByName(item.Name);
                    if (exist == null)
                        _webSettingService.Add(item);
                }
            }
        }

        public void SeedResouces(IList<ModuleInfo> modules = null)
        {
            var resources = _excelHelper.GetResources("/seeds/Resources.xlsx");

            foreach (var item in resources)
            {
                var current = _resourceService.GetResources(item.ResourceName);

                if (current != null && current.Any(x => x.LanguageId == item.LanguageId))
                    continue;

                try
                {
                    _resourceService.Add(item);
                }
                catch (Exception)
                {
                    continue;
                }
            }

            if (modules == null)
                return;

            foreach (var module in modules)
            {
                var wwwrootDir = new DirectoryInfo(Path.Combine(module.Path, "wwwroot"));
                if (!wwwrootDir.Exists)
                    continue;

                var seedsDir = new DirectoryInfo(Path.Combine(wwwrootDir.FullName, "seeds"));
                if (!seedsDir.Exists)
                    continue;

                var resourcesPath = Path.Combine(seedsDir.FullName, "Resources.xlsx");
                if (!File.Exists(resourcesPath))
                    continue;

                resources = _excelHelper.GetResources(resourcesPath, true);

                foreach (var item in resources)
                {
                    var current = _resourceService.GetResources(item.ResourceName);

                    if (current != null && current.Any(x => x.LanguageId == item.LanguageId))
                        continue;
                    try
                    {
                        _resourceService.Add(item);
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }
        }

        public void SeedArea(string sourcePath)
        {
            var resources = _excelHelper.GetArea(sourcePath);

            foreach (var item in resources)
            {
                var current = _areaMappingService.GetListAreaByCodeArea(item.AreaKode);

                if (current != null && current.Any(x => x.AreaKode == item.AreaKode))
                    continue;

                try
                {
                    _areaMappingService.Add(item);
                }
                catch (Exception)
                {
                    continue;
                }
            }
            
        }
    }
}
