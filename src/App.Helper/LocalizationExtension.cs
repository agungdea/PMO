﻿using App.Domain;
using App.Services.Localization;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace App.Helper
{
    public static class LocalizationExtension
    {
        private static ILanguageService LanguageService { get; set; }
        private static ILocaleResourceService LocaleResourceService { get; set; }
        private static LocalizationHelper LocalizationHelper { get; set; }

        public static void Initialize(ILocaleResourceService localeResourceService,
            ILanguageService languageService,
            LocalizationHelper localizationHelper)
        {
            LocaleResourceService = localeResourceService;
            LanguageService = languageService;
            LocalizationHelper = localizationHelper;
        }

        public static string GetLocalized<T>(this T entity,
               Expression<Func<T, string>> keySelector)
               where T : ILocalizedEntity
        {
            var langId = LanguageService.GetByCulture(LocalizationHelper.GetCulture()).Id;
            return GetLocalized(entity, keySelector, langId);
        }

        public static string GetLocalized<T>(this T entity,
            Expression<Func<T, string>> keySelector, int languageId,
            bool returnDefaultValue = true, bool ensureTwoPublishedLanguages = true)
            where T : ILocalizedEntity
        {
            return GetLocalized<T, string>(entity, keySelector, languageId, returnDefaultValue, ensureTwoPublishedLanguages);
        }

        public static TPropType GetLocalized<T, TPropType>(this T entity,
            Expression<Func<T, TPropType>> keySelector, int languageId,
            bool returnDefaultValue = true, bool ensureTwoPublishedLanguages = true)
            where T : ILocalizedEntity
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var member = keySelector.Body as MemberExpression;
            if (member == null)
            {
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    keySelector));
            }

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
            {
                throw new ArgumentException(string.Format(
                       "Expression '{0}' refers to a field, not a property.",
                       keySelector));
            }

            TPropType result = default(TPropType);
            string resultStr = string.Empty;

            //load localized value
            string localeKeyGroup = typeof(T).Name;
            string localeKey = propInfo.Name;

            //if (languageId > 0)
            //{
            //    //ensure that we have at least two published languages
            //    bool loadLocalizedValue = true;
            //    if (ensureTwoPublishedLanguages)
            //    {
            //        var lService = EngineContext.Current.Resolve<ILanguageService>();
            //        var totalPublishedLanguages = lService.GetAllLanguages().Count;
            //        loadLocalizedValue = totalPublishedLanguages >= 2;
            //    }

            //    //localized value
            //    if (loadLocalizedValue)
            //    {
            //        var leService = EngineContext.Current.Resolve<ILocalizedEntityService>();
            //        resultStr = leService.GetLocalizedValue(languageId, entity.Id, localeKeyGroup, localeKey);
            //        if (!String.IsNullOrEmpty(resultStr))
            //            result = CommonHelper.To<TPropType>(resultStr);
            //    }
            //}

            ////set default value if required
            //if (String.IsNullOrEmpty(resultStr) && returnDefaultValue)
            //{
            //    var localizer = keySelector.Compile();
            //    result = localizer(entity);
            //}

            return result;
        }
    }
}
