﻿using System.Linq;
using System.Security.Claims;

namespace App.Helper
{
    public static class ClaimHelper
    {
        #region const   
        public const string READ = "Read";
        public const string CREATE = "Create";
        public const string EDIT = "Edit";
        public const string DELETE = "Delete";
        #endregion

        public static bool IsRead(this ClaimsPrincipal user, string claimName)
        {
            var claim = user.Claims.Where(x => x.Type == claimName);

            foreach (var item in claim)
            {
                if (item.Value == READ)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsCreate(this ClaimsPrincipal user, string claimName)
        {
            var claim = user.Claims.Where(x => x.Type == claimName);

            foreach (var item in claim)
            {
                if (item.Value == CREATE)
                {
                    return true;
                }
            }

            return false;
        }


        public static bool IsEdit(this ClaimsPrincipal user, string claimName)
        {
            var claim = user.Claims.Where(x => x.Type == claimName);

            foreach (var item in claim)
            {
                if (item.Value == EDIT)
                {
                    return true;
                }
            }

            return false;
        }


        public static bool IsDelete(this ClaimsPrincipal user, string claimName)
        {
            var claim = user.Claims.Where(x => x.Type == claimName);

            foreach (var item in claim)
            {
                if (item.Value == DELETE)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
