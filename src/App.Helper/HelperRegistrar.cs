﻿using Microsoft.Extensions.DependencyInjection;

namespace App.Helper
{
    public static class HelperRegistrar
    {
        public static void Register(IServiceCollection services)
        {
            services.AddScoped<ViewRender>();
            services.AddScoped<ConfigHelper>();
            services.AddScoped<CommonHelper>();
            services.AddScoped<LocalizationHelper>();
            services.AddScoped<FileHelper>();
            services.AddScoped<SeedingHelper>();
            services.AddScoped<IUserHelper, UserHelper>();
            services.AddScoped<MailingHelper>();
            services.AddScoped<ExcelHelper>();
            services.AddScoped<PdfHelper>();
            services.AddScoped<PdfGenerator>();
        }
    }
}
