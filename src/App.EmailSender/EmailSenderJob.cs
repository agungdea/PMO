﻿using App.Domain.Models.Core;
using App.Helper;
using App.Services.Core;
using NLog;
using System;
using System.Collections.Generic;

namespace App.EmailSender
{
    public class EmailSenderJob
    {
        private readonly IEmailArchieveService _emailArchieveService;
        private readonly MailingHelper _mailing;
        private readonly ConfigHelper _config;
        private readonly CommonHelper _common;
        private readonly int _maxTrySend;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public EmailSenderJob(IEmailArchieveService emailArchieveService,
            MailingHelper mailingHelper,
            ConfigHelper configHelper,
            CommonHelper commonHelper)
        {
            _emailArchieveService = emailArchieveService;
            _mailing = mailingHelper;
            _config = configHelper;
            _common = commonHelper;
            _maxTrySend = _config.GetConfigAsInt("email.scheduler.max.send.try");
        }

        public void Execute()
        {
            var emails = _emailArchieveService.GetUnsentEmail(_maxTrySend);

            Console.WriteLine("Total un-sent email: {0} [{1}]",
                emails.Count,
                _common.ToLongString(DateTime.Now));

            if (emails.Count > 0)
            {
                SendEmails(emails);
            }
        }

        private void SendEmails(List<EmailArchieve> emails)
        {
            foreach (var email in emails)
            {
                Console.WriteLine();
                Console.WriteLine("Sending email [{0}]", email.Id);
                try
                {
                    var result = _mailing.SendEmail(email).Result;
                }
                catch ( Exception e)
                {
                    logger.Error("Error Send Email :"+ email+ " Exception : "+ e);
                    
                }
               

                Console.WriteLine("Status : {0}", email.IsSent);
                Console.WriteLine("Try Sending : {0}", email.TrySentCount);
                Console.WriteLine("Last Try Date: {0}", 
                    _common.ToLongString(email.LastTrySentDate));
                Console.WriteLine();
            }
        }
    }
}
