﻿using App.Domain;
using App.Helper;
using App.Services;
using App.Services.Identity;
using AutoMapper;
using DataTables.AspNet.AspNetCore;
using DataTables.AspNet.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace App.Infrastructure
{
    public class BaseCacheApiController<TEntity, TService, TDto, TId>
        : BaseApiController<TEntity, TService, TDto, TId>
        where TEntity : class, ICacheEntity<TId>
        where TService : ICacheService<TEntity, TId>
        where TDto : class
    {
        public BaseCacheApiController(IHttpContextAccessor httpContextAccessor, 
            IUserService userService, 
            IMapper mapper, 
            TService service, 
            IUserHelper userHelper) 
            : base(httpContextAccessor, userService, mapper, service, userHelper)
        {
            service.Includes.AddRange(Includes);
        }

        [HttpGet]
        [Route("Datatables")]
        public override IActionResult GetDataTables(IDataTablesRequest request)
        {
            var response = Service.GetDataTablesResponseFromCache<TDto>(request,
                Mapper, Where);

            return new DataTablesJsonResult(response, true);
        }

        [HttpPost]
        [Route("PostDatatables")]
        public override IActionResult PostDataTables(IDataTablesRequest request)
        {
            var response = Service.GetDataTablesResponseFromCache<TDto>(request,
                Mapper, Where);

            return new DataTablesJsonResult(response, true);
        }
    }
}
