﻿using App.Services.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace App.Infrastructure
{
    public class ClaimDrivenHandler : AuthorizationHandler<ClaimDrivenRequirement>
    {
        private readonly IAppActionService _appActionService;

        public ClaimDrivenHandler(IAppActionService appActionService)
        {
            _appActionService = appActionService;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, 
            ClaimDrivenRequirement requirement)
        {
            var authorizationFilterContext = (AuthorizationFilterContext)context.Resource;
            var actionDescriptor = authorizationFilterContext.ActionDescriptor;
            var appAction = _appActionService.GetByName(actionDescriptor.DisplayName);

            if(appAction == null)
            {
                return Task.CompletedTask;
            }

            foreach(var claim in appAction.ActionClaims)
            {
                if(context.User.HasClaim(x => x.Type == claim.ClaimType && x.Value == claim.ClaimValue))
                {
                    context.Succeed(requirement);
                    break;
                }
            }


            return Task.CompletedTask;
        }
    }
}
