﻿using App.Domain.Models.Identity;
using App.Helper;
using App.Services;
using App.Services.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace App.Infrastructure
{
    public class BaseController<TEntity, TService, TViewModel, TModelForm, TId> : Controller
        where TEntity : class
        where TService : IService<TEntity>
        where TModelForm : class
        where TViewModel : class
    {
        protected bool IsAdministrator { get; set; }
        protected bool IsUser { get; set; }
        protected ApplicationUser CurrentUser { get; set; }
        protected IUserService UserService { get; set; }
        protected IUserHelper UserHelper { get; set; }
        protected IMapper Mapper { get; set; }
        protected TService Service { get; set; }

        public BaseController(IHttpContextAccessor httpContextAccessor,
           IUserService userService,
           IMapper mapper,
           TService service,
           IUserHelper userHelper)
        {
            UserService = userService;
            UserHelper = userHelper;
            Mapper = mapper;
            IsAdministrator = false;
            IsUser = false;
            CurrentUser = null;
            Service = service;

            if (httpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
            {
                //if didn't use SSO use----------------------------------------------------------
                // var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                //------------------------------------------------------------------------
                //if using SSO use----------------------------------------------------------
                var userId = UserHelper.GetUserId(httpContextAccessor.HttpContext.User);
                //------------------------------------------------------------------------
                CurrentUser = UserHelper.GetCache(userId);
                IsAdministrator = httpContextAccessor.HttpContext.User.IsInRole(ApplicationRole.Administrator);
                IsUser = httpContextAccessor.HttpContext.User.IsInRole(ApplicationRole.User);
            }
        }

        public virtual IActionResult Index()
        {
            return View();
        }

        public virtual IActionResult Details(TId id)
        {
            var item = Service.GetById(id);
            if (item != null)
            {
                return View(Mapper.Map<TViewModel>(item));
            }

            return NotFound();
        }

        public virtual IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public virtual IActionResult Create(TModelForm model)
        {
            if (ModelState.IsValid)
            {
                var item = Mapper.Map<TEntity>(model);

                CreateData(item);

                Service.Add(item);

                AfterCreateData(item);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public virtual IActionResult Edit(TId id)
        {
            var item = Service.GetById(id);
            if (item != null)
            {
                return View(Mapper.Map<TModelForm>(item));
            }

            return NotFound();
        }

        [HttpPost]
        public virtual IActionResult Edit(TId id, TModelForm model)
        {
            if (ModelState.IsValid)
            {
                var item = Service.GetById(id);

                if (item == null)
                {
                    return NotFound();
                }

                var before = item;

                UpdateData(item, model);

                var result = Service.Update(item);

                AfterUpdateData(before, item);

                return RedirectToAction("Details", new { id });
            }

            return View(model);
        }

        protected virtual void CreateData(TEntity item)
        {
        }

        protected virtual void AfterCreateData(TEntity item)
        {
        }

        protected virtual void UpdateData(TEntity item, TModelForm model)
        {
        }

        protected virtual void AfterUpdateData(TEntity before, TEntity after)
        {
        }

        protected virtual void AddError(IEnumerable<IdentityError> errors)
        {
            var errorString = "";
            var first = true;
            var last = errors.Last();
            foreach (var error in errors)
            {
                if (!first)
                {
                    errorString += "<li>";
                }

                errorString += $"{error.Description}";

                if (error.Code != last.Code)
                {
                    errorString += "</li>";
                }

                first = false;
            }
            ModelState.AddModelError("", errorString);
        }
    }
}
